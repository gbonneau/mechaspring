import meshio
import numpy as np

# three lines, two triangles and one quad
points = [
    [0.0, 0.0],
    [1.0, 0.0],
    [0.0, 1.0],
    [1.0, 1.0],
    [2.0, 0.0],
    [2.0, 1.0],
]
cells = [
    ("line", [[0,1], [1,2], [2,3]]),
    ("triangle", [[0, 1, 2], [1, 3, 2]]),
    ("quad", [[1, 4, 5, 3]]),
]

nPoint = len(points)
with meshio.xdmf.TimeSeriesWriter("foo.xdmf") as writer:
    writer.write_points_cells(points, cells)
    for t in [0.0, 0.1, 0.21]:
        data = np.full( nPoint, t)
        writer.write_data(t, point_data={"phi": data})
