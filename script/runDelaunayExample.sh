# produce the periodic delaunay triangulation with minimal distance minDistance between samples
python ../script/periodicDelaunay.py $1
# convert it to spring file format
python ../script/periodicMinimalConstruct.py
# compute the Young and Poisson modulus with the strain fixed in the script
python ../script/tractionUniaxialeRotating.py pointEdgeAngle.spg