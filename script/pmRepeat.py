import periodicMinimalConstruct as pmC
import numpy as np
import sys

# GIVEN AN INTEGER number AND A POSITIVE INTEGER width
# RETURN AN INTEGER result AND AN INTEGER translate
# SUCH THAT
# number = result + translate * 2 * width, AND 
# -width < result < width
def centerIndex( number, width):
    print(f"width = {width}")
    result = number
    translate = 0
    print("==")
    print(result)
    while not result < width:
        result = result - (2 * width - 1)
        translate = translate + 1
        print(result)
    while not -width < result:
        result = result + (2 * width - 1)   
        translate = translate - 1
        print(result)
    print("==")
    assert( number == result + translate * (2 * width-1))
    assert( -width < result)
    assert( result < width)
    return result, translate
    
def convertIndex( periodicPoint, width, index):
    uR, uT = centerIndex( periodicPoint[1], width)
    vR, vT = centerIndex( periodicPoint[2], width)
    vUnit = index[(periodicPoint[0], uR, vR)]
    print(f"converting periodic point {periodicPoint} = ({uR} {uT}, {vR} {vT}) = {index[(periodicPoint[0], uR, vR)]} = ({vUnit}, {uT}, {vT})")
    return (vUnit, uT, vT)

def main():
    if len(sys.argv) != 3:
        print(f"usage: {sys.argv[0]} <spgFilename> <numberOfSymetricRepeat>")
        sys.exit(1)

    fileName = sys.argv[1]
    width = int(sys.argv[2])
    widthRange = range( -(width-1), width)

    # READ A NETWORK IN SPG FORMAT
    vert, u0, v0, lUnitList, aUnitList, sArray = pmC.networkFromIndirectSpringFile( fileName)

    # REPEAT LENGTH AND ANGULAR SPRING
    lList, aList = pmC.springInBox( widthRange, lUnitList, aUnitList)

    # REPEAT UNIT CELL VERTICES AND BUILD INDEX MAP TO NEW REPEATED UNIT CELL VERTICES
    vertRepeat = np.zeros( (0,2)) # MATRIX OF 0 2D VECTORS
    offset = 0
    index = dict()
    for ui in widthRange:
        for vj in widthRange:
            vertRepeat = np.concatenate( (vertRepeat, vert + ui * u0 + vj * v0))
            for pl in range(vert.shape[0]):
                index[ (pl, ui, vj)] = pl + offset
            offset = offset + vert.shape[0]

    print(index)
    # FOR EACH NEW SPRING, CONVERT THE PREVIOUS TRIPLET ENCODING OF EACH OF ITS PERIODIC VERTICES
    lListNew = [ [ convertIndex( l[0], width, index),
                   convertIndex( l[1], width, index),
                   l[2]] for l in lList ]
    
    aListNew = [ [ convertIndex( a[0], width, index),
                   convertIndex( a[1], width, index), 
                   convertIndex( a[2], width, index), 
                   a[3]] for a in aList ]
    
    u0Repeat = (2. * (width-1) + 1) * u0
    v0Repeat = (2. * (width-1) + 1) * v0

    pmC.writeInIndirectSpringFileFormat( vertRepeat, u0Repeat, v0Repeat, lListNew, aListNew, sArray, "outputRepeat.spg")


if __name__=="__main__":
    main()
