import periodicMinimalConstruct as pmC
import numpy as np

pmC.writeInIndirectSpringFileFormatDefaultStiffness( *pmC.networkFromPointEdgeAngleFiles(np.array([1.,0.]),np.array([0.,1.])), "pointEdgeAngle.spg")
print("file pointEdngeAngle.spg has been written")
