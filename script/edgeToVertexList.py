import sys
import logging
import numpy as np

# ---------------------------------------------------------------------------------------
# A UTILISER QUAND JE VOUDRAIS TRIER LES SOMMETS AUTOUR D'UN SOMMET CENTRAL
        # SortedIndices = sorted(range(len(Angle)), key=lambda k: Angle[k]) # return the list of indices of sorted values
        # logging.debug( SortedIndices)
        # for i in SortedIndices:
        #     logging.debug( Dart[i])
# ---------------------------------------------------------------------------------------

def OrientedAngle( t1, t2, n):
    "return the oriented angle between tangent vectors t1 and t2 with respect to the orientation given by n"
    "t1, t2 and n must be 3d vectors of norm 1"
    "t1 and t2 must be orthogonal to n"
    logging.debug( "computing angle between " + str(t1) + " and " + str(t2) + " at normal " + str(n))
    CrP = np.cross( t1, t2) # cross product of the two tangents
    if (np.linalg.norm( t1-t2) < 1e-5):
        logging.critical( "two tangents almost equal around a normal:  " + str(t1) + " and " + str(t2))
    Sign = np.dot( CrP, n) # if Sign is positive then t2 is in first half circle of t1 in ccw order with respect to the orientation given by n
    DotProduct = np.dot(t1, t2)
    DotProduct = np.clip( DotProduct, -1., 1.)
    Angle = math.acos( DotProduct)
    if (Sign < 0.):
        logging.debug( " t2 is in the second half circle of t1, returning 2 Pi - Angle")
        Angle = 2. * np.pi - Angle
    logging.debug( " angle = " + str(Angle))
    return Angle

def readingVertexPairFile( filename):
    with open(filename) as fp:
        line = fp.readline()
        lineSplit = line.split(" ")
        nVertex = int(lineSplit[0])
        points = np.zeros((nVertex,3))
        for i in range(nVertex):
            line = fp.readline()
            lineSplit = line.split(" ")     
            points[i,0:3] = np.array( [float(d) for d in lineSplit])      
            # print("point {} = {}".format( i, points[i,:]))

        # INITIALIZE EMPTY SETS FOR THE INDICES OF INCIDENT VERTICES AROUND A VERTEX
        edgeList= [set() for i in range(nVertex)]

        line = fp.readline()
        lineSplit = line.split(" ")
        while lineSplit[0] != "Boundary":
            # FOR EACH EDGE ADD THE OTHER VERTEX TO THE VERTEX INCIDENT VERTICES LIST
            # !!! IN MATLAB INDICES STARTING AT 1 !!!
            # print(lineSplit)
            edgeList[int(lineSplit[0])] = edgeList[int(lineSplit[0])].union({int(lineSplit[1])+1})
            edgeList[int(lineSplit[1])] = edgeList[int(lineSplit[1])].union({int(lineSplit[0])+1})
            
            line = fp.readline()
            lineSplit = line.split(" ")

        print(nVertex, flush=True) # NUMBER OF VERTICES
        np.savetxt( sys.stdout.buffer, points) # COORDINATES OF VERTICES
        for i in range(nVertex):
            print(*edgeList[i]) # INCIDENT VERTICES LIST OF EACH VERTEX

        print(line.strip()) # string "Boundary conditions"
        line = fp.readline()
        print(line.strip()) # number of vertices in first constraint group
        line = fp.readline()
        matlabIndices = [int(d)+1 for d in line.split()]
        print(*matlabIndices) # VERTICES IN FIRST CONSTRAINT GROUP
        line = fp.readline()
        print(line.strip()) # number of vertices in second constraint group
        line = fp.readline()
        matlabIndices = [int(d)+1 for d in line.split()]
        print(*matlabIndices) # VERTICES IN SECOND CONSTRAINT GROUP





if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print("Usage: " + sys.argv[0] + "<filename>")

    readingVertexPairFile( sys.argv[1]) 
