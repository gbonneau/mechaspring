import periodicMinimalConstruct3D as pmC
import sys

class periodicMinimalStructure:
    def __init__( self, vert, u0, v0, w0, lList, aList, sArray):
        self.vert = vert
        self.u0 = u0
        self.v0 = v0
        self.w0 = w0
        self.lList = lList
        self.aList = aList
        self.sArray = sArray

    def __init__( self, fileName):
        assert( fileName[-3:] == "spg" and "file must be of type spg")
        self.vert, self.u0, self.v0, self.w0, self.lList, self.aList, self.sArray = pmC.networkFromIndirectSpringFile( fileName)

    def boundingBox( self):
        return pmC.boundingBox( self.vert, self.u0, self.v0, self.w0, self.lList, self.aList)
    
    def lengthSpringStatistic( self):
        return pmC.lengthSpringStatistic( self.vert, self.u0, self.v0, self.w0, self.lList)
    
    def barycenterSpringList( self):
        return pmC.barycenterSpringList( self.vert, self.u0, self.v0, self.w0, self.lList)
    
    def allPeriodicPointInSpringList( self):
        return pmC.allPeriodicPointInSpringList( self.lList, self.aList)
    
    def allUnitCellPointInSpringList( self):
        return pmC.allUnitCellPointInSpringList( self.lList, self.aList)
    
    def explode( self, width):
        lList = [pmC.randomTranslate2Spring( l, width, width) for l in self.lList]
        aList = [pmC.randomTranslate3Spring( a, width, width) for a in self.aList]
        self.lList = lList
        self.aList = aList

    def write( self, fileName):
        pmC.writeInIndirectSpringFileFormat( self.vert, self.u0, self.v0, self.w0, self.lList, self.aList, self.sArray, fileName)

    def centerAllUnitCellVertices( self):
        pmC.centerAllUnitCellVertices( self.vert, self.u0, self.v0, self.w0, self.lList, self.aList)

    def subdivide( self, targetlength):
        indexOfLengthStiffnessToSubdivide = 0
        
        self.vert, self.u0, self.v0, self.w0, self.lList, self.aList = pmC.subdivideAllEdgeWithGivenStiffness( self.vert, self.u0, self.v0, self.w0, self.lList, self.aList, indexOfLengthStiffnessToSubdivide, targetlength)

def main():
    myNetwork = periodicMinimalStructure( sys.argv[1])
    myNetwork.subdivide(0.01)
    myNetwork.write("bidon.spg")
    return None

if __name__ == "__main__":
    main()
