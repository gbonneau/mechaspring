# from cProfile import label
import numpy as np
import sys
import matplotlib.pyplot as plt

# GIVEN AN ARRAY OF FLOATS, RETURN TWO FLOATS DEFINING
# AN INTERVAL d% LARGER THAN THE [min,max] INTERVAL
def largerInterval( a, d):
    min = np.min(a)
    max = np.max(a)
    addedLength = (max - min) * d/100./2.
    return min-addedLength, max+addedLength

YoungMatlab = np.loadtxt(sys.argv[1]+"/E_plot")*833.
PRMatlab = np.loadtxt(sys.argv[1]+"/PR_plot")

YoungSpring = np.loadtxt(sys.argv[2]+"/E_plot")
PRSpring = np.loadtxt(sys.argv[2]+"/PR_plot")

thetaMin = 0.0
thetaMax = 2. * np.pi 

nTraction = len(PRMatlab)
thetaMatlab=np.linspace( thetaMin, thetaMax, num=nTraction, endpoint=True)

nTraction = len(PRSpring)
thetaSpring=np.linspace( thetaMin, thetaMax, num=nTraction, endpoint=True)

fig, ax = plt.subplots( 1,2, subplot_kw={'projection': 'polar'})
ax[0].plot(thetaMatlab, YoungMatlab, marker="s")
ax[0].plot(thetaSpring, YoungSpring, marker="x")

ax[1].plot(thetaMatlab, PRMatlab, marker="s")
ax[1].plot(thetaSpring, PRSpring, marker="x")

rmin, rmax= largerInterval( list(YoungMatlab)+list(YoungSpring)+[.0], 10.)
ax[0].set_rmin( rmin)
ax[0].set_rmax( rmax)
rmin, rmax= largerInterval( list(PRMatlab)+list(PRSpring)+[.0], 10.)
ax[1].set_rmin( rmin)
ax[1].set_rmax( rmax)

# ax.plot(theta, PR3, label="fine subdivided")
# ax.set_rmax(2.)
# ax.set_rticks([ 0.,0.5,1.,1.5])  # Less radial ticks
# ax.set_rticks([-1., -0.5, 0., 0.5, 1, 1.5, 2])  # Less radial ticks
ax[0].set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax[1].set_rlabel_position(-22.5)  # Move radial labels away from plotted line
lines, labels = plt.thetagrids(range(0, 360, 30)) # theta ticks min, max, delta
ax[0].grid(True)
ax[1].grid(True)

plt.title("Comparison Matlab Mechaspring")

# ax.set_title("A line plot on a polar axis", va='bottom')
ax[0].set_title(f"Young's modulus", va='bottom')
ax[1].set_title(f"Poisson's ratio", va='bottom')
# ax.legend(loc="upper right")
plt.savefig( "comparisonMatlabMechaspring.pdf", format="pdf")
plt.show()

