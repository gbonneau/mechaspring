from matplotlib import pyplot as plt
import numpy as np
import torch
from torchmin import minimize
from torchmin import minimize_constr
import torch.nn.functional as F
import time
import sys

# a is a matrix of shape nRow x 2
# b is a matrix of shape 1 x 2
# returns a vector of dimension nRow containing the cross product of each line of a by b
def computeMultiple2dCrossProduct( a, b):
    ap = F.pad( a, (0,1)) # ADD 0 TO TRANSFORM THE 2D VECTORS a INTO 3D VECTORS ap
    br = b.repeat( a.shape[0], 1) # REPEAT THE SINGLE ROW OF b SO THAT br HAS SAME DIMENSION AS a
    brp = F.pad( br, (0,1))
    print(ap)
    print(brp)
    vectorCross = torch.cross( ap, brp, dim=1)
    print(vectorCross)
    return vectorCross[...,2]

def constraintFunction( doF):
    return doF.square().sum() - 1.

def objectiveFunction( doF):
    evolutionDoF.append( doF)

    allScalarProduct = normedPoint @ doF.reshape(2,1)
    # print(f"allScalarProduct = {allScalarProduct}")
    # result = allScalarProduct.square().sum()
    result = allScalarProduct.square().max()
    # result = (allScalarProduct-0.5).square().max()
    # print(f"result = {result}")
    return result

def drawResult( point, allIterations, title, thisPlot):

    thisPlot.set_xlim( -1.2, 1.2)
    thisPlot.set_ylim( -1.2, 1.2)
    thisPlot.set_xticks([])
    thisPlot.set_yticks([])

    result = np.array([ oneIteration.detach().numpy() for oneIteration in allIterations])

    thisPlot.scatter( point[:,0], point[:,1], c="tab:blue") # DRAW INPUT POINTS
    for pi in range(point.shape[0]):
        thisPlot.plot( [point[pi,0], 0.], [point[pi,1], 0.], color="tab:blue") # DRAW SEGMENTS FROM INPUT POINTS TO CENTER
        thisPlot.plot( [point[pi,0], result[-1,0]], [point[pi,1], result[-1,1]], color="tab:purple") # DRAW SEGMENTS FROM INPUT POINTS TO SOLUTION
    thisPlot.plot( [0., result[-1,0]], [0., result[-1,1]], color="tab:green") # DRAW SEGMENT FROM SOLUTION TO CENTER
    thisPlot.scatter( [0],[0], c="tab:blue") # DRAW CENTER
    thisPlot.scatter( result[:,0], result[:,1], color="tab:red", alpha=0.1) # DRAW ALL ITERATIONS
    thisPlot.scatter( result[-1,0], result[-1,1], c="tab:green") # DRAW SOLUTION = FINAL ITERATION
    thisPlot.set_title(title)

def displayProgress( currentDoF, state):
    global nIter
    if (nIter % 1 == 0):
        f = objectiveFunction( currentDoF)
        print(f" objective function at current doF = {f}")
        g = constraintFunction( currentDoF)
        print(f"constraint function at current doF = {g}")
    nIter = nIter+1

def solveFromStartingPoint( doF):
    global evolutionDoF, solutionIndex, ax
    # print('-'*16+" start "+'-'*16)
    evolutionDoF = []
    start = time.time()
    sys.stdout.flush()
    res = minimize_constr( objectiveFunction, doF, \
                              constr=dict( \
        fun=lambda x: constraintFunction(x), \
        lb=0., ub=0.), disp=0)
    stop = time.time()
    # print('-'*16+" stop "+'-'*16)
    # print("="*10+f" OPTIMIZATION ACHIEVED IN {stop-start:5.2f}s")
    optimalValue = objectiveFunction( res.x).item()
    print(f"                          optimum at   {res.x.numpy()}")
    print(f" objective function value at optimum = {optimalValue}")
    print(f"constraint function value at optimum = {constraintFunction( res.x).item()}")
    allScalarProduct = normedPoint @ res.x.reshape(2,1)
    # print(f"allScalarProduct = {allScalarProduct}")


    fi = solutionIndex // nColumns
    fj = solutionIndex - fi * nColumns
    thisPlot = ax[fi,fj]

    drawResult( normedPoint.numpy(), evolutionDoF, str(round(optimalValue,4)), thisPlot)
    solutionIndex += 1
    return res.x.numpy(), optimalValue

def storeSolutionAndOptimalValueInSet( solution, optimalValue, setOfSolution):
    solution = solution.round(nDecimal) # CONVERT TO TUPLE REQUIRED TO BE HASHED INTO A SET LATER IN THE CODE
    optimalValue = round(optimalValue, nDecimal) # ROUNDING REQUIRED TO CANCEL CONVERGENCE ERROR
    solution = ( solution[0], solution[1], optimalValue) # CONVERT TO TUPLE REQUIRED TO BE HASHED INTO A SET LATER IN THE CODE
    setOfSolution.add(solution)

def initMultiplePlots( nPlot):
    nRows = round(np.sqrt(nPlot))+1
    nColumns = nPlot // nRows + 1
    fig, ax = plt.subplots(nRows, nColumns)
    return nRows, nColumns, fig, ax

if __name__ == "__main__":
    evolutionDoF = []
    solutionIndex = 0

    torch.set_default_dtype(torch.float64)

    nIter = 1
    nPoint = 6
    nGrid = 3
    nPlot = nPoint*2 + (nGrid+1)**2

    nRows, nColumns, fig, ax = initMultiplePlots( nPlot)

    # point = np.loadtxt(sys.argv[1])
    point = np.random.rand( nPoint,2)-np.array([0.5,0.5])
    normOfPoint = np.linalg.norm(point,axis=1)
    normedPoint = torch.tensor(point / normOfPoint.reshape(nPoint,1))

    nDecimal = 6 # USED FOR ROUNDING VALUES INSERTED INTO SETS
    allOptimalValues = set()
    allSolutionsNearPoint = set()
    for pi in range(nPoint):
        doF = torch.tensor( point[pi] + np.random.rand(2)*0.1, requires_grad=True) # START NEAR INPUT POINT OF INDEX pi
        solution, optimalValue = solveFromStartingPoint( doF) # COMPUTE OPTIMUM VALUE FROM THIS STARTING POINT
        storeSolutionAndOptimalValueInSet( solution, optimalValue, allSolutionsNearPoint)
        doF = torch.tensor( -point[pi] + np.random.rand(2)*0.1, requires_grad=True) # START NEAR INPUT POINT OF INDEX pi
        solution, optimalValue = solveFromStartingPoint( doF) # COMPUTE OPTIMUM VALUE FROM THIS STARTING POINT
        storeSolutionAndOptimalValueInSet( solution, optimalValue, allSolutionsNearPoint)

    allSolutionsOnGrid = set()
    for i in range(nGrid+1):
        for j in range(nGrid+1):
            doF = torch.tensor( [2*i/nGrid-1., 2*j/nGrid-1], requires_grad=True)
            solution, optimalValue = solveFromStartingPoint( doF)
            storeSolutionAndOptimalValueInSet( solution, optimalValue, allSolutionsOnGrid)

    nPlot = len(allSolutionsNearPoint)
    nRows, nColumns, fig, ax = initMultiplePlots( nPlot)
    for si, solution in enumerate(allSolutionsNearPoint):
        positionOfSingleSolution = torch.tensor([[solution[0], solution[1]]])

        fi = si // nColumns
        fj = si - fi * nColumns
        drawResult( normedPoint.numpy(), positionOfSingleSolution, str(round(solution[2], 4)), ax[fi,fj])

    plt.show()


