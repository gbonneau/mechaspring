#!/bin/bash
if [ "$#" -ne 4 ]; then
	echo "Usage: $0 xMin xMax yMin yMax"
        echo
        exit
else
        echo "== display final spring set"
fi

xMin=$1
xMax=$2
yMin=$3
yMax=$4

ratio=`echo "(($yMin)-($yMax))/(($xMin)-($xMax))" | bc -l`

cat > tmp.txt << EOF
set xrange [$xMin:$xMax]
set yrange [$yMin:$yMax]
set size ratio $ratio
plot "final.txt" with lines lc "orange", "final.txt" ps 0.5 pt 7 lc "orange"
pause -1 "hit return to close and exit"
EOF

gnuplot tmp.txt
rm tmp.txt
