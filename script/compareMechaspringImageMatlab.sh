set -x
spgFilename=$1
imageFilename=$2
pushd ../matlab; octave ./mainImageHomogenizationComplete.m ../build/$imageFilename; popd
python ../script/polarPlotMatlab.py ../matlab/OUTPUT/
cp matlabYoungPR.pdf "${imageFilename}_YoungPR_Matlab.pdf"
python ../script/tractionUniaxialeRotating.py $spgFilename
python ../script/polarPlotCompareMatlabMechaspring.py ../matlab/OUTPUT/ .
cp comparisonMatlabMechaspring.pdf "${imageFilename}_MatlabAndMechaspring.pdf"
