import periodicMinimalConstruct as pmC
import numpy as np
import sys

if len(sys.argv) != 3:
    print(f"usage: {sys.argv[0]} <spgFilename> <targetLengthOfSubdividedEdge>")
    sys.exit(1)
# READ A NETWORK IN SPG FORMAT, PROCESS IT AND SAVE IT
vert, u0, v0, lList, aList, sArray = pmC.networkFromIndirectSpringFile(sys.argv[1])
vert, u0, v0, lList, aList = pmC.subdivideAllEdgeWithGivenStiffness( vert, u0, v0, lList, aList, 0, float(sys.argv[2]))
pmC.writeInIndirectSpringFileFormat( vert, u0, v0, lList, aList, sArray, "sortie_subdivided.spg")

print("file sortie_subdivided.spg has been written")
