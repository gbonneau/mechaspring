import sys
from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderViewRight = CreateView('RenderView')
renderViewRight.ViewSize = [800, 800]
renderViewRight.InteractionMode = '2D'
renderViewRight.AxesGrid.Visibility = 0
renderViewRight.OrientationAxesVisibility = 0
renderViewRight.Background = [1.0, 1.0, 1.0]

renderViewRight.GetRenderWindow().SetPosition(1000, 10)

# Create a new 'Render View'
renderViewLeft = CreateView('RenderView')
renderViewLeft.ViewSize = [800, 800]
renderViewLeft.InteractionMode = '2D'
renderViewLeft.AxesGrid.Visibility = 0
renderViewLeft.OrientationAxesVisibility = 0
renderViewLeft.Background = [1.0, 1.0, 1.0]

SetActiveView(renderViewRight)

leftvtk = LegacyVTKReader(FileNames=[sys.argv[1]])
rightvtk = LegacyVTKReader(FileNames=[sys.argv[2]])

rightvtkDisplay = Show(rightvtk, renderViewRight)
rightvtkDisplay.ColorArrayName = ['POINTS', '']
rightvtkDisplay.DiffuseColor = [ 0., 0., 1.]
rightvtkDisplay.LineWidth = 5.0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView2'
# ----------------------------------------------------------------

SetActiveView(renderViewLeft)

# show data from final_prunedvtk
leftvtkDisplay = Show(leftvtk, renderViewLeft)
leftvtkDisplay.ColorArrayName = ['POINTS', '']
leftvtkDisplay.DiffuseColor = [0.0, 1.0, 0.0]
leftvtkDisplay.LineWidth = 5.0

# ----------------------------------------------------------------
renderViewLeft.ResetCamera()
renderViewRight.ResetCamera()
AddCameraLink(renderViewRight, renderViewLeft, 'CameraLink0')

SetActiveSource(rightvtk)
SetActiveSource(leftvtk)

Interact()
# SaveScreenshot('image_1.png', magnification=5, quality=100, view=renderView1)
# SaveScreenshot('image_2.png', magnification=5, quality=100, view=renderView1)
# ----------------------------------------------------------------
