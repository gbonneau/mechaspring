from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
fenetreDeVue = GetActiveViewOrCreate('RenderView')
fenetreDeVue.ViewSize = [800, 800]
fenetreDeVue.GetRenderWindow().SetPosition(10, 10)
fenetreDeVue.ResetCamera()
fenetreDeVue.InteractionMode = '2D'
fenetreDeVue.CameraPosition = [0.5, 0.5, 10000.0]
fenetreDeVue.CameraFocalPoint = [0.5, 0.5, 0.0]
fenetreDeVue.OrientationAxesVisibility = 0

# show data in view
finalvtk = LegacyVTKReader(registrationName='final.vtk', FileNames=[sys.argv[2]])
finalvtkDisplay = Show(finalvtk, fenetreDeVue, 'UnstructuredGridRepresentation')
referencevtk = LegacyVTKReader(registrationName='reference.vtk', FileNames=[sys.argv[1]])
referencevtkDisplay = Show(referencevtk, fenetreDeVue, 'UnstructuredGridRepresentation')

referencevtkDisplay.ColorArrayName = ['POINTS', '']
referencevtkDisplay.DiffuseColor = [0.0, 1.0, 0.0]
referencevtkDisplay.LineWidth = 2.0
referencevtkDisplay.RenderLinesAsTubes = 1

finalvtkDisplay.ColorArrayName = ['POINTS', '']
finalvtkDisplay.DiffuseColor = [0.0, 0.0, 1.0]
finalvtkDisplay.LineWidth = 2.0
finalvtkDisplay.RenderLinesAsTubes = 1


# Changer le couleur de fond
colorPalette = GetSettingsProxy('ColorPalette')
colorPalette.Background = [1.0, 1.0, 1.0]

# Activer le rendu
SetActiveSource(referencevtk)
fenetreDeVue.Update()

Interact()
