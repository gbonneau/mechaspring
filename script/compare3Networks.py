import sys

# state file generated using paraview version 5.7.0

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.7.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderViewRightMost = CreateView('RenderView')
renderViewRightMost.ViewSize = [800, 800]
renderViewRightMost.InteractionMode = '2D'
renderViewRightMost.AxesGrid.Visibility = 0
renderViewRightMost.OrientationAxesVisibility = 0
renderViewRightMost.Background = [1.0, 1.0, 1.0]

renderViewRightMost.GetRenderWindow().SetPosition(1600, 100)

# Create a new 'Render View'
renderViewCenter = CreateView('RenderView')
renderViewCenter.ViewSize = [800, 800]
renderViewCenter.InteractionMode = '2D'
renderViewCenter.AxesGrid.Visibility = 0
renderViewCenter.OrientationAxesVisibility = 0
renderViewCenter.Background = [1.0, 1.0, 1.0]

renderViewCenter.GetRenderWindow().SetPosition(800, 100)


# Create a new 'Render View'
renderViewLeftMost = CreateView('RenderView')
renderViewLeftMost.ViewSize = [800, 800]
renderViewLeftMost.InteractionMode = '2D'
renderViewLeftMost.AxesGrid.Visibility = 0
renderViewLeftMost.OrientationAxesVisibility = 0
renderViewLeftMost.Background = [1.0, 1.0, 1.0]

renderViewLeftMost.GetRenderWindow().SetPosition(0, 100)

SetActiveView(None)

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

network1 = LegacyVTKReader(FileNames=[sys.argv[1]])
network2 = LegacyVTKReader(FileNames=[sys.argv[2]])
network3 = LegacyVTKReader(FileNames=[sys.argv[3]])

# ----------------------------------------------------------------
# setup the visualization in view 'renderViewRightMost'
# ----------------------------------------------------------------

rightMostDisplay = Show(network3, renderViewRightMost)

# get color transfer function/color map for 'bondEnergy'
gradientEnergyLUT = GetColorTransferFunction('gradient')
gradientEnergyLUT.ScalarRangeInitialized = 1.0

gradientEnergyPWF = GetOpacityTransferFunction('gradient')
gradientEnergyPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
rightMostDisplay.Representation = 'Surface'
rightMostDisplay.ColorArrayName = ['POINTS', 'gradient']
rightMostDisplay.LookupTable = gradientEnergyLUT
rightMostDisplay.LineWidth = 5.0
rightMostDisplay.ScalarOpacityFunction = gradientEnergyPWF # PWF means most probably PieceWise Function
rightMostDisplay.RescaleTransferFunctionToDataRange()

# setup the color legend parameters for each legend in this view

# get color legend/bar for gradientEnergyLUT in view renderViewRightMost
gradientEnergyLUTColorBar = GetScalarBar(gradientEnergyLUT, renderViewRightMost)
gradientEnergyLUTColorBar.Orientation = 'Horizontal'
gradientEnergyLUTColorBar.Position = [0.35, 0.01]
gradientEnergyLUTColorBar.Title = 'gradient of energy'
gradientEnergyLUTColorBar.ComponentTitle = ''
gradientEnergyLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
gradientEnergyLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
gradientEnergyLUTColorBar.ScalarBarLength = 0.32999999999999974

# set color bar visibility
gradientEnergyLUTColorBar.Visibility = 1

# show color legend
rightMostDisplay.SetScalarBarVisibility(renderViewRightMost, True)


# ----------------------------------------------------------------
# setup the visualization in view 'renderViewCenter'
# ----------------------------------------------------------------

centerDisplay = Show(network2, renderViewCenter)

# trace defaults for the display properties.
centerDisplay.Representation = 'Surface'
centerDisplay.ColorArrayName = ['POINTS', 'gradient']
centerDisplay.LookupTable = gradientEnergyLUT
centerDisplay.LineWidth = 5.0
centerDisplay.ScalarOpacityFunction = gradientEnergyPWF # PWF means most probably PieceWise Function
centerDisplay.RescaleTransferFunctionToDataRange()

# setup the color legend parameters for each legend in this view

# get color legend/bar for bondEnergyLUT in view renderView1
gradientEnergyLUTColorBar_Center = GetScalarBar(gradientEnergyLUT, renderViewCenter)
gradientEnergyLUTColorBar_Center.Orientation = 'Horizontal'
gradientEnergyLUTColorBar_Center.Position = [0.35, 0.01]
gradientEnergyLUTColorBar_Center.Title = 'gradient of energy'
gradientEnergyLUTColorBar_Center.ComponentTitle = ''
gradientEnergyLUTColorBar_Center.TitleColor = [0.0, 0.0, 0.0]
gradientEnergyLUTColorBar_Center.LabelColor = [0.0, 0.0, 0.0]
gradientEnergyLUTColorBar_Center.ScalarBarLength = 0.33

# set color bar visibility
gradientEnergyLUTColorBar_Center.Visibility = 1

# show color legend
centerDisplay.SetScalarBarVisibility(renderViewCenter, True)

# ----------------------------------------------------------------
# setup the visualization in view 'renderViewLeftMost'
# ----------------------------------------------------------------


# show data from final_prunedvtk
leftMostDisplay = Show(network1, renderViewLeftMost)

# trace defaults for the display properties.
leftMostDisplay.Representation = 'Surface'
leftMostDisplay.ColorArrayName = ['POINTS', 'gradient']
leftMostDisplay.LookupTable = gradientEnergyLUT
leftMostDisplay.LineWidth = 5.0
leftMostDisplay.SelectScaleArray = 'gradient'
leftMostDisplay.ScalarOpacityFunction = gradientEnergyPWF
leftMostDisplay.ScalarOpacityUnitDistance = 0.15

# setup the color legend parameters for each legend in this view

# get color legend/bar for bondEnergyLUT in view renderView2
gradientEnergyLUTColorBar_1 = GetScalarBar(gradientEnergyLUT, renderViewLeftMost)
gradientEnergyLUTColorBar_1.Orientation = 'Horizontal'
gradientEnergyLUTColorBar_1.Position = [0.35, 0.01]
gradientEnergyLUTColorBar_1.Title = 'gradient of energy'
gradientEnergyLUTColorBar_1.ComponentTitle = ''
gradientEnergyLUTColorBar_1.TitleColor = [0.0, 0.0, 0.0]
gradientEnergyLUTColorBar_1.LabelColor = [0.0, 0.0, 0.0]

# set color bar visibility
gradientEnergyLUTColorBar_1.Visibility = 1

# show color legend
leftMostDisplay.SetScalarBarVisibility(renderViewLeftMost, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(network3)
SetActiveSource(network2)
SetActiveSource(network1)
renderViewRightMost.ResetCamera()
AddCameraLink(renderViewRightMost, renderViewLeftMost, 'CameraLink0')
AddCameraLink(renderViewRightMost, renderViewCenter, 'CameraLink1')
SetActiveView(renderViewLeftMost)
SetActiveView(renderViewCenter)
SetActiveView(renderViewRightMost)
Interact()
# SaveScreenshot('image_1.png', magnification=5, quality=100, view=renderView1)
# SaveScreenshot('image_2.png', magnification=5, quality=100, view=renderView2)
# ----------------------------------------------------------------
