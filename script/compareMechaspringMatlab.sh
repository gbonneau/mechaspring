filename=$1
echo $filename
./apps/016_periodicSpringFile_bin $filename 1.001 0.0
python ../script/drawPeriodicGrid.py reference.vtk periodicityReference.txt 3 3
pvpython ../script/periodicParaviewScreenShot.py reference_repeat.vtk 1.0 1.0 1.0 5 800
# pvpython ../script/periodicParaviewScreenShot.py reference_repeat.vtk 1.2 0.0 1.4 5 800
mv sortie.png ../matlab/INPUT_HOMOG/networkMechaspring.png
# pushd ../matlab/; octave mainImageHomogenization.m; popd
pushd ../matlab/; octave mainImageHomogenizationComplete.m; popd

open ../matlab/INPUT_HOMOG/networkMechaspring.png
open ../malab/OUTPUT/networkMechaspring_PRplot.png
python ../script/polarPlot.py ../matlab/OUTPUT/E_plot
python ../script/polarPlot.py ../matlab/OUTPUT/PR_plot
