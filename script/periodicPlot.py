# state file generated using paraview version 5.9.1

import sys
import numpy as np

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [800,800]
renderView1.InteractionMode = '2D'
renderView1.CenterOfRotation = [0.6075364999999999, 0.59753165, 0.0]
renderView1.CameraPosition = [0.7026469739701093, 1.025620115125255, 10000.0]
renderView1.CameraFocalPoint = [0.7026469739701093, 1.025620115125255, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.9259273863644448
renderView1.OrientationAxesVisibility = 0

SetActiveView(None)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# read input data
# ----------------------------------------------------------------

centralNetwork = LegacyVTKReader(FileNames=[sys.argv[1]])
vecteursDePeriodicite = np.loadtxt(sys.argv[2])

# periodicityVectorLeftRight=[ 1.1754950714350362873, 0.14781745949826918807]
# periodicityVectorBottomTop=[ 0.14781745949826907705, 1.1245049285649639792]

periodicityVectorLeftRight=vecteursDePeriodicite[0]
periodicityVectorBottomTop=vecteursDePeriodicite[1]

# ----------------------------------------------------------------
# translate networks
# ----------------------------------------------------------------

offsets=[ [1., 0.], [1.,1.], [0.,1.], [-1.,1.], [-1.,0.], [-1.,-1.], [0.,-1.],[1.,-1.]]



translatedNetworks = []
for offset in offsets:
    # create a new 'Calculator'
    translatedNetworks.append(Calculator(registrationName='Calculator1', Input=centralNetwork))
    translatedNetworks[-1].CoordinateResults = 1
    translatedNetworks[-1].Function = '(coordsX+1.1754950714350362873)*iHat+(coordsY+0.14781745949826918807)*jHat'
    coordsX = '(coordsX+'+str(offset[0])+'*'+str(periodicityVectorLeftRight[0])+'+'+str(offset[1])+'*'+str(periodicityVectorBottomTop[0])+')'
    coordsY = '(coordsY+'+str(offset[0])+'*'+str(periodicityVectorLeftRight[1])+'+'+str(offset[1])+'*'+str(periodicityVectorBottomTop[1])+')'
    functionCalculator=coordsX+'*iHat+'+coordsY+'*jHat'
    translatedNetworks[-1].Function = functionCalculator

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from loadedvtk
centralNetworkDisplay = Show(centralNetwork, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'gradient'
gradientLUT = GetColorTransferFunction('gradient')
gradientLUT.RGBPoints = [3.4356687455282997, 0.231373, 0.298039, 0.752941, 219.48239061642835, 0.865003, 0.865003, 0.865003, 435.5291124873284, 0.705882, 0.0156863, 0.14902]
gradientLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'gradient'
gradientPWF = GetOpacityTransferFunction('gradient')
gradientPWF.Points = [3.4356687455282997, 0.0, 0.5, 0.0, 435.5291124873284, 1.0, 0.5, 0.0]
gradientPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
centralNetworkDisplay.Representation = 'Surface'
centralNetworkDisplay.ColorArrayName = ['POINTS', 'gradient']
centralNetworkDisplay.LookupTable = gradientLUT
centralNetworkDisplay.LineWidth = 5.0
centralNetworkDisplay.RenderLinesAsTubes = 1

for translatedNetwork in translatedNetworks:
# show data from calculator1
    translatedNetworkDisplay = Show(translatedNetwork, renderView1, 'UnstructuredGridRepresentation')
    # trace defaults for the display properties.
    translatedNetworkDisplay.Representation = 'Surface'
    translatedNetworkDisplay.ColorArrayName = ['POINTS', '']
    translatedNetworkDisplay.LineWidth = 5.0
    translatedNetworkDisplay.RenderLinesAsTubes = 1

# get color legend/bar for gradientLUT in view renderView1
gradientLUTColorBar = GetScalarBar(gradientLUT, renderView1)
gradientLUTColorBar.WindowLocation = 'UpperRightCorner'
gradientLUTColorBar.Title = 'gradient'
gradientLUTColorBar.ComponentTitle = 'Magnitude'

# set color bar visibility
gradientLUTColorBar.Visibility = 1

# show color legend
centralNetworkDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(centralNetwork)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
    Interact()