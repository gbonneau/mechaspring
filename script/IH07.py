import sys
import svgwrite
from svgwrite import mm
import numpy as np
	
quatrePiSurTrois = 4. * np.pi / 3.
deuxPiSurTrois = 2. * np.pi / 3.
piSurSix = np.pi / 6.

currentColor = [255, 0, 0]

def drawLine( a, b):
	colorCut = svgwrite.rgb( currentColor[0], currentColor[1], currentColor[2], '%')
	strokeWidthCut  = 1.
	xCentre  = 300.
	yCentre  = 300.
	odwg.add(odwg.line(start=(xCentre - a[0], yCentre-a[1]), end=(xCentre - b[0], yCentre-b[1]), stroke=colorCut, stroke_width=strokeWidthCut))

def drawPath( P):
	a = P[0]
	for b in P[1:]:
		drawLine( a, b)
		a = b

def drawNetwork( vertex, connectivity):
	for vi in range(len(connectivity)):
		for vj in connectivity[vi]:
			drawLine( vertex[vi], vertex[vj])

def drawCoreStructure( coreStructure):
	a = coreStructure[0]
	b = coreStructure[1]
	c = coreStructure[2]
	d = coreStructure[3]
	e = coreStructure[4]
	f = coreStructure[5]
	g = coreStructure[6]
	h = coreStructure[7]
	i = coreStructure[8]
	j = coreStructure[9]
	k = coreStructure[10]
	l = coreStructure[11]
	m = coreStructure[12]

	drawPath( [m, a, i, f, j, e])
	drawPath( [m, b, g, c, h, a])
	drawPath( [m, e, k, d, l, b])

def translate( vecteur, sommet):
	return vecteur + sommet

def rotation( centre, angle, sommet):
	rM = np.array( [[np.cos(angle), -np.sin(angle)], \
	                [np.sin(angle),  np.cos(angle)]])
	return( (rM @ (sommet - centre)) + centre)
	# return( (np.transpose(rM) @ (sommet - centre)) + centre)

def isocelePiSur6( a, b):
	d = rotation( a, piSurSix, b)
	numerateur = 0.5 * np.dot( b-a, b-a)
	denominateur = np.dot( d-a, b-a)
	alpha = numerateur / denominateur
	c = a + alpha * (d-a)
	return c


def IH07_core( a, b, c):
	d = rotation( a, quatrePiSurTrois, c) + (b-a)
	e = rotation( a, quatrePiSurTrois, b) + (b-a)
	f = rotation( b, quatrePiSurTrois, d) + (e-b)

	g = isocelePiSur6( c, b)
	h = isocelePiSur6( a, c)
	i = isocelePiSur6( f, a)
	j = isocelePiSur6( e, f)
	k = isocelePiSur6( d, e)
	l = isocelePiSur6( b, d)
	m = isocelePiSur6( b, a)

	coreStructure = [a, b, c, d, e, f, g, h, i, j, k, l, m]

	vertexConnectivity = [ [8, 12, 7],
		[6, 12, 11],
		[6, 7],
		[10, 11],
		[9, 10],
		[ 8, 9],
		[ 1, 2],
		[ 2, 0],
		[ 0, 5],
		[5, 4],
		[ 4, 3],
		[3, 1],
		[ 1, 0, 4]
		]


	return coreStructure, vertexConnectivity

def IH07_large( a, b, c):
	global currentColor
	coreVertex, coreVertexConnectivity = IH07_core( a, b, c)

	upVector = coreVertex[5] - coreVertex[1] # f - b
	downVector = coreVertex[0] - coreVertex[3] # s - d
	rightVector = coreVertex[8] - coreVertex[11] + coreVertex[7] - coreVertex[6] # (i - l) + (h - g)

	upVertex = [ translate( upVector, p) for p in coreVertex]
	downVertex = [ translate( downVector, p) for p in coreVertex]
	rightVertex = [ translate( rightVector, p) for p in coreVertex]

	# currentColor = [ 255, 0, 0]
	# drawCoreStructure( coreStructure)
	# currentColor = [ 0, 255 , 0]
	# drawCoreStructure( upStructure)
	# currentColor = [ 100, 255 , 0]
	# drawCoreStructure( downStructure)
	# currentColor = [ 0, 0 , 255]
	# drawCoreStructure( rightStructure)

	# BORD EXTERIEUR
	vertex = [i for i in range(39)]
	vertex[0] = rightVertex[8]
	vertex[1] = rightVertex[5]
	vertex[2] = rightVertex[9]
	vertex[3] = rightVertex[4]
	vertex[4] = upVertex[8]
	vertex[5] = upVertex[5]
	vertex[6] = upVertex[9]
	vertex[7] = upVertex[4]
	vertex[8] = upVertex[10]
	vertex[9] = upVertex[3]
	vertex[10] = coreVertex[9]
	vertex[11] = coreVertex[4]
	vertex[12] = coreVertex[10]
	vertex[13] = coreVertex[3]
	vertex[14] = coreVertex[11]
	vertex[15] = coreVertex[1]
	vertex[16] = coreVertex[6]
	vertex[17] = coreVertex[2]
	vertex[18] = coreVertex[7]
	vertex[19] = downVertex[1]
	vertex[20] = downVertex[6]
	vertex[21] = downVertex[2]
	vertex[22] = downVertex[7]
	vertex[23] = downVertex[0]
	vertex[24] = downVertex[8]
	vertex[25] = rightVertex[2]
	vertex[26] = rightVertex[7]
	vertex[27] = rightVertex[0]

	# currentColor = [0, 0, 0]
	# drawPath( periodicStructure[0:28])

	# AUTRES SOMMETS

	vertex[28] = rightVertex[12]
	vertex[29] = upVertex[12]
	vertex[30] = coreVertex[12]
	vertex[31] = downVertex[12]

	vertex[32] = downVertex[5]
	vertex[33] = upVertex[0]
	vertex[34] = coreVertex[5]
	vertex[35] = coreVertex[0]

	vertex[36] = upVertex[7]
	vertex[37] = coreVertex[8]
	vertex[38] = upVertex[2]

	vertexConnectivity = [
		[1, 27],
		[2, 0],
		[1, 3],
		[ 2, 4, 28],
		[ 3, 5, 33],
		[ 4, 6],
		[ 5, 7],
		[ 6, 8, 29],
		[ 7, 9],
		[ 8, 10],
		[ 9, 11, 34],
		[ 10, 12, 30],
		[ 11, 13],
		[ 12, 14],
		[ 13, 15],
		[ 14, 16, 30],
		[ 15, 17],
		[ 16, 18],
		[ 17, 19, 35],
		[ 18, 20, 31],
		[ 19, 21],
		[ 20, 22],
		[ 21, 23],
		[ 22, 24, 31],
		[ 23, 25, 32],
		[ 24, 26],
		[ 25, 27],
		[ 26, 0, 28],
		[ 3, 32, 27],
		[ 7, 34, 33],
		[ 11, 15, 35],
		[ 19, 23, 38],
		[ 24, 28, 36],
		[ 4, 29, 36],
		[ 10, 37, 29],
		[ 18, 37, 30],
		[ 32, 33, 38],
		[ 34, 35, 38],
		[ 31, 36, 37]
	]

	return vertex, vertexConnectivity

def IH07_periodic( a, b, c):
	coreVertex, coreVertexConnectivity = IH07_core( a, b, c)

	upVector = coreVertex[5] - coreVertex[1] # f - b
	downVector = coreVertex[0] - coreVertex[3] # s - d
	rightVector = coreVertex[8] - coreVertex[11] + coreVertex[7] - coreVertex[6] # (i - l) + (h - g)

	upVertex = [ translate( upVector, p) for p in coreVertex]
	downVertex = [ translate( downVector, p) for p in coreVertex]
	rightVertex = [ translate( rightVector, p) for p in coreVertex]

	vertex = [i for i in range(11)]
	vertex[0] = rightVertex[12]
	vertex[1] = upVertex[0]
	vertex[2] = upVertex[12]
	vertex[3] = coreVertex[5]
	vertex[4] = coreVertex[12]
	vertex[5] = coreVertex[0]
	vertex[6] = downVertex[12]
	vertex[7] = downVertex[5]
	vertex[8] = upVertex[7]
	vertex[9] = coreVertex[8]
	vertex[10] = upVertex[2]

	vertexConnectivity = [ [7],
		[2],
		[1, 3],
		[2, 9],
		[5],
		[9, 4],
		[10],
		[0,8],
		[1, 10, 7],
		[3, 5, 10],
		[6, 8, 9]
	]

	periodicity = [
		[ 6, 0, "C", "R"],
		[ 5, 1, "C", "R"],
		[ 4, 2, "C", "R"],
		[ 6, 4, "C", "U"],
		[ 7, 3, "C", "U"],
		[ 0, 2, "C", "U"],
	]

	return vertex, vertexConnectivity, periodicity



def writePeriodicStructure( periodicStructure, vertexConnectivity, periodicity):
	print(len(periodicStructure))
	for v in periodicStructure:
		print( v[0], v[1], 0.0)
	for c in vertexConnectivity:
		for v in c:
			print( v+1,  " ", end="")
		print("")

	for p in periodicity:
		print(p[0]+1, p[1]+1, p[2], p[3])


if __name__ == "__main__":
	if (len(sys.argv) != 7):
		print("Usage: " + sys.argv[0] + "ax ay bx by cx cy")
		sys.exit(1)

	odwg = svgwrite.Drawing('IH07_output.svg', size=('250mm','180mm'), viewBox=('0 0 800 800'))

	a = np.array( [float(sys.argv[1]), float(sys.argv[2])])
	b = np.array( [float(sys.argv[3]), float(sys.argv[4])])
	c = np.array( [float(sys.argv[5]), float(sys.argv[6])])

	periodicStructure, vertexConnectivity, periodicity = IH07_periodic( a, b, c)
	# periodicStructure, vertexConnectivity = IH07_large( a, b, c)
	# periodicStructure, vertexConnectivity, periodicity = IH07_core( a, b, c)

	currentColor = [0, 0, 0]
	drawNetwork( periodicStructure, vertexConnectivity)

	writePeriodicStructure( periodicStructure, vertexConnectivity, periodicity)

	odwg.save()
