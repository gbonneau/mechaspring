import periodicMinimalConstruct as pmC
import sys
import meshio
import numpy as np
import os
import shutil

class exportPeriodic:
    def __init__( self, vert, u0, v0, lList, aList, lListDisplay, aListDisplay, sArray, dirName):
        self.lList = lList
        self.aList = aList
        self.lListDisplay = lListDisplay
        self.aListDisplay = aListDisplay
        self.sArray = sArray

        self.dirName = dirName
        if os.path.isdir( self.dirName):
            print(f"Warning: directory {self.dirName} already exists, data will be overwritten")
        else:
            os.mkdir( self.dirName)

        self.index, self.invIndex, self.meshPoints, self.meshLines = networkToIndexAndMesh( vert, u0, v0, lListDisplay, aListDisplay)
        
        self.timeStep = 0

    def appendTimeStep( self, vert, u0, v0, sArray):

        # EXPORT IN SPRING FORMAT
        prefix = self.dirName+f"/timeStep_{self.timeStep:03d}"
        pmC.writeInIndirectSpringFileFormat( vert, u0, v0, self.lList, self.aList, sArray, prefix+".spg")

        # COMPUTE CELL DATA, I.E. ONE SCALAR FOR EACH SEGMENT IN THE NETWORK

        # length = lengthOfLengthSpring( vert, u0, v0, networkGlobal.pIndexLength, networkGlobal.uTransLength, networkGlobal.vTransLength)
        # relativeDeltaLength = (length - networkGlobal.lengthAtRest) / networkGlobal.lengthAtRest

        # EXPORT IN VTK FORMAT
        # COMPUTE ABSOLUTE 2D POINT POSITION, COPY IN 3D AND USE MESHIO TO EXPORT
        point2D = []
        for oneIndex in self.index:
            absolutePoint = pmC.periodicToAbsolute( oneIndex, vert, u0, v0)
            point2D.append(absolutePoint)
        point2D = np.array(point2D) # CONVERT LIST TP NUMPY ARRAY
        points = np.zeros( (point2D.shape[0], 3)) # EXTEND TO 3D
        points[:,:2] = point2D

        cells = [("line", self.meshLines)]
        mesh = meshio.Mesh( points, cells)
        mesh.write( prefix+".vtk")

        self.timeStep = self.timeStep + 1

        return None
    
    def finalizeExport( self):
        lastTimeStep = self.timeStep-1
        prefix = self.dirName+f"/timeStep_{lastTimeStep:03d}"
        shutil.copyfile( prefix+".vtk", self.dirName+"/equilibrium.vtk")
        shutil.copyfile( prefix+".spg", self.dirName+"/equilibrium.spg")
        prefix = self.dirName+f"/timeStep_000"
        shutil.copyfile( prefix+".vtk", self.dirName+"/rest.vtk")
        shutil.copyfile( prefix+".spg", self.dirName+"/rest.spg")
        prefix = self.dirName+f"/timeStep_001"
        shutil.copyfile( prefix+".vtk", self.dirName+"/load.vtk")
        shutil.copyfile( prefix+".spg", self.dirName+"/load.spg")
        print(f"all timesteps exported in files {self.dirName}/timeStep_XXX.vtk and {self.dirName}/timeStep_XXX.spg")
        print(f"rest configuration exported in files {self.dirName}/rest.vtk and {self.dirName}/rest.spg")
        print(f"load configuration exported in files {self.dirName}/load.vtk and {self.dirName}/load.spg")
        print(f"equilibrium configuration exported in files {self.dirName}/equilibrium.vtk and {self.dirName}/equilibrium.spg")


def networkToIndexAndMesh( vert, u0, v0, lList, aList):
    # SEE FILE periodicMinimalConstruct.py FOR A DETAILED EXPLANATION OF indexAllVertexInSpringList
    # AND THE MEANING OF THE STRUCTURES index, invIndex, nUnitCellVertex, nPeriodicVertex
    index, invIndex, nUnitCellVertex, nPeriodicVertex = pmC.indexAllVertexInSpringList( lList, aList)
    # for i, a in enumerate(index):
    #     if index[a] != i:
    #         print(a, index[a])

    # sys.exit(1)
    nPoint = nUnitCellVertex + nPeriodicVertex
    points = np.zeros((nPoint,2))
    for vi, oneIndex in enumerate( index):
        points[ index[oneIndex],:] = pmC.periodicToAbsolute( oneIndex, vert, u0, v0)
    lines = []
    nPointInLengthSpring = 2
    for lSpring in lList:
        line = [ index[tuple(lSpring[ci])] for ci in range(nPointInLengthSpring) ]
        lines.append( line)
    # print(lines)
        
    return index, invIndex, points, lines

# WRITE XDMF
# with meshio.xdmf.TimeSeriesWriter(filename) as writer:
#     writer.write_points_cells(points, cells)
#     for t in [0.0, 0.1, 0.21]:
#         writer.write_data(t, point_data={"phi": data})

# READ XDMF
# with meshio.xdmf.TimeSeriesReader(filename) as reader:
#     points, cells = reader.read_points_cells()
#     for k in range(reader.num_steps):
#         t, point_data, cell_data = reader.read_data(k)

# WRITE NETWORK IN XDMF
# with meshio.xdmf.TimeSeriesWriter( filename) as writer:
#     points = [ list(p) for p in self.meshPoints] # CONVERT NUMPY ARRAY TO LIST
#     cells = [( 'line', self.meshLines)]

#     print(cells)
#     writer.write_points_cells( points, cells)
#     for timeStep in range(len(self.allDisplacement)):
#         writer.write_data( t=float(timeStep), point_data={"displacement": self.allDisplacement[ timeStep]})