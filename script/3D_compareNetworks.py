import sys

# state file generated using paraview version 5.7.0

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.7.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [800, 800]
renderView1.InteractionMode = '3D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.AxesGrid.Visibility = 0
renderView1.OrientationAxesVisibility = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

renderView1.GetRenderWindow().SetPosition(1000, 10)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.ViewSize = [800, 800]
renderView2.InteractionMode = '3D'
renderView2.AxesGrid = 'GridAxes3DActor'
renderView2.AxesGrid.Visibility = 0
renderView2.OrientationAxesVisibility = 0
renderView2.StereoType = 'Crystal Eyes'
renderView2.Background = [1.0, 1.0, 1.0]
renderView2.BackEnd = 'OSPRay raycaster'
renderView2.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)

# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

final_prunedvtk = LegacyVTKReader(FileNames=[sys.argv[1]])
final_spikyvtk = LegacyVTKReader(FileNames=[sys.argv[2]])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from final_spikyvtk
final_spikyvtkDisplay = Show(final_spikyvtk, renderView1)

# get color transfer function/color map for 'bondEnergy'
bondEnergyLUT = GetColorTransferFunction('bondEnergy')
# bondEnergyLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.0007163800182752311, 0.865003, 0.865003, 0.865003, 0.0014327600365504622, 0.705882, 0.0156863, 0.14902]
bondEnergyLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'bondEnergy'
bondEnergyPWF = GetOpacityTransferFunction('bondEnergy')
# bondEnergyPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.0014327600365504622, 1.0, 0.5, 0.0]
bondEnergyPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
final_spikyvtkDisplay.Representation = 'Surface'
final_spikyvtkDisplay.ColorArrayName = ['CELLS', 'bondEnergy']
final_spikyvtkDisplay.LookupTable = bondEnergyLUT
final_spikyvtkDisplay.LineWidth = 5.0
final_spikyvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
final_spikyvtkDisplay.SelectOrientationVectors = 'None'
final_spikyvtkDisplay.ScaleFactor = 0.12191999554634095
final_spikyvtkDisplay.SelectScaleArray = 'bondEnergy'
final_spikyvtkDisplay.GlyphType = 'Arrow'
final_spikyvtkDisplay.GlyphTableIndexArray = 'bondEnergy'
final_spikyvtkDisplay.GaussianRadius = 0.006095999777317047
final_spikyvtkDisplay.SetScaleArray = [None, '']
final_spikyvtkDisplay.ScaleTransferFunction = 'PiecewiseFunction'
final_spikyvtkDisplay.OpacityArray = [None, '']
final_spikyvtkDisplay.OpacityTransferFunction = 'PiecewiseFunction'
final_spikyvtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
final_spikyvtkDisplay.PolarAxes = 'PolarAxesRepresentation'
final_spikyvtkDisplay.ScalarOpacityFunction = bondEnergyPWF
final_spikyvtkDisplay.ScalarOpacityUnitDistance = 0.14565412472886435

# setup the color legend parameters for each legend in this view

# get color legend/bar for bondEnergyLUT in view renderView1
bondEnergyLUTColorBar = GetScalarBar(bondEnergyLUT, renderView1)
bondEnergyLUTColorBar.Orientation = 'Horizontal'
bondEnergyLUTColorBar.WindowLocation = 'AnyLocation'
bondEnergyLUTColorBar.Position = [0.3412548015364917, 0.014318555008210304]
bondEnergyLUTColorBar.Title = 'bondEnergy'
bondEnergyLUTColorBar.ComponentTitle = ''
bondEnergyLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
bondEnergyLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
bondEnergyLUTColorBar.ScalarBarLength = 0.32999999999999974

# set color bar visibility
bondEnergyLUTColorBar.Visibility = 1

# show color legend
final_spikyvtkDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView2'
# ----------------------------------------------------------------

SetActiveView(renderView2)

# show data from final_prunedvtk
final_prunedvtkDisplay = Show(final_prunedvtk, renderView2)

# trace defaults for the display properties.
final_prunedvtkDisplay.Representation = 'Surface'
final_prunedvtkDisplay.ColorArrayName = ['CELLS', 'bondEnergy']
final_prunedvtkDisplay.LookupTable = bondEnergyLUT
final_prunedvtkDisplay.LineWidth = 5.0
final_prunedvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
final_prunedvtkDisplay.SelectOrientationVectors = 'None'
final_prunedvtkDisplay.ScaleFactor = 0.11949999406933785
final_prunedvtkDisplay.SelectScaleArray = 'bondEnergy'
final_prunedvtkDisplay.GlyphType = 'Arrow'
final_prunedvtkDisplay.GlyphTableIndexArray = 'bondEnergy'
final_prunedvtkDisplay.GaussianRadius = 0.005974999703466892
final_prunedvtkDisplay.SetScaleArray = [None, '']
final_prunedvtkDisplay.ScaleTransferFunction = 'PiecewiseFunction'
final_prunedvtkDisplay.OpacityArray = [None, '']
final_prunedvtkDisplay.OpacityTransferFunction = 'PiecewiseFunction'
final_prunedvtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
final_prunedvtkDisplay.PolarAxes = 'PolarAxesRepresentation'
final_prunedvtkDisplay.ScalarOpacityFunction = bondEnergyPWF
final_prunedvtkDisplay.ScalarOpacityUnitDistance = 0.14065503080055233

# setup the color legend parameters for each legend in this view

# get color legend/bar for bondEnergyLUT in view renderView2
bondEnergyLUTColorBar_1 = GetScalarBar(bondEnergyLUT, renderView2)
bondEnergyLUTColorBar_1.Orientation = 'Horizontal'
bondEnergyLUTColorBar_1.WindowLocation = 'AnyLocation'
bondEnergyLUTColorBar_1.Position = [0.3507692307692308, 0.019244663382594514]
bondEnergyLUTColorBar_1.Title = 'bondEnergy'
bondEnergyLUTColorBar_1.ComponentTitle = ''
bondEnergyLUTColorBar_1.TitleColor = [0.0, 0.0, 0.0]
bondEnergyLUTColorBar_1.LabelColor = [0.0, 0.0, 0.0]

# set color bar visibility
bondEnergyLUTColorBar_1.Visibility = 1

# show color legend
final_prunedvtkDisplay.SetScalarBarVisibility(renderView2, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# finally, restore active source
# SetActiveSource(final_spikyvtk)
SetActiveSource(final_spikyvtk)
SetActiveSource(final_prunedvtk)
renderView1.ResetCamera()
AddCameraLink(renderView1, renderView2, 'CameraLink0')
Interact()
# SaveScreenshot('image_1.png', magnification=5, quality=100, view=renderView1)
# SaveScreenshot('image_2.png', magnification=5, quality=100, view=renderView1)
# ----------------------------------------------------------------
