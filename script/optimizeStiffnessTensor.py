#-----------------------------------------------------------------------------------
# GIVEN N STRAIN AND STRESS (2ND ORDER) TENSORS,
# COMPUTE A STIFFNESS (4TH ORDER) TENSOR FITTING IN THE LEAST-SQUARE SENSE THIS DATA
# SEE THE MAIN ROUTINE elasticityTensorFromStrainAndStressTensors BELOW FOR MORE DETAILS
#
# ADDITIONALLY DRAW POLAR PLOTS OF THE YOUNG MODULUS AND POISSON RATIO
# FROM THE SIX COEFFICIENTS OF THE ELASTICITY TENSOR
#-----------------------------------------------------------------------------------

import numpy as np
import glob
import sys
import matplotlib.pyplot as plt

sq2 = np.sqrt(2)
dim3 = 3
dim6 = 6

def betcherew2ndOrder2D():
    M=np.zeros((6,3,3))
    M[0,0,0] = 1
    M[1,1,1] = 1
    M[2,2,2] = 2
    M[3,0,1] = 1
    M[3,1,0] = M[3,0,1]
    M[4,1,2] = np.sqrt(2)
    M[4,2,1] = M[4,1,2]
    M[5,0,2] = np.sqrt(2)
    M[5,2,0] = M[5,0,2]
    return M

def matrixFamilyProduct( M, i, j):
    return M[i,:,:] @ M[j,:,:]

def elasticityVoigtMatrixFromVector( Cvect):
    Cmat = np.array( [ [ Cvect[0], Cvect[3], Cvect[5] ], \
                       [ Cvect[3], Cvect[1], Cvect[4] ], \
                       [ Cvect[5], Cvect[4], Cvect[2] ] ])
    return Cmat

# CONVERT A 2X2 SYMMETRIC MATRIX TO A 3-DIMENSIONAL VECTOR
def voigtVectorFrom2ndOrderSymmetricMatrix( oneMat):
    oneVect = np.array([ oneMat[0,0], oneMat[1,1], oneMat[0,1]])
    return oneVect

# GIVEN THE SIX COEFFICIENTS OF THE ELASTICIY TENSOR
# RETURN THE 3X3 MATRIX WITH THE BETCHEREW CONVENTION
def elasticityBetcherewMatrixFromVector( Cvect):
    Cmat = np.array( [ [ Cvect[0], Cvect[3], sq2 * Cvect[5] ], \
                       [ Cvect[3], Cvect[1], sq2 * Cvect[4] ], \
                       [ sq2 * Cvect[5], sq2 * Cvect[4], 2 * Cvect[2] ] ])
    return Cmat

# GIVEN A 3X3 MATRIX ENCODING THE ELASTICITY TENSOR WITH THE BECHTEREW CONVENTION,
# RETURN THE 6 COEFFICIENTS
# C1111, C2222,  C1212, C1122, C2212, C1112
# INVERSE OPERATION OF FUNCTION elasticityBetcherewMatrixFromVector

def elasticityVectorFromBetcherewMatrix( Cmat):
    Cvect = np.zeros(6)
    Cvect[0] = Cmat[0,0]
    Cvect[1] = Cmat[1,1]
    Cvect[2] = Cmat[2,2] / 2
    Cvect[3] = Cmat[0,1]
    Cvect[4] = Cmat[1,2] / sq2
    Cvect[5] = Cmat[0,2] / sq2
    return Cvect

# GIVEN THE SIX COEFFICIENTS OF THE ELASTICIY TENSOR
# C1111, C2222,  C1212, C1122, C2212, C1112
# RETURNS THE SIX COEFFICIENTS OF THE COMPLIANCE TENSOR
# S1111, S2222,  S1212, S1122, S2212, S1112

def complianceVectorFromElasticityVector( Cvect):
    cBechterew = elasticityBetcherewMatrixFromVector( Cvect)
    sBechtrew = np.linalg.inv( cBechterew)
    Svect = elasticityVectorFromBetcherewMatrix( sBechtrew)
    return Svect

# GIVEN THE SIX COEFFICIENTS OF THE ELASTICIY/COMPLIANCE TENSOR
# C1111, C2222,  C1212, C1122, C2212, C1112
# RETURNS THE 4TH ORDER ELASTICITY/COMPLIANCE TENSOR OF DIMENSION 2,2,2,2
def fourthOrderTensorFromVector( Cvect):
    C = np.zeros( (2, 2, 2, 2))
    C[0,0,0,0] = Cvect[0]
    C[1,1,1,1] = Cvect[1]
    C[0,1,0,1] = Cvect[2]
    C[0,0,1,1] = Cvect[3]
    C[1,1,0,1] = Cvect[4]
    C[0,0,0,1] = Cvect[5]

    C[0,0,1,0] = C[0,0,0,1]

    C[0,1,0,0] = C[0,0,0,1] 
    C[0,1,1,0] = C[0,1,0,1]
    C[0,1,1,1] = C[1,1,0,1] 

    C[1,0,0,0] = C[0,0,0,1]
    C[1,0,0,1] = C[0,1,0,1]
    C[1,0,1,0] = C[0,1,1,0]
    C[1,0,1,1] = C[1,1,0,1]

    C[1,1,0,0] = C[0,0,1,1]
    C[1,1,1,0] = C[1,0,1,1]
    return C

# GIVEN THE (2,2,2,2) 4TH ORDER COMPLIANCE TENSOR Stensor
# AND A NORMED 2D-VECTOR n
# RETURNS THE YOUNG MODULUS FOR THIS DIRECTION
def youngModulusFromComplianceTensor( Stensor, n):
    nTn = np.tensordot( n, n, axes=0) # n (x) n
    SnTn = np.tensordot( Stensor, nTn, axes=2) # double contraction
    youngModulus = 1. / np.tensordot( nTn, SnTn, axes=2) # scalar
    # print(f"young modulus = {youngModulus}")
    return youngModulus

# GIVEN THE (2,2,2,2) 4TH ORDER COMPLIANCE TENSOR Stensor
# AND A NORMED 2D-VECTOR n
# RETURNS THE POISSON RATIO FOR THIS LONGITUDINAL DIRECTION
# AND THE ORTHOGONAL TRANSVERSE DIRECTION
def poissonRatioFromComplianceTensor( Stensor, n):
    m = np.array( [n[1], -n[0]])
    nTn = np.tensordot( n, n, axes=0) # n (x) n
    mTm = np.tensordot( m, m, axes=0)
    SnTn = np.tensordot( Stensor, nTn, axes=2) # double contraction
    SmTm = np.tensordot( Stensor, mTm, axes=2)
    nTnSmTm = np.tensordot( nTn, SmTm, axes=2)
    nTnSnTn = np.tensordot( nTn, SnTn, axes=2)
    poissonRatio = - nTnSmTm / nTnSnTn
    # print(f"poisson ratio = {poissonRatio}")
    return poissonRatio

# GIVEN THE SIX COEFFICIENTS OF THE ELASTICIY/COMPLIANCE TENSOR
# C1111, C2222,  C1212, C1122, C2212, C1112
# COMPUTES nAngle VALUES OF THE YOUNG MODULUS AND THE POISSON RATIO
# RETURN THE 3 ARRAYS theta, young, poisson
def youngModulusAndPoissonRatioFromElasticity( Cvect, nAngle):
    Svect = complianceVectorFromElasticityVector( Cvect)
    nAngle = 100
    theta = np.linspace( 0., 2.*np.pi, num=nAngle, endpoint=True )
    Stensor = fourthOrderTensorFromVector( Svect)
    young = np.zeros(nAngle)
    poisson = np.zeros(nAngle)
    for i in range(nAngle):
        n = np.array([ np.cos(theta[i]), np.sin(theta[i])])
        young[i] = youngModulusFromComplianceTensor( Stensor, n)
        poisson[i] = poissonRatioFromComplianceTensor( Stensor, n)
    # print(f"young = {young}")
    # print(f"poisson = {poisson}")
    return theta, young, poisson

# DRAW THE YOUNG MODULUS AND POISSON RATIO POLAR PLOTS
def youngModulusAndPoissonRatioPolarPlotFromElasticity( Cvect):
    theta, young, poisson = youngModulusAndPoissonRatioFromElasticity( Cvect, 100)
    fig, ax = plt.subplots(1,2, subplot_kw={'projection': 'polar'})
    ax[0].plot( theta, young)
    ax[1].plot( theta, poisson)
    # ax[0].set_rmin(0.)
    # ax[0].set_rmax(11.)
    # ax[1].set_rmax(1.5) # MAXIMUM POISSON RATIO IN PLOT
    # ax[1].set_rmin(-1.)
    # ax[1].set_rticks([-1.0, -0.5, 0., 0.5, 1.0, 1.5])  # radial ticks
    # ax[1].set_rticks([ 0., 0.2, 0.4, 0.6, 0.8, 1])  # Less radial ticks
    ax[0].set_title("Macroscopic Young")
    ax[1].set_title("Macroscopic Poisson")
    plt.savefig( "polarPlots_macroscopic.pdf", format="pdf")
    plt.show()

def poissonRatioFromStrain( Eps, n):
    return 1.

def youngModulusFromStrainAndStress( Eps, Sig):
    return 1.
                       
# GIVEN A 4TH ORDER ELASTICITY TENSOR C, OUTPUTS N RANDOM STRAIN AND STRESS 2ND ORDER TENSORS Eps[i] AND Sig[i]
# SUCH THAT Eps[i] = C : Sig[i]
# C IS GIVEN BY THE 6-DIMENSIONAL VECTOR [C1111, C2222, C1212, C1122, C2212, C1112]
# Eps[i] AND Sig[i] ARE GIVEN BY 3-DIMENSIONAL VECTORS [T11, T22, T12]
def randomStrainAndStressTensorFromElasticityTensor( N, C):
    Sig = np.zeros((N,dim3))
    Eps = np.zeros((N,dim3))
    Cmat = elasticityVoigtMatrixFromVector( C)
    for i in range(N):
        Eps[i,:] = np.random.rand(dim3)
        Eps[i,2] = Eps[i,2] * 2 # MULTIPLY BY 2 BEFORE MULTIPLYING IN VOIGT BASIS
        Sig[i,:] = Cmat @ Eps[i,:]
        Eps[i,2] = Eps[i,2] / 2
    return Eps, Sig

# PERTURBATE VECTORS
def perturbData( data, Epsilon):
    data = data + Epsilon * np.random.rand(*data.shape) # *data.shape IS A PYTHON COMMAND TO "UNPACK" THE T-UPLE data.shape INTO MULTIPLE INTEGERS
    return data

# UTILITY ROUTINE THAT MULTIPLIES BY SQRT(2) THE 3RD COORDINATE
# OF N 3D VECTORS
# THIS IS USED TO CONVERT TO/FROM THE BECHTEREW CONVENTION
def convert2ndOrderTensorVoigtToBetcherew( T):
    for i in range(T.shape[0]):
        T[i,2] = T[i,2] * sq2

# UTILITY ROUTINE THAT DIVIDES BY SQRT(2) THE 3RD COORDINATE
# OF N 3D VECTORS
# THIS IS USED TO CONVERT TO/FROM THE BECHTEREW CONVENTION
def convert2ndOrderTensorBetcherewToVoigt( T):
    for i in range(T.shape[0]):
        T[i,2] = T[i,2] / sq2

#-----------------------------------------------------
# THIS IS THE MAIN ROUTINE
# GIVEN N STRAIN TENSORS AND STRESS TENSORS
# ENCODED IN N 3D VECTORS Eps AND Sig
# RETURNS THE 4TH ORDER ELASTICITY TENSOR
# ENCODED IN A 6D VECTOR, WHICH BEST FIT
# THE PAIRS (STRAIN, STRESS)
#-----------------------------------------------------
def elasticityTensorFromStrainAndStressTensors( Eps, Sig):

    N = Eps.shape[0]
    assert( N == Sig.shape[0])

    betch = betcherew2ndOrder2D()
    # print(betch)
    betchProduct = np.zeros((dim6,dim6,dim3,dim3))
    mat = np.zeros((dim6,dim6))
    rhs = np.zeros(dim6)

    # MULTIPLY LAST COORDINATE OF ELONGATION AND STRAIN TENSOR BY SQRT(2) TO MAP TO BETCHEREW BASIS
    convert2ndOrderTensorVoigtToBetcherew( Eps)
    convert2ndOrderTensorVoigtToBetcherew( Sig)

    for k in range(dim6):
        for l in range(dim6):
            betchProduct[k,l,:,:] = matrixFamilyProduct( betch, k, l)
            for i in range(N):
                mat[k,l] = mat[k,l] + np.transpose(Eps[i,:]) @ betchProduct[k,l] @ Eps[i,:]

    for k in range(dim6):
        for i in range(N):
            rhs[k] = rhs[k] + np.transpose( Sig[i,:]) @ betch[k,:,:] @ Eps[i,:]

    convert2ndOrderTensorBetcherewToVoigt( Eps)
    convert2ndOrderTensorBetcherewToVoigt( Sig)

    print(f"RANK OF FITTING MATRIX = {np.linalg.matrix_rank( mat, hermitian=True)}")
    Crecovered = np.linalg.solve( mat, rhs)
    loss = lossFunction( Crecovered, Eps, Sig)
    print( f"Loss function (absolute) = {loss}")
    print( f"Loss function (divided by norm of elasticity tensor) = {loss/np.linalg.norm(Crecovered)}")
    return Crecovered


#-------------------------------------------------------------------------------------
# GIVEN N STRAIN TENSORS Eps[i] AND STRESS TENSORS Sig[i], AND AN ELASTICITY TENSOR C
# COMPUTE THE SCALAR SQRT( SUM_i{ || Sig[i] - C : Eps[i] || ^2})
# C IS GIVEN BY THE 6-DIMENSIONAL VECTOR [C1111, C2222, C1212, C1122, C2212, C1112]
# Eps[i] AND Sig[i] ARE GIVEN BY 3-DIMENSIONAL VECTORS [T11, T22, T12]
def lossFunction( C, Eps, Sig):
    N = Eps.shape[0]
    assert( N == Sig.shape[0])
    assert( Eps.shape == (N,3))
    assert( Sig.shape == (N,3))
    # MULTIPLY THE LAST COORDINATE OF STRAIN AND STRESS TENSORS BY SQRT(2),
    # SO THAT THE 3-DIMENSIONAL VECTOR ENCODING THE 2X2 SYMETRIC TENSOR HAS
    # THE SAME NORM AS THE TENSOR
    convert2ndOrderTensorVoigtToBetcherew( Eps)
    convert2ndOrderTensorVoigtToBetcherew( Sig)
    Cmat = elasticityBetcherewMatrixFromVector( C)
    lossValue = 0.
    for i in range(N):
        lossValue = lossValue + (np.linalg.norm( Sig[i] - Cmat @ Eps[i] ))**2
    convert2ndOrderTensorBetcherewToVoigt( Eps)
    convert2ndOrderTensorBetcherewToVoigt( Sig)
    return np.sqrt(lossValue)

def printVoigtLossVectors( C, Eps, Sig):
    print(f"next {N} Voigt stress loss vectors should have a small norm")
    Cmat = elasticityVoigtMatrixFromVector(C)
    for i in range(N):
        Eps[i,2] = Eps[i,2] * 2  # MULTIPLY BY 2 LAST COORDINATE IN VOIGT BASIS
        print( Sig[i,:] -  Cmat @ Eps[i,:])
        Eps[i,2] = Eps[i,2] / 2

def printBetcherewLossVectors( C, Eps, Sig):
    print(f"next {N} Betcherew stress loss vectors should have a small norm")
    Cmat = elasticityBetcherewMatrixFromVector(Ctesting)
    convert2ndOrderTensorVoigtToBetcherew( Eps)
    convert2ndOrderTensorVoigtToBetcherew( Sig)
    for i in range(N):
        print( Sig[i,:] -  Cmat @ Eps[i,:])
    convert2ndOrderTensorBetcherewToVoigt( Eps)
    convert2ndOrderTensorBetcherewToVoigt( Sig)

# INITIALIZE RANDOM DATA FOR TESTING PURPOSES
# RETURNS AN ELASTICITY TENSORS AND N STRAIN AND STRESS TENSORS
def initializeDataForTesting( N):
    # np.random.seed(123)
    Ctesting = np.random.rand(6) # INITIALIZING A RANDOM ELASTICTY TENSOR FOR TESTING
    # GENERATE N STRAIN AND STRESS TENSOR FROM THE ELASTICITY TENSOR, FOR TESTING
    Eps, Sig = randomStrainAndStressTensorFromElasticityTensor( N, Ctesting)
    return Ctesting, Eps, Sig

#--------------------------------------------------------------------------------------------------------------------------
# THE DEFAULT PROGRAM INTIALIZES RANDOM TESTING DATA AND CALLS THE MAIN FUNCTION elasticityTensorFromStrainAndStressTensors
if __name__=="__main__":

    N = 5
    Ctesting, Eps, Sig = initializeDataForTesting( N)
    # Ctesting = np.array([0.02181923, 0.02192400, 0.00024737, 0.02142785, 0.00000091,  -0.00000004]) # Stefanie Hexagon Light
    Eps = perturbData( Eps, 1e-3)
    Sig = perturbData( Sig, 1e-3)
    # VERIFY THAT THE TEST-GENERATED STRAIN, STRESS AND ELASTICITY TENSORS ARE COMPATIBLE
    # for i in range(N):
    #     print( Sig[i,:] - elasticityVoigtFromVector(Ctesting) @ Eps[i,:])

    Crecovered = elasticityTensorFromStrainAndStressTensors( Eps, Sig)

    print("elasticity tensor for testing:")
    print(Ctesting)
    print("recovered elasticity tensor:")
    print(Crecovered)

    printVoigtLossVectors( Crecovered, Eps, Sig)
    printBetcherewLossVectors( Crecovered, Eps, Sig)
    print( f"Loss function = {lossFunction( Crecovered, Eps, Sig)}")


    
