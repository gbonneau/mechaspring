import periodicMinimalConstruct as pmC
import numpy as np
import sys


objFileName = sys.argv[1]

# u0 = np.array([1.,0.])
# v0 = np.array([0.,1.])

u0 = np.array([1.,0.])
v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])

# vert, u0, v0, lList, aList = networkFromObjLineFile( u0, v0, "square.obj")
vert, u0, v0, lList, aList = pmC.chiralNetworkFromObjLineFile( u0, v0, objFileName)
pmC.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "outputChiral.spg")
print("file outputChiral.spg written")