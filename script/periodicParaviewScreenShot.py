import sys
import numpy as np
from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

xCenter = float(sys.argv[2]) # 0.0
yCenter = float(sys.argv[3]) # 0.0
width = float(sys.argv[4]) # 1.0
lineWidth = int(sys.argv[5]) # 5
imageWidth = int(sys.argv[6]) # 800

# xCenter = 0.5
# yCenter = 0.5
# width = 1.
# lineWidth = 2
# imageWidth = 800

myMesh = LegacyVTKReader(registrationName='myMesh', FileNames=[sys.argv[1]])
# myMesh = WavefrontOBJReader(registrationName='myMesh', FileName=sys.argv[1])

renderView = GetActiveViewOrCreate('RenderView')

myMeshDisplay = Show(myMesh, renderView, 'UnstructuredGridRepresentation')

renderView.InteractionMode = '2D'
renderView.CameraPosition = [xCenter,yCenter, 1.0]
renderView.CameraFocalPoint = [xCenter,yCenter, 0.0]
# renderView.CameraPosition = [0.5,0.5, 1.0]
# renderView.CameraFocalPoint = [0.5,0.5, 0.0]
# renderView.CameraParallelScale = 2.0 # TO SHOW OUTSIDE OF -1. TO 2.
# renderView.CameraParallelScale = 1.5 # TO FILL IN THE WHOLE STRUCTURE FROM -1. to +2.
# renderView.CameraParallelScale = 0.5 # TO FILL IN A SQUARE OF SIZE 1.
renderView.CameraParallelScale = 0.5*width # TO FILL IN A SQUARE OF SIZE width

# WIREFRAME RENDERING
myMeshDisplay.SetRepresentationType('Wireframe')
# myMeshDisplay.SetRepresentationType('Surface With Edges')

# LINE WIDTH
myMeshDisplay.LineWidth = lineWidth
# BLACK LINES
myMeshDisplay.AmbientColor = [0.0, 0.0, 0.0]
myMeshDisplay.DiffuseColor = [0.0, 0.0, 0.0]

# WHITE BACKGROUND
renderView.UseColorPaletteForBackground = 0
renderView.Background = [1.0, 1.0, 1.0]

# HIDE ORIENTATION AXES
renderView.OrientationAxesVisibility = 0

# AXIS GRID TO CHECK
# renderView.AxesGrid.Visibility = 1
# renderView.CenterAxesVisibility = 1
# renderView.AxesGrid.ShowGrid = 1


layout = GetLayout()
layout.SetSize(imageWidth, imageWidth)

# FONCTIONNEMENT STANDARD: ON PRODUIT UNE IMAGE CENTREE EN xCenter, yCenter (sys.argv[2,3])
# SaveScreenshot("sortie.png", viewOrLayout=renderView, TransparentBackground=1)
SaveScreenshot("sortie.png", viewOrLayout=layout, TransparentBackground=0)
sys.exit(0)


# AUTRE OPTION: ON SAUVE UNE SERIE D'IMAGES PERIODIQUES 
# OBTENUES EN TRANSLATANT LE CENTRE ENTRE 0 ET 1. EN X ET EN Y
xValues = np.linspace( 0.,1., num=5, endpoint=True)
yValues = np.linspace( 0.,1., num=5, endpoint=True)

for xCenter in xValues:
    for yCenter in yValues:
        renderView.CameraPosition = [xCenter,yCenter, 1.0]
        renderView.CameraFocalPoint = [xCenter,yCenter, 0.0]
        outputFilename = f"sortie_{xCenter}_{yCenter}.png" 
        SaveScreenshot(outputFilename, viewOrLayout=renderView, TransparentBackground=0) 