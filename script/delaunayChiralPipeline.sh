density=$1
python ../script/periodicDelaunay3Fold.py $density
python ../script/periodicMinimalObj2ChiralSpg.py delaunayMesh.obj
python ../script/changeStiffnessValues.py outputChiral.spg delaunayChiral.spg 10. 0.1 1000. 1000.
python ../script/periodicMinimalVisualization.py delaunayChiral.spg 2