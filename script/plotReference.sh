#!/bin/bash
if [ "$#" -ne 4 ]; then
	echo "Usage: $0 xMin xMax yMin yMax"
        echo
        exit
else
        echo "== display reference spring set"
fi

xMin=$1
xMax=$2
yMin=$3
yMax=$4

ratio=`echo "(($yMin)-($yMax))/(($xMin)-($xMax))" | bc -l`

cat > tmp.txt << EOF
set xrange [$xMin:$xMax]
set yrange [$yMin:$yMax]
set size ratio $ratio
plot "reference.txt" with lines lc "blue", "reference.txt" ps 0.005 pt 7 lc "blue"
pause -1 "hit return to close and exit"
EOF

gnuplot tmp.txt
rm tmp.txt
