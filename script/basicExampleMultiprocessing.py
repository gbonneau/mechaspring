import os
import sys
from multiprocessing import Pool

def launchShellCommand( oneShellCommand):
	print("Starting " + oneShellCommand)
	os.system( oneShellCommand)
	print("Finished " + oneShellCommand)
	return "Finished " + oneShellCommand


if __name__ == "__main__":
	listOfShellCommands = ['sleep 3', 'sleep 5', 'sleep 2', 'sleep 1', 'sleep 4']

	nProcessInParallel = 4

	with Pool(nProcessInParallel) as p:
		print(p.map( launchShellCommand, listOfShellCommands))	
