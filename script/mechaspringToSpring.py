import numpy as np
from tqdm import tqdm
import periodicMinimalConstruct
import sys


# RETURN TRUE IF POINT v LIES IN THE SQUARE DOMAIN [0.2,0.8]x[0.2,0.8]
def admissiblePoint( v, u0, v0):
    # admissible = True
    # admissible = (v[0] > -1.5) and (v[0] < 1.5) and (v[1] > -1.5) and (v[1] < +1.5) # FOR SQUARE
    # admissible = (v[0] > -1.) and (v[0] < 2.) and (v[1] > -1.5) and (v[1] < +1.5) # FOR HEXAGON
    admissible = (v[0] > 0.5/3.) and (v[0] < 2.5/3.) and (v[1] > 0.5/3.) and (v[1] < 2.5/3.) # POUR REPETITION 3x3, 2 PERIODES
    # admissible = (v[0] > 0.5/7.) and (v[0] < 6.5/7.) and (v[1] > 0.5/7.) and (v[1] < 6.5/7.) # POUR REPETITION 7x7, 5 PERIODES
    # admissible = (v[0] > 0.2) and (v[0] < 0.8) and (v[1] > 0.2) and (v[1] < 0.8) # POUR REPETITION 3x3, 1 PERIODE
    # admissible = (v[0] > 0.1) and (v[0] < 3.8) and (v[1] > 0.1) and (v[1] < 3.8) # POUR REPETITION ???PERIODIQUE
    return admissible

# READ FROM A MECHASPRING FILE
def convertMechaspringFile( fileName: str):
    # HEXAGONE
    # u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.)])
    # v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.)]

    # REENTRANT HONEYCOMB
    # u0 = np.array([0.7,np.sin(np.pi/3.)])
    # v0 = np.array([0.7,-np.sin(np.pi/3.)])
    # u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.)])*0.5
    # v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.)])*0.5


    # MECHASPRING ET SQUARE
    u0 = np.array([1.,0.])
    v0 = np.array([0.,1.])

    # TWO/THREE PERIODS MECHASPRING ET SQUARE
    # u0 = np.array([5.,0.])
    # v0 = np.array([0.,5.])

    # CHIRAL
    # u0 = np.array([4.,0.])
    # v0 = np.array([0.,4.])

    # TRIANGLE
    # u0 = np.array([1.,0.])
    # v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])

    with open(fileName, 'r') as f:
        firstRow = f.readline()
        nVertex = int(firstRow)
        points = np.zeros((nVertex, 2))
        for i in range(nVertex):
            vertexRow = f.readline()
            allFieldsAsString = vertexRow.split()
            x = float(allFieldsAsString[0])
            y = float(allFieldsAsString[1])
            z = float(allFieldsAsString[2])
            points[i,0] = x
            points[i,1] = y
        vi=0
        # INITIALIZE EMPTY LISTS TO STORE THE LENGTH AND ANGULAR SPRINGS,
        # RESPECTIVELY AS DOUBLET AND TRIPLET OF INTEGERS
        lList = []
        aList = []
        for line in f.readlines():
            # READ ONE LINE CONTAINING THE INDICES OF THE NEIGHBORING VERTICES
            vij = [int(p)-1 for p in line.split()]
            # ADD LENGTH AND ANGULAR SPRINGS ONLY FOR THOSE VERTICES
            # WHICH STAR IS PERIODIC
            if admissiblePoint( points[vi], u0, v0):
            # if True:
                # ADD AN LENGTH SPRING ONLY IF THE FIRST VERTEX INDEX IS STRICLY LESS THAN THE SECOND
                for w in vij:
                    if vi < w:
                        lList.append([vi, w])
                # ADD ALL ANGULAR SPRINGS AROUND vi
                # IT DOESNT MATTER IF THERE ARE PERIODIC DUPLICATES,
                # SINCE THEY WILL BE REMOVED LATER ON
                aList = aList + [[vij[j],vi,vij[(j+1) % len(vij)]] for j in range(len(vij))]
            else:
                print(f"non admissible point {points[vi]}")
            vi = vi + 1
    f.close()
    # STORE POINT COORDINATES
    # CAUTION: MULTIPLICATION BY 3 REQUIRED BECAUSE OF THE MECHASPRING FORMAT WHICH ENCLOSES A 3X3 GRID BETWEEN 0. AND 1.
    np.savetxt( "points.txt", points*3.)
    # np.savetxt( "points.txt", points)
    # STORE ANGULAR SPRINGS
    np.savetxt( "2Spring.txt", np.array(lList).astype(int))
    # STORE LENGTH SPRINGS
    np.savetxt( "3Spring.txt", np.array(aList).astype(int))
    # CAUTION: MULTIPLICATION BY 3 REQUIRED BECAUSE OF THE MECHASPRING FORMAT WHICH ENCLOSES A 3X3 GRID BETWEEN 0. AND 1.

    # vert, u0, v0, lList, aList = periodicMinimalConstruct.networkFromPointEdgeAngle( u0, v0, points, lList, aList)
    vert, u0, v0, lList, aList = periodicMinimalConstruct.networkFromPointEdgeAngle( u0, v0, points*3., lList, aList)
    periodicMinimalConstruct.writeInSpringFileFormat( vert, u0, v0, lList, aList, fileName+".spg")
    periodicMinimalConstruct.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, fileName+"_indirect.spg")

    # lList = [periodicMinimalConstruct.randomTranslate2Spring( l, 4, 4) for l in lList]
    # aList = [periodicMinimalConstruct.randomTranslate3Spring( a, 4, 4) for a in aList]
    # periodicMinimalConstruct.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, fileName+"_indirect_exploded.spg")

    return

if __name__=="__main__":
    convertMechaspringFile( sys.argv[1])