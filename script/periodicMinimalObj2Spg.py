import periodicMinimalConstruct as pmC
import numpy as np
import sys

objFileName = sys.argv[1]

# u0 = np.array([1.,0.])
# v0 = np.array([0.,1.])

# u0 = np.array([1.,1.]) # FOR KIRIGAMI
# v0 = np.array([-1.,1.])

u0 = np.array([1.,0.]) # FOR 3-FOLD SYMETRY
pi3 = np.pi / 3.
v0 = np.array([np.cos(pi3), np.sin(pi3)])

vert, u0, v0, lList, aList = pmC.networkFromObjLineFile( u0, v0, objFileName)
pmC.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "outputObj.spg")
print("file outputObj.spg written")