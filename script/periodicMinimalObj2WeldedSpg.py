import periodicMinimalConstruct as pmC
import numpy as np
import sys


objFileName = sys.argv[1]

# u0 = np.array([1.,0.]) # FOR PRUNED MESHES
# v0 = np.array([0.,1.])
 
u0 = np.array([1.,0.]) # FOR DELAUNAY 3 FOLD
v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])

# u0 = np.array([1.,1.]) # FOR KIRIGAMI
# v0 = np.array([-1.,1])

# vert, u0, v0, lList, aList = networkFromObjLineFile( u0, v0, "square.obj")
vert, u0, v0, lList, aList = pmC.weldedNetworkFromObjLineFile( u0, v0, objFileName)
pmC.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "outputWelded.spg")
print("file outputWelded.spg written")