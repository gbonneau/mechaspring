import numpy as np

# MULTIPLY MATRIX M WITH ROW VECTOR V
def linearTransformation( M, v):
    return( M @ v)

# APPLY LINEAR TRANSFORMATION TO EACH ROW OF A MATRIX WITH np.apply_along_axis
def applyToEachRow():
    M = np.random.randn(2,2) # 2X2 MATRIX
    oneRow = np.random.randn(1,2) # MATRIX OF A SINGLE ROW
    tenRows = np.repeat( oneRow, 10, axis=0) # MATRIX OF TEN EQUAL ROWS
    multiplyByM = lambda x: linearTransformation( M, x) # FUNCTION MULTIPLYING ROW VECTOR x BY MATRIX M
    outputOneRow = multiplyByM( oneRow[0])
    outputTenRows = np.apply_along_axis( multiplyByM, axis=1, arr=tenRows)
    print("linear transformation applied to row vector:")
    print(outputOneRow)
    print("linear transformation applied each row vector in matrix:")
    print(outputTenRows)

if __name__=="__main__":
    applyToEachRow()
