import sys

def changeStiffnessValues( inputFilename, outputFilename, stiffnessValues):
    numberOfChangedStiffnesses = 0
    with (open(inputFilename, "r" ) as f):
        allLines = f.readlines()
        newLines=[]
        for line in allLines:
            allWords = line.split()
            if (allWords[0] == "s") and (numberOfChangedStiffnesses < len(stiffnessValues)):
                newValue = stiffnessValues[numberOfChangedStiffnesses]
                allWords[1] = newValue
                numberOfChangedStiffnesses = numberOfChangedStiffnesses+1
            newLines.append(' '.join(allWords))
        with (open(outputFilename,"w") as fOut):
            for line in newLines:
                fOut.write(f"{line}\n")

if __name__ == "__main__":
    if (len(sys.argv) < 4):
        print(f"usage: {sys.argv[0]} <networkInput.spg> <networkOutput.spg> <stiffness1> <stiffness2> ... ")
        sys.exit(1)
    inputFilename = sys.argv[1]
    outputFilename = sys.argv[2]
    stiffnessValues = sys.argv[3:]
    changeStiffnessValues( inputFilename, outputFilename, stiffnessValues)

