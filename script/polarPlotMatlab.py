# from cProfile import label
import numpy as np
import sys
import matplotlib.pyplot as plt

# GIVEN AN ARRAY OF FLOATS, RETURN TWO FLOATS DEFINING
# AN INTERVAL d% LARGER THAN THE [min,max] INTERVAL
def largerInterval( a, d):
    min = np.min(a)
    max = np.max(a)
    addedLength = (max - min) * d/100./2.
    return min-addedLength, max+addedLength

Young = np.loadtxt(sys.argv[1]+"/E_plot")
PR = np.loadtxt(sys.argv[1]+"/PR_plot")
Young_iso = np.loadtxt(sys.argv[1]+"/E")
PR_iso = np.loadtxt(sys.argv[1]+"/PR")

thetaMin = 0.0
thetaMax = 2. * np.pi 
nTraction = len(PR)
theta=np.linspace( thetaMin, thetaMax, num=nTraction, endpoint=True)
fig, ax = plt.subplots( 1,2, subplot_kw={'projection': 'polar'})
ax[0].plot(theta, Young)
ax[1].plot(theta, PR)
rmin, rmax= largerInterval( list(Young)+[.0], 10.)
ax[0].set_rmin( rmin)
ax[0].set_rmax( rmax)
rmin, rmax= largerInterval( list(PR)+[.0], 10.)
ax[1].set_rmin( rmin)
ax[1].set_rmax( rmax)

# ax.plot(theta, PR3, label="fine subdivided")
# ax.set_rmax(2.)
# ax.set_rticks([ 0.,0.5,1.,1.5])  # Less radial ticks
# ax.set_rticks([-1., -0.5, 0., 0.5, 1, 1.5, 2])  # Less radial ticks
ax[0].set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax[1].set_rlabel_position(-22.5)  # Move radial labels away from plotted line
lines, labels = plt.thetagrids(range(0, 360, 30)) # theta ticks min, max, delta
ax[0].grid(True)
ax[1].grid(True)

plt.title("from Matlab")

# ax.set_title("A line plot on a polar axis", va='bottom')
ax[0].set_title(f"Young's modulus\niso = {Young_iso:.5e}", va='bottom')
ax[1].set_title(f"Poisson's ratio\niso = {PR_iso:.5f}", va='bottom')
# ax.legend(loc="upper right")
plt.savefig( "matlabYoungPR.pdf", format="pdf")
plt.show()

