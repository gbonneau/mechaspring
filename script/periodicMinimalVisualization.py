#=====================================================================
# IMPLEMENTATION OF A STRUCTURE HANDLING PERIODIC PHYSICAL SYSTEMS
# G.P. BONNEAU, APRIL 2023, BERLIN
#
# IT IS IMPLEMENTED IN 2D BUT MAY BE TRIVIALLY EXTENDED TO 3D
#
# IT IS DEFINED BY
#
# 1) A SET OF N "UNIT CELL" POINTS vert.
#    THESE POINTS MAY BE ARBITRARY AND NEED NOT TO BE INSIDE A PARTICULAR GEOMETRIC DOMAIN
#
# 2) TWO PERIODICITY VECTORS u0 v0
#    THE ONLY CONSTRAINTS IS THAT THESE TWO VECTORS SHOULD SPAN THE PLANE (I.E. NOT BE COLINEAR)
#
# 3) THE WHOLE PLANE IS SAMPLED BY PERIODIC POINTS DEFINED BY TRIPLET OF INTEGERS [i,j,k]
#    WHERE i IS AN INTEGER INDEXING THE UNIT CELL POINTS
#    j, k ARE INTEGERS ENCODING THE TRANSLATION ALONG THE PERIODIC VECTORS u0, v0
#    THE GEOMETRIC POSITION OF PERIODIC POINT [i,j,k] IS THEREFORE vert[i] + j * u0 + k * v0
#
# 4) A SET OF PRIMITIVE PHYSICAL SYSTEMS,
#    EACH ONE DEPENDEND ON A FINITE NUMBER OF *PERIODIC* (NOT UNIT CELL) POINTS
#    IN THE CURRENT IMPLEMENTATION, LENGTH SPRINGS, NOTED l, DEPENDING ON TWO PERIODIC POINTS,
#    AND ANGULAR SPRINGS, NOTED a, DEPENDING ON THREE PERIODIC POINTS, ARE IMPLEMENTED
#    BUT IT CAN BE EASILY EXTENTED TO ANY NUMBER OF PERIODIC POINTS.
#
#    THEREFORE LENGTH SPRINGS ARE DEFINED BY TWO TRIPLET OF INTEGERS [ [i,j,k], [l,m,n] ]
#    AND ANGULAR SPRINGS ARE DEFINED BY THREE TRIPLET OF INTEGERS [ [i,j,k], [l,m,n], [p,q,r] ]
#
#
# 

import numpy as np
import sys
import io
import matplotlib.pyplot as plt
import matplotlib.colors as Colors
from matplotlib.patches import Wedge, Circle, Arrow
from matplotlib.collections import PatchCollection
from matplotlib.collections import LineCollection
import periodicMinimalConstruct
# import thibaultSpring2Spring

#---------------------------------------------------
# wedgeRadius = 0.003
# pointRadius = 0.001
wedgeRadius = 0.2
pointRadius = 0.1
fineSegmentWidth = 0.1
boldSegmentWidth = 0.2
shortedSegmentRatio = 0.1
arrowWidth = 0.02
# 'tab:purple'
# 'tab:brown'
# 'tab:olive'
gradientColor='tab:olive'
springColor = 'tab:orange'
springConstraintColor = 'tab:purple'
gridColor = 'tab:blue'
# pointColor = 'tab:green'
unitCellPointColor = 'tab:green'
periodicPointColor = 'tab:red'
wedgeColor = 'tab:blue'
wedgeConstraintColor = 'tab:purple'

#----------------------------------------------------
# wedgeRadius = 0.
# pointRadius = 0.
# fineSegmentWidth = 3.
# boldSegmentWidth = fineSegmentWidth
# shortedSegmentRatio = 0.
# springColor = 'black'


def displayGradient( v, g):
    tailPosition = periodicToAbsolute( v)
    arrow = Arrow( tailPosition[0], tailPosition[1], g[0], g[1], width = arrowWidth)
    patchGradient.append( arrow)

def displayAbsoluteUnitCellPoint( v):
    circle = Circle((v[0], v[1]), radius = pointRadius)
    patchCirclesUnitCell.append(circle)

def displayAbsolutePeriodicPoint( v):
    circle = Circle((v[0], v[1]), radius = pointRadius)
    patchCirclesPeriodic.append(circle)

def periodicToAbsolute( v):
    return vert[v[0]] + v[1] * u0 + v[2] * v0

def displayPeriodicPoint( v):
    if (v[1] == 0) and (v[2] == 0):
        displayAbsoluteUnitCellPoint( periodicToAbsolute( v))
    else:
        displayAbsolutePeriodicPoint( periodicToAbsolute( v))

def displaySegment( width, v0, v1, color): # DISPLAY SEGMENT FROM POINT V0 TO POINT V1
    allSegmentsAtt.append([[(v0[0], v0[1]), (v1[0],v1[1])], width, color])
    # plt.plot( [v0[0], v1[0]], [v0[1],v1[1]], c=color,linewidth=width)

def displayShortedSegment( d, segmentWidth, v0, v1, color):
    v2 = (1.-d)*v0 + d*v1
    v3 = d*v0+(1.-d)*v1
    displaySegment( segmentWidth, v2, v3, color)

def display2Spring( l):
    v0 = periodicToAbsolute( l[0])
    v1 = periodicToAbsolute( l[1])
    displayPeriodicPoint( l[0])
    displayPeriodicPoint( l[1])
    stiffnessIndex = 3-l[2]
    if l[2] == -1:
        colorToUse = springConstraintColor
    else:
        colorToUse = springColor
    displaySegment( fineSegmentWidth * stiffnessIndex+0.1, v0, v1, colorToUse)
    displayShortedSegment( shortedSegmentRatio, boldSegmentWidth * stiffnessIndex + 0.1, v0, v1, colorToUse)

def display3Spring( a):
    v0 = periodicToAbsolute( a[0])
    v1 = periodicToAbsolute( a[1])
    v2 = periodicToAbsolute( a[2])
    displayPeriodicPoint( a[0])
    displayPeriodicPoint( a[1])
    displayPeriodicPoint( a[2])
    u0 = v0 - v1
    u1 = v2 - v1
    theta0 = np.arctan2( u0[1], u0[0]) * 180 / np.pi
    theta1 = np.arctan2( u1[1], u1[0]) * 180 / np.pi
    wedge = Wedge((v1[0], v1[1]), wedgeRadius, theta0, theta1)
    # AN INDEX a[3] OF -1 ENCODES A HARD ANGLE CONSTRAINT, STORE THE WEDGE IN A DIFFERENT LIST TO LATER MODIFY ITS APPEARANCE
    if a[3] != -1:
        patchWedges.append(wedge)
    else:
        patchWedgesConstraint.append(wedge)

def displaySpringList( lList, aList):
    for l in lList:
        display2Spring(l)
    for a in aList:
        display3Spring(a)

def displayPeriodicGrid( width):
    for i in width:
        for j in width:
            displayCellParallelogram( i, j)

def displayCellParallelogram( i, j):
    center = u0/2.+v0/2.
    displaySegment( 0.9, i*u0+j*v0-center,(i+1)*u0
    +j*v0-center, gridColor)
    displaySegment( 0.9, i*u0+j*v0-center, i*u0+(j+1)*v0-center, gridColor)
    displaySegment( 0.9, (i+1)*u0+j*v0-center, (i+1)*u0+(j+1)*v0-center, gridColor)
    displaySegment( 0.9, i*u0+(j+1)*v0-center, (i+1)*u0+(j+1)*v0-center, gridColor)

def initPeriodicVisualization( vertLocal, u0Local, v0Local, lList):
    fig, ax = plt.subplots(1,1, figsize=(5,5))
    ax.axis('equal')
    initPeriodicVisualizationInFigure( fig, ax, vertLocal, u0Local, v0Local, lList)

def initPeriodicVisualizationInFigure( figLocal, axLocal, vertLocal, u0Local, v0Local, lList):
    global wedgeRadius, pointRadius
    global fig, ax
    global vert, u0, v0
    global allSegmentsAtt, patchWedges, patchWedgesConstraint, patchCirclesUnitCell, patchCirclesPeriodic, patchGradient, patchPeriodicGradient

    fig = figLocal
    ax = axLocal

    allSegmentsAtt = []
    patchWedges = []
    patchWedgesConstraint = []
    patchCirclesUnitCell = []
    patchCirclesPeriodic = []
    patchGradient = []
    patchPeriodicGradient = []

    vert = vertLocal
    u0 = u0Local
    v0 = v0Local

    lengthMin, lengthMax, lengthMean, lengthStd = periodicMinimalConstruct.lengthSpringStatistic( vertLocal, u0Local, v0Local, lList)
    scale = lengthMin
    wedgeRadius = wedgeRadius * scale
    pointRadius = pointRadius * scale

    # lengthStatistics = periodicMinimalConstruct.lengthSpringStatistic( vertLocal, u0Local, v0Local,)

def showFigure(title=None, display=True, save=False):
    p = PatchCollection(patchWedges, alpha=0.4, color=wedgeColor)
    ax.add_collection(p)
    p = PatchCollection(patchWedgesConstraint, alpha=0.4, color=wedgeConstraintColor)
    ax.add_collection(p)
    p = PatchCollection(patchCirclesUnitCell, alpha=0.4,color=unitCellPointColor)
    ax.add_collection(p)
    p = PatchCollection(patchCirclesPeriodic, alpha=0.4,color=periodicPointColor)
    ax.add_collection(p)
    p = PatchCollection(patchGradient, color=gradientColor )
    ax.add_collection(p)
    # FOR EACH l IN allLines l = [l[0], l[1], l[2]] WHERE l[0] IS A LIST OF TWO 2-TUPLES, l[1] IS A WIDTH, l[2] IS A COLOR
    allSegmentsVert = [l[0] for l in allSegmentsAtt] # EXTRACT VERTICES COORDINATES l
    allColors = [Colors.to_rgb(l[2]) for l in allSegmentsAtt] # EXTRACT COLORS
    allWidths = [l[1] for l in allSegmentsAtt] # EXTRACT WIDTH
    # COMPUTING BOUNDING BOX
    allXCoord = [l[0][0] for l in allSegmentsVert] + [l[1][0] for l in allSegmentsVert]
    allYCoord = [l[0][1] for l in allSegmentsVert] + [l[1][1] for l in allSegmentsVert]
    xMin = min(allXCoord)
    xMax = max(allXCoord)
    yMin = min(allYCoord)
    yMax = max(allYCoord)
    scale = 0.5 * 1.1
    xExt = xMax - xMin
    yExt = yMax - yMin
    xMid = 0.5 * (xMin + xMax)
    yMid = 0.5 * (yMin + yMax)
    xMin = xMid - xExt * scale
    xMax = xMid + xExt * scale
    yMin = yMid - yExt * scale
    yMax = yMid + yExt * scale
    plt.xlim( xMin, xMax)
    plt.ylim( yMin, yMax)
    p = LineCollection( allSegmentsVert, color = allColors, linewidths = allWidths)
    ax.add_collection(p)
    if title != None:
        plt.title( title)
    if save:
        plt.savefig( title+".pdf")
        plt.savefig( title+".png")
    if display:
        plt.show()
    plt.close()

def main():
    filename = sys.argv[1]
    nRepeat = int(sys.argv[2])

    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.squareNetwork()
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.triangleNetwork()
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.hexagonalNetwork()
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.irregularNetwork()
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.chelouNetwork()
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.networkFromPointEdgeAngleFiles(np.array([1.,0.]), np.array([0.,1.]))
    # vert, u0, v0, lUnitList, aUnitList = periodicMinimalConstruct.networkFromSpringFile( sys.argv[1])
    # vert, u0, v0, lUnitList, aUnitList = thibaultSpring2Spring.networkFromThibaultSpring( sys.argv[1])
    vert, u0, v0, lUnitList, aUnitList, sList = periodicMinimalConstruct.networkFromIndirectSpringFile( filename)

    # lList0, aList0 = filterSpringByVertex( [0,0,0], lUnitList, aUnitList)
    # lList1, aList1 = filterSpringByVertex( [1,0,0], lUnitList, aUnitList)
    # lList2, aList2 = filterSpringByVertex( [2,0,0], lUnitList, aUnitList)
    # lListRedundant = lList0 + lList1 + lList2
    # aListRedundant = aList0 + aList1 + aList2
    # # lList = lListRedundant
    # # aList = aListRedundant
    # lList = suppressRedundant2Spring( lListRedundant)
    # aList = suppressRedundant3Spring( aListRedundant)
    # lListT = [periodicMinimalConstruct.randomTranslate2Spring( l, 5, 5) for l in lList]
    # aListT = [periodicMinimalConstruct.randomTranslate3Spring( a, 5, 5) for a in aList]
    # # lList = lListT
    # # aList = aListT

    # print(sorted(lList) == sorted(lUnitList))
    # print(sorted(aList) == sorted(aUnitList))

    my_dpi= 192
    # STEFANIE'S NETWORK [0,1]X[0,1]    
    aspectRatio = 1.
    # REGULAR HEXAGON OR REENTRANT
    # aspectRatio = np.sqrt(3.)/3.
    # fig = plt.figure(figsize=(3.93701, 3.93701*aspectRatio), dpi=my_dpi)
    # ax = fig.add_axes([0.,0.,1.,1.])
    # ax.set_axis_off()

    fig, ax = plt.subplots(1,1)
    ax.axis('equal')

    # fig,ax = plt.subplots(frameon=False)

    # width = range(-4,5)
    # width = range(-2,3)
    # width = range(-1,2)
    # width = range(1)
    width = range( -(nRepeat-1), nRepeat)
    lList, aList = periodicMinimalConstruct.springInBox( width, lUnitList, aUnitList)

    remapOrtho = False
    if (remapOrtho):
        aspectRatio = np.linalg.norm( v0) / np.linalg.norm( u0)
        u1 = np.array([1.,0.])
        v1 = np.array([0.,1.]) * aspectRatio
        vert = periodicMinimalConstruct.changePeriodicityVectors( vert, u0, v0, u1, v1)
        u0 = u1
        v0 = v1
    else:
        aspectRatio = 1.0

    initPeriodicVisualizationInFigure( fig, ax, vert, u0, v0, lUnitList)

    # STEFANIE'S NETWORK [0,1]X[0,1]    
    # ax.set_xlim(0.,1.)
    # ax.set_ylim(0.,1.)

    # REGULAR HEXAGON IMAGE FOR MATLAB    
    # ax.set_xlim(-3.+0.25,3.+0.25)
    # ax.set_ylim(-2.*np.sqrt(3.)/2.+0.25,2.*np.sqrt(3.)/2.+0.25)

    # REENTRANT HONEYCOMB IMAGE FOR MATLAB
    # ax.set_xlim(-3.+0.4,3.+0.4)
    # ax.set_ylim(-2.*np.sqrt(3.)/2.+0.25,2.*np.sqrt(3.)/2.+0.25)

    displayPeriodicGrid( width)

    # lList = [periodicMinimalConstruct.randomTranslate2Spring( l, 4, 4) for l in lList]
    # aList = [periodicMinimalConstruct.randomTranslate3Spring( a, 4, 4) for a in aList]

    displaySpringList( lList, aList)
    # displaySpringList( [], aList)
    # displaySpringList( lListUnitEdge, [])
    # print(lList)

    # periodicPointsInvolved = periodicMinimalConstruct.allPeriodicPointInSpringList( lList, aList)
    # unitCellPointsInvolved = periodicMinimalConstruct.allUnitCellPointInSpringList( lList, aList)

    showFigure( title=filename)


if __name__=="__main__":
    main()

