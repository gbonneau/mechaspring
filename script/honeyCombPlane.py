import sys
import numpy as np

def Indice2DVers1D( i, j, nHorizontalReseau):
	return j * (2*nHorizontalReseau+1) + i + 1 # !!!!!! ATTENTION !!!!!! +1 POUR ETRE COMPATIBLE AVEC MATLAB!!!!!

def FFD( xIn, yIn):
	xOut = xIn
	yOut = yIn
	return np.array([xOut, yOut])

def auxeticNetwork( nHorizontalReseau = 5, nVerticalReseau = 4, LargeurReseau = 10., HauteurReseau = -8., LambdaReseau = 4.):
	Xorigine = -5.
	Yorigine = 0.

	GrandeHauteur = HauteurReseau / ( ((nVerticalReseau-1) * (1. + 1./LambdaReseau) + 1.) )
	PetiteHauteur = GrandeHauteur / LambdaReseau
	DeltaLargeur = LargeurReseau / (2. * nHorizontalReseau)

	nSommet = (2*nHorizontalReseau+1)*(2*nVerticalReseau)
	MesSommets = np.zeros((nSommet,2))
	nSegmentADessiner = 2 * ((2 * nHorizontalReseau * 2 * nVerticalReseau)
				+ (nVerticalReseau - 1) * (2 * nHorizontalReseau + 1) + nHorizontalReseau + 1)

	# print("DeltaLargeur = %lf\n", DeltaLargeur)
	# print("nHorizontalReseau, nVerticalReseau = %d, %d\n", nHorizontalReseau, nVerticalReseau)
	# print("GrandeHauteur = %lf, PetiteHauteur = %lf, Lambda = %lf\n", GrandeHauteur, PetiteHauteur, LambdaReseau)
	# print("on doit avoir %lf = %lf\n", nVerticalReseau*(GrandeHauteur+PetiteHauteur)+GrandeHauteur, HauteurReseau)

	Yc = Yorigine
	for j in range(nVerticalReseau):
		Xc = Xorigine
		for i in range(nHorizontalReseau+1):
			MesSommets[Indice2DVers1D( 2*i, 2*j, nHorizontalReseau)-1, :] = FFD(Xc, Yc)
			# MesSommets[ %lf %lf\n", MesSommets[Indice2DVers1D( 2*i, 2*j)].x,MesSommets[Indice2DVers1D( 2*i, 2*j)].y, MesSommets[Indice2DVers1D( 2*i, 2*j)].z );
			if (i!=nHorizontalReseau):
				MesSommets[Indice2DVers1D( 2*i+1, 2*j, nHorizontalReseau)-1,:] = FFD(Xc+DeltaLargeur,Yc + GrandeHauteur/2. - PetiteHauteur/2.)
			MesSommets[Indice2DVers1D( 2*i, 2*j+1, nHorizontalReseau)-1,:] = FFD(Xc,Yc+GrandeHauteur)
			# MesSomm"%lf %lf %lf\n", MesSommets[Indice2DVers1D( 2*i, 2*j+1)].x,MesSommets[Indice2DVers1D( 2*i, 2*j+1)].y, MesSommets[Indice2DVers1D( 2*i, 2*j)].z );
			if (i!=nHorizontalReseau):
				MesSommets[Indice2DVers1D( 2*i+1, 2*j+1, nHorizontalReseau)-1,:] = FFD(Xc+DeltaLargeur,Yc + GrandeHauteur/2. + PetiteHauteur/2.)

			Xc += 2. * DeltaLargeur
		
		Yc = Yc + GrandeHauteur + PetiteHauteur

	print( nSommet)
	for i in range(nSommet):
		print( MesSommets[i,0], MesSommets[i,1], 0.)
#	np.savetxt("auxeticNetwork.txt", MesSommets, fmt="%10.5lf")

	# PREMIERE LIGNE, j=0
	print( Indice2DVers1D(0,1, nHorizontalReseau),Indice2DVers1D(1,0, nHorizontalReseau))
	for i in range(1,nHorizontalReseau+1):
		print( Indice2DVers1D(2*i, 0, nHorizontalReseau), Indice2DVers1D( 2*i-2, 0, nHorizontalReseau))
		if (i != nHorizontalReseau):
			print( Indice2DVers1D(2*i-1,0, nHorizontalReseau), Indice2DVers1D(2*i,1, nHorizontalReseau), Indice2DVers1D(2*i+1,0, nHorizontalReseau))
		else:
			print( Indice2DVers1D(2*i-1,0, nHorizontalReseau), Indice2DVers1D(2*i,1, nHorizontalReseau))

	# LIGNES INTERIEURES 2j-1 2j
	for j in range( 1, nVerticalReseau):
		# ligne 2j-1
		print( Indice2DVers1D(1, 2*j-1, nHorizontalReseau),Indice2DVers1D(0,2*j-2, nHorizontalReseau))
		for i in range(0,nHorizontalReseau):
			print( Indice2DVers1D(2*i, 2*j-1, nHorizontalReseau), Indice2DVers1D(2*i+1, 2*j, nHorizontalReseau), Indice2DVers1D(2*i+2, 2*j-1, nHorizontalReseau))
			if (i != nHorizontalReseau-1):
				print( Indice2DVers1D(2*i+3, 2*j-1, nHorizontalReseau), Indice2DVers1D(2*i+2, 2*j-2, nHorizontalReseau), Indice2DVers1D(2*i+1, 2*j-1, nHorizontalReseau))
			else:
				print( Indice2DVers1D(2*i+2, 2*j-2, nHorizontalReseau), Indice2DVers1D(2*i+1, 2*j-1, nHorizontalReseau))
		# ligne 2j
		print( Indice2DVers1D(0, 2*j+1, nHorizontalReseau),Indice2DVers1D(1,2*j, nHorizontalReseau))
		for i in range(0,nHorizontalReseau):
			print( Indice2DVers1D(2*i+2, 2*j, nHorizontalReseau), Indice2DVers1D(2*i+1, 2*j-1, nHorizontalReseau), Indice2DVers1D(2*i, 2*j, nHorizontalReseau))
			if (i != nHorizontalReseau-1):
				print( Indice2DVers1D(2*i+1, 2*j, nHorizontalReseau), Indice2DVers1D(2*i+2, 2*j+1, nHorizontalReseau), Indice2DVers1D(2*i+3, 2*j, nHorizontalReseau))
			else:
				print( Indice2DVers1D(2*i+1, 2*j, nHorizontalReseau), Indice2DVers1D(2*i+2, 2*j+1, nHorizontalReseau))

	# DERNIERE LIGNE
	jLast = 2*nVerticalReseau-1
	print( Indice2DVers1D(1,jLast, nHorizontalReseau),Indice2DVers1D(0,jLast-1, nHorizontalReseau))
	for i in range(1,nHorizontalReseau+1):
		print( Indice2DVers1D(2*i, jLast, nHorizontalReseau), Indice2DVers1D( 2*i-2, jLast, nHorizontalReseau))
		if (i != nHorizontalReseau):
			print( Indice2DVers1D(2*i+1,jLast, nHorizontalReseau), Indice2DVers1D(2*i,jLast-1, nHorizontalReseau), Indice2DVers1D(2*i-1,jLast, nHorizontalReseau))
		else:
			print( Indice2DVers1D(2*i,jLast-1, nHorizontalReseau), Indice2DVers1D(2*i-1,jLast, nHorizontalReseau))

	# CONTRAINTES
	print("boundary conditions")
	print(2*nVerticalReseau)
	for j in range(2*nVerticalReseau):
		print( Indice2DVers1D( 0, j, nHorizontalReseau), end =" ")
	print("")
	print(2*nVerticalReseau)
	for j in range(2*nVerticalReseau):
		print( Indice2DVers1D( 2*nHorizontalReseau, j, nHorizontalReseau), end =" ")



if __name__ == "__main__":
	if (len(sys.argv) != 3):
		print("Usage: " + sys.argv[0] + " ")
		sys.exit(1)

	nHorizontalReseau = int (sys.argv[1])
	nVerticalReseau =  int (sys.argv[2])
	LargeurReseau = 10.0	
	HauteurReseau = -8.0
	LambdaReseau = 20
	auxeticNetwork( nHorizontalReseau, nVerticalReseau, LargeurReseau, HauteurReseau, LambdaReseau)



