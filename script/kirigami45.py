import sys

global nVertices

def vertexValid( v):
    # print("nVertices = {}".format(nVertices))
    return ((v>=0) and (v<nVertices))

def printPoint( x, y):
    print("v {:7.5} {:7.5} 0.".format( x, y))
#    print( x, y, 0.)
#    print( x, y)

def printCSpring2Cells( vi, offi, vj, offj, k):
    # print("je vais afficher si possible")
    if (vertexValid(vi+offi) & vertexValid(vj+offj)):
        print("l {} {}".format( vi+offi+1, vj+offj+1))
    # print("... fini")

def printCSpringSameCell( vi, vj, off, k):
    printCSpring2Cells( vi, off, vj, off, k)


def printCell( xC, yC, a, b):
    L = [ xC-b, yC+b, \
          xC-a, yC-a, \
          xC+b, yC-b, \
          xC+a, yC+a, \
          xC+a, yC-a-2*b, \
          xC+a+2*b, yC-a, \
          xC+2*a+b, yC+b, \
          xC+b, yC+2*a+b ]
    [printPoint( L[2*i], L[2*i+1]) for i in range(len(L)//2)]

def kirigami45( nHorizontal = 2, nVertical = 2, a = 0.2, b = 0.8):
    yL = 0.
    for j in range(nVertical):
        xC = 0.
        yC = yL
        for i in range(nHorizontal):
            printCell( xC, yC, a, b)
            printCell( xC+2*a+2*b, yC-2*a-2*b, a, b)
            xC = xC+4*a+4*b
        yL = yL-4*a-4*b
    # ------ INTRA CELL CONNECTIVITY -----
    off = 0
    for j in range(nVertical):
        for i in range(nHorizontal*2):
            printCSpringSameCell( 0, 1, off, 1.)
            printCSpringSameCell( 1, 2, off, 1.)
            printCSpringSameCell( 2, 3, off, 1.)
            printCSpringSameCell( 3, 0, off, 1.)
            printCSpringSameCell( 5, 2, off, 1.)
            printCSpringSameCell( 2, 4, off, 1.)
            printCSpringSameCell( 7, 3, off, 1.)
            printCSpringSameCell( 3, 6, off, 1.)
            printCSpringSameCell( 6, 5, off, 1.)
            off = off + 8
    

if __name__ == "__main__":
    if (len(sys.argv) != 3):
        print("Usage: " + sys.argv[0] + " <nHorizontal> <nVertical>")
        sys.exit(1)

    nHorizontal = int(sys.argv[1])
    nVertical = int(sys.argv[2])

    nPCell = 8
    nVertices = 2*nHorizontal*nVertical*nPCell

    # kirigami45( nHorizontal = nHorizontal, nVertical = nVertical, a=0.2, b=0.8)
    kirigami45( nHorizontal = nHorizontal, nVertical = nVertical, a=0.3, b=0.7)