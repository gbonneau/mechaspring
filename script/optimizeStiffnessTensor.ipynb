{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook a method is presented to compute a $4^{\\text{th}}$ order elasticity tensor $\\mathbb{C}$ from a set of $N$ $2^{\\text{nd}}$ order strain tensors $\\epsilon^i$ and stress tensors $\\sigma^i$, such that $ \\forall i, \\sigma^i \\simeq \\mathbb{C} : \\epsilon^i$. <br><br><br>\n",
    "The Voigt and Bechterew notations for encoding $4^{\\text{th}}$ order symmetric tensors are introduced. It is illustrated that the Bechterew notation preserves the Frobenius norm, while the Voigt notation does not. <br>\n",
    "The elasticity tensor is then computed as the solution of a least-square minimization problem expressed in the Bechterew notation. <br>\n",
    "For simplicity the case of 2D deformation is studied."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A $2^{\\text{nd}}$ order symmetric tensor $T$ in 2D is uniquely defined by $3$ scalars $T_{11},T_{12},T_{22}$ by:<br>\n",
    "$$ T = \\begin{bmatrix}\n",
    "T_{11} & T_{12} \\\\\n",
    "T_{12} & T_{22} \n",
    "\\end{bmatrix}$$\n",
    "The $2^4=16$ coefficients of a $4^{\\text{th}}$ order elasticity tensor $\\mathbb{C}=(c_{ijkl})_{i,j,k,l \\in \\{1,2\\}}$ in 2D are uniquely defined by $6$ scalars $c_{1111}, c_{2222}, c_{1212}, c_{1122}, c_{2212}, c_{1112}$, given the minor symmetries $c_{ijkl} = c_{jikl}$ , $c_{ijkl}=c_{ijlk}$ and the major symmetries $c_{ijkl}=c_{klij}$\n",
    "<br>\n",
    "Hook's law gives the stress tensor from the elasticity and strain tensors:\n",
    "$$ \\sigma = \\mathbb{C} : \\epsilon$$\n",
    "The \":\" operator is the double contraction operator between a $4^{\\text{th}}$ and a $2^{\\text{nd}}$ order tensor. Following the Einstein summation convention:\n",
    "$$ \\sigma_{ij} = c_{ijkl} \\epsilon_{kl}$$\n",
    "The Voigt notation consists in representing Hooke's law as follows:\n",
    "$$\n",
    "\\begin{bmatrix} \\sigma_{11} \\\\ \\sigma_{22} \\\\ \\sigma_{12} \\end{bmatrix} =\n",
    "\\begin{bmatrix} c_{1111} & c_{1122} & c_{1112} \\\\\n",
    "                c_{1122} & c_{2222} & c_{2212} \\\\\n",
    "                c_{1112} & c_{2212} & c_{1212} \\end{bmatrix}\n",
    "\\begin{bmatrix} \\epsilon_{11} \\\\ \\epsilon_{22} \\\\ 2 \\epsilon_{12} \\end{bmatrix}\n",
    "$$\n",
    "Note the multiplication by $2$ of $\\epsilon_{12}$, which implies a dissymmetry between the writing of the stress tensor on the left and the strain tensor on the right.\n",
    "In the Bechterew notation Hooke's law is represented in a more symmetric manner:\n",
    "$$\n",
    "\\begin{bmatrix} \\sigma_{11} \\\\ \\sigma_{22} \\\\ \\sqrt{2}\\ \\sigma_{12} \\end{bmatrix} =\n",
    "\\begin{bmatrix} c_{1111} & c_{1122} & \\sqrt{2}\\ c_{1112} \\\\\n",
    "                c_{1122} & c_{2222} & \\sqrt{2}\\ c_{2212} \\\\\n",
    "                \\sqrt{2}\\ c_{1112} & \\sqrt{2}\\ c_{2212} &  2\\ c_{1212} \\end{bmatrix}\n",
    "\\begin{bmatrix} \\epsilon_{11} \\\\ \\epsilon_{22} \\\\ \\sqrt{2}\\ \\epsilon_{12} \\end{bmatrix}\n",
    "$$\n",
    "We introduce the following notations:\n",
    "$$ B( c_{1111}, c_{2222}, c_{1212}, c_{1122}, c_{2212}, c_{1112})=\n",
    "\\begin{bmatrix} c_{1111} & c_{1122} & \\sqrt{2}\\ c_{1112} \\\\\n",
    "                c_{1122} & c_{2222} & \\sqrt{2}\\ c_{2212} \\\\\n",
    "                \\sqrt{2}\\ c_{1112} & \\sqrt{2}\\ c_{2212} &  2\\ c_{1212} \\end{bmatrix}\n",
    "$$\n",
    "$$ \\bar\\sigma = \\begin{bmatrix} \\sigma_{11} \\\\ \\sigma_{22} \\\\ \\sqrt{2}\\ \\sigma_{12} \\end{bmatrix} $$\n",
    "and accordingly\n",
    "$$ \\bar\\epsilon = \\begin{bmatrix} \\epsilon_{11} \\\\ \\epsilon_{22} \\\\ \\sqrt{2}\\ \\epsilon_{12} \\end{bmatrix}\\ ,$$\n",
    "So that Hooke's law is now expressed as:\n",
    "$$ \\bar\\sigma = B \\bar \\epsilon$$\n",
    "Importantly, the Bechterew notation preserves the Frobenius norm in the following sense:\n",
    "$$ {\\left\\lVert \\bar\\sigma \\right\\rVert}_F = {\\lVert \\sigma \\rVert}_F\n",
    "$$\n",
    "The same holds for the strain tensor $\\epsilon$.<br>\n",
    "The Frobenius norm of the elasticity tensor is also preserved by the Bechterew notation:\n",
    "$${\\left\\lVert B \\right\\rVert}_F =\n",
    "                {\\lVert \\mathbb{C} \\rVert}_F\n",
    "$$\n",
    "The preservation of the Frobenius norm with the Bechterew notation allows to express our initial optimization problem as follows. We seek to find the six scalars $b_k, k=1,\\cdots,6$ such that \n",
    "$$\\Phi(b_k) \\longrightarrow \\text{min}\\ ,$$\n",
    "where $\\Phi$ is the following quadratic positive definite function:\n",
    "$$\\Phi(b_k) = \\sum_{i=1}^{N}\n",
    "{\\left\\lVert \n",
    "\\bar\\sigma^i - B(b_k) \\bar\\epsilon^i\n",
    "\\right\\rVert}^2_F\n",
    "$$\n",
    "This is a simple linear least-square problem. To express its solution we introduce the six 3x3 matrices $B_k, k=1,\\cdots,6$\n",
    "$$\n",
    "\\begin{matrix}\n",
    "B_1 = \\begin{pmatrix} 1 & 0 & 0 \\\\\n",
    "                      0 & 0 & 0 \\\\\n",
    "                      0 & 0 & 0 \\end{pmatrix} &\n",
    "B_2 = \\begin{pmatrix} 0 & 0 & 0 \\\\\n",
    "                      0 & 1 & 0 \\\\\n",
    "                      0 & 0 & 0 \\end{pmatrix} &\n",
    "B_3 = \\begin{pmatrix} 0 & 0 & 0 \\\\\n",
    "                      0 & 0 & 0 \\\\\n",
    "                      0 & 0 & 2 \\end{pmatrix} \\\\\n",
    "B_4 = \\begin{pmatrix} 0 & 1 & 0 \\\\\n",
    "                      1 & 0 & 0 \\\\\n",
    "                      0 & 0 & 0 \\end{pmatrix} &\n",
    "B_5 = \\begin{pmatrix} 0 & 0 & 0 \\\\\n",
    "                      0 & 0 & \\sqrt{2} \\\\\n",
    "                      0 & \\sqrt{2} &  0\\end{pmatrix} &\n",
    "B_6 = \\begin{pmatrix} 0 & 0 & \\sqrt{2} \\\\\n",
    "                      0 & 0 &  0\\\\\n",
    "                      \\sqrt{2} & 0 & 0\\end{pmatrix}\n",
    "\\end{matrix}\n",
    "$$\n",
    "The matrix $B$ expressing the elasticity tensor using the Bechterew notation is therefore equal to:\n",
    "$$ B(b_1,\\cdots,b_6) = \\sum_{k=1}^{6} b_k B_k $$\n",
    "Straightforward calculus shows that:\n",
    "$$ {\\partial \\Phi \\over \\partial b_k} = -2 r_k + 2 \\sum_{l=1}^6 m_{kl} b_l\\ ,$$\n",
    "where\n",
    "$$ r_k = \\sum_{i=1}^N (\\bar\\sigma^i)^T B_k \\bar\\epsilon^i$$\n",
    "and\n",
    "$$ m_{kl} = \\sum_{i=1}^N (\\bar\\epsilon^i)^T B_l^T B_k \\bar\\epsilon^i $$\n",
    "Therefore the six scalars $b = (b_1,\\cdots b_6)^T$ solution of the linear least-square problem are computed by solving the following 6x6 linear system:\n",
    "$$ M b = r\\ ,$$\n",
    "where $M=(m_{kl})$ and  $r = (r_k)$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following we implement our optimization problem, and test it with randomly generated data. <br>\n",
    "The only required python module is numpy. We also introduce constants used throughout the code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "sq2 = np.sqrt(2)\n",
    "dim3 = 3\n",
    "dim6 = 6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function takes as input a 6D vector containing $c_{1111}, c_{2222}, c_{1212}, c_{1122}, c_{2212}, c_{1112}$ and outputs the 6x6 matrix $B$ encoded the elasticity tensor in Bechterew notation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "def elasticityBechterewMatrixFromVector( Cvect):\n",
    "    Cmat = np.array( [ [ Cvect[0], Cvect[3], sq2 * Cvect[5] ], \\\n",
    "                       [ Cvect[3], Cvect[1], sq2 * Cvect[4] ], \\\n",
    "                       [ sq2 * Cvect[5], sq2 * Cvect[4], 2 * Cvect[2] ] ])\n",
    "    return Cmat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Analogous function in Voigt notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [],
   "source": [
    "def elasticityVoigtMatrixFromVector( Cvect):\n",
    "    Cmat = np.array( [ [ Cvect[0], Cvect[3], Cvect[5] ], \\\n",
    "                       [ Cvect[3], Cvect[1], Cvect[4] ], \\\n",
    "                       [ Cvect[5], Cvect[4], Cvect[2] ] ])\n",
    "    return Cmat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We initialize a random elasticity tensor, $N=5$ random strain tensors and compute the corresponding $N$ stress tensors. Then we perturb the strain and stress tensors. This data will be used to test the ability of our algorithm to \"recover\" the elasticity tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [],
   "source": [
    "def randomStrainAndStressTensorFromElasticityTensor( N, C):\n",
    "    Sig = np.zeros((N,dim3))\n",
    "    Eps = np.zeros((N,dim3))\n",
    "    Cmat = elasticityVoigtMatrixFromVector( C)\n",
    "    for i in range(N):\n",
    "        Eps[i,:] = np.random.rand(dim3)\n",
    "        Eps[i,2] = Eps[i,2] * 2 # MULTIPLY BY 2 BEFORE MULTIPLYING IN VOIGT BASIS\n",
    "        Sig[i,:] = Cmat @ Eps[i,:]\n",
    "        Eps[i,2] = Eps[i,2] / 2\n",
    "    return Eps, Sig\n",
    "\n",
    "def perturbData( data, Epsilon):\n",
    "    data = data + Epsilon * np.random.rand(*data.shape) # *data.shape IS A PYTHON COMMAND TO \"UNPACK\" THE T-UPLE data.shape INTO MULTIPLE INTEGERS\n",
    "    return data\n",
    "\n",
    "def initializeDataForTesting( N):\n",
    "    # np.random.seed(123)\n",
    "    Ctesting = np.random.rand(dim6) # INITIALIZING A RANDOM ELASTICTY TENSOR FOR TESTING\n",
    "    # GENERATE N STRAIN AND STRESS TENSOR FROM THE ELASTICITY TENSOR, FOR TESTING\n",
    "    Eps, Sig = randomStrainAndStressTensorFromElasticityTensor( N, Ctesting)\n",
    "    return Ctesting, Eps, Sig\n",
    "\n",
    "N = 5\n",
    "Ctesting, Eps, Sig = initializeDataForTesting( N)\n",
    "Eps = perturbData( Eps, 1e-3)\n",
    "Sig = perturbData( Sig, 1e-3)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function returns the six 3x3 matrices $B_k$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [],
   "source": [
    "def bechterew2ndOrder2D():\n",
    "    B=np.zeros((6,3,3))\n",
    "    B[0,0,0] = 1\n",
    "    B[1,1,1] = 1\n",
    "    B[2,2,2] = 2\n",
    "    B[3,0,1] = 1\n",
    "    B[3,1,0] = B[3,0,1]\n",
    "    B[4,1,2] = np.sqrt(2)\n",
    "    B[4,2,1] = B[4,1,2]\n",
    "    B[5,0,2] = np.sqrt(2)\n",
    "    B[5,2,0] = B[5,0,2]\n",
    "    return B"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next function will be used to compute the products $B_k B_l$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [],
   "source": [
    "def matrixFamilyProduct( M, i, j):\n",
    "    return M[i,:,:] @ M[j,:,:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will also need two utility functions multiplying or dividing by $\\sqrt{2}$ the 3rd coordinate of an array of 3D vectors. It will be useful to map the 3d vectors encoding the strains and stresses following the Bechterew notation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [],
   "source": [
    "def convert2ndOrderTensorVoigtToBechterew( T):\n",
    "    for i in range(T.shape[0]):\n",
    "        T[i,2] = T[i,2] * sq2\n",
    "\n",
    "def convert2ndOrderTensorBechterewToVoigt( T):\n",
    "    for i in range(T.shape[0]):\n",
    "        T[i,2] = T[i,2] / sq2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next follows the main function which computes the matrix $M$ and the vector $r$, and solves the linear system $M b = r$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [],
   "source": [
    "def elasticityTensorFromStrainAndStressTensors( Eps, Sig):\n",
    "\n",
    "    N = Eps.shape[0]\n",
    "    assert( N == Sig.shape[0])\n",
    "\n",
    "    betch = bechterew2ndOrder2D()\n",
    "    # print(betch)\n",
    "    betchProduct = np.zeros((dim6,dim6,dim3,dim3))\n",
    "    mat = np.zeros((dim6,dim6))\n",
    "    rhs = np.zeros(dim6)\n",
    "\n",
    "    # MULTIPLY LAST COORDINATE OF ELONGATION AND STRAIN TENSOR BY SQRT(2) TO MAP TO Bechterew BASIS\n",
    "    convert2ndOrderTensorVoigtToBechterew( Eps)\n",
    "    convert2ndOrderTensorVoigtToBechterew( Sig)\n",
    "\n",
    "    for k in range(dim6):\n",
    "        for l in range(dim6):\n",
    "            betchProduct[k,l,:,:] = matrixFamilyProduct( betch, k, l)\n",
    "            for i in range(N):\n",
    "                mat[k,l] = mat[k,l] + np.transpose(Eps[i,:]) @ betchProduct[k,l] @ Eps[i,:]\n",
    "\n",
    "    for k in range(dim6):\n",
    "        for i in range(N):\n",
    "            rhs[k] = rhs[k] + np.transpose( Sig[i,:]) @ betch[k,:,:] @ Eps[i,:]\n",
    "\n",
    "    convert2ndOrderTensorBechterewToVoigt( Eps)\n",
    "    convert2ndOrderTensorBechterewToVoigt( Sig)\n",
    "\n",
    "    print(f\"RANK OF FITTING MATRIX = {np.linalg.matrix_rank( mat, hermitian=True)}\")\n",
    "    Crecovered = np.linalg.solve( mat, rhs)\n",
    "    return Crecovered"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We apply our algorithm to \"recover\" the elasticity tensor from the strains and stresses"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "RANK OF FITTING MATRIX = 6\n",
      "coefficients of the original tensor = [0.59150487 0.96049974 0.67639483 0.70901634 0.17640856 0.87076302]\n",
      "coefficients of the recovered tensor = [0.590649   0.95996671 0.67559721 0.70826876 0.17654242 0.8704321 ]\n"
     ]
    }
   ],
   "source": [
    "Crecovered = elasticityTensorFromStrainAndStressTensors( Eps, Sig)\n",
    "print(f\"coefficients of the original tensor = {Ctesting}\")\n",
    "print(f\"coefficients of the recovered tensor = {Crecovered}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.13 ('base')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "c0ceec81981b5d2d2ebed85af21e2076261f23998bc6857a653711b91909db2b"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
