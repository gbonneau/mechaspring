import periodicMinimalConstruct as pmC

import sys
import numpy as np

def rotationMatrix2D( theta: float):
    return np.array([ [ np.cos(theta), -np.sin(theta)],
                      [ np.sin(theta),  np.cos(theta)]])

def scalingMatrix2D( lmbda: float):
    return np.array([ [lmbda, 0.],
                      [   0., 1.]])

def uniaxialStretchingLinear( vert, u0, v0, lList, aList, lmbda, theta):
    # barycenter = pmc.barycenterSpringList( vert, u0, v0, lList, aList)
    barycenter = pmC.barycenterSpringList( vert, u0, v0, lList, [])
    rotTheta = rotationMatrix2D( theta)
    rotMinusTheta = rotationMatrix2D( -theta)
    scaleLmbda = scalingMatrix2D( lmbda)
    linearStretching = rotTheta @ scaleLmbda @ rotMinusTheta # LINEAR STRETCHING MATRIX OF SCALE lmbda IN DIRECTION theta. @ MULTIPLIES MATRICES
    affineStretchingFunction = lambda x: linearStretching @ (x - barycenter) + barycenter
    vertStretched = np.apply_along_axis( affineStretchingFunction, axis=1, arr=vert)
    u0Stretched = linearStretching @ u0
    v0Stretched = linearStretching @ v0
    return vertStretched, u0Stretched, v0Stretched

