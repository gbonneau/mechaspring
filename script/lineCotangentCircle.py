import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection

def plotCoTangentLine( p1, r1, s1, p2, r2, s2):
    c1 = Circle( (p1[0], p1[1]), r1)
    c2 = Circle( (p2[0], p2[1]), r2)
    patches = [c1, c2]
    p = PatchCollection(patches)

    ax.add_collection( p)
    a1, a2 = coTangentLine( p1, r1, s1, p2, r2, s2)
    plt.plot( [a1[0], a2[0]], [a1[1],a2[1]])

# p[1,2]: CENTER OF CIRCLES, NUMPY ARRAYS OF SHAPE (2,)
# r[1,2]: RADIUS OF CIRCLES, SCALARS
# s[1,2] = +/- 1 = SIGN OF CHIRALITY, INTEGERS +/- 1
# RETURNS a[1,2] THE TWO POINTS OF INTERSECTION BETWEEN THE CO-TANGENT LINE
# AND THE CIRCLES
def coTangentLine( p1, r1, s1, p2, r2, s2):
    v = p2 - p1
    vOrtho = np.array([-v[1], v[0]])
    vDotv = np.dot(v,v)
    n1 = (s1 * r1 + s2 * r2) / vDotv
    n2 = np.sqrt(1. / vDotv - n1 * n1)
    n = n1 * v + n2 * vOrtho
    a1 = p1 + s1 * r1 * n
    a2 = p2 - s2 * r2 * n
    # print(np.linalg.norm( a1-p1) - r1)
    # print(np.linalg.norm( a2-p2) - r2)
    # print(np.dot( a1-p1, a2-a1))
    # print(np.dot( a2-p2, a2-a1))
    return a1, a2

if __name__=="__main__":
    fig,ax = plt.subplots()

    p1 = np.array([-0.1,0.2])
    p2 = np.array([1.,0.])
    r1 = 0.4
    r2 = 0.3
    s1 = +1
    s2 = +1
    plotCoTangentLine( p1, r1, s1, p2, r2, s2)
    # plotCoTangentLine( p2, r2, s2, p1, r1, s1)
    plt.show()