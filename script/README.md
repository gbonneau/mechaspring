# generation, conversion and plotting scripts

### Generation and conversion scripts

- ```python honeyCombCylinder.py 10 5 > output.txt```: generates an ascii file containing a honeycomb full cylindrical network of size 10 along the axis of the cylinder and 5 along the cyclic direction, with constraint vertices at the two ends of the cylinder. The output file ```output.txt``` can be used as input to the simulation code.

- ``python honeyCombHalfCylinder.py 10 5 > output.txt```: generates an ascii file containing a honeycomb cylindrical network cut in half, of size 10 along the axis of the cylinder and 5 along the cyclic direction, with constraint vertices at the two ends of the cylinder. The output file ```output.txt``` can be used as input to the simulation code.


- ```python honeyCombPlane.py 10 5 > output.txt```: generates an ascii file containing a honeycomb planar network of size 10 along the 'zig-zag' direction and 5 in the other direction, with constraint vertices at the ends of the 'zig-zag' direction. The output file ```output.txt``` can be used as input to the simulation code.

- ```python edgeToVertexList.py input.txt > output.txt```: convert a network with vertex pair connectivity to a network with indicent vertices connectivity that can be used as input to the simulation code

- ```pvpython displayBondEnergy.py myFile.vtk```: paraview-python script that visualize a network with potential energy values attached to the edges


- ```pvpython compareNetworks.py leftNetwork.vtk rightNetwork.vtk```: paraview-python script that visualizes side-by-side two networks with potential energy values attached to the edges. The same view is used in both windows to facilitate comparisons.