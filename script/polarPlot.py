# from cProfile import label
import numpy as np
import sys
import matplotlib.pyplot as plt

# GIVEN AN ARRAY OF FLOATS, RETURN TWO FLOATS DEFINING
# AN INTERVAL d% LARGER THAN THE [min,max] INTERVAL
def largerInterval( a, d):
    min = np.min(a)
    max = np.max(a)
    addedLength = (max - min) * d/100./2.
    return min-addedLength, max+addedLength

def polarPlot( ax, thetaMin, thetaMax, value, title):
    value = list(value)
    nTraction = len(value)
    theta=np.linspace( thetaMin, thetaMax, num=nTraction, endpoint=False)
    ax.plot(theta, value)
    # ax.plot(theta, PR3, label="fine subdivided")
    # ax.set_rmax(2.)
    # rmin, rmax= largerInterval( value+[.0], 10.)
    rmin, rmax= largerInterval( value, 10.)
    meanValue = 0.5*(max(value)+min(value))
    ax.set_rmin( min(rmin, meanValue-0.1))
    ax.set_rmax( max(rmax, meanValue+0.1))
    # ax.set_rticks([ 0.,0.5,1.,1.5])  # Less radial ticks
    # ax.set_rticks([-1., -0.5, 0., 0.5, 1, 1.5, 2])  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    lines, labels = plt.thetagrids(range(0, 360, 30)) # theta ticks min, max, delta
    ax.grid(True)

    # ax.set_title("A line plot on a polar axis", va='bottom')
    ax.set_title( title, va='bottom')
    # ax.legend(loc="upper right")
    # plt.savefig( "outputPolarPlot.pdf", format="pdf")
    # plt.show()

def extendedPolarPlot( ax, thetaMin, thetaMax, value, title):
    value = list(value)
    nTraction = len(value)
    theta=np.linspace( thetaMin, thetaMax, num=nTraction, endpoint=False)

    theta=list(theta) + [thetaMax] # EXTEND PLOT, USEFUL WHEN THE FUNCTION IS CALLED FROM 0 TO PI OR 0 TO 2 PI
    value = value + [value[0]]

    ax.plot(theta, value)
    # ax.plot(theta, PR3, label="fine subdivided")
    # ax.set_rmax(2.)
    # rmin, rmax= largerInterval( value+[.0], 10.)
    rmin, rmax= largerInterval( value, 10.)
    meanValue = 0.5*(max(value)+min(value))
    ax.set_rmin( min(rmin, meanValue-0.3))
    ax.set_rmax( max(rmax, meanValue+0.3))
    # ax.set_rticks([ 0.,0.5,1.,1.5])  # Less radial ticks
    # ax.set_rticks([-1., -0.5, 0., 0.5, 1, 1.5, 2])  # Less radial ticks
    ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    lines, labels = plt.thetagrids(range(0, 360, 30)) # theta ticks min, max, delta
    ax.grid(True)

    # ax.set_title("A line plot on a polar axis", va='bottom')
    ax.set_title( title, va='bottom')
    # ax.legend(loc="upper right")
    # plt.savefig( "outputPolarPlot.pdf", format="pdf")
    # plt.show()

def polarPlotFromFile( filename, title):
    value = np.loadtxt( filename)
    polarPlot( value, title)

if __name__=="__main__":
    filename = sys.argv[1]
    title = "Poisson"
    fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
    polarPlotFromFile( ax, 0., 2.*np.pi, filename, title)

