# state file generated using paraview version 5.5.2
import sys

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.5.2

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1648, 1194]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
# renderView1.CenterOfRotation = [0.5099977341087651, 0.5115499729872681, 0.0]
renderView1.StereoType = 0
# renderView1.CameraPosition = [0.6401033987534838, 0.509548347377349, 10000.0]
# renderView1.CameraFocalPoint = [0.6401033987534838, 0.509548347377349, 0.0]
# renderView1.CameraParallelScale = 0.597485244560746
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.XTitleFontFile = ''
renderView1.AxesGrid.YTitleFontFile = ''
renderView1.AxesGrid.ZTitleFontFile = ''
renderView1.AxesGrid.XLabelFontFile = ''
renderView1.AxesGrid.YLabelFontFile = ''
renderView1.AxesGrid.ZLabelFontFile = ''

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Legacy VTK Reader'
finalvtk = LegacyVTKReader(FileNames=[sys.argv[1]])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from finalvtk
finalvtkDisplay = Show(finalvtk, renderView1)

# get color transfer function/color map for 'bondEnergy'
bondEnergyLUT = GetColorTransferFunction('bondEnergy')
bondEnergyLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'bondEnergy'
bondEnergyPWF = GetOpacityTransferFunction('bondEnergy')
bondEnergyPWF.Points = [0.0, 0.0, 0.5, 0.0, 4.8765100473247e-06, 1.0, 0.5, 0.0]
bondEnergyPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
finalvtkDisplay.Representation = 'Surface'
finalvtkDisplay.ColorArrayName = ['CELLS', 'bondEnergy']
finalvtkDisplay.LookupTable = bondEnergyLUT
finalvtkDisplay.LineWidth = 5.0
finalvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
finalvtkDisplay.SelectOrientationVectors = 'None'
finalvtkDisplay.ScaleFactor = 0.10250999460695312
finalvtkDisplay.SelectScaleArray = 'bondEnergy'
finalvtkDisplay.GlyphType = 'Arrow'
finalvtkDisplay.GlyphTableIndexArray = 'bondEnergy'
finalvtkDisplay.GaussianRadius = 0.005125499730347656
finalvtkDisplay.SetScaleArray = [None, '']
finalvtkDisplay.ScaleTransferFunction = 'PiecewiseFunction'
finalvtkDisplay.OpacityArray = [None, '']
finalvtkDisplay.OpacityTransferFunction = 'PiecewiseFunction'
finalvtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
finalvtkDisplay.SelectionCellLabelFontFile = ''
finalvtkDisplay.SelectionPointLabelFontFile = ''
finalvtkDisplay.PolarAxes = 'PolarAxesRepresentation'
finalvtkDisplay.ScalarOpacityFunction = bondEnergyPWF
finalvtkDisplay.ScalarOpacityUnitDistance = 0.11877645869577784


# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
finalvtkDisplay.DataAxesGrid.XTitleFontFile = ''
finalvtkDisplay.DataAxesGrid.YTitleFontFile = ''
finalvtkDisplay.DataAxesGrid.ZTitleFontFile = ''
finalvtkDisplay.DataAxesGrid.XLabelFontFile = ''
finalvtkDisplay.DataAxesGrid.YLabelFontFile = ''
finalvtkDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
finalvtkDisplay.PolarAxes.PolarAxisTitleFontFile = ''
finalvtkDisplay.PolarAxes.PolarAxisLabelFontFile = ''
finalvtkDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
finalvtkDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# setup the color legend parameters for each legend in this view

# get color legend/bar for bondEnergyLUT in view renderView1
bondEnergyLUTColorBar = GetScalarBar(bondEnergyLUT, renderView1)
# bondEnergyLUTColorBar.Position = [0.8083282766990291, 0.31698492462311584]
bondEnergyLUTColorBar.Title = 'bondEnergy'
bondEnergyLUTColorBar.ComponentTitle = ''
bondEnergyLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
bondEnergyLUTColorBar.TitleFontFile = ''
bondEnergyLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
bondEnergyLUTColorBar.LabelFontFile = ''
# bondEnergyLUTColorBar.ScalarBarLength = 0.3299999999999991

# set color bar visibility
bondEnergyLUTColorBar.Visibility = 1

# show color legend
finalvtkDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# finally, restore active source
# reset view to fit data
renderView1.ResetCamera()
SetActiveSource(finalvtk)
Interact()
# ----------------------------------------------------------------
