from scipy.spatial import Voronoi
from scipy.spatial import Delaunay
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import meshio
import sys

def printEdge( a, b):
    if (a<b):
        print(a,b)
# GENERE DES POINTS EN 2D
# CALCUL ET AFFICHE LE DIAGRAMME DE VORONOI
# ET LA TRIANGULATION DE DELAUNAY

def validPoint( p):
    return (p[0]>-1.) and (p[0]<2.) and (p[1]>-1.)and (p[1]<2.)

def validRegion( points, simplex):
    for v in simplex:
        if not validPoint( points[v]):
            return False
    return True

    return validPoint( points[simplex[0]]) and validPoint( points[simplex[1]]) and validPoint( points[simplex[2]])

# GENERATE VECTORS FOR PERIODICITY TESTING
def generateTranslateVector( dimension):
    points = np.zeros((1,dimension))
    for d in range(dimension):
        transVect = np.zeros((dimension,))
        newPoints = np.zeros((0,dimension))
        for offset in [-1.,0.,1.]:
            transVect[d] = offset
            transPoint = [ cP + transVect for cP in points]
            newPoints = np.concatenate(( newPoints, transPoint))
        points = newPoints
    return newPoints

def random_periodic_sequential_adsorption(d, dimension, num_tests=10000):
    np.random.seed(43)
    # np.random.seed(int(datetime.now().timestamp()))
    points = []
    zeroVector = np.zeros( (dimension,))
    oneVector = np.ones( (dimension,))
    translateVector = generateTranslateVector( dimension)

    for i in tqdm(range(num_tests)):
        point_candidate = np.random.random(dimension)*3 - oneVector
        # accept = True
        accept = np.all( point_candidate > zeroVector) and np.all( point_candidate < oneVector)
        for point in points:
            for v in translateVector:
                    if np.linalg.norm( point + v - point_candidate) < d:
                        accept = False
                        break
        if accept:
            points.append(point_candidate)
    return points

def clockWisePoints2D( a, b, c):
    u = a-b
    v = c-b
    return np.sign( np.cross(u,v)) == 1.    

def objWrite( filename, vert, lList):
    with open( filename, "w") as f:
        for v in vert:
            f.write(f"v {v[0]} {v[1]} 0.\n")
        for e in lList:
            f.write(f"l {e[0]+1} {e[1]+1}\n")
    f.close()

if __name__ == "__main__":

    dimensionGlobal = 2
    #points = np.random.random((30, 2))
    if (len(sys.argv) != 2):
        print(f"{sys.argv[0]} generate periodic Delaunay triangulation with a given input minimal distance between samples")
        print(f"the unit cell samples are randomly generated between 0 and 1")
        print(f"a smaller minimal distance produces more points!")
        print(f"usage:  {sys.argv[0]} <minimalDistance>")
    smallestDistance = float(sys.argv[1])
    # smallestDistance = 0.1
    unitPoints = random_periodic_sequential_adsorption( smallestDistance, dimensionGlobal) # THE FIRST PARAMETER CONTROLS THE SMALLEST DISTANT BETWEEN TWO STOCHASTIC SAMPLES
    # unitPoints = random_sequential_adsorption(0.1, 2)
    unitPoints = np.array(unitPoints)

    points = np.zeros((0,2))
    for i in range(-2,3):
        for j in range(-2,3):
            points = np.concatenate( (points, unitPoints + np.array([i,j])))

    monVoronoi = Voronoi(points)

    np.savetxt( "points.txt", monVoronoi.vertices)
    print(len(monVoronoi.vertices))
    validVoronoiRegion = []
    for aRegion in monVoronoi.regions:
        # print(aRegion)
        if validRegion(monVoronoi.vertices, aRegion):
            validVoronoiRegion.append(aRegion)


    lList=[]
    aList=[]

    for aRegion in validVoronoiRegion:
    # for aRegion in validVoronoiRegion[3:5]:
        if (dimensionGlobal == 2) and (len(aRegion) >= 3):
            if not clockWisePoints2D( monVoronoi.vertices[aRegion[0]], monVoronoi.vertices[aRegion[1]], monVoronoi.vertices[aRegion[2]]):
                aRegion.reverse()
        nP = len(aRegion)
        for i, v in enumerate( aRegion):
            assert(v == aRegion[i])
            nextV = aRegion[(i+1) % nP]
            nextNextV = aRegion[(i+2) % nP]
            if v < nextV:
            # if True:
                lList.append( [v, nextV])
            aList.append( [ v, nextV, nextNextV])

    np.savetxt( "2Spring.txt", np.array(lList).astype(int))
    np.savetxt( "3Spring.txt", np.array(aList).astype(int))

    mesh=meshio.Mesh( monVoronoi.vertices, [("line", lList)])
    mesh.write("voronoiMesh2D.vtk")

    objWrite( "voronoi2D.obj", monVoronoi.vertices, lList)

    print("output voronoi mesh is saved in files voronoi2D.obj, voronoiMesh2D.vtk and [points.txt, 2Spring.txt, 3Spring.txt]")

