#!/bin/bash
if [ "$#" -ne 4 ]; then
	echo "Usage: $0 xMin xMax yMin yMax"
        echo
        exit
else
        echo "== display reference and final spring set"
fi

xMin=$1
xMax=$2
yMin=$3
yMax=$4

ratio=`echo "(($yMin)-($yMax))/(($xMin)-($xMax))" | bc -l`

cat > tmp.txt << EOF
set xrange [$xMin:$xMax]
set yrange [$yMin:$yMax]
set size ratio $ratio

plot "reference.txt" with lines lc "blue", "reference.txt" ps 0.5 pt 7 lc "blue", "final.txt" with lines lc "orange", "final.txt" ps 0.5 pt 7 lc "orange"
pause -1 "hit return to close and exit"
EOF

# plot "reference.txt" with lines lc "blue", "final.txt" with lines lc "orange"
# plot "reference.txt" with lines lc "blue", "reference.txt" ps 0.5 pt 7 lc "blue", "final.txt" with lines lc "orange", "final.txt" ps 0.5 pt 7 lc "orange" 
gnuplot tmp.txt
rm tmp.txt
