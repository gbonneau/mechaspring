import numpy as np
import matplotlib.pyplot as plt
import sys

# ------------------------------------------------------------------------------------------ 
# PYTHON SCRIPT TO COMPUTE THE MACROSCOPIC STRAIN AND STRESS TENSORS
# FROM THE RESULTS OF A PERIODIC 2D SIMULATION
# THE PERIODIC SIMULATION OUTPUTS TWO PERIODICITY VECTORS BEFORE AND AFTER LOADING
# AS WELL AS TWO 2D VECTORS WHICH ARE THE FORCES EXTERTED ON THE EDGES OF THE PARALLELOGRAM
# SEE THE MAIN ROUTINE macroStressAndStrainTensor BELOW FOR MORE DETAILS
# ------------------------------------------------------------------------------------------ 

def plotVector( ax, P, V, lb):
    ax.plot( [P[0], P[0]+V[0]], [P[1], P[1]+V[1]], label=lb)

def pointOfParallelogram( barycenter, twoVectors):
    firstPoint = barycenter - 0.5 *  (twoVectors[:,0] + twoVectors[:,1])
    fourPoints = np.array([ firstPoint, firstPoint + twoVectors[:,0], firstPoint + twoVectors[:,0] + twoVectors[:,1], firstPoint + twoVectors[:,1]])
    return fourPoints

def plotParallelogram( ax, fourPoints, color):
    ax.fill( fourPoints[:,0], fourPoints[:,1], facecolor='none', edgecolor=color, linewidth=1)
    return

def Rot( angle):
    return np.array([ [np.cos(angle), -np.sin(angle)],
                        [np.sin(angle), np.cos(angle)]])

#--------------------------------------------------------------------------------------------------------------------------------------
# GIVEN TWO 2D PERIODICITY VECTORS perReference[0:2,0:2] AND perFinal[0:2,0:2]
# BEFORE AND AFTER APPLICATION OF A LOAD ON A PERIODIC NETWORK
# AND GIVEN THE (MACROSCOPIC) FORCES energyGradient[0:2,0:2] EXTERTED ON THE EDGES AT EQUILIBRIUM
# RETURNS THE STRAIN AND THE STRESS TENSORS
#
# OPTIONALLY DRAW THE PERIODIC PARALLELOGRAM BEFORE AND AFTER LOADING, THE NORMAL TO THE EDGES, THE FORCES EXTERED ON THE EDGES,
# AND THE TWO 2D COLUMN VECTORS OF THE STRESS AND STRAIN TENSORS
# IF displayInfo IS SET TO TRUE TEXTUAL INFORMATION IS DISPLAYED IN THE TERMINAL.
#--------------------------------------------------------------------------------------------------------------------------------------

def processStressTensorForSignAndDirection( stressTensor):
    # CHECK IF TENSOR IS NON ZERO
    epsilonStressValue = 1e-8

    displayInfo = False

    # PREPROCESS VERY SMALL VALUES TO PREVENT COMPUTING SQUARE ROOT OF NEGATIVE VALUES
    stressTensor[np.abs(stressTensor) < epsilonStressValue] = 0

    zeroStress = False
    if displayInfo:
        print("======")
        print( f"stressTensor at input of processing = {stressTensor}")
    if (np.linalg.norm(stressTensor) < epsilonStressValue):
        print(f"ALMOST FATAL ERROR: SIMULATION PRODUCES ZERO STRESS IN FIRST DIRECTION\n")
        print(f"IT CAN HAPPEN FOR EXAMPLE WHEN THERE ARE HINGE VERTICES IN THE NETWORK\n")
        print(f"WHICH CAN ABSORB THE DEFORMATION WITHOUT PRODUCING STRESS\n")
        zeroStress = True
        # sys.exit(1)
    else:
        zeroStress = False

    diagonalStress = np.diag(stressTensor)
    maxAbsValDiagIndex = np.argmax(np.fabs( diagonalStress))
    if diagonalStress[maxAbsValDiagIndex] * diagonalStress[1 - maxAbsValDiagIndex] < 0.:
        print(diagonalStress)
        print(f"WARNING: WEIRD SITUATION WHERE THE DIAGONAL ELEMENTS {diagonalStress} OF THE STRESS TENSOR ARE OF DIFFERENT SIGNS\n")
        print("THIS SHOULD ONLY HAPPEN IF ONE VALUE IS NEAR ZERO")
        stressTensor[ 1 - maxAbsValDiagIndex, 1 - maxAbsValDiagIndex] = 0.

    negativeStress = False
    if stressTensor[maxAbsValDiagIndex,maxAbsValDiagIndex] < 0.: # POSSIBLE IF THE MATERIAL IS CONTRACTED
        negativeStress = True
        if displayInfo:
            print(f"stressTensor[{maxAbsValDiagIndex},{maxAbsValDiagIndex}] = {stressTensor[maxAbsValDiagIndex,maxAbsValDiagIndex]} < 0., CONTRACTION.")
    else:
        if displayInfo:
            print(f"stressTensor[{maxAbsValDiagIndex},{maxAbsValDiagIndex}] = {stressTensor[maxAbsValDiagIndex,maxAbsValDiagIndex]} > 0., STRETCHING.")

    if negativeStress:
        retrievedDirectionOfStretch = np.sqrt(-np.diag(stressTensor))  
    else:  
        retrievedDirectionOfStretch = np.sqrt(+np.diag(stressTensor))

    if (stressTensor[1,0]*stressTensor[0,0] < 0.): 
        if displayInfo:
            print(f"COORDINATES OF UNIAXIAL DEFORMATION OF DIFFERENT SIGNS")
        retrievedDirectionOfStretch[1] = -retrievedDirectionOfStretch[1]  

    magnitudeStress = np.transpose( retrievedDirectionOfStretch) @ retrievedDirectionOfStretch
    retrievedDirectionOfStretch = retrievedDirectionOfStretch / np.sqrt(magnitudeStress)
    if displayInfo:
        print(f"retrieved direction of stress = {retrievedDirectionOfStretch}")
        print(f"magnitude = {magnitudeStress}")
        angle = np.arctan2( retrievedDirectionOfStretch[1], retrievedDirectionOfStretch[0])
        print(f"angle = {angle} = {angle/np.pi*180.} degrees")

    return magnitudeStress, retrievedDirectionOfStretch

def macroStressAndStrainTensor( perReference: np.ndarray, perFinal: np.ndarray, macroscopicForces: np.ndarray, showPlot: bool)->np.ndarray:

    epsilonNormalTensorDegenerate = 1.e-6

    print( f"--- Compute Strain and Stress tensor from periodicity vectors and gradient in U and V...")
    displayInfo = False

    #=========== PART 1: COMPUTING STRAIN TENSOR =============
    # deformation gradient F = Delta_x o (Delta_X)^(-1)
    deformationGradient = perFinal @ np.linalg.inv( perReference)
    # 2nd order tensor DX_U = Delta_u o (Delta_X)^(-1) = F - I
    gradientDisplacement = deformationGradient - np.identity(2)
    # infinitesimal strain tensor = 1/2(DX_U + DX_U^t)
    infinitesimalStrainTensor = 0.5 * (np.transpose( gradientDisplacement) + gradientDisplacement)
    # finite strain tensor = 1/2(DX_U + DX_U^t + DX_U^t o DX_U)
    finiteStrainTensor = infinitesimalStrainTensor + 0.5 * np.transpose( gradientDisplacement) @ gradientDisplacement
    if displayInfo:
        print(f"determinant of finite strain tensor = {np.linalg.det(finiteStrainTensor)}")

    #=========== PART 2: COMPUTING STRESS TENSOR =============
    if displayInfo:
        print(f"d(W)/d(U), d(W)/d(V) = macroscopic forces = {macroscopicForces}")

    # NORMALS TO THE FINAL PARALLELOGRAM, UNNORMALIZED!!!
    Zvector = np.array([0.,0.,1.])
    perFinal3D = np.zeros((3,2)) # EXTEND TO 3D OTHERWISE np.cross CANNOT BE USED
    perFinal3D[0:2,:] = perFinal
    n0 = np.cross( perFinal3D[:,1], Zvector).reshape(-1,1) # 0 to left and 1 to right, n0 and perFinal[:,1]
    n1 = np.cross( perFinal3D[:,0], -Zvector).reshape(-1,1) # -Zvector to have outward pointing normal
    n = np.concatenate((n0,n1), axis=1)
    normals = n[0:2,:]
    signNormalTensor = np.cross(normals[:,0],normals[:,1])
    assert(np.fabs(signNormalTensor) > epsilonNormalTensorDegenerate) # STOP IN THE DEGENERATE CASE WHERE THE TWO PERIODICITY VECTORS AT EQUILIBRIUM ARE ALMOST PARALLEL
    signNormalTensor = np.sign(signNormalTensor)

    if displayInfo:
        print("check normalTensor is orthogonal to (swaped) periodicity vectors after simulation: ")
        print(normals[:,0] @ perFinal[:,1])
        print(normals[:,1] @ perFinal[:,0])
        print(f"sign of normal tensor = {signNormalTensor}")

    # stress tensor is the tensor of macroscopic forces multiplied by the inverse of the unnormalized normal tensor
    stressTensor =  macroscopicForces @ np.linalg.inv( normals)
    stressTensor = signNormalTensor * stressTensor

    if displayInfo:
        magnitudeStress, retrievedDirectionOfStretch = processStressTensorForSignAndDirection( stressTensor)
        print( f"deformationGradient = {deformationGradient}")
        print( f"gradientDisplacement = {gradientDisplacement}")
        print( f"infinitesimalStrainTensor = {infinitesimalStrainTensor}")
        print( f"finiteStrainTensor = {finiteStrainTensor}")
        print( f"stressTensor = {stressTensor}")
        print( f"check stressTensor is symmetric: {stressTensor[0,1] - stressTensor[1,0]}")
        print( f"magnitude of stress = {magnitudeStress}")
        print( f"retrieved direction of stretch = {retrievedDirectionOfStretch}")
        angle = np.arctan2( retrievedDirectionOfStretch[1], retrievedDirectionOfStretch[0])
        print(f"angle = {angle} = {angle/np.pi*180.} degrees")
        print( f"check macroscopic forces = normals @ stressTensor: {np.linalg.norm(macroscopicForces - signNormalTensor * stressTensor @ normals)}")

    if showPlot:
        barycenter = np.array([0.,0.])
        oStrain = np.array([2.,0.])
        oStress = np.array([4.,0.])
        firstPoint = barycenter - 0.5 *  (perFinal[:,0] + perFinal[:,1])
        pointMidTop = firstPoint + perFinal[:,1] + 0.5 * perFinal[:,0]
        pointMidRight = firstPoint + perFinal[:,0] + 0.5 * perFinal[:,1]
        fig,ax = plt.subplots(1,3)
        ax[0].axis("equal")
        ax[1].axis("equal")
        ax[2].axis("equal")
        plotParallelogram( ax[0], pointOfParallelogram( barycenter, perFinal), "r")
        plotParallelogram( ax[0], pointOfParallelogram( barycenter, perReference), "g")

        scaleEnergyGradient = 1.
        scaleStress = 1.
        plotVector( ax[0], pointMidRight, normals[:,0], "normal right")
        plotVector( ax[0], pointMidRight, macroscopicForces[:,0]*scaleEnergyGradient, "force right")
        ax[0].scatter( pointMidRight[0], pointMidRight[1],s=30)
        plotVector( ax[0], pointMidTop, normals[:,1], "normal top")
        plotVector( ax[0], pointMidTop, macroscopicForces[:,1]*scaleEnergyGradient, "force top")
        ax[0].legend()
        ax[0].scatter( pointMidTop[0], pointMidTop[1],s=30)

        plotVector( ax[1], [0.,0.], finiteStrainTensor[:,0]*5., "1st strain vector")
        plotVector( ax[1], [0.,0.], finiteStrainTensor[:,1]*5., "2nd strain vector")
        ax[1].legend()
        ax[1].scatter( 0., 0.,s=30)

        plotVector( ax[2], [0.,0.], stressTensor[:,0]*scaleStress, "1st stress vector")
        plotVector( ax[2], [0.,0.], stressTensor[:,1]*scaleStress, "2nd stress vector")
        ax[2].scatter( 0., 0.,s=30)
        ax[2].legend()
        # ax[2].grid()
        
        plt.show()
    # return infinitesimalStrainTensor, stressTensor
    print( f"... done compute Stress and Strain tensor from periodicity vectors and gradient in U and V")

    return finiteStrainTensor, stressTensor

def poissonRatioFromStrainTensorAndAngle( strainTensor: np.ndarray, angle: float) -> float:
    print("--- compute Poisson's ratio from strain tensor and angle...")
    displayInfo = False

    rotMat = Rot( angle)
    longVec = rotMat[:,0] # longitudinal direction: direction of stretch
    transVec = rotMat[:,1] # transverse direction: orthogonal to stretch
    longScalar = np.transpose( longVec) @ strainTensor @ longVec # extension in the longitudinal direction, computed from the infinitesimal strain tensor
    transScalar = np.transpose( transVec) @ strainTensor @ transVec # extension (or compression) orthogonal to the longitudinal direction

    if displayInfo:
        print(f"longScalar = {longScalar}")
        print(f"transScalar = {transScalar}")  
        print(f"direction of stretch from angle = {longVec}")
        print(f"Poisson's ratio from strain tensor and angle = {-transScalar/longScalar}") # Poisson ratio with the infinitesimal strain tensor

    # return -transInfScalar/longInfScalar
    print("... done compute Poisson's ratio from strain tensor and angle")

    return -transScalar/longScalar

def PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor: np.ndarray, stressTensor: np.ndarray) -> float:
    print( f"--- Compute PR and Young from strain and stress tensors ...")
    displayInfo = False
    # stressTensor = sigma nx*nx nx*ny
    #                      ny*nx ny*ny

    magnitudeStress, retrievedDirectionOfStretch = processStressTensorForSignAndDirection( stressTensor)
    longScalar = np.transpose( retrievedDirectionOfStretch) @ strainTensor @ retrievedDirectionOfStretch# extension in the longitudinal direction, computed from the strain tensor
    transverseDirection = np.array([-retrievedDirectionOfStretch[1], retrievedDirectionOfStretch[0]])
    transScalar = np.transpose( transverseDirection) @ strainTensor @ transverseDirection # extension (or compression) orthogonal to the longitudinal direction

    youngModulus = magnitudeStress / np.fabs(longScalar)
    PR = -transScalar/longScalar
    if displayInfo:
        print( f"finiteStrainTensor = {strainTensor}")
        print( f"stressTensor = {stressTensor}")
        print( f"check stressTensor is symmetric: {stressTensor[0,1] - stressTensor[1,0]}")
        print( f"magnitude of stress: {magnitudeStress}")
        print( f"retrieved direction of stretch: {retrievedDirectionOfStretch}")
        angle = np.arctan2( retrievedDirectionOfStretch[1], retrievedDirectionOfStretch[0])
        print(f"angle = {angle} = {angle/np.pi*180.} degrees")
        print( f"transverse direction: {transverseDirection}")
        print( f"longScalar = {longScalar}")
        print( f"transScalar = {transScalar}")
        print( f"Young's modulus = {youngModulus}")
        print( f"Poisson's ratio = {PR}")
    print("... done compute PR and young from strain and stress tensors")
    return PR, youngModulus, magnitudeStress

#---------------------------------------------------------------------------------------------------------------
# READS PERIODICITY AND ENERGY GRADIENT DATA AS OUTPUT FROM THE MECHASPRING SIMULATION
# AND CALLS THE MAIN ROUTINE macroStressAndStrainTensor THAT COMPUTES THE MACROSCOPIC STRESS AND STRAIN TENSORS
#---------------------------------------------------------------------------------------------------------------
def macroStressAndStrainTensorFromFile( perReferenceFileName: str, perFinalFileName: str, energyGradientFileName: str, showPlot: bool)->np.ndarray:
    perReference = np.transpose(np.loadtxt(perReferenceFileName)[:,0:2]) # READ ONLY TWO FIRST COORDINATES
    perFinal = np.transpose(np.loadtxt(perFinalFileName)[:,0:2])
    energyGradient = np.transpose(np.loadtxt(energyGradientFileName)[:,0:2])

    finiteStrainTensor, stressTensor = macroStressAndStrainTensor( perReference, perFinal, energyGradient, showPlot)
    return finiteStrainTensor, stressTensor

#------------------------------------------
# THE DEFAULT SCRIPT CALLS THE MAIN ROUTINE
#------------------------------------------
if __name__=="__main__":

    if len(sys.argv) != 4 and len(sys.argv) != 5:
        print(f"usage: {sys.argv[0]} periodicityReference.txt periodicityFinal.txt gradientUV.txt")
        sys.exit(1)

    # strainTensor, stressTensor =  macroStressAndStrainTensorFromFile( sys.argv[1], sys.argv[2], sys.argv[3], False)
    strainTensor, stressTensor =  macroStressAndStrainTensorFromFile( sys.argv[1], sys.argv[2], sys.argv[3], True)
    # PR, youngModulus, magnitudeStress = PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor, stressTensor)
    # print("the latter Poisson's ratio and Young's modulus are correct IFF the periodicity vectors and the gradient in U and V come from an uniaxial stretching simulation")


    if len(sys.argv) == 5:
        angle = float( sys.argv[4])
        PR = poissonRatioFromStrainTensorAndAngle( strainTensor, angle)
        print("the latter PR is correct IFF the angle given was the one used in the uniaxial stretching applied to compute the stress")


