import numpy as np
import torch
import torchmin
from torchmin import minimize
from torchmin import minimize_constr
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from torchvision import io

import time
import sys

import periodicMinimalStructure as pmS
import periodicMinimalConstruct as pmC
import periodicMinimalVisualization as pmV
import periodicMinimalLoad as pmL
import periodicMinimalExport as pmE

import poissonRatioFromPeriodicityVectors
import macroStressAndStrainTensor

# CLASS USED TO STORE VARIABLES WHICH ARE MONITORED DURING THE OPTIMIZATION
class monitor:
    def __init__( self):
        self.nIter = 0
        return None

    def update( self, lengthTerm, angleTerm, barycenterConstraintTerm, slidingConstraintTerm, objectiveFunction, weightLength, weightAngle, weightBarycenterConstraint, weightSlidingConstraint):

        self.monitorWeightedLength = weightLength * lengthTerm
        self.monitorWeightedAngle = weightAngle * angleTerm
        self.monitorWeightedBarycenterConstraint = weightBarycenterConstraint * barycenterConstraintTerm
        self.monitorWeightedSlidingConstraint = weightSlidingConstraint * slidingConstraintTerm

        self.monitorLength = lengthTerm
        self.monitorAngle = angleTerm
        self.monitorBarycenterConstraint = barycenterConstraintTerm
        self.monitorSlidingConstraint = slidingConstraintTerm

        self.monitorObjectiveFunction = objectiveFunction

    def timeStep( self):
        self.nIter = self.nIter+1

# CLASS USED TO STORE VARIABLES ENCODING THE LOADING CONFIGURATION
# CURRENTLY ONLY UNIAXIAL LOAD CONFIGURATION ARE CONSIDERED, DEFINED WITH A SCALING PARAMETER lmbda AND AN ANGLE theta
class loading:
    def __init__( self, lmbda, theta):
        global torchDevice
        self.lmbda = lmbda
        self.theta = theta
        self.direction = torch.tensor( [ np.cos(theta), np.sin(theta)], requires_grad=False).to(torchDevice)

# MAIN CLASS USED TO STORE THE PERIODIC NETWORK, ITS REST, LOAD AND CURRENT CONFIGURATIONS AS WELL AS VARIOUS VARIABLES USEFUL FOR EXPORTING AND MONITORING
class networkOptim:
    "defines a set of periodic vertices and physical systems connecting these periodic vertices"

    def __init__( self, filename):
        global torchDevice
        self.network = pmS.periodicMinimalStructure( filename)
        # vert, u0, v0, self.lList, self.aList, self.sArray = pmC.networkFromIndirectSpringFile( filename)

        # THREE CONFIGURATIONS OF THE NETWORK ARE STORED: REST, LOADING AND OPTIMIZED
        # REST AND LOADING DO NOT REQUIRE GRADIENTS AS THEY ARE CONSTANT DURING OPTIMIZATION
        
        # INITIALIZE GEOMETRIC INTRINSINCS THAT WILL BE OPTIMIZED
        self.vert = torch.tensor( self.network.vert, requires_grad=True).to(torchDevice) # GRADIENTS REQUIRED FOR QUANTITIES THAT WILL BE OPTIMIZED 
        self.u0 = torch.tensor( self.network.u0, requires_grad=True).to(torchDevice)
        self.v0 = torch.tensor( self.network.v0, requires_grad=True).to(torchDevice)

        # INITIALIZE GEOMETRIC INTRINSINCS FOR THE REST CONFIGURATION
        self.vertRest = torch.tensor( self.network.vert, requires_grad=False).to(torchDevice) # NO GRADIENTS REQUIRED FOR REST QUANTITIES
        self.u0Rest = torch.tensor( self.network.u0, requires_grad=False).to(torchDevice)
        self.v0Rest = torch.tensor( self.network.v0, requires_grad=False).to(torchDevice)

        # COMPUTING CONSTANT ARRAYS FOR EFFICIENT SPARSE ENCODING (INDIRECT ACCESS WITHOUT LOOPS)
        self.initializeConstantArrays()

        self.uTransLength = torch.tensor( self.uTransLength, requires_grad=False).to(torchDevice) # INTEGER PERIODIC TRANSLATION ARRAYS FOR LENGTH AND ANGLE SPRINGS
        self.vTransLength = torch.tensor( self.vTransLength, requires_grad=False).to(torchDevice) # NO GRADIENT REQUIRED, THESE ARRAYS ARE CONSTANT
        self.uTransAngle = torch.tensor( self.uTransAngle, requires_grad=False).to(torchDevice)
        self.vTransAngle = torch.tensor( self.vTransAngle, requires_grad=False).to(torchDevice)

        # BARYCENTER AT REST
        self.barycenterAtRest = torch.mean( self.vertRest, axis=0)

        # LENGTH AND ANGLE ARRAYS AT REST
        self.lengthAtRest = lengthOfLengthSpring( self.vertRest, self.u0Rest, self.v0Rest, self.pIndexLength, self.uTransLength, self.vTransLength)
        self.angleAtRest = angleOfAngleSpring( self.vertRest, self.u0Rest, self.v0Rest, self.pIndexAngle, self.uTransAngle, self.vTransAngle) # CAREFUL: ANGLE AND NOT LENGTH HERE!!!
        self.voronoiWeight = voronoiWeightOfAngleSpring( self.vertRest, self.u0Rest, self.v0Rest, self.pIndexAngle, self.uTransAngle, self.vTransAngle)

        # ARRAYs OF STIFFNESS VALUES
        self.sArray = torch.tensor( self.network.sArray, requires_grad=False).to(torchDevice)

    #===============================================================================
    # CONSTANT ARRAYS TO EFFICIENTLY COMPUTE ALL ENERGY TERMS IN PYTORCH CALLS
    def initializeConstantArrays( self):    
        self.pIndexLength, self.uTransLength, self.vTransLength, self.sIndexLength = constantArrayForSystemOfVertex( self.network.lList, dim=2)
        self.pIndexAngle, self.uTransAngle, self.vTransAngle, self.sIndexAngle = constantArrayForSystemOfVertex( self.network.aList, dim=3)

    #=======================================================================
    # LOADING
    # THIS FUNCTION IS ONLY USED FOR TESTING
    def circleLoading( self):
        radius = 1.
        print(self.vert)

        cercle = torch.stack( (torch.cos( self.vert[:,0]/1.), torch.sin( self.vert[:,0]/1.)))
        cercle = torch.transpose( cercle, 1, 0)
        print(cercle)
        self.vertLoad = radius * cercle

        self.u0Load = torch.tensor([0.,0.])
        self.v0Load = torch.tensor([0.,1.])

        # COPY LOAD QUANTITIES IN VARYING QUANTITIES
        self.vert = self.vertLoad.clone().requires_grad_(True)
        self.u0 = self.u0Load.clone().detach().requires_grad_(True)
        self.v0 = self.v0Load.clone().detach().requires_grad_(True)

    def uniaxialLoading( self, lmbda, theta):

        self.loadSetting = loading( lmbda, theta)

        vertStretched, u0Stretched, v0Stretched = pmL.uniaxialStretchingLinear( self.vert.detach().cpu().numpy(), self.u0.detach().cpu().numpy(), self.v0.detach().cpu().numpy(), self.network.lList, self.network.aList, lmbda, theta)

        self.vertLoad = torch.tensor(vertStretched, requires_grad = False).to(torchDevice)
        self.u0Load = torch.tensor(u0Stretched, requires_grad = False).to(torchDevice)
        self.v0Load = torch.tensor(v0Stretched, requires_grad = False).to(torchDevice)

        # COPY LOAD QUANTITIES IN VARYING QUANTITIES
        self.vert = self.vertLoad.clone().requires_grad_(True).to(torchDevice)
        self.u0 = self.u0Load.clone().detach().requires_grad_(True).to(torchDevice)
        self.v0 = self.v0Load.clone().detach().requires_grad_(True).to(torchDevice)

    #=======================================================================
    # POTENTIAL ENERGY
        
    def lengthOfLengthSpring( self):
        result = lengthOfLengthSpring( self.vert, self.u0, self.v0, self.pIndexLength, self.uTransLength, self.vTransLength)
        return( result)

    def angleOfAngleSpring( self):
        result = angleOfAngleSpring( self.vert, self.u0, self.v0, self.pIndexAngle, self.uTransAngle, self.vTransAngle)
        return( result)

    def voronoiWeightOfAngleSpring( self):
        result = voronoiWeightOfAngleSpring( self.vert, self.u0, self.v0, self.pIndexAngle, self.uTransAngle, self.vTransAngle)
        return( result)

    def stretchingPotentialEnergy( self):
        result = stretchingPotentialEnergy( self.vert, self.u0, self.v0, self.pIndexLength, self.uTransLength, self.vTransLength, self.lengthAtRest, self.sIndexLength, self.sArray)
        return( result)

    def bendingPotentialEnergy( self):
        result = bendingPotentialEnergy( self.vert, self.u0, self.v0, self.pIndexAngle, self.uTransAngle, self.vTransAngle, self.angleAtRest, self.voronoiWeight, self.sIndexAngle, self.sArray)
        return( result)

    #=======================================================================
    # DoF TO NETWORK AND NETWORK TO DoF

    def dofToNetwork( self, doF):
        # vertexDofToNetwork( doF)
        self.vertexAndPeriodicityVectorDofToNetwork(doF)

    def vertexDofToNetwork( self, doF): # FIRST POSSIBILITY: DOF ARE THE VERTICES
        self.vert = doF.reshape( -1, 2)

    def vertexAndPeriodicityVectorDofToNetwork( self, doF): # SECOND POSSIBILITY: DOF ARE THE VERTICES AND THE PERIODICITY VECTORS
        dofAsVector = doF.reshape( -1, 2)
        self.vert = dofAsVector[:-2]
        self.u0 = dofAsVector[-2]
        self.v0 = dofAsVector[-1]

    def networkToDoF( self):
        # return self.networkToVertexDof()
        return self.networkToVertexAndPeriodicityVectorDof()

    def networkToVertexDof( self): # FIRST POSSIBILITY: DOF ARE THE VERTICES
        doF = self.vert.reshape(-1)
        return doF    
    
    def networkToVertexAndPeriodicityVectorDof( self): # SECOND POSSIBILITY: DOF ARE THE VERTICES AND THE PERIODICITY VECTORS
        dofAsVector = torch.cat(( self.vert, self.u0.reshape(-1,2), self.v0.reshape(-1,2)))
        doF = dofAsVector.reshape(-1)
        return doF     

    #=======================================================================
    # CONSTRAINTS         
    
    def squareDistanceOfBarycentreToRestPosition( self):
        barycenter = torch.mean( self.vert, axis=0)
        return (barycenter - self.barycenterAtRest).square().sum()
    
    def slidingError( self):
        deltaU0 = self.u0 - self.u0Load
        deltaV0 = self.v0 - self.v0Load

        result = (deltaU0 * self.loadSetting.direction).sum().square() + (deltaV0 * self.loadSetting.direction).sum().square()

        return result    

    #=======================================================================
    # OBJECTIVE FUNCTION
         
    def objectiveFunctionDeltaLengthAngleAndConstraint( self, doF):

        self.dofToNetwork( doF)
        lengthTerm = self.stretchingPotentialEnergy()
        angleTerm = self.bendingPotentialEnergy()
        barycenterConstraintTerm = self.squareDistanceOfBarycentreToRestPosition()
        slidingConstraintTerm = self.slidingError()

        weightLength = 1.
        weightAngle = 1.
        weightBarycenterConstraint = 1.
        weightSlidingConstraint = 1.e5

        objectiveFunction = \
                weightLength * lengthTerm \
                + weightAngle * angleTerm \
                + weightBarycenterConstraint * barycenterConstraintTerm \
                + weightSlidingConstraint * slidingConstraintTerm
        
        self.monitor.update( lengthTerm, angleTerm, barycenterConstraintTerm, slidingConstraintTerm, objectiveFunction, weightLength, weightAngle, weightBarycenterConstraint, weightSlidingConstraint)
        
        return objectiveFunction

    #=======================================================================
    # DISPLAY
    def initDisplay( self, width):
        # COMPUTE PERIODICALLY EXTENDED LIST OF SPRINGS, FOR DISPLAY ONLY
        # widthDisplay = range(-1,2)
        self.widthDisplay = width
        self.lListDisplay, self.aListDisplay = pmC.springInBox( self.widthDisplay, self.network.lList, self.network.aList)

        # CLASS FOR EXPORTING NETWORKS IN VARIOUS FORMATS
        self.networkExport = pmE.exportPeriodic( self.vert.detach().cpu().numpy(), self.u0.detach().cpu().numpy(), self.v0.detach().cpu().numpy(), self.network.lList, self.network.aList, self.lListDisplay, self.aListDisplay, self.sArray.detach().cpu().numpy(), "outputFiles")
        # CLASS CONTAINING VALUES WHICH ARE MONITORED THROUGHOUT OPTIMIZATION
        self.monitor = monitor()
        self.logToTensorBoard = SummaryWriter("tensorBoardLogs") # FOR LOGGING IN TENSORBOARD

    def displayRestNetwork( self, filename):
        showGraphics( self.vertRest, self.u0Rest, self.v0Rest, self.widthDisplay, self.lListDisplay, self.aListDisplay, self.monitor.nIter, filename, self.logToTensorBoard)

    def displayCurrentNetwork( self, filename):
        showGraphics( self.vert.reshape(-1,2), self.u0, self.v0, self.widthDisplay, self.lListDisplay, self.aListDisplay, self.monitor.nIter, filename, self.logToTensorBoard)

    def appendTimeStep( self):
        self.networkExport.appendTimeStep( self.vert.detach().cpu().numpy(), self.u0.detach().cpu().numpy(), self.v0.detach().cpu().numpy(), self.sArray.detach().cpu().numpy())    

    def displayProgress( self, currentDoF):

        self.dofToNetwork( currentDoF)

        sys.stdout.write(f"iter = { self.monitor.nIter:02d} fun Value = {self.monitor.monitorObjectiveFunction}\r")
        tensorBoardScalarLog = True
        if tensorBoardScalarLog:
            self.logToTensorBoard.add_scalars(
                "geometric", {
                    "length": self.monitor.monitorLength.item(),
                    "angle": self.monitor.monitorAngle.item()
                },
                global_step=self.monitor.nIter,walltime=True)        
            self.logToTensorBoard.add_scalars(
                "constraint", {
                    "sliding constraint": self.monitor.monitorSlidingConstraint.item(),
                    "barycenter constraint": self.monitor.monitorBarycenterConstraint.item()
                },
                global_step=self.monitor.nIter,walltime=True)
            self.logToTensorBoard.add_scalars(
                "weightedGeometric", {
                    "weighted length": self.monitor.monitorWeightedLength.item(),
                    "weighted angle": self.monitor.monitorWeightedAngle.item()
                },
                global_step=self.monitor.nIter,walltime=True)        
            self.logToTensorBoard.add_scalars(
                "weightedConstraint", {
                    "weighted sliding constraint": self.monitor.monitorWeightedSlidingConstraint.item(),
                    "weighted barycenter constraint": self.monitor.monitorWeightedBarycenterConstraint.item(),
                },
                global_step=self.monitor.nIter,walltime=True)        
            
        tensorBoardImageLog = False
        stepIterSaveImage = 5
        if tensorBoardImageLog and self.monitor.nIter % stepIterSaveImage == 0:
            showGraphics( self.vert, self.u0, self.v0, self.widthDisplay, self.lListDisplay, self.aListDisplay, self.monitor.nIter, "currentNetwork", self.logToTensorBoard)

        saveNetworkAsVtkAtEachIteration = True
        if saveNetworkAsVtkAtEachIteration:
            self.appendTimeStep()    

        self.monitor.timeStep()

# END OF NETWORK CLASS0
#================================================================================

#-------------------------------------------------------------
# VECTOR FUNCTIONS MEASURING GEOMETRIC INTRINSINCS OF NETWORK
#-------------------------------------------------------------

# VECTOR OF ALL LENGTHS OF LENGTH SPRINGS
def lengthOfLengthSpring( vert, u0, v0, pIndex, uTrans, vTrans):
    # TWO NEXT LINES VERY IMPORTANT: SPARSE ENCODING OF ALL VERTICES INVOLVED IN THE LENGTH SPRINGS
    # ALLOWED THANKS TO THE THREE CONSTANT ARRAYS pIndex, uTrans AND vTrans, COMPUTED IN A PREPROCESS STEP
    beginPoint = vert[pIndex[0]] + uTrans[0][:,np.newaxis] * u0 + vTrans[0][:,np.newaxis] * v0
    endPoint = vert[pIndex[1]] + uTrans[1][:,np.newaxis] * u0 + vTrans[1][:,np.newaxis] * v0

    length = torch.linalg.vector_norm( endPoint - beginPoint, axis=1)
    return length

# HELP FUNCTION FOR COMPUTING CROSS-PRODUCTS OF MULTIPLE 2D VECTORS
def computeMultiple2dCrossProduct( a, b):
    ap = F.pad( a, (0,1)) # ADD 0 TO TRANSFORM THE 2D VECTORS a INTO 3D VECTORS ap
    bp = F.pad( b, (0,1))
    vectorCross = torch.cross( ap, bp, dim=1) # NOW THAT WE HAVE 3D VECTORS, COMPUTE THE CROSS PRODUCT
    return vectorCross[...,2] # ONLY RETURNS THE LAST COORDINATE, THE FIRST TWO ARE ZERO

# VECTOR OF ALL ANGLES OF ANGLES SPRINGS
def angleOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans):

    # THREE NEXT LINES VERY IMPORTANT: SPARSE ENCODING OF ALL VERTICES INVOLVED IN THE ANGLE SPRINGS
    # ALLOWED THANKS TO THE THREE CONSTANT ARRAYS pIndex, uTrans AND vTrans, COMPUTED IN A PREPROCESS STEP
    beginPoint = vert[pIndex[0]] + uTrans[0][:,np.newaxis] * u0 + vTrans[0][:,np.newaxis] * v0
    centerPoint = vert[pIndex[1]] + uTrans[1][:,np.newaxis] * u0 + vTrans[1][:,np.newaxis] * v0
    endPoint = vert[pIndex[2]] + uTrans[2][:,np.newaxis] * u0 + vTrans[2][:,np.newaxis] * v0

    firstEdge = centerPoint - beginPoint
    secondEdge = endPoint - centerPoint

    firstEdgeNorm = torch.linalg.vector_norm( firstEdge, axis=1)
    secondEdgeNorm = torch.linalg.vector_norm( secondEdge, axis=1)

    crossProduct = computeMultiple2dCrossProduct( firstEdge, secondEdge)
    normProduct = firstEdgeNorm * secondEdgeNorm
    scalarProduct = torch.sum( firstEdge * secondEdge, dim=-1) # SCALAR PRODUCTS OF ROWS

    # curvatureVector = 2. * crossProduct / (normProduct + scalarProduct)

    angle = 2. * torch.atan2( crossProduct, normProduct + scalarProduct)

    displayLog = False
    if displayLog:
        print(firstEdge)
        print(secondEdge)
        print(crossProduct)
        print(normProduct)
        print(scalarProduct)
        print(f"(norm + scalar, cross)= ({normProduct + scalarProduct}, {crossProduct})")
        print(f"angle = {angle}")

    return angle

# VECTOR OF ALL VORONOI WEIGHTS OF ANGLES SPRINGS
def voronoiWeightOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans):

    # THREE NEXT LINES VERY IMPORTANT: SPARSE ENCODING OF ALL VERTICES INVOLVED IN THE ANGLE SPRINGS
    # ALLOWED THANKS TO THE THREE CONSTANT ARRAYS pIndex, uTrans AND vTrans, COMPUTED IN A PREPROCESS STEP
    beginPoint = vert[pIndex[0]] + uTrans[0][:,np.newaxis] * u0 + vTrans[0][:,np.newaxis] * v0
    centerPoint = vert[pIndex[1]] + uTrans[1][:,np.newaxis] * u0 + vTrans[1][:,np.newaxis] * v0
    endPoint = vert[pIndex[2]] + uTrans[2][:,np.newaxis] * u0 + vTrans[2][:,np.newaxis] * v0

    firstEdge = centerPoint - beginPoint
    secondEdge = endPoint - centerPoint

    firstEdgeNorm = torch.linalg.vector_norm( firstEdge, axis=1)
    secondEdgeNorm = torch.linalg.vector_norm( secondEdge, axis=1)

    voronoiWeight = 0.5 * (firstEdgeNorm + secondEdgeNorm)

    displayLog = False
    if displayLog:
        print(f"firstEdgeNorm = {firstEdgeNorm}")
        print(f"secondEdgeNorm = {secondEdgeNorm}")
        print(f"voronoi weight = {voronoiWeight}")

    return voronoiWeight

#-------------------------------------------------------------
# PREPROCESS FOR EFFICIENT SPARSE ENCODING OF ALL VERTICES IN SPRINGS
#-------------------------------------------------------------

def constantArrayForSystemOfVertex( springList, dim):
    pIndex = [ [] for i in range(dim)]  # INITIALIZE LIST OF EMPTY LISTS
    uTrans = [ [] for i in range(dim)] 
    vTrans = [ [] for i in range(dim)] 
    sIndex = []
    for s in springList:
        # SPRING s = [ [i,j,k], [l,m,n], ... ] i,l: INDEX OF UNIT CELL VERTICES, j,k,m,n: INDEX OF PERIODIC CELL
        for p in range(dim): # TWO POINTS IN A LENGTH SPRING
            pIndex[p].append( s[p][0]) # INDEX OF UNIT CELL VERTEX
            uTrans[p].append( s[p][1]) # NUMBER OF UNITS TO TRANSLATE IN THE U PERIODIC DIRECTION
            vTrans[p].append( s[p][2]) # SAME AS PREVIOUS FOR THE V PERIODIC DIRECTION
        sIndex.append( s[dim]) # INDEX OF SPRING STIFFNESS 
    return pIndex, np.array(uTrans), np.array(vTrans), sIndex

#-------------------------------------------------------------
# OBJECTIVE FUNCTIONS
#-------------------------------------------------------------

def stretchingPotentialEnergy( vert, u0, v0, pIndex, uTrans, vTrans, lengthAtRest, sIndexLength, stiffnessArray):
    length = lengthOfLengthSpring( vert, u0, v0, pIndex, uTrans, vTrans)
    relativeDeltaLength = (length - lengthAtRest) / lengthAtRest
    weightedSquareRelativeDeltaLength = stiffnessArray[sIndexLength] * relativeDeltaLength.square() * lengthAtRest
    result = 0.5 * weightedSquareRelativeDeltaLength.sum()
    displayLog = False
    if displayLog:
        print(f"length = {length}")
        print(f"lengthAtRest = {lengthAtRest}")
        print(f"relativeDeltaLength = {relativeDeltaLength}")
        print(f"stiffnessArray = {stiffnessArray}")
        print(f"sIndexLength = {sIndexLength}")
        print(f"stiffnessArray[sIndexLength] = {stiffnessArray[sIndexLength]}")
    return result

def bendingPotentialEnergy( vert, u0, v0, pIndex, uTrans, vTrans, angleAtRest, voronoiWeight, sIndexAngle, stiffnessArray):
    angle = angleOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans)
    deltaAngle = angle - angleAtRest

    squareDeltaAngle = deltaAngle.square()
    weightedSquareDeltaAngle = stiffnessArray[sIndexAngle] * squareDeltaAngle / voronoiWeight
    result = 0.5 * weightedSquareDeltaAngle.sum()
    displayLog = False
    if displayLog:
        print(f"angle = {angle}")
        print(f"angleAtRest = {angleAtRest}")
        print(f"deltaAngle = {deltaAngle}")
        print(f"voronoiWeight = {voronoiWeight}")
        print(f"sIndexAngle = {sIndexAngle}")
        print(f"stiffnessArray = {stiffnessArray}")
        print(f"stiffnessArray[sIndexAngle] = {stiffnessArray[sIndexAngle]}")
        print(f"spring bending energy = {result}")
    return result

def showGraphics( vert, u0, v0, widthDisplay, lListDisplay, aListDisplay, nIter, filename, logToTensorBoard):

    pmV.initPeriodicVisualization( vert.detach().cpu().numpy(), u0.detach().cpu().numpy(), v0.detach().cpu().numpy(), lListDisplay)
    pmV.displayPeriodicGrid( widthDisplay)
    pmV.displaySpringList( lListDisplay, aListDisplay)
    pmV.showFigure( filename, display=False, save=True)

    imageAsTensor = io.read_image(filename+".png")
    print(f"je sauve l'image {filename} a l'iteration {nIter}")
    logToTensorBoard.add_image( "networkImage", imageAsTensor, nIter)

#---------------------------------------------------------------
# MAIN ROUTINE: FIND EQUILIBRIUM UNDER UNIAXIAL STRETCHING
#---------------------------------------------------------------
def uniaxialStretchEquilibrium( myNetwork: networkOptim, lmbda, theta):
    
    print("="*32)

    # EXPORT REST CONFIGURATION
    myNetwork.appendTimeStep()

    # LOAD NETWORK
    myNetwork.uniaxialLoading( lmbda, theta)

    # EXPORT LOAD CONFIGURATION
    myNetwork.appendTimeStep()
    
    # DISPLAY STRETCHING AND BENDING ENERGIES BEFORE OPTIMIZATION
    stretchingWorkAfterLoading = myNetwork.stretchingPotentialEnergy()
    bendingWorkAfterLoading = myNetwork.bendingPotentialEnergy()
    print(f"stretchingWorkAfterLoading = {stretchingWorkAfterLoading}")
    print(f"bendingWorkAfterLoading = {bendingWorkAfterLoading}")
    print(f"totalWork after loading and before optimization = {stretchingWorkAfterLoading + bendingWorkAfterLoading}")

    print("="*16+" STARTING OPTIMIZATION "+"="*16)
    start = time.time()

    # EXTRACT DEGREES OF FREEDOM FROM CURRENT CONFIGURATION, WHICH IS THE LOADING CONFIGURATION
    # THIS IS THE STARTING POINT OF THE OPTIMIZATION
    doF = myNetwork.networkToDoF()

    # LAUNCH OPTIMIZATION
    # res = minimize( myNetwork.objectiveFunctionDeltaLengthAngleAndConstraint, doF, method='newton-cg', tol=1.e-20, disp=False, callback=myNetwork.displayProgress)
    res = minimize( myNetwork.objectiveFunctionDeltaLengthAngleAndConstraint, doF, method='newton-cg', tol=1.e-20, disp=False, callback=myNetwork.displayProgress)
    # res = torchmin.newton._minimize_newton_cg( objectiveFunctionDeltaLengthAngleAndConstraint, doF, twice_diffable = True, xtol=1.e-20, disp=False, callback=myNetwork.displayProgress)
    
    stop = time.time()
    print("="*16+f" OPTIMIZATION ACHIEVED IN {round(stop-start,2)}s AND {myNetwork.monitor.nIter} time steps"+"="*16)

    print(f"objective function at optimum = {res.fun.item()}")
    print(f"norm of gradient at optimum = {torch.linalg.vector_norm(res.grad)}")
    print(f"sliding constraint at optimum = {myNetwork.monitor.monitorSlidingConstraint}")
    print(f"barycentric constraint at optimum = {myNetwork.monitor.monitorBarycenterConstraint}")

    # COMPUTE GRADIENTS OF STRETCHING AND BENDING ENERGIES RELATIVE TO THE VERTICES AND THE PERIODICITY VECTORS
    myNetwork.vert = myNetwork.vert.clone().detach().requires_grad_(True).to(torchDevice) # CLONE VERTICES INTO GRADIENT ENABLED TENSOR
    myNetwork.u0 = myNetwork.u0.clone().detach().requires_grad_(True).to(torchDevice) # CLONE PERIODICITY VECTORS INTO GRADIENT ENABLED TENSORS
    myNetwork.v0 = myNetwork.v0.clone().detach().requires_grad_(True).to(torchDevice)
    stretchingWorkEquilibrium = myNetwork.stretchingPotentialEnergy()  # RECOMPUTE STRETCHING ENERGY, BENDING ENERGY AND THEIR SUM
    bendingWorkEquilibrium = myNetwork.bendingPotentialEnergy()
    totalWorkEquilibrium = stretchingWorkEquilibrium + bendingWorkEquilibrium

    totalWorkEquilibrium = stretchingWorkEquilibrium + bendingWorkEquilibrium
    print("="*32)
    print(f"stretchingWorkEquilibrium = {stretchingWorkEquilibrium}")
    print(f"bendingWorkEquilibrium = {totalWorkEquilibrium}")
    print(f"totalWorkEquilibrium = {totalWorkEquilibrium}")
    totalWorkEquilibrium.backward()
    print( f"|| d(totalWork)/d(vert) || = {np.linalg.norm(myNetwork.vert.grad.detach().cpu().numpy())}")
    print( f"d(totalWork)/d(u0) = {myNetwork.u0.grad.detach().cpu().numpy()}")
    print( f"d(totalWork)/d(v0) = {myNetwork.v0.grad.detach().cpu().numpy()}")
    print("="*32)

    # showGraphics( res.x.reshape(-1,2), myNetwork.u0, myNetwork.v0, myNetwork.widthDisplay, myNetwork.lListDisplay, myNetwork.aListDisplay, myNetwork.monitor.nIter, "optimNetwork", myNetwork.logToTensorBoard)

    myNetwork.appendTimeStep()
    myNetwork.networkExport.finalizeExport()

    print("="*16+" POST-PROCESSING "+"="*16)
    periodicityReference = torch.vstack( (myNetwork.u0Rest, myNetwork.v0Rest)).detach().cpu().numpy().transpose()
    periodicityFinal = torch.vstack( (myNetwork.u0, myNetwork.v0)).detach().cpu().numpy().transpose()

    macroscopicForces = np.zeros((2,2))
    macroscopicForces[ :, 0] = myNetwork.u0.grad.detach().cpu().numpy()
    macroscopicForces[ :, 1] = myNetwork.v0.grad.detach().cpu().numpy()

    strainTensor, stressTensor = macroStressAndStrainTensor.macroStressAndStrainTensor( periodicityReference, periodicityFinal, macroscopicForces, showPlot=False)
    # print(f"periodicityReference={periodicityReference}")
    # print(f"periodicityEquilibrium={periodicityFinal}")
    # print(f"macroscopicForces={energyGradient}")

    # THE FOLLOWING CALLS ARE ONLY WRITTEN FOR SHOWING WHAT IS POSSIBLE TO COMPUTE
    poissonRatioFinite = poissonRatioFromPeriodicityVectors.poissonRatio( periodicityReference, periodicityFinal, theta)
    print(f"PR from strain and angle = {poissonRatioFinite}")
    oneMesoPR, oneMesoYoung, oneMesoMagnitudeStress = macroStressAndStrainTensor.PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor, stressTensor)
    print(f"PR meso = {oneMesoPR}")
    print(f"Young meso = {oneMesoYoung}")
    print("="*32)

    return strainTensor, stressTensor
    
def exportTensor2D( u, file):
    uN = u.detach().cpu().numpy()
    file.write( f"{uN[0]} {uN[1]}\n")
           
def testComputeAngle( vert, u0, v0, aUnitList):
    pIndex, uTrans, vTrans = constantArrayForSystemOfVertex( aUnitList, dim=3)
    uTrans = torch.Tensor(uTrans)
    vTrans = torch.Tensor(vTrans)
    vert = torch.tensor( vert, requires_grad=True)
    u0 = torch.tensor( u0, requires_grad=True)
    v0 = torch.tensor( v0, requires_grad=True)
    angle = angleOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans)
    print(angle)

def main( filename, lmbda, theta):
    global torchDevice

    torch.set_printoptions(precision=14)

    # torchDevice = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    torchDevice = "cpu"

    myNetwork = networkOptim( filename)
    myNetwork.initDisplay( range(-2,3))
    # myNetwork.initDisplay( range(-1,2))
    # myNetwork.initDisplay( range(1))

    return( uniaxialStretchEquilibrium( myNetwork, lmbda, theta))
    
if __name__=="__main__":
    filename = sys.argv[1]
    lmbda = float(sys.argv[2])
    theta = float(sys.argv[3])
    main( filename, lmbda, theta)


#=====================================================
# CONSTRAINT MINIMIZATION
# res = minimize_constr( objectiveFunctionLengthAndCurvatureVector, vert, \
#                           constr=dict( \
#     fun=lambda x: constraintFunction(x), \
#     lb=0., ub=0.), disp=3)

#=====================================================
# UNCONSTRAINED MINIMIZATION
# 'newton-exact','newton-cg','cg','bfgs','l-bfgs'
# newton-exact very slow
# newton-cg fast and accurate
# cg fast but innacurate
# bfgs slow and innacurate
# l-bfgs fast but innacurate
# BEST IS NEWTON-CG
