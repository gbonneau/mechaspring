from matplotlib import pyplot as plt
import numpy as np

# a is a matrix of shape nRow x 2
# b is a matrix of shape 1 x 2
# returns a vector of dimension nRow containing the cross product of each line of a by b
def drawResult( point, title, thisPlot):

    thisPlot.set_xlim( -1.2, 1.2)
    thisPlot.set_ylim( -1.2, 1.2)
    thisPlot.set_xticks([])
    thisPlot.set_yticks([])

    thisPlot.scatter( point[:,0], point[:,1], c="tab:blue") # DRAW INPUT POINTS
    for pi in range(point.shape[0]):
        thisPlot.plot( [point[pi,0], 0.], [point[pi,1], 0.], color="tab:blue") # DRAW SEGMENTS FROM INPUT POINTS TO CENTER
    thisPlot.scatter( [0],[0], c="tab:blue") # DRAW CENTER
    thisPlot.set_title(title)


def initMultiplePlots( nPlot):
    nRows = round(np.sqrt(nPlot))+1
    nColumns = nPlot // nRows + 1
    fig, ax = plt.subplots(nRows, nColumns)
    return nRows, nColumns, fig, ax

if __name__ == "__main__":
    nPoint = 6
    nGrid = 3
    nPlot = nPoint*2 + (nGrid+1)**2

    nRows, nColumns, fig, ax = initMultiplePlots( 5)

    # point = np.loadtxt(sys.argv[1])
    point = np.random.rand( nPoint,2)-np.array([0.5,0.5])
    normOfPoint = np.linalg.norm(point,axis=1)
    normedPoint = np.array(point / normOfPoint.reshape(nPoint,1))

    drawResult( normedPoint, "Rigid connection", ax[ 0,0])

    plt.show()


