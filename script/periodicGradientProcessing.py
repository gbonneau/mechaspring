import periodicMinimalVisualization
import periodicMinimalConstruct
import numpy as np
import sys
import matplotlib.pyplot as plt

def readGradientList( filename):
    per = np.loadtxt( filename)
    gradientList = [ [ int(row[0]), (int(row[1]), int(row[2]), int(row[3])), (float(row[4]), float(row[5]), float(row[6]))] for row in per]
    return gradientList

def displayAllGradient( gradientList):
    allGradient = np.array([g[2] for g in gradientList])
    allNormOfGradient = np.linalg.norm( allGradient, axis=1)
    allDirection = allGradient / allNormOfGradient[:,np.newaxis]
    maxScale = np.log(np.max(allNormOfGradient))
    finalScale = 0.3

    for i, g in enumerate(gradientList):
        periodicMinimalVisualization.displayGradient( g[1], finalScale * allNormOfGradient[i] * np.exp(-maxScale) * allDirection[i])

def displayAllPeriodicGradient( periodicGradient):
    gradientList = []
    for p in periodicGradient:
        for oneGradient in p[2]:
            gradientList.append( [ p[0], p[1], oneGradient ])
    displayAllGradient( gradientList)


def collectPeriodicGradient( gradientList):
    periodicGradient = [ [ g[0], g[1], []] for g in gradientList]
    for p in periodicGradient:
        for g in gradientList:
            if (periodicMinimalConstruct.periodicVertexEqual( g[1], p[1])):
                p[2].append(g[2])
                
    return periodicGradient

if __name__ == "__main__":

    vert, u0, v0, lList, aList, sList = periodicMinimalConstruct.networkFromIndirectSpringFile( sys.argv[1])

    fig,ax = plt.subplots()

    periodicMinimalVisualization.initPeriodicVisualizationInFigure( fig, ax, vert, u0, v0, lList)

    gradientList = readGradientList( sys.argv[2])
    periodicGradient = collectPeriodicGradient( gradientList)
    displayAllPeriodicGradient( periodicGradient)

    periodicMinimalVisualization.displaySpringList( lList, aList)

    displayAllGradient( gradientList)

    periodicMinimalVisualization.displayPeriodicGrid( range(1))

    periodicMinimalVisualization.showFigure()
