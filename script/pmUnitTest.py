import macroStressAndStrainTensor
import numpy as np
import sys

# periodicityReference=[[ 1.5        0.8660254]
#  [ 1.5       -0.8660254]]
# periodicityEquilibrium=[[ 1.35000005  0.92580257]
#  [ 1.35000005 -0.92580257]]
# macroscopicForces=[[-9.36918518e-03  1.87545947e-11]
#  [-9.36918518e-03 -1.87545947e-11]]

periodicVectorAtRest = np.array([[ 1.5, 0.8660254],[ 1.5, -0.8660254]]).transpose()
periodicVectorEquilibrium = np.array([[ 1.35000005, 0.92580257],[ 1.35000005, -0.92580257]]).transpose()
macroscopicForces = np.array([[ -9.36918518e-03, 1.87545947e-11],[-9.36918518e-03, -1.87545947e-11]]).transpose()

deformationGradient = periodicVectorEquilibrium @ np.linalg.inv( periodicVectorAtRest)
print(deformationGradient)

strainTensor, stressTensor = macroStressAndStrainTensor.macroStressAndStrainTensor( periodicVectorAtRest, periodicVectorEquilibrium, macroscopicForces, showPlot=True)

PR, youngModulus, magnitudeStress = macroStressAndStrainTensor.PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor, stressTensor)

print(PR)