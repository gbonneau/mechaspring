from scipy.spatial import Voronoi
from scipy.spatial import Delaunay
from datetime import datetime
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import meshio
import sys

def validPoint( p):
    return (p[0]>-1.) and (p[0]<2.) and (p[1]>-1.)and (p[1]<2.)

def validRegion( points, simplex):
    for v in simplex:
        if not validPoint( points[v]):
            return False
    return True

def clockWisePoints2D( a, b, c):
    u = a-b
    v = c-b
    return np.sign( np.cross(u,v)) == 1. 

# GENERATE ALL VECTORS OF DIMENSION dimension WITH COORDINATES IN ONE OF THE offsets VALUES
# THERE ARE len(offsets)^dimension VECTORS LIKE THAT
# EXAMPLE: offsets = [-1.,0.,1.] dimension = 2
#          output = [ [-1.,-1.], [-1.,0.], [-1.,1.], [0.,-1.], [0.,0.], [1.,0.], [1.,-1.], [1.,0.], [1.,1.] ]
def generateNeighborhoodVectors( offsets, dimension):
    points = np.zeros((1,dimension))
    for d in range(dimension):
        transVect = np.zeros((dimension,))
        newPoints = np.zeros((0,dimension))
        for offset in offsets:
            transVect[d] = offset
            transPoint = [ cP + transVect for cP in points]
            newPoints = np.concatenate(( newPoints, transPoint))
        points = newPoints
    return newPoints

def random_periodic_sequential_adsorption(d, dimension, num_tests=3000):
    # np.random.seed(43)
    np.random.seed(int(datetime.now().timestamp()))
    points = []
    zeroVector = np.zeros( (dimension,)) # VECTOR [0,...,0]
    oneVector = np.ones( (dimension,)) # VECTOR [1,...,1]

    for i in tqdm(range(num_tests)):
        # GENERATE CANDIDATE POINT IN A REGION WITH COORDINATES BETWEEN 0. AND 1
        point_candidate = np.random.random(dimension)
        accept = True
        for point in points:
            # COMPARE CANDIDATE POINT WITH ALL NEIGHBORING PERIODIC DUPLICATED OF ACCEPTED POINTS
            # STOP AS A SOON AS A POINT TOO CLOSE IS FOUND
            for v in neighborhoodVectors:
                    if np.linalg.norm( point + v[0] * u0 + v[1] * v0 - point_candidate) < d:
                        accept = False
                        break
        # IF DISTANCE FROM CANDIDATE POINT TO ALL PERIODIC DUPLICATE OF ALL ACCEPTED POINTS
        # IS LARGER THAT d, ADD THE POINT TO THE LIST OF ACCEPTED POINTS
        if accept:
            points.append(point_candidate)
    return points

# RETURN TRUE IFF COORDINATES LIE BETWEEN -1. AND 2.
def validPoint( p):
    valid = True
    for c in p:
        valid = valid and (c>-1.) and (c<2.)
    return valid

# RETURN TRUE IF ALL POINTS OF simplex HAVE COORDINATES BETWEEN -1. AND 2.
def validSimplex( points, simplex):
    simplexIsValid = True
    for vi in simplex:
        vertexIsValid = validPoint( points[vi])
        # print(f"validity of point[{vi}] = {points[vi]} = {vertexIsValid}")
        simplexIsValid = simplexIsValid and vertexIsValid
    return simplexIsValid

def objWrite( filename, vert, lList):
    with open( filename, "w") as f:
        for v in vert:
            f.write(f"v {v[0]} {v[1]} 0.\n")
        for e in lList:
            f.write(f"l {e[0]+1} {e[1]+1}\n")
    f.close()

if __name__ == "__main__":

    dimensionGlobal = 2

    u0 = np.array([1.,0.])
    pi3 = np.pi / 3.
    v0 = np.array([np.cos(pi3), np.sin(pi3)])

    if (len(sys.argv) != 2):
        print(f"{sys.argv[0]} generate periodic Delaunay triangulation with a given input minimal distance between samples")
        print(f"the unit cell samples are randomly generated between 0 and 1")
        print(f"a smaller minimal distance produces more points!")
        print(f"usage:  {sys.argv[0]} <minimalDistance>")
    smallestDistance = float(sys.argv[1])

    neighborhoodVectors = generateNeighborhoodVectors( [-1.,0.,1.], dimensionGlobal) # VECTORS FOR TESTING NEIGHBORING PERIODIC VERTICES
    duplicateVectors = generateNeighborhoodVectors( [-2.,-1.,0.,1.,2.], dimensionGlobal) # VECTORS FOR PERIODICALLY DUPLICATING THE UNIT VERTICES BEFORE COMPUTING THE DELAUNAY TRIANGULATION

    unitPoints = random_periodic_sequential_adsorption( smallestDistance, dimensionGlobal)
    # CONVERT LIST TO NUMPY ARRAY
    unitPoints = np.array(unitPoints)
    print(f"{len(unitPoints)} points have been generated")

    # unitPoints HAVE COORDINATES BETWEEN 0 AND 1
    # WE REPEAT PERIODICALLY THESE POINTS
    # SO THAT THE NEW SET OF POINTS COVERS THE RANGE OF COORDINATES BETWEEN -2. AND 3.
    points = np.zeros((0, dimensionGlobal))
    for translateVector in duplicateVectors:
        points = np.concatenate( (points, unitPoints + translateVector[0] * u0 + translateVector[1] * v0))

    # COMPUTE THE DELAUNAY TRIANGULATION WITH THESE POINTS WITH COORDINATES BETWEEN -2. AND 3.
    # monDelaunay = Delaunay( points)
    monVoronoi = Voronoi( points)

    np.savetxt( "points.txt", points)

    validVoronoiRegion = []
    for aRegion in monVoronoi.regions:
        # print(aRegion)
        if validRegion(monVoronoi.vertices, aRegion):
            validVoronoiRegion.append(aRegion)
    periodicSimplicies = []

    lList=[]
    aList=[]

    for aRegion in validVoronoiRegion:
    # for aRegion in validVoronoiRegion[3:5]:
        if (dimensionGlobal == 2) and (len(aRegion) >= 3):
            if not clockWisePoints2D( monVoronoi.vertices[aRegion[0]], monVoronoi.vertices[aRegion[1]], monVoronoi.vertices[aRegion[2]]):
                aRegion.reverse()
        nP = len(aRegion)
        for i, v in enumerate( aRegion):
            assert(v == aRegion[i])
            nextV = aRegion[(i+1) % nP]
            nextNextV = aRegion[(i+2) % nP]
            if v < nextV:
            # if True:
                lList.append( [v, nextV])
            aList.append( [ v, nextV, nextNextV])

    np.savetxt( "2Spring.txt", np.array(lList).astype(int))
    np.savetxt( "3Spring.txt", np.array(aList).astype(int))
    
    mesh=meshio.Mesh( monVoronoi.vertices, [("line", lList)])
    mesh.write("voronoiMesh2D.vtk")

    objWrite( "voronoi2D.obj", monVoronoi.vertices, lList)

    print(f"Periodic Voronoi tesselation in dimension {dimensionGlobal}")
    print("output Voronoi tesselation is saved in files voronoi.obj delaunayMesh.vtk and [points.txt, 2Spring.txt, 3Spring.txt]")


