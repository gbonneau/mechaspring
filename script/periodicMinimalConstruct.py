#=====================================================================
# IMPLEMENTATION OF A STRUCTURE HANDLING PERIODIC PHYSICAL SYSTEMS
# G.P. BONNEAU, APRIL 2023, BERLIN
#
# IT IS IMPLEMENTED IN 2D BUT MAY BE TRIVIALLY EXTENDED TO 3D
#
# IT IS DEFINED BY
#
# 1) A SET OF N "UNIT CELL" POINTS vert.
#    THESE POINTS MAY BE ARBITRARY AND NEED NOT TO BE INSIDE A PARTICULAR GEOMETRIC DOMAIN
#
# 2) TWO PERIODICITY VECTORS u0 v0
#    THE ONLY CONSTRAINTS IS THAT THESE TWO VECTORS SHOULD SPAN THE PLANE (I.E. NOT BE COLINEAR)
#
# 3) THE WHOLE PLANE IS SAMPLED BY PERIODIC POINTS DEFINED BY TRIPLET OF INTEGERS [i,j,k]
#    WHERE i IS AN INTEGER INDEXING THE UNIT CELL POINTS
#    j, k ARE INTEGERS ENCODING THE TRANSLATION ALONG THE PERIODIC VECTORS u0, v0
#    THE GEOMETRIC POSITION OF PERIODIC POINT [i,j,k] IS THEREFORE vert[i] + j * u0 + k * v0
#
# 4) A SET OF PRIMITIVE PHYSICAL SYSTEMS,
#    EACH ONE DEPENDEND ON A FINITE NUMBER OF *PERIODIC* (NOT UNIT CELL) POINTS
#    IN THE CURRENT IMPLEMENTATION, LENGTH SPRINGS, NOTED l, DEPENDING ON TWO PERIODIC POINTS,
#    AND ANGULAR SPRINGS, NOTED a, DEPENDING ON THREE PERIODIC POINTS, ARE IMPLEMENTED
#    BUT IT CAN BE EASILY EXTENTED TO ANY NUMBER OF PERIODIC POINTS.
#
#    THEREFORE LENGTH SPRINGS ARE DEFINED BY TWO TRIPLET OF INTEGERS [ [i,j,k], [l,m,n] ]
#    AND ANGULAR SPRINGS ARE DEFINED BY THREE TRIPLET OF INTEGERS [ [i,j,k], [l,m,n], [p,q,r] ]
#
#
# 

import numpy as np
from random import randrange
from tqdm import tqdm
import lineCotangentCircle
import time
import sys

#----------------------------------------------------
# BOUNDING-BOX OF ALL PERIODIC POINTS INVOLVED IN A SET OF SPRINGS
# DIMENSION DEPENDANT
def boundingBox( vert, u0, v0, lList, aList):
    displayLog = False
    allPeriodicPoint = allPeriodicPointInSpringList( lList, aList)
    bbMin = [+sys.float_info.max, +sys.float_info.max]
    bbMax = [-sys.float_info.max, -sys.float_info.max]
    xLabel = 0
    yLabel = 1
    for pp in allPeriodicPoint:
        absolutePoint = periodicToAbsolute( pp, vert, u0, v0)
        if displayLog:
            print(absolutePoint)
        for label in [xLabel, yLabel]:
            bbMin[label] = min( bbMin[label], absolutePoint[label])
            bbMax[label] = max( bbMax[label], absolutePoint[label])
    return bbMin, bbMax

# DIMENSION DEPENDANT
def lengthSpringStatistic( vert, u0, v0, lList):
    if lList == []:
        return np.nan, np.nan, np.nan, np.nan
    length = []
    for lSpring in lList:
        p = [ periodicToAbsolute( lSpring[vi], vert, u0, v0) for vi in [0,1]]
        length.append( np.linalg.norm(p[1]-p[0]))
    return np.min(length), np.max(length), np.mean(length), np.std(length)

# DIMENSION DEPENDANT
def angleSpringStatistic( vert, u0, v0, aList):
    if aList == []:
        return np.nan, np.nan, np.nan, np.nan
    angle = []
    for aSpring in aList:
        p = [ periodicToAbsolute( aSpring[vi], vert, u0, v0) for vi in [0,1,2]]
        firstEdge = np.array(p[1] - p[0])
        secondEdge = np.array(p[2] - p[1])
        firstEdgeNorm = np.linalg.norm( firstEdge)
        secondEdgeNorm = np.linalg.norm( secondEdge)
        crossProduct = np.cross( firstEdge, secondEdge)
        normProduct = firstEdgeNorm * secondEdgeNorm
        scalarProduct = np.dot( firstEdge, secondEdge)
        oneAngle = 2. * np.arctan2( crossProduct, normProduct + scalarProduct)
        angle.append(oneAngle)
    return np.min( angle), np.max( angle), np.mean( angle), np.std( angle)

#----------------------------------------------------
# BARYCENTER OF ALL UNIQUE PERIODIC POINTS INVOLVED IN A SET OF SPRINGS
# AKA "BARYCENTER OF THE PHYSICAL SYSTEM"
# DIMENSION DEPENDANT
def barycenterSpringList( vert, u0, v0, lList, aList):
    allPeriodicPoint = allPeriodicPointInSpringList( lList, aList)
    barycenter = np.zeros( vert[0].shape) # VALID IN DIMENSION 2 AND 3 AS WELL
    for pp in allPeriodicPoint:
        absolutePoint = periodicToAbsolute( pp, vert, u0, v0)
        barycenter = barycenter + absolutePoint
    return barycenter / len(allPeriodicPoint)

#----------------------------------------------------
# RETURNS ALL UNIQUE PERIODIC POINTS - INTEGER TRIPLETS - INVOLVED IN A LIST OF LENGTH AND ANGULAR SPRINGS
# DIMENSION DEPENDANT
def allPeriodicPointInSpringList( lList, aList):
    pSet = set()
    for l in lList:
        pSet.add((l[0][0],l[0][1],l[0][2]))
        pSet.add((l[1][0],l[1][1],l[1][2]))
    for a in aList:
        pSet.add((a[0][0],a[0][1],a[0][2]))
        pSet.add((a[1][0],a[1][1],a[1][2]))
        pSet.add((a[2][0],a[2][1],a[2][2]))
    return pSet

#----------------------------------------------------
# RETURNS ALL UNIT CELL POINT INDICES INVOLVED IN A LIST OF LENGTH AND ANGULAR SPRINGS
# THIS IS USE ONLY FOR DEBUGGING AT THIS TIME
# DIMENSION AGNOSTIC
def allUnitCellPointInSpringList( lList, aList):
    pSet = set()
    for l in lList:
        pSet.add(l[0][0])
        pSet.add(l[1][0])
    for a in aList:
        pSet.add(a[0][0])
        pSet.add(a[1][0])
        pSet.add(a[2][0])
    return pSet

#----------------------------------------------------
# RETURN TRUE IFF A PERIODIC VERTEX [i,j,k] IS A UNIT CELL VERTEX
# DIMENSION DEPENDANT
def isUnitCell( pVert):
    return (pVert[1] == 0) and (pVert[2] == 0)

#----------------------------------------------------
# GIVEN A LIST OF LENGTH AND ANGULAR SPRINGS DEFINED AS TUPLET OF TRIPLETS OF INTEGERS
# RETURNS THE DICTIONNARY index AND THE LIST OF TRIPLETS invIndex
# SUCH THAT:
# index AND invIndex ENCODES ONLY THE PERIODIC VERTICES APPEARING IN THE LIST OF SPRINGS
#
# index[(i,j,k)] IS THE INTEGER INDEXING THE PERIODIC VERTEX (i,j,k)
#
#                IF (j=0 AND k=0) THEN index[(i,0,0)] < nAbsVertex
#                IF index[(i,j,k)] >= nAbsVertex THEN j!=0 OR k!=0
#
# IF a < nAbsVertex THEN invIndex[a] IS THE TRIPLET (i,0,0)
#                   WHERE a IS THE NEW INDEX OF UNIT CELL i
#                   I.E. index[(i,0,0)] = a
# IF a >= nAbsVertex THEN invIndex[a] IS THE TRIPLET (i,j,k)
#                    WHERE j!=0 OR k!=0 AND a IS THE NEW INDEX OF UNIT CELL i
#                    I.E. index[(i,0,0)] = a

# DIMENSION AGNOSTIC
def indexOneUnitCellVertex( oldTriplet, newTriplet, index, invIndex, nVertex):
    if tuple(oldTriplet) not in index:
        index[tuple(oldTriplet)] = nVertex
        invIndex.append(tuple(newTriplet))
        nVertex = nVertex+1
    return index, invIndex, nVertex

# DIMENSION DEPENDENT
def indexAllVertexInSpringList( lList, aList):
    index = {}
    invIndex = []
    nVertex = 0
    # INDEX ALL UNIT CELL VERTICES
    for s in lList + aList:
        for v in s[:-1]: # ALL BUT LAST INDEX ARE TRIPLET OF INTEGERS ENCODING PERIODIC VERTICES
            if isUnitCell( v):
                index, invIndex, nVertex = indexOneUnitCellVertex( v, v, index, invIndex, nVertex)
    # ADD UNIT CELL VERTICES FOR EACH NON UNIT CELL VERTICES WITHOUT CORRESPONDING UNIT CELL VERTICES
    for s in lList + aList:
        for v in s[:-1]:
            if not isUnitCell( v):
                # (v[0],0,0) IS A VALID KEY IFF THE UNIT CELL VERTEX APPEARS IN ONE OF THE SPRINGS
                # IF NOT, THEN A NEW UNIT-CELL VERTEX SHOULD BE ADDED
                unitV = (v[0],0,0)
                if unitV not in index:
                    # print( f"CAS TRES SPECIAL: SOMMET NON-UNIT SANS SOMMET UNIT CORRESPONDANT!!! {v[0]}")
                    index, invIndex, nVertex = indexOneUnitCellVertex( unitV, unitV, index, invIndex, nVertex)    
    # INDEX ALL NON UNIT CELL VERTICES
    nAbsVertex = nVertex
    for s in lList + aList:
        for v in s[:-1]:
            if not isUnitCell( v):
                # (v[0],0,0) IS A VALID KEY IFF THE UNIT CELL VERTEX APPEARS IN ONE OF THE SPRINGS
                # IF NOT, THEN A NEW UNIT-CELL VERTEX SHOULD BE ADDED
                unitV = (v[0],0,0)
                if unitV not in index:
                    print("CA VA PLANTER TOUT DE SUITE!!!")
                    print("CE PROBLEME AURAIT DU ETRE CORRIGE PAR LE PARCOURS PRECEDENT TRAITANT LES SOMMETS NON UNIT SANS SOMMET UNIT ASSOCIES")
                w = (index[unitV], v[1], v[2])
                index, invIndex, nVertex = indexOneUnitCellVertex( v, w, index, invIndex, nVertex)
    nPerVertex = nVertex - nAbsVertex

    return index, invIndex, nAbsVertex, nPerVertex

#----------------------------------------------------
# 3 FUNCTIONS TO TRANSLATE PERIODIC POINTS, LENGTH SPRINGS, ANGULAR SPRINGS
# DIMENSION DEPENDENT
def translatePeriodicPoint( p, i, j):
    return [p[0],p[1]+i,p[2]+j]

# RETURN LENGTH SPRING
# DIMENSION DEPENDENT
def translate2Spring(l, i, j):
    return [translatePeriodicPoint(l[0],i,j), translatePeriodicPoint(l[1],i,j), l[2]]

# RETURN ANGULAR SPRING
# DIMENSION DEPENDENT
def translate3Spring(a, i, j):
    return [translatePeriodicPoint(a[0],i,j), translatePeriodicPoint(a[1],i,j), translatePeriodicPoint(a[2],i,j), a[3]]

#----------------------------------------------------
# RANDOMLY TRANSLATE LENGTH AND ANGULAR SPRINGS
# USEFUL FOR TESTING INVARIANCE OF THE ENERGY FUNCTION AND THE OPTIMIZATION
# RETURN LENGTH SPRING
# DIMENSION DEPENDENT
def randomTranslate2Spring( l, iMax, jMax):
    return translate2Spring( l, randrange( iMax+1) - iMax//2, randrange( jMax+1) - jMax//2)

# RETURN ANGULAR SPRING
# DIMENSION DEPENDENT
def randomTranslate3Spring( a, iMax, jMax):
    return translate3Spring( a, randrange( iMax+1) - iMax//2, randrange( jMax+1) - jMax//2)

#----------------------------------------------------
# RETURNS THE LISTS OF ALL DUPLICATED SPRRINGS IN A BOX OF RANGE widthxwidth
# WIDTH MUST BE A INTERVAL, E.G. width = range(-1,2) or width = range(1)
# RETURN TWO LISTS OF LENGTH SPRING AND ANGULAR SPRING
# DIMENSION AGNOSTIC
def springInBox( width, lUnitList, aUnitList):
    lList=[]
    aList=[]

    for i in width:
        for j in width:
            lList = lList +[translate2Spring(l,i,j) for l in lUnitList] 
            aList = aList +[translate3Spring(a,i,j) for a in aUnitList]

    return lList, aList

#----------------------------------------------------
# TRANSLATE THE UNIT CELL VERTICES vert SO THAT THERE NEW BARYCENTER IS [0,0]
# DIMENSION AGNOSTIC
def translateVerticesAroundBarycenter( vert):
    barycenter = np.mean( vert, axis=0)
    return vert - barycenter

#----------------------------------------------------
# RETURNS THE GEOMETRIC POSITION OF A PERIODIC VERTEX v=[i,j,k]
# DIMENSION DEPENDENT
def periodicToAbsolute( v, vert, u0, v0):
    return vert[v[0]] + v[1] * u0 + v[2] * v0

#----------------------------------------------------
# RETURNS TRUE IF POINTS V1 AND V2 ARE EQUAL UP TO PERIODICITY
# DIMENSION AGNOSTIC
def periodicVertexEqual( v1, v2):
    return v1[0] == v2[0]

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRINGS l1 AND l2 ARE PERIODIC DUPLICATES
# DO NOT TAKE INTO ACCOUNT ORIENTATION OF THE EDGE
# DIMENSION AGNOSTIC
def periodic2SpringEqual( l1, l2):
    result = periodic2SpringEqualOriented( l1, l2) or periodic2SpringEqualOriented( reverse2SpringDirection(l1), l2)
    return result

# DIMENSION AGNOSTIC
def reverse2SpringDirection( l):
    return [l[1],l[0],l[2]] # l[2] IS THE STIFFNESS INDEX

# DIMENSION DEPENDENT
def periodic2SpringEqualOriented( l1, l2):
    isEqual =  (l1[0][0] == l2[0][0]) and (l1[1][0] == l2[1][0]) # FIRST AND SECOND UNIT VERTICES SHOULD BE EQUAL
    uTranslate = l2[0][1] - l1[0][1] # Translation of the first vertex from the first spring to the second spring
    vTranslate = l2[0][2] - l1[0][2]
    isEqual = isEqual and ((l2[1][1] - l1[1][1]) == uTranslate) # the translation should be the same for the second vertex
    isEqual = isEqual and ((l2[1][2] - l1[1][2]) == vTranslate)
    return isEqual

#----------------------------------------------------
# RETURNS TRUE IF ANGULAR SPRINGS a1 AND a2 ARE PERIODIC DUPLICATES
# DIMENSION DEPENDENT
def periodic3SpringEqual( a1, a2):
    isEqual =  (a1[0][0] == a2[0][0]) and (a1[1][0] == a2[1][0]) and (a1[2][0] == a2[2][0]) # THE THREE UNIT VERTICES OF a1 SHOULD BE THE SAME AS THOSE OF a2
    uTranslate = a2[0][1] - a1[0][1] # Translation of the first vertex from the first spring to the second spring
    vTranslate = a2[0][2] - a1[0][2]
    isEqual = isEqual and ((a2[1][1] - a1[1][1]) == uTranslate) # the translation should be the same for the second vertex
    isEqual = isEqual and ((a2[1][2] - a1[1][2]) == vTranslate)
    isEqual = isEqual and ((a2[2][1] - a1[2][1]) == uTranslate) # the translation should be the same for the third vertex
    isEqual = isEqual and ((a2[2][2] - a1[2][2]) == vTranslate) 
    return isEqual

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRING l DEPENDS ON PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def vertexIn2Spring( v, l):
    return periodicVertexEqual(v, l[0]) or periodicVertexEqual(v,l[1])

#----------------------------------------------------
# RETURNS TRUE IFF ANGULAR SPRING a DEPENDS ON PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def vertexIn3Spring( v, a):
    return periodicVertexEqual(v, a[0]) or periodicVertexEqual(v,a[1]) or periodicVertexEqual(v,a[2])

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRING l IS A PERIODIC DUPLICATE OF ONE OF THE SPRINGS IN TEH LIST lList
# DIMENSION AGNOSTIC
def springIn2SpringList( l, lList):
    for lCompare in lList:
        if (periodic2SpringEqual( l, lCompare)):
            return True
    return False

#----------------------------------------------------
# RETURNS TRUE IFF ANGULAR SPRING a IS A PERIODIC DUPLICATE OF ONE OF THE SPRINGS IN THE LIST aList
# DIMENSION AGNOSTIC
def springIn3SpringList( a, aList):
    for aCompare in aList:
        if (periodic3SpringEqual( a, aCompare)):
            return True
    return False

#----------------------------------------------------
# REMOVE PERIODIC DUPLICATES IN THE LIST OF LENGTH SPRINGS lList
# DIMENSION AGNOSTIC
def suppressRedundant2Spring( lList):
    lListUnique = []
    for l in tqdm(lList):
        if not springIn2SpringList( l, lListUnique):
            lListUnique.append(l)
        # else:
        #     print(f"2Spring {l} is redundant in list")
    return lListUnique

#----------------------------------------------------
# REMOVE PERIODIC DUPLICATES IN THE LIST OF ANGULAR SPRINGS aList
# DIMENSION AGNOSTIC
def suppressRedundant3Spring( aList):
    aListUnique = []
    for a in tqdm(aList):
        if not springIn3SpringList( a, aListUnique):
            aListUnique.append(a)
        # else:
        #     print(f"3Spring {a} is redundant in list")
    return aListUnique

#----------------------------------------------------
# FROM A LIST OF LENGTH SPRINGS lList AND ANGULAR SPRING aList,
# RETURNS THE LIST OF THOSE SPRINGS DEPENDING FROM PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def filterSpringByVertex( v, lList, aList):
    checkValidity = lambda l: vertexIn2Spring( v, l)
    lListContainsVertex = list(filter( checkValidity, lList))
    checkValidity = lambda a: vertexIn3Spring( v, a)
    aListContainsVertex = list(filter( checkValidity, aList))
    return lListContainsVertex, aListContainsVertex

#----------------------------------------------------
# RETURNS THE COORDINATE OF A 2D VECTOR v IN FRAME (u0,v0)
# DIMENSION DEPENDENT
def periodicFrameCoordinate( v, u0, v0):
    return np.linalg.solve( np.array([u0,v0]).transpose(),v)

#----------------------------------------------------
# GIVEN AN ARBITRARY 2D POINT p AND TWO PERIODICY VECTORS u0, v0,
# RETURNS THE UNIQUE POINT q AND TWO INTEGERS j, k
# SUCH THAT p = q + j u0 + k v0
# AND q = alpha u0 + beta v0, with 0 <= alpha < 1 and 0 <= beta < 1
# IN OTHER WORDS, q IS THE UNIQUE PERIODIC DUPLICATE OF p WHICH LIES INSIDE THE AFFINE
# PARALLELOGRAM { (0,0) + alpha u0 + beta v0, 0 <= alpha < 1 and 0 <= beta < 1}
# DIMENSION DEPENDENT
def unitCellPeriodicDuplicate( p, u0, v0):
    intCoord = periodicIntegerCoordinate( p, u0, v0)
    q = p - intCoord[0] * u0 - intCoord[1] * v0
    return q, intCoord[0], intCoord[1]

#----------------------------------------------------
# CHANGE A UNIT CELL VERTEX BY A PERIODIC DUPLICATE
# CHANGE ALL OCCURENCES OF [ i, m, n] IN ANY OF THE SPRINGS BY [ i, m+j, n+k]
# THEN CHANGE THE UNIT CELL VERTEX vert[i] BY vert[i] - j u0 - k v0
# DIMENSION DEPENDENT
def changeUnitCellVertexByPeriodicDuplicate( i, j, k, vert, u0, v0, lList, aList):
    lListOut = []
    aListOut = []
    for s in lList + aList: # TRAVERSE ALL SPRINGS
        sOut=[]
        indexStiffness = s[-1]
        for v in s[:-1]: # TRAVERSE ALL VERTICES IN CURRENT SPRING
            if (v[0] == i):
                sOut.append([ v[0], v[1] + j, v[2] + k])
            else:
                sOut.append([v[0], v[1], v[2]])
        sOut.append(s[-1]) # ADD INDEX OF STIFFNESS
        if (len(s) == 3): # A LENGTH SPRING HAS 3 INTEGERS
            lListOut.append(sOut)
        else:
            assert(len(s) == 4) # AN ANGULAR SPRING HAS 4 INTERGERS
            aListOut.append(sOut)
    vert[i] = vert[i] - j * u0 - k * v0
    return vert, u0, v0, lListOut, aListOut

#----------------------------------------------------
# DIMENSION DEPENDENT
def centerAllUnitCellVertices( vert, u0, v0, lList, aList):
    # vert, u0, v0, lList, aList =  changeUnitCellVertexByPeriodicDuplicate( 0, -10, -10, vert, u0, v0, lList, aList)
    for i in range(len(vert)):
        q, j, k = unitCellPeriodicDuplicate( vert[i], u0, v0)
        vert, u0, v0, lList, aList = changeUnitCellVertexByPeriodicDuplicate( i, j, k, vert, u0, v0, lList, aList)
    return vert, u0, v0, lList, aList


#----------------------------------------------------
# RETURN TRUE IFF TWO POINTS GIVEN BY THEIR 2D CARTESIAN COORDINATES
# ARE PERIODIC DUPLICATES WITH RESPECT TO THE PERIODICITY VECTORS u0, v0
# THIS IS THE ONLY FUNCTION DEPENDING ON A NUMERICAL VALUE EPSILON.
# IN PRACTICE, EPSILON SHOULD BE SMALLER THAN THE SMALLEST DISTANCE
# BETWEEN TWO UNIT CELL VERTICES
#
# THIS FUNCION IS USED TO DETECT PERIODIC POINTS IN A SET OF POINTS
# GIVEN BY THEIR CARTESIAN COORDINATES
# DIMENSION DEPENDENT
def periodicPair( v1, v2, u0, v0):
    EPSILON = 1e-8
    vRel = periodicFrameCoordinate( v2-v1, u0, v0)
    return np.linalg.norm(np.rint( vRel) - vRel) < EPSILON

#----------------------------------------------------
# RETURNS THE NEAREST INTEGER COORDINATES OF  VECTOR v
# DIMENSION DEPENDENT
def periodicIntegerCoordinate( v, u0, v0):
    return np.rint(periodicFrameCoordinate( v, u0, v0)).astype(int)

#----------------------------------------------------
# GIVEN A LIST pList OF POINTS DEFINED BY THEIR 2D CARTESIAN COORDINATES
# AND PERIODICY VECTORS u0, v0,
# RETURNS THE MINIMAL LIST OF UNIT CELL VERTICES unitV AND  LIST OF TRIPLETS
# [i,j,k] OF PERIODIC VERTICES WHICH ENCODE pList
#
# IN OTHER WORDS THIS FUNCTION ENCODES REDUNDANT PERIODIC COPIES OF VERTICES
# AS TRIPLET OF INTEGERS [i,j,k]
# NOTE THAT A PREFERENCE IS GIVEN TO THE FIRST POINTS IN THE LIST
# WHICH MEANS THAT FOR EACH SET OF PERIODIC DUPLICATES, THE CARTESIAN
# COORDINATES OF THE FIRST ENCOUNTERED POINT ARE STORED AS A UNIT CELL POINT
# DIMENSION DEPENDENT
def minimalPeriodicPointSet( pList, u0, v0):
    unitV=[]
    vList=[]
    for p in tqdm(pList):
        isNew = True
        for qi, q in enumerate(unitV):
            isNew = isNew and (not periodicPair( q, p, u0, v0))
            if (not isNew):
                break
        if isNew:
            unitV.append(p)
            vList.append([len(unitV)-1,0,0])
        else:
            pPairCoord = periodicIntegerCoordinate( p - q, u0, v0)
            vList.append( [qi, pPairCoord[0], pPairCoord[1]] )

    return np.array(unitV), vList

#----------------------------------------------------
# GIVEN A LIST OF LENGTH AND ANGULAR SPRINGS, RETURNS PERIODIC DUPLICATES OF THESE SPRINGS
# SUCH THAT THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX, I.E. IS ENCODED AS [i, 0, 0], WHERE
# i IS AN INDEX OF A UNIT CELL VERTEX,
# AND THE SECOND - CENTRAL - VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
# DIMENSION DEPENDENT
def translateSpringsInUnitCell( lList, aList):
    # TRANSLATE THE LENGTH SPRINGS SO THEIR FIRST VERTEX IS IN THE UNIT CELL
    lListUnitCell = [ [ [ l[0][0], 0, 0],[ l[1][0], l[1][1] - l[0][1], l[1][2] - l[0][2]], l[2] ] for l in lList]
    # TRANSLATE THE ANGULAR SPRINGS SO THEIR FIRST VERTEX IS IN THE UNIT CELL
    # aListUnitCell = [ [ [ a[0][0], 0, 0],[ a[1][0], a[1][1] - a[0][1], a[1][2] - a[0][2]],[ a[2][0], a[2][1] - a[0][1], a[2][2] - a[0][2]] ] for a in aList]
    # TRANSLATE THE ANGULAR SPRINGS SO THEIR SECOND, I.E. CENTRAL, VERTEX IS IN THE UNIT CELL
    aListUnitCell = [ [ [ a[0][0], a[0][1] - a[1][1], a[0][2] - a[1][2]],[ a[1][0], 0, 0],[ a[2][0], a[2][1] - a[1][1], a[2][2] - a[1][2]], a[3] ] for a in aList]

    return lListUnitCell, aListUnitCell


# DIMENSION DEPENDENT
def changePeriodicityVectors( vert, u0, v0, u1, v1):
    M = np.linalg.inv(np.array([u0,v0]))
    vert = np.dot( vert, M)
    vert = np.dot( vert, np.array([u1, v1]))
    return vert

#----------------------------------------------------
# TRIVIAL EXAMPLES OF USAGE OF minimalPeriodicPointSet
# AND periodicPair
# DIMENSION DEPENDENT
def exampleByHand():

    u0 = np.array([0.9, 0.1])
    v0 = np.array([-0.1, 1.05])

    a = np.array([-1.2, 2.3])

# periodicPair RETURNS TRUE BECAUSE THE CARTESIAN POINTS a AND a+5.*u0-2.*v0 ARE
# PERIODIC DUPLICATES
    print(periodicPair( a, a+5.*u0-2.*v0, u0, v0))

# pList CONTAINS THREE CARTESIAN POINTS, TWO OF WHICH A PERIODIC DUPLICATES
    pList = np.array( [a, a+5.*u0-2.*v0, a-1.2*u0+2.3*v0])
# minimalPeriodicPointSet RETURNS THE TWO UNIT CELL VERTICES a AND a-1.2*u0+2.3*v0
# AND THE THREE TRIPLETS OF INTEGER [0, 0, 0], [0, 5, 2], [1, 0, 0]
    print(minimalPeriodicPointSet( pList, u0, v0))

#====================================== SQUARE
# DIMENSION DEPENDENT
def squareNetwork():
    # SPRINGS
    l0 = [[0,0,0], [0,1,0], 0] # LINEAR SPRING BETWEEN vert[0] in cell[0,0] and vert[0] in cell[1,0]
    l1 = [[0,0,0], [0,0,1], 0]
    # ANGLES AROUND SINGLE "CENTRAL" VERTEX [0,0,0]
    a0 = [[0,1,0],[0,0,0],[0,0,1], 1]
    a1 = [[0,0,1],[0,0,0],[0,-1,0], 1]
    a2 = [[0,-1,0],[0,0,0],[0,0,-1], 1]
    a3 = [[0,0,-1],[0,0,0],[0,1,0], 1]

    lList = [l0,l1]
    aList = [a0,a1,a2,a3]

    vert = np.array( [
        [ 0., 0.],
    ])
    u0 = np.array([1.,0.])
    v0 = np.array([0.,1.])
    return vert, u0, v0, lList, aList

#====================================== TRIANGLE
# DIMENSION DEPENDENT
def triangleNetwork():
    # SPRINGS AROUND THE SINGLE "CENTRAL" VERTEX
    l0 = [[0,0,0], [0,1,0], 0] 
    l1 = [[0,0,0], [0,0,1], 0]
    l2 = [[0,0,0], [0,-1,1], 0]
    a0 = [[0,1,0],[0,0,0],[0,0,1], 1]
    a1 = [[0,0,1],[0,0,0],[0,-1,1], 1]
    a2 = [[0,-1,1],[0,0,0],[0,-1,0], 1]
    a3 = [[0,-1,0],[0,0,0],[0,0,-1], 1]
    a4 = [[0,0,-1],[0,0,0],[0,1,-1], 1]
    a5 = [[0,1,-1],[0,0,0],[0,1,0], 1]

    lList = [l0,l1,l2]
    aList = [a0,a1,a2,a3,a4,a5]

    vert = np.array( [
        [ 0., 0.],
    ])
    u0 = np.array([1.,0.])
    v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])
    return vert, u0, v0, lList, aList

#====================================== HEXAGON
# DIMENSION DEPENDENT
def hexagonalNetwork():
    # LENGTH SPRINGS AROUND VERTEX [1,0,0]
    l0 = [[0,0,0], [1,0,0], 0] 
    l1 = [[1,0,0], [0,1,0], 0]
    l2 = [[1,0,0], [0,0,1], 0]
    # ANGLES AROUND INTERIOR VERTEX [0,0,0]
    a0 = [[1,0,0], [0,0,0], [1,0,-1], 1]
    a1 = [[1,-1,0], [0,0,0], [1,0,0], 1]
    a2 = [[1,0,-1], [0,0,0], [1,-1,0], 1]
    # ANGLES AROUND INTERIOR VERTEX [1,0,0]
    a3 = [[0,0,0], [1,0,0], [0,0,1], 1]
    a4 = [[0,0,1], [1,0,0], [0,1,0], 1]
    a5 = [[0,1,0], [1,0,0], [0,0,0], 1]

    lList = [l0,l1,l2]
    aList = [a0,a1,a2,a3,a4,a5]

    # vert = np.array( [
    #     [ 0., 0.],
    #     [ 1., 0.]
    # ])

    vert = np.array( [
        [ 0.2, 0.],
        [ 0.8, 0.]
    ])

    # REGULAR HEXAGON
    u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.)])
    v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.)])
    # REENTRANT HONEYCOMB ,-1 POISSON RATIO IN X AND Y
    # u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.)])*0.5
    # v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.)])*0.5


    # COMPUTE VALUES h,l, theta FROM GIBSON+ASHBY CELLULAR SOLIDS BOOK. FIG. 4.8 PAGE 101
    hVector = periodicToAbsolute([1,0,0],vert,u0,v0) - periodicToAbsolute([0,0,0],vert,u0,v0)
    lVector = periodicToAbsolute( [1, -1, 0],vert,u0,v0) - periodicToAbsolute( [0,0,0],vert,u0,v0)
    h = np.linalg.norm( hVector)
    l = np.linalg.norm( lVector)
    theta = np.arccos( np.dot(hVector,lVector) / h / l) - np.pi/2.

    print(f"h={h}, l={l}, theta(degres)={theta*180/np.pi} FROM FIG. 4.8 PAGE 101")

    cT = np.cos(theta)
    sT = np.sin(theta)

    youngCoeff = cT / ( h/l + sT) / sT / sT # COEFFICIENT cos(theta) / [ (h/l + sin(theta)) * sin(theta)^2] EQUATION 4.7 PAGE 103

    print( f"Young modulus = {youngCoeff} (t/l)^3 FROM EQUATION 4.7 PAGE 103") 

    nu12 = cT * cT / ( h/l + sT) / sT # EQUATION 4.13 PAGE 103
    nu21 = 1. / nu12 # EQUATION 4.14 PAGE 104

    print(f"nu12={nu12} nu21={nu21} FROM EQUATION 4.14 PAGE 104")

    return vert, u0, v0, lList, aList

#====================================== IRREGULAR
# DIMENSION DEPENDENT
def irregularNetwork():

    l0 = [[0,0,0],[1,0,0], 0]
    l1 = [[1,0,0],[2,0,0], 0]
    l2 = [[0,0,0],[1,0,-1], 0]
    l3 = [[1,0,0],[1,1,1], 0]
    l4 = [[2,0,0],[0,1,1], 0]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [0,0,0]
    a0 = [[1,0,0],[0,0,0],[2,-1,-1], 1]
    a1 = [[2,-1,-1],[0,0,0],[1,0,-1], 1]
    a2 = [[1,0,-1],[0,0,0],[1,0,0], 1]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [1,0,0]
    a3 = [[0,0,1],[1,0,0],[1,-1,-1], 1]
    a4 = [[1,-1,-1],[1,0,0],[0,0,0], 1]
    a5 = [[0,0,0],[1,0,0],[2,0,0], 1]
    a6 = [[2,0,0],[1,0,0],[1,1,1], 1]
    a7 = [[1,1,1],[1,0,0],[0,0,1], 1]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [2,0,0]
    a8 = [[0,1,1],[2,0,0],[1,0,0], 1]
    a9 = [[1,0,0],[2,0,0],[0,1,1], 1]

    lList = [l0,l1,l2,l3,l4]
    aList = [a0,a1,a2,a3,a4,a5,a6,a7,a8,a9]

    # "REGULAR LOOK"
    # vert = np.array([ [0.5,-0.5],[1.,0.],[1.5,-0.5]])
    # u0 = np.array([ 1., -1.])
    # v0 = np.array([ 1., 1.])

    # "IRREGULAR LOOK"
    vert = np.array([ [0.6,-0.3],[1.,0.5],[1.6,-0.2]])
    u0 = np.array([ 1.3, -1.3])
    v0 = np.array([ 1.1, 1.4])
    
    return vert, u0, v0, lList, aList


#====================================== IRREGULAR
# DIMENSION DEPENDENT
def chelouNetwork():

    l0 = [[0,1,0],[1,0,1], 0]
    l1 = [[1,1,0],[2,0,1], 0]

    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [0,0,0]
    a0 = [[1,1,0],[0,0,1],[2,-1,-1], 1]


    lList = [l0,l1]
    aList = [a0]

    # "REGULAR LOOK"
    # vert = np.array([ [0.5,-0.5],[1.,0.],[1.5,-0.5]])
    # u0 = np.array([ 1., -1.])
    # v0 = np.array([ 1., 1.])

    # "IRREGULAR LOOK"
    vert = np.array([ [0.6,-0.3],[1.,0.5],[1.6,-0.2]])
    u0 = np.array([ 1.3, -1.3])
    v0 = np.array([ 1.1, 1.4])
    
    return vert, u0, v0, lList, aList



#--------------------------------------------------------
# GIVEN THE FILES points.txt CONTAINING A LIST OF 2D-COORDINATES OF POINTS
# 2Spring.txt CONTAINING A LIST OF PAIRS OF INTEGERS [i,j] REPRESENTING EDGES
# 3Spring.txt A LIST OF TRIPLET OF INTEGERS [i,j,k] REPRESENTING ANGLES
# AND u0, v0 TWO 2D VECTORS
# RETURNS THE PERIODIC PHYSICAL SYSTEM vert, u0, v0, lList, aList
# WITH *NO* REDUNDANCIES IN THE PERIODIC VERTICES
# AND THE PERIODIC PHYSICAL PRIMITIVES lList, aList
#
# NOTE THAT THERE MAY BE PERIODIC DUPLICATES IN THE FILES 2Spring.txt AND 3Spring.txt
# THESE PERIODIC DUPLICATES ARE DETECTED AND PROPERLY ENCODED IN THE STRUCTURE
# 
 
def networkFromPointEdgeAngleFiles( u0, v0):
    # MULTIPLICATION BY 3 - HACK POUR STEFANIE
    # pList = np.loadtxt("points.txt") * 3.
    pList = np.loadtxt("points.txt")
    # READ LENGTH AND ANGLE SPRINGS WITH ABSOLUTE INTEGER INDICES OF VERTEX
    lListAbsVert =  np.loadtxt("2Spring.txt").astype(int).tolist()
    aListAbsVert =  np.loadtxt("3Spring.txt").astype(int).tolist()
    return networkFromPointEdgeAngle( u0, v0, pList, lListAbsVert, aListAbsVert)
    
def networkFromPointEdgeAngle( u0, v0, pList, lListAbsVert, aListAbsVert):

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    vert, vList = minimalPeriodicPointSet( pList, u0, v0)

    # CONVERT TO SPRING WITH PERIODIC VERTICES
    # STIFFNESS INDEX 0 FOR LENGTH SPRINGS, 1 FOR ANGULAR SPRINGS
    lListRedundant = [ [ vList[ lAbs[0]], vList[ lAbs[1]], 0] for lAbs in lListAbsVert]
    aListRedundant = [ [ vList[ aAbs[0]], vList[ aAbs[1]], vList[ aAbs[2]], 1] for aAbs in aListAbsVert]

    # REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    vert, u0, v0, lList, aList = centerAllUnitCellVertices( vert, u0, v0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    vert = translateVerticesAroundBarycenter( vert)

    return vert, u0, v0, lList, aList

#--------------------------------------------------------
# SUBDIVIDE A NETWORK FOR A GIVEN MAXIMAL targetLength LENGTH OF THE EDGES
# EACH LENGTH SPRING IS *UNIFORMLY* SUBDIVIDED
# SO THAT THE LENGTH OF THE NEW EDGES IS SMALLER THAN targetLength
# ADDITIONNALY EACH ANGULAR SPRING WHICH CONTAINS A SUBDIVIDED EDGE IS ADAPTED TO THE SUBDIVISION


def uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0):
    EPSILON = 1e-10
    lengthOfEdge = np.linalg.norm( p2 - p1)
    # print(f"length of edge = {lengthOfEdge}")
    nSubEdge = np.floor((lengthOfEdge) / (targetLength + EPSILON)).astype(int) + 1
    # nSubEdge = int((lengthOfEdge) / (targetLength + EPSILON)) + 1
    # subLength = lengthOfEdge / nSubEdge
    # print(f"length of subdivided edge = {subLength} < {targetLength} = target length")
    # print(f"number of subdivided edges = {nSubEdge}, total length = {nSubEdge * subLength}={lengthOfEdge}")
    subVector = (p2 - p1) / nSubEdge
    return nSubEdge,  subVector

def subdivideNetwork( vert, u0, v0, lList, aList, targetLength):
    # print(f"subdivide the network for a target maximal length of edges of {targetLength}")
    indexStiffnessNewAngularSpring = 2
    lListOut = []
    aListOut = []
    vertOut = vert.tolist()
    # print(f"number of unit cell before subdivision = {len(vertOut)}")

    for l in lList:
        # print(f"processing length spring {l} with stiffness index {l[2]}")

        # FIRST STEP: SUBDIVIDE THIS LENGTH SPRING

        p1 = periodicToAbsolute( l[0], vert, u0, v0)
        p2 = periodicToAbsolute( l[1], vert, u0, v0)
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0)

        # if (nSubEdge <=1):
            # print("edge is not subdivided")
            # sys.exit(1)
        newVert = p1
        indexPrevious = l[0]
        indexMinus2 = None
        for i in range( nSubEdge-1):

            newVert = newVert + subVector # COORDINATES OF NEW UNIT CELL VERTEX
            indexCurrent = [len(vertOut), 0, 0] # INDEX OF NEW UNIT CELL VERTEX
            vertOut.append( newVert)

            # print(f"adding edge {newEdge}")
            newEdge = [ indexPrevious, indexCurrent, l[2]] # NEW LENGTH EDGE HAS SAME STIFFNESS INDEX AS ORIGINAL EDGE
            lListOut.append( newEdge )

            if (i>=1):
                # print(f"je rajoute deux ressorts d'angle interieur entre les sommets {indexMinus2} {indexPrevious} {indexCurrent}")
                aListOut.append( [indexMinus2, indexPrevious, indexCurrent, indexStiffnessNewAngularSpring])
                aListOut.append( [indexCurrent, indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])

            indexMinus2 = indexPrevious
            indexPrevious = indexCurrent

        newEdge = [ indexPrevious, l[1], l[2]]
        # print(f"adding edge {newEdge}")
        lListOut.append( newEdge )
        if (nSubEdge > 1): # ONLY ADD THE LAST INTERIOR ANGULAR SPRING IF IT EXISTS,
            # E.G. IF THE EDGE IS AT LEAST ONE TIME SUBDIVIDED
            aListOut.append( [indexMinus2, indexPrevious, l[1], indexStiffnessNewAngularSpring])
            aListOut.append( [l[1], indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])
        # else:
        #     print(f"{indexMinus2} {indexPrevious} {l[1]}")

    # SECOND STEP: ADAPT ALL ANGULAR SPRINGS TO THE SUBDIVIDED EDGES

    aListAdapted = []
    for a in aList:
        # print(f"processing angular spring {a}")
        p0 = periodicToAbsolute( a[0], vert, u0, v0)
        p1 = periodicToAbsolute( a[1], vert, u0, v0)
        p2 = periodicToAbsolute( a[2], vert, u0, v0)
        adaptedEdgeNotSubdivided = False
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p0, p1, targetLength, vert, u0, v0)
        if (nSubEdge <=1):
            adaptedEdgeNotSubdivided = True
            # print("adapted edge 1 is not subdivided")
        a0SubdAbs = p1 - subVector
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0)
        if (nSubEdge <=1):
            adaptedEdgeNotSubdivided = True
            # print("adapted edge 2 is not subdivided")
        a2SubdAbs = p1 + subVector
        foundA0 = False
        foundA2 = False
        for i, p in enumerate(vertOut):
            if periodicPair( a0SubdAbs, p, u0, v0):
                foundA0 = True
                intCoordFloat = periodicFrameCoordinate( a0SubdAbs - p, u0, v0)
                # print(intCoordFloat)
                intCoord = np.rint(intCoordFloat).astype(int)
                # print(intCoord)
                if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                    sys.exit(1)
                pSubd0 = [ i, intCoord[0], intCoord[1]]
                checkAbsolute = periodicToAbsolute(pSubd0, vertOut, u0, v0)
                # print(f"found periodic point {pSubd0}={checkAbsolute}={a0SubdAbs} from {p} for anchoring to point {a[0]}")
                if np.linalg.norm( checkAbsolute - a0SubdAbs) > 1e-6:
                    print(f"Fatal error: for periodic point {pSubd0}, {checkAbsolute} != {a0SubdAbs} from {p} for anchoring to point {a[0]}")
                    sys.exit(1)
            if periodicPair( a2SubdAbs, p, u0, v0):
                foundA2 = True
                intCoordFloat = periodicFrameCoordinate( a2SubdAbs - p, u0, v0)
                # print(intCoordFloat)
                intCoord = np.rint(intCoordFloat).astype(int)
                # print(intCoord)
                if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                    sys.exit(1)
                pSubd2 = [ i, intCoord[0], intCoord[1]]
                checkAbsolute = periodicToAbsolute(pSubd2, vertOut, u0, v0)
                # print(f"found periodic point {pSubd2}={checkAbsolute}={a2SubdAbs} from {p} for anchoring to point {a[2]}")
                if np.linalg.norm( checkAbsolute - a2SubdAbs) > 1e-6:
                    print(f"Fatal error: for periodic point {pSubd2}, {checkAbsolute} != {a2SubdAbs} from {p} for anchoring to point {a[2]}")
                    sys.exit(1)
        if not foundA0:
            print(f"unable to find anchor point for point {a[0]}")
            sys.exit(1)
        if not foundA2:
            print(f"unable to find anchor point for point {a[2]}")
            sys.exit(1)
        # if adaptedEdgeNotSubdivided:
            # print("Adapted edge is not subdivided")
            # sys.exit(1)
        assert(foundA0 and foundA2)
        aListAdapted.append([ pSubd0, a[1], pSubd2, a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
    vertOut = np.array(vertOut)
    aListOut = aListOut + aListAdapted
    return vertOut, u0, v0, lListOut, aListOut

def subdivideAllEdgeWithGivenStiffness( vert, u0, v0, lList, aList, stiffnessIndex, targetLength):
    print(f"Subdividing network with {len(lList)} edges")
    nIter = 0
    allSubdivided = False
    while not allSubdivided: 
        print(nIter)
        nIter = nIter + 1
        allSubdivided = True
        for ei, l in enumerate(lList):
            if l[2] == stiffnessIndex:
                allSubdivided = False
                break
        if not allSubdivided:
            vert, u0, v0, lList, aList = subdivideOneEdge( vert, u0, v0, lList, aList, ei, targetLength)

    for l in lList:
        if l[2] == -1:
            l[2] = stiffnessIndex

    return vert, u0, v0, lList, aList

def subdivideOneEdge( vert, u0, v0, lList, aList, ei, targetLength):
    lSpring = lList[ei]
    # print(f"subdivide the edge {ei}={lSpring} for a target maximal length of edges of {targetLength}")

    indexStiffnessSubdividedEdge = -1
    indexStiffnessNewAngularSpring = 1 # NEW ANGULAR SPRINGS HAVE THIS SPECIFIC STIFFNESS INDEX
    
    lListOut = []
    aListOut = []
    vertOut = vert.tolist()
    # print(f"number of unit cell before subdivision = {len(vertOut)}")

    # print(f"processing length spring {lSpring} with stiffness index {lSpring[2]}")

    # FIRST STEP: SUBDIVIDE THIS LENGTH SPRING

    p1 = periodicToAbsolute( lSpring[0], vert, u0, v0)
    p2 = periodicToAbsolute( lSpring[1], vert, u0, v0)
    nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0)

    # if (nSubEdge <=1):
        # print("edge is not subdivided")
        # sys.exit(1)
    newVert = p1
    indexPrevious = lSpring[0]
    indexMinus2 = None
    for i in range( nSubEdge-1):

        newVert = newVert + subVector # COORDINATES OF NEW UNIT CELL VERTEX
        indexCurrent = [len(vertOut), 0, 0] # INDEX OF NEW UNIT CELL VERTEX
        vertOut.append( newVert)

        # print(f"adding edge {newEdge}")
        # newEdge = [ indexPrevious, indexCurrent, lSpring[2]] # NEW LENGTH EDGE HAS SAME STIFFNESS INDEX AS ORIGINAL EDGE
        newEdge = [ indexPrevious, indexCurrent, indexStiffnessSubdividedEdge] # NEW LENGTH EDGE HAS THIS STIFFNESS INDEX
        lListOut.append( newEdge )

        if (i>=1):
            # print(f"je rajoute deux ressorts d'angle interieur entre les sommets {indexMinus2} {indexPrevious} {indexCurrent}")
            aListOut.append( [indexMinus2, indexPrevious, indexCurrent, indexStiffnessNewAngularSpring])
            aListOut.append( [indexCurrent, indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])

        indexMinus2 = indexPrevious
        indexPrevious = indexCurrent

    # newEdge = [ indexPrevious, lSpring[1], lSpring[2]]
    newEdge = [ indexPrevious, lSpring[1], indexStiffnessSubdividedEdge]
    # print(f"adding edge {newEdge}")
    lListOut.append( newEdge )

    if (nSubEdge > 1): # ONLY ADD THE LAST INTERIOR ANGULAR SPRING IF IT EXISTS,
        # E.G. IF THE EDGE IS AT LEAST ONE TIME SUBDIVIDED
        aListOut.append( [indexMinus2, indexPrevious, lSpring[1], indexStiffnessNewAngularSpring])
        aListOut.append( [lSpring[1], indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])
    # else:
    #     print(f"{indexMinus2} {indexPrevious} {l[1]}")

    # SECOND STEP: ADAPT ANGULAR SPRINGS TO THE SUBDIVIDED EDGE

    aListAdapted = []
    for a in aList:
        # print(f"processing angular spring {a}")
        la1 = [ a[0], a[1], a[3]]
        la2 = [ a[1], a[2], a[3]]
        attached = 0
        if periodic2SpringEqual( la1, lSpring):
            # print(f"angle spring {a} is attached to length spring {lSpring}")
            attached = 1
        if periodic2SpringEqual( la2, lSpring):
            # print(f"angle spring {a} is attached to length spring {lSpring}")
            attached = 2

        if attached == 0:
            # print(f"je recopie l ancien ressort d'angle {a}")
            aListOut.append(a)
        else:
            if attached == 1:
                p1 = periodicToAbsolute( a[1], vert, u0, v0)
                pn1 = periodicToAbsolute( a[0], vert, u0, v0)
            else: 
                p1 = periodicToAbsolute( a[1], vert, u0, v0)
                pn1 = periodicToAbsolute( a[2], vert, u0, v0)
            adaptedEdgeNotSubdivided = False
            nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, pn1, targetLength, vert, u0, v0)
            if (nSubEdge <=1):
                adaptedEdgeNotSubdivided = True
                # print("adapted edge 1 is not subdivided")
            a1SubdAbs = p1 + subVector
            foundAnchor = False
            for i, p in enumerate(vertOut):
                if periodicPair( a1SubdAbs, p, u0, v0):
                    foundAnchor = True
                    intCoordFloat = periodicFrameCoordinate( a1SubdAbs - p, u0, v0)
                    # print(intCoordFloat)
                    intCoord = np.rint(intCoordFloat).astype(int)
                    # print(intCoord)
                    if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                        sys.exit(1)
                    pSub1 = [ i, intCoord[0], intCoord[1]]
                    checkAbsolute = periodicToAbsolute(pSub1, vertOut, u0, v0)
                    # print(f"found periodic point {pSubd0}={checkAbsolute}={a0SubdAbs} from {p} for anchoring to point {a[0]}")
                    if np.linalg.norm( checkAbsolute - a1SubdAbs) > 1e-6:
                        print(f"Fatal error: for periodic point {pSub1}, {checkAbsolute} != {a1SubdAbs} from {p} for anchoring to point {a[1]}")
                        sys.exit(1)
            if not foundAnchor:
                print(f"unable to find anchor point for point {a[0]}")
                sys.exit(1)
            # if adaptedEdgeNotSubdivided:
                # print("Adapted edge is not subdivided")
                # sys.exit(1)
            assert(foundAnchor)
            if (attached == 1):
                aListAdapted.append([ pSub1, a[1], a[2], a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
            else:
                aListAdapted.append([ a[0], a[1], pSub1, a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
    vertOut = np.array(vertOut)
    lListOut = lList[:ei] + lListOut + lList[ei+1:]
    aListOut = aListOut + aListAdapted
    return vertOut, u0, v0, lListOut, aListOut

def modifyStiffnesses( lList, aList):
    for l in lList:
        match l[2]:
            case 0:
                l[2] = 0
            case 1:
                l[2] = 3
            case 2:
                l[2] = 1
            case 3:
                l[2] = 2
            case 4:
                l[2] = 4
    for a in aList:
        match a[3]:
            case 0:
                a[3] = 0
            case 1:
                a[3] = 3
            case 2:
                a[3] = 1
            case 3:
                a[3] = 2
            case 4:
                a[3] = 4  
    return lList, aList


#--------------------------------------------------------
# GIVEN AN OBJ FILE filename WITH ONLY EDGES (KEYCHAR "l")
# AND 2 PERIODICITY VECTORS
# RETURNS A PERIODIC NETWORK WITH LENGTH SPRINGS AT EACH EDGE
# AND ANGLE SPRINGS BETWEEN EACH EDGE AND A RIGID "VIRTUAL" EDGE
# CONNECTED AT EACH VERTEX
#
# THE VERTICES AND EDGES MAY BE PERIODICALLY DUPLICATED IN THE INPUT FILE
# THE PERIODIC DUPLICATES ARE PROPERLY PROCESSED SO THAT A UNIQUE REPRESENTATIVE
# VERTEX/LENGTH SPRING/ANGULAR SPRING IS ENCODED IN THE OUTPUT
def networkFromObjLineFile( u0, v0, filename):

    softSpringLengthStiffnessIndex = 0

    inputVertices = []
    inputEdge = []
    with open( filename, "r") as f:
        allLines = f.readlines()
        for oneLine in allLines:
            word = oneLine.split()
            if (len(word)>0):
                if (word[0] == "v"):
                    inputVertices.append([float(word[1]), float(word[2])])
                elif (word[0] == "l"):
                    inputEdge.append([int(word[1])-1, int(word[2])-1])

    nNode = len(inputVertices)
    nEdge = len(inputEdge)

    inputVertices = np.array( inputVertices)

    print(f"nNode = {nNode} nEdge = {nEdge}")

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    inpVerPer, vListInpVer = minimalPeriodicPointSet( inputVertices, u0, v0)
    print(f"nNode + 1 * nNode = {len(inputVertices)}")
    print(f"nNode after removing duplicated periodic nodes = {len(inpVerPer)}")
    assert(len(vListInpVer) == inputVertices.shape[0])

# SOFT LENGTH SPRINGS FOR RODS: INDEX EDGE < nEdge
    lListRedundant = [ [ vListInpVer[ lAbs[0]], vListInpVer[ lAbs[1]], softSpringLengthStiffnessIndex] for lAbs in inputEdge[:nEdge]]
# TANGENT ANGULAR SPRINGS, INDEX < nEdge
    aListRedundant = []
    # aListRedundant = aListRedundant + [ [ vListInpVer[lAbs[1]], vListInpVer[lAbs[0]], vListInpVer[lAbs[0] + nNode], tangentAngularStiffnessIndex]  for lAbs in inputEdge[:nEdge]]
    # aListRedundant = aListRedundant + [ [ vListInpVer[lAbs[0]], vListInpVer[lAbs[1]], vListInpVer[lAbs[1] + nNode], tangentAngularStiffnessIndex]  for lAbs in inputEdge[:nEdge]]

    print( f"number of length springs, including duplicates = {len(lListRedundant)}")
    print( f"number of angular springs, including duplicates = {len(aListRedundant)}")

# REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    print(f"number of unique length springs  = {len(lList)}")
    print(f"number of unique angular springs  = {len(aList)}")

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    inpVerPer, u0, v0, lList, aList = centerAllUnitCellVertices( inpVerPer, u0, v0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    inpVerPer = translateVerticesAroundBarycenter( inpVerPer)

    return inpVerPer, u0, v0, lList, aList


#--------------------------------------------------------
# GIVEN AN OBJ FILE filename WITH ONLY EDGES (KEYCHAR "l")
# AND 2 PERIODICITY VECTORS
# RETURNS A PERIODIC NETWORK WITH LENGTH SPRINGS AT EACH EDGE
# AND ANGLE SPRINGS BETWEEN EACH EDGE AND A RIGID "VIRTUAL" EDGE
# CONNECTED AT EACH VERTEX
#
# THE VERTICES AND EDGES MAY BE PERIODICALLY DUPLICATED IN THE INPUT FILE
# THE PERIODIC DUPLICATES ARE PROPERLY PROCESSED SO THAT A UNIQUE REPRESENTATIVE
# VERTEX/LENGTH SPRING/ANGULAR SPRING IS ENCODED IN THE OUTPUT
def weldedNetworkFromObjLineFile( u0, v0, filename):

    softSpringLengthStiffnessIndex = 0
    softAngularStiffnessIndex = 1 # NOT USED 
    rigidSpringLengthStiffnessIndex = 2
    weldAngularStiffnessIndex = 3

    #===========================
    # READ OBJ LINE FILE
    inputVertices = []
    inputEdge = []
    with open( filename, "r") as f:
        allLines = f.readlines()
        for oneLine in allLines:
            word = oneLine.split()
            if (len(word)>0):
                if (word[0] == "v"):
                    inputVertices.append([float(word[1]), float(word[2])])
                elif (word[0] == "l"):
                    inputEdge.append([int(word[1])-1, int(word[2])-1])

    inputVertices = np.array(inputVertices)
    # END OF READ OBJ LINE FILE
    #============================

    #================================================
    # COMPUTE THE PERIODIC ENCODING OF ALL VERTICES
    inpVerPer, vListInpVer = minimalPeriodicPointSet( inputVertices, u0, v0)
    
    # inpVerPer IS A SET OF UNIT CELL VERTICES WITH THEIR 2D CARTESIAN COORDINATES
    # vListInpVer IS A LIST OF INTEGER TRIPLETS, WITH THE SAME SIZE AS inputVertices
    # SUCH THAT vListInpVer[vi] ENCODES inputVertices[vi], I.E.:
    # inputVertices[vi] = inpVerPer[ vListInpPer[vi][0] ] + vListInpPer[vi][1] * u0 + vListInpPer[vi][2] * v0
    #=======================================================

    
    #=======================================================
    # COMPUTING MIN LENGTH OF ALL EDGES AROUND EACH VERTEX
    # TAKING INTO ACCOUNT PERIODCITY


    # 1) COMPUTE MINIMUM OF LENGTH OF EDGES WITHOUT PERIODICITY
    minLengthNonPer = np.ones(inputVertices.shape[0])* 1.e20
    for e in inputEdge:
        p0 = inputVertices[ e[0]]
        p1 = inputVertices[ e[1]]
        length = np.linalg.norm(p1-p0)
        if (minLengthNonPer[e[0]] > length):
            minLengthNonPer[e[0]] = length
        if (minLengthNonPer[e[1]] > length):
            minLengthNonPer[e[1]] = length

    # 2) INTIALIZE MINIMUM WITH PERIODICITY
    minLengthPer = np.zeros(inpVerPer.shape[0])
    for vi, v in enumerate(vListInpVer):
        minLengthPer[ v[0]] = minLengthNonPer[ vi]
    # 3) COMPUTE MINIMUM WITH PERIODICITY
    for vi, v in enumerate(vListInpVer):
        if minLengthPer[ v[0]] > minLengthNonPer[ vi]:
            minLengthPer[ v[0]] = minLengthNonPer[ vi]
    # 4) TRANSFER PERIODIC MINIMUM TO NON PERIODIC VERTICES
    for vi, v in enumerate(vListInpVer):
        minLengthNonPer[ vi] = minLengthPer[ v[0]]
        
    # END OF COMPUTING MINIMUM LENGTH OF EDGES AROUND VERTICES
    #=======================================================

    nNode = len(inputVertices)
    nEdge = len(inputEdge)

    print(f"nNode = {nNode} nEdge = {nEdge}")

    # ADD ONE VERTEX PER INPUT NODE FOR ENCODING WELDED CONNECTIONS
    # THIS VERTEX IS AT THE RIGHT HORIZONTAL DIRECTION, IN A DISTANCE COMPUTED FROM THE MINIMUM LENGTH OF EDGES AROUND THE PERIODIC VERTEX
    # STARTING AT THE RIGHT HORIZONTAL DIRECTION
    weight = minLengthNonPer[:, np.newaxis] # CONVERT VECTOR minLengthNonPer TO SINGLE COLUMN MATRIX
    distanceRigidTriangle = 0.1
    direction = np.array([ 1., 0.])
    rigidVertices = inputVertices + direction * weight * distanceRigidTriangle

    # CONCATENATE ALL NEW VERTICES
    vertDuplicate = np.concatenate( (inputVertices, rigidVertices))

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    vert, vList = minimalPeriodicPointSet( vertDuplicate, u0, v0)
    print(f"nNode + 1 * nNode = {len(vertDuplicate)}")
    print(f"nNode after removing duplicated periodic nodes = {len(vert)}")
    assert(len(vList) == vertDuplicate.shape[0])

    # ADD ONE RIGID LENGTH SPRING BETWEEN EACH INPUT NODE AND ITS RIGID NODE
    rigidEdge = []
    # ADD ONE EDGE PER NODE FOR ENCODING WELDED CONNECTIONS
    for i in range(nNode):
        rigidEdge.append([i, i+nNode])

# SOFT LENGTH SPRINGS FOR EACH INPUT EDGE
    lListRedundant = [ [ vList[ lAbs[0]], vList[ lAbs[1]], softSpringLengthStiffnessIndex] for lAbs in inputEdge]
# RIGID LENGTH SPRING BETWEEN EACH NODE AND ITS RIGID NODE
    lListRedundant = lListRedundant + [ [ vList[vi], vList[vi + nNode], rigidSpringLengthStiffnessIndex] for vi in range(nNode)]
# ANGULAR SPRING ENCODING WELDED CONNECTION
    aListRedundant = []
    aListRedundant = aListRedundant + [ [ vList[lAbs[0] + nNode],  vList[lAbs[0]], vList[lAbs[1]], weldAngularStiffnessIndex]  for lAbs in inputEdge]
    aListRedundant = aListRedundant + [ [ vList[lAbs[1] + nNode],  vList[lAbs[1]], vList[lAbs[0]], weldAngularStiffnessIndex]  for lAbs in inputEdge]

    print( f"number of length springs, including duplicates = {len(lListRedundant)}")
    print( f"number of angular springs, including duplicates = {len(aListRedundant)}")

# REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    print(f"number of unique length springs  = {len(lList)}")
    print(f"number of unique angular springs  = {len(aList)}")

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    vert, u0, v0, lList, aList = centerAllUnitCellVertices( vert, u0, v0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    vert = translateVerticesAroundBarycenter( vert)

    return vert, u0, v0, lList, aList

# GIVEN THREE 2D POINTS a, b, c, RETURNS THE SIGNED AREA OF THE TRIANGLE WITH VERTICES a, b, c
def signedAreaOfTriangle( a, b, c):
    u = b-a
    v = c-a
    return u[0]*v[1] - u[1] * v[0]

#--------------------------------------------------------
# GIVEN AN OBJ FILE filename WITH ONLY EDGES (KEYCHAR "l")
# AND 2 PERIODICITY VECTORS
# RETURNS A PERIODIC NETWORK WITH LENGTH SPRINGS AT EACH EDGE
# AND ANGLE SPRINGS BETWEEN EACH EDGE AND A RIGID "VIRTUAL" EDGE
# CONNECTED AT EACH VERTEX
#
# THE VERTICES AND EDGES MAY BE PERIODICALLY DUPLICATED IN THE INPUT FILE
# THE PERIODIC DUPLICATES ARE PROPERLY PROCESSED SO THAT A UNIQUE REPRESENTATIVE
# VERTEX/LENGTH SPRING/ANGULAR SPRING IS ENCODED IN THE OUTPUT
def chiralNetworkFromObjLineFile( u0, v0, filename):

    softSpringLengthStiffnessIndex = 0
    softSpringAngularStiffnessIndex = 1
    tangentAngularStiffnessIndex = 2
    rigidSpringLengthStiffnessIndex = 3
    rigidSpringAngularStiffnessIndex = 4


    #===========================
    # READ OBJ LINE FILE
    inputVertices = []
    inputEdge = []
    with open( filename, "r") as f:
        allLines = f.readlines()
        for oneLine in allLines:
            word = oneLine.split()
            if (len(word)>0):
                if (word[0] == "v"):
                    inputVertices.append([float(word[1]), float(word[2])])
                elif (word[0] == "l"):
                    inputEdge.append([int(word[1])-1, int(word[2])-1])

    inputVertices = np.array(inputVertices)
    # END OF READ OBJ LINE FILE
    #============================

    #================================================
    # COMPUTE THE PERIODIC ENCODING OF ALL VERTICES
    inpVerPer, vListInpVer = minimalPeriodicPointSet( inputVertices, u0, v0)
    
    # inpVerPer IS A SET OF UNIT CELL VERTICES WITH THEIR 2D CARTESIAN COORDINATES
    # vListInpVer IS A LIST OF INTEGER TRIPLETS, WITH THE SAME SIZE AS inputVertices
    # SUCH THAT vListInpVer[vi] ENCODES inputVertices[vi], I.E.:
    # inputVertices[vi] = inpVerPer[ vListInpPer[vi][0] ] + vListInpPer[vi][1] * u0 + vListInpPer[vi][2] * v0
    #=======================================================


    #=======================================================
    # COMPUTING MIN LENGTH OF ALL EDGES AROUND EACH VERTEX
    # TAKING INTO ACCOUNT PERIODCITY


    # 1) COMPUTE MINIMUM OF LENGTH OF EDGES WITHOUT PERIODICITY
    minLengthNonPer = np.ones(inputVertices.shape[0])* 1.e20
    for e in inputEdge:
        p0 = inputVertices[ e[0]]
        p1 = inputVertices[ e[1]]
        length = np.linalg.norm(p1-p0)
        if (minLengthNonPer[e[0]] > length):
            minLengthNonPer[e[0]] = length
        if (minLengthNonPer[e[1]] > length):
            minLengthNonPer[e[1]] = length

    # 2) INTIALIZE MINIMUM WITH PERIODICITY
    minLengthPer = np.zeros(inpVerPer.shape[0])
    for vi, v in enumerate(vListInpVer):
        minLengthPer[ v[0]] = minLengthNonPer[ vi]
    # 3) COMPUTE MINIMUM WITH PERIODICITY
    for vi, v in enumerate(vListInpVer):
        if minLengthPer[ v[0]] > minLengthNonPer[ vi]:
            minLengthPer[ v[0]] = minLengthNonPer[ vi]
    # 4) TRANSFER PERIODIC MINIMUM TO NON PERIODIC VERTICES
    for vi, v in enumerate(vListInpVer):
        minLengthNonPer[ vi] = minLengthPer[ v[0]]
        
    # END OF COMPUTING MINIMUM LENGTH OF EDGES AROUND VERTICES
    #=======================================================

    nNode = len(inputVertices)
    nEdge = len(inputEdge)

    print(f"nNode = {nNode} nEdge = {nEdge}")

    # ADD THREE VERTICES PER INPUT NODE FOR ENCODING RIGID CONNECTIONS
    # THESE THREE VERTICES ARE UNIFORMLY DISTRIBUTED AROUND THE CENTER VERTEX
    # STARTING AT THE RIGHT HORIZONTAL DIRECTION
    weight = minLengthNonPer[:, np.newaxis] # CONVERT VECTOR minLengthNonPer TO SINGLE COLUMN MATRIX
    nRigid = 3
    twoThirdPi = 2. * np.pi/3.
    radiusRigidTriangle = 0.1
    rigidVertices = np.zeros((0,2)) # ZERO ROWS X 2 COLUMNS 
    for rvi in range(nRigid):
        direction = np.array([ np.cos(rvi * twoThirdPi), np.sin( rvi * twoThirdPi)])
        rigidVertices = np.concatenate( (rigidVertices, inputVertices + direction * weight * radiusRigidTriangle))

    # rigidVertices NOW CONTAINS
    #      - ALL CENTER POINTS SHIFTED TO THE RIGHT,
    #      - ALL CENTERPOINTS SHIFTED TO THE TOP LEFT
    #      - ALL CENTERPOINTS SHIFTED TO THE BOTTOM LEFT
        
    # RADIUS AND SIGN OF CHIRALITY AT EACH INPUT VERTEX
    radiusVertex = 0.15
    # radiusVertex = 0.0
    scaledRadiusVertex = minLengthNonPer * radiusVertex

    # RANDOM CHIRALITY OF PERIODIC VERTICES
    # np.random.seed(seed=int(time.time())) 
    # signVertexPeriodic = np.random.randint( 2, size=inpVerPer.shape[0])*2-1
    # POSITIVE CHIRALITY OF PERIODIC VERTICES
    signVertexPeriodic = np.ones( inpVerPer.shape[0], dtype=int)
    # NEGATIVE CHIRALITY OF PERIODIC VERTICES
    # signVertexPeriodic = np.ones( inpVerPer.shape[0], dtype=int) * -1


    # 4) TRANSFER PERIODIC SIGN TO NON PERIODIC VERTICES
    signVertex = np.zeros( (inputVertices.shape[0],), dtype=int) 
    for vi, v in enumerate(vListInpVer):
        signVertex[ vi] = signVertexPeriodic[ v[0]]

    # ADD TWO CONNECTION VERTICES FOR EACH EDGE OF THE INPUT OBJ FILE
    # AND A NEW EDGE BETWEEN THESE TWO CONNECTION VERTICES
    # THIS NEW EDGE REPLACES THE EDGE OF THE INPUT OBJ FILE IN THE CHIRAL NETWORK
    connectionVertices = np.zeros( (0,2) ) # MATRIX WITH 0 ROWS AND 2 COLUMNS
    shrinkedEdge = []
    positionFixingEdge = []
    tangentFixingAngle = []

    indexVertex = 0
    offsetedIndexVertex = (1 + nRigid) * nNode + indexVertex
    
    for e in inputEdge:
        # edgeVector = inputVertices[e[1]] - inputVertices[e[0]]
        # edgeVector = edgeVector / np.linalg.norm(edgeVector)
        # p0 = inputVertices[e[0]] + radiusVertex[e[0]] * edgeVector
        # p1 = inputVertices[e[1]] - radiusVertex[e[1]] * edgeVector

        p0, p1 = lineCotangentCircle.coTangentLine( inputVertices[e[0]], scaledRadiusVertex[e[0]], signVertex[e[0]], inputVertices[e[1]], scaledRadiusVertex[e[1]], signVertex[e[1]])
        # CONCATENATE NEW VERTICES, p0: FIRST ROW, p1: SECOND ROW
        connectionVertices = np.concatenate( (connectionVertices, [p0]) )
        connectionVertices = np.concatenate( (connectionVertices, [p1]) )
        shrinkedEdge.append( [ offsetedIndexVertex, offsetedIndexVertex+1])
        # TWO NEW EDGES RIGIDLY CONNECTING THE NEW VERTICES
        # THE VERTEX INDEX indexVertex MAY BE CONNECTED TO ONE OF THE THREE FOLLOWING EDGES
        # e[0], e[0] + nNode
        # e[0] + nNode, e[0] + 2 * nNode
        # e[0] + 2 * nNode, e[0]
        #
        triangleQuality = np.zeros(3)
        indices = [ [e[0], e[0]+nNode], [ e[0] + nNode, e[0] + 2 * nNode], [ e[0] + 2 * nNode, e[0]] ]
        for i, ind in enumerate(indices):
            triangleQuality[i] = signedAreaOfTriangle( connectionVertices[indexVertex], rigidVertices[ ind[0]], rigidVertices[ ind[1]])
        indMax = np.argmax( np.abs( triangleQuality))

        positionFixingEdge.append( [offsetedIndexVertex, nNode + indices[ indMax][0]])
        positionFixingEdge.append( [offsetedIndexVertex, nNode + indices[ indMax][1]])
        tangentFixingAngle.append( [offsetedIndexVertex+1, offsetedIndexVertex, nNode + indices[ indMax][0]])

        indices = [ [e[1], e[1]+nNode], [ e[1] + nNode, e[1] + 2 * nNode], [ e[1] + 2 * nNode, e[1]] ]
        for i, ind in enumerate(indices):
            triangleQuality[i] = signedAreaOfTriangle( connectionVertices[indexVertex+1], rigidVertices[ ind[0]], rigidVertices[ ind[1]])
        indMax = np.argmax( np.abs( triangleQuality))
        
        positionFixingEdge.append( [offsetedIndexVertex+1, nNode + indices[ indMax][0]])
        positionFixingEdge.append( [offsetedIndexVertex+1, nNode + indices[ indMax][1]])
        tangentFixingAngle.append( [offsetedIndexVertex, offsetedIndexVertex+1, nNode + indices[ indMax][0]])

        # tangentFixingAngle.append( [offsetedIndexVertex+1, offsetedIndexVertex, e[0]])
        # tangentFixingAngle.append( [offsetedIndexVertex, offsetedIndexVertex+1, e[1]])

        indexVertex = indexVertex + 2
        offsetedIndexVertex = (1 + nRigid) * nNode + indexVertex

    print(f"nShortEdge = {len(shrinkedEdge)}")
    # CONCATENATE ALL NEW VERTICES
    vertDuplicate = np.concatenate( (inputVertices, rigidVertices, connectionVertices))

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    vert, vList = minimalPeriodicPointSet( vertDuplicate, u0, v0)
    print(f"nNode + 1 * nNode = {len(vertDuplicate)}")
    print(f"nNode after removing duplicated periodic nodes = {len(vert)}")
    assert(len(vList) == vertDuplicate.shape[0])

    rigidEdge = []
    # ADD THREE EDGES PER NODE FOR ENCODING RIGID CONNECTIONS
    for i in range(nNode):
        rigidEdge.append([i+nNode,i+2*nNode])
        rigidEdge.append([i+2*nNode,i+3*nNode])
        rigidEdge.append([i+3*nNode,i+nNode])

    lListRedundant = []
# SOFT LENGTH SPRINGS FOR RODS: INDEX EDGE < nEdge
    lListRedundant = [ [ vList[ lAbs[0]], vList[ lAbs[1]], softSpringLengthStiffnessIndex] for lAbs in shrinkedEdge]
# RIGID LENGTH SPRINGS FOR RIDIG CONNECTIONS: INDEX EDGE >= nEdge
    # lListRedundant = lListRedundant + [ [ vList[ lAbs[0]], vList[ lAbs[1]], rigidSpringLengthStiffnessIndex] for lAbs in rigidEdge]
    lListRedundant = lListRedundant + [ [ vList[ lAbs[0]], vList[ lAbs[1]], rigidSpringLengthStiffnessIndex] for lAbs in rigidEdge+positionFixingEdge]
# TANGENT ANGULAR SPRINGS, INDEX < nEdge
    aListRedundant = []
    aListRedundant = aListRedundant + [ [ vList[aAbs[0]], vList[aAbs[1]], vList[aAbs[2]], tangentAngularStiffnessIndex]  for aAbs in tangentFixingAngle]

    print( f"number of length springs, including duplicates = {len(lListRedundant)}")
    print( f"number of angular springs, including duplicates = {len(aListRedundant)}")

# REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    print(f"number of unique length springs  = {len(lList)}")
    print(f"number of unique angular springs  = {len(aList)}")

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    vert, u0, v0, lList, aList = centerAllUnitCellVertices( vert, u0, v0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    vert = translateVerticesAroundBarycenter( vert)

    return vert, u0, v0, lList, aList


#--------------------------------------------------------
# READ FROM AN ASCII FILE WITH THE SPRING FILE FORMAT SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# v x y  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# u0 x y -> FIRST VECTOR OF PERIODICITY
# v0 x y -> SECOND VECTOR OF PERIODICITY
# s ks -> STIFFNESS FLOAT VALUE ks
# l i j k p q r si -> LENGTH SPRING BETWEEN PERIODIC VERTICES [i,j,k] AND [p,q,r] 
#                     WITH STIFFNESS VALUE OF INDEX si
# a i j k l m n p q r si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES [i,j,k]
#                           [l,m,n] AND [p,q,r] WITH STIFFNESS VALUE OF INDEX si
def networkFromSpringFile( filename: str):
    vert = np.zeros((0,2))
    u0 = np.zeros(2)
    v0 = np.zeros(2)
    lList = []
    aList = []  
    with open( filename, "r" ) as f:
        allLines = f.readlines()
        for aLine in allLines:
            allWords = aLine.split()
            match allWords[0]:
                case "v":
                    # ARRAY OF SHAPE (1,2)
                    onePoint = np.array([[float(allWords[1]), float(allWords[2])]])
                    vert = np.concatenate(  (vert, onePoint))
                case "u0":
                    # ARRAY OF SHAPE (2)
                    u0 = np.array([float(allWords[1]), float(allWords[2])])
                case "v0":
                    # ARRAY OF SHAPE (2)
                    v0 = np.array([float(allWords[1]), float(allWords[2])])
                case "s":
                    print("stiffness value ignored")
                case "l":
                    index = [int(aWord) for aWord in allWords[1:]]
                    # [0,1,2]: FIRST PERIODIC VERTEX OF LENGTH SPRING, [3,4,5]: SECOND VERTEX, [6]: STIFFNESS INDEX
                    lList.append( [ [index[0], index[1], index[2]], [index[3], index[4], index[5]], index[6]])
                case "a":
                    index = [int(aWord) for aWord in allWords[1:]]
                    # [0,1,2]: FIRST PERIODIC VERTEX OF ANGULAR SPRING, [3,4,5],[6,7,8]: SECOND AND THIRD VERTEX, [9]: STIFFNESS INDEX
                    aList.append( [ [index[0], index[1], index[2]], [index[3], index[4], index[5]], [index[6], index[7], index[8]], index[9]])
    # lList, aList = translateSpringsInUnitCell( lList, aList)
    
    return vert, u0, v0, lList, aList

#--------------------------------------------------------
# READ FROM AN ASCII FILE WITH THE INDIRECT SPRING FILE FORMAT SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# 
# v x y  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# 
# u0 x y -> FIRST VECTOR OF PERIODICITY
# v0 x y -> SECOND VECTOR OF PERIODICITY
# 
# p i j k -> PERIODIC VERTEX WITH INTEGER TRIPLET [i,j,k]
#            WHERE i IS THE INDEX OF A UNIT CELL VERTEX
#            AND p = v_i + j * u0 + k * v0

# s ks -> STIFFNESS FLOAT VALUE ks
# 
# l i j si -> LENGTH SPRING BETWEEN VERTICES i AND j 
#             WITH STIFFNESS VALUE OF INDEX si
# a i j k si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES i, j AND k,
#               WITH STIFFNESS VALUE OF INDEX si

def networkFromIndirectSpringFile( filename: str):
    vert = np.zeros((0,2))
    u0 = np.zeros(2)
    v0 = np.zeros(2)
    lList = []
    aList = []
    sList = []
    allTriplet = []
    with open( filename, "r" ) as f:
        allLines = f.readlines()
        for aLine in allLines:
            allWords = aLine.split()
            match allWords[0]:
                case "v":
                    # ARRAY OF SHAPE (1,2)
                    onePoint = np.array([[float(allWords[1]), float(allWords[2])]])
                    allTriplet.append([ vert.shape[0], 0, 0])
                    vert = np.concatenate(  (vert, onePoint))
                case "u0":
                    # ARRAY OF SHAPE (2)
                    u0 = np.array([float(allWords[1]), float(allWords[2])])
                case "v0":
                    # ARRAY OF SHAPE (2)
                    v0 = np.array([float(allWords[1]), float(allWords[2])])
                case "p":
                    index = [int(aWord) for aWord in allWords[1:]]
                    allTriplet.append([ index[0], index[1], index[2]])
                case "s":
                    # print(f"stiffness value {allWords[1]} read")
                    sList.append(float(allWords[1]))
                case "l" | "lc": # READING A LENGTH SPRING [3,1,5]. 3,1: (INDIRECT) INDICES OF THE TWO VERTICES, 5: INDEX OF STIFFNESS
                    index = [int(aWord) for aWord in allWords[1:]]
                    # allTriplet[index[0]] IS THE INTEGER TRIPLET ENCODING THE FIRST PERIODIC VERTEX OF THE LENGTH SPRING,
                    # allTriplet[index[1]] IS THE SECOND VERTEX, index[2] IS THE STIFFNESS INDEX
                    # index[2] IS THE INDEX OF THE STIFFNESS COEFFICIENT
                    # HACK TO ENCODE HARD CONSTRAINTS
                    if allWords[0] == "lc":
                        index[2] = -1
                    lList.append( [allTriplet[index[0]], allTriplet[ index[1]], index[2]])
                case "a" | "ac": # READING AN ANGULAR SPRING [3,1,2,5]. 3,1,2: (INDIRECT) INDICES OF THE THREE VERTICES, 5: INDEX OF STIFFNESS
                    index = [int(aWord) for aWord in allWords[1:]]
                    # allTriplet[index[0]] IS THE INTEGER TRIPLET ENCODING THE FIRST PERIODIC VERTEX OF THE ANGULAR SPRING,
                    # allTriplet[index[1,2]] ENCODE THE SECOND AND THIRD VERTICES, index[3] IS THE STIFFNESS INDEX
                    # index[3] IS THE INDEX OF THE STIFFNESS COEFFICIENT
                    # HACK TO ENCODE HARD CONSTRAINTS
                    if allWords[0] == "ac":
                        index[3] = -1
                    aList.append( [allTriplet[index[0]], allTriplet[ index[1]], allTriplet[ index[2]], index[3]])
    # lList, aList = translateSpringsInUnitCell( lList, aList)
                    
    print( f"read periodic network with {len(vert)} unit cell points, {len(lList)} length springs and {len(aList)} angle springs")
    
    return vert, u0, v0, lList, aList, np.array(sList)

#--------------------------------------------------------
# WRITE THE PERIODIC STRUCTURE IN AN ASCII FILE WITH THE FOLLOWING SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# v x y  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# u0 x y -> FIRST VECTOR OF PERIODICITY
# v0 x y -> SECOND VECTOR OF PERIODICITY
# s ks -> STIFFNESS FLOAT VALUE ks
# l i j k p q r si -> LENGTH SPRING BETWEEN PERIODIC VERTICES [i,j,k] AND [p,q,r] 
#                     WITH STIFFNESS VALUE OF INDEX si
# a i j k l m n p q r si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES [i,j,k]
#                           [l,m,n] AND [p,q,r] WITH STIFFNESS VALUE OF INDEX si

def writeInSpringFileFormat( vert, u0, v0, lList, aList, filename: str):
    with open( filename, "w" ) as f:
        # WRITE UNIT CELL VERTICES
        for p in vert[:,]:
            f.write( f"v {p[0]} {p[1]}\n")
        # WRITE PERIODICITY VECTORS
        f.write( f"u0 {u0[0]} {u0[1]}\n")
        f.write( f"v0 {v0[0]} {v0[1]}\n")
        # WRITE FICTIVE STIFFNESS VALUES FOR LENGTH AND ANGULAR SPRINGS
        # THIS IS FOR COMPATIBLITY WITH MECHASPRING
        defaultStiffnesses = [ 1., 0.03/50000.]
        f.write( f"s {defaultStiffnesses[0]}\n")
        f.write( f"s {defaultStiffnesses[1]}\n")
        # WRITE LENGTH SPRINGS
        for l in lList: # l[0][0,1,2]: FIRST PERIODIC POINT, l[1][0,1,2]: SECOND PERIODIC POINT, l[2]: INDEX OF STIFFNESS
            f.write( f"l {l[0][0]} {l[0][1]} {l[0][2]} {l[1][0]} {l[1][1]} {l[1][2]} {l[2]}\n" )
         # WRITE ANGULAR SPRINGS
        for a in aList: # a[0][0,1,2]: FIRST PERIODIC POINT, a[1,2][0,1,2]: SECOND AND THIRD PERIODIC POINTS, a[3]: INDEX OF STIFFNESS
            f.write( f"a {a[0][0]} {a[0][1]} {a[0][2]} {a[1][0]} {a[1][1]} {a[1][2]} {a[2][0]} {a[2][1]} {a[2][2]} {a[3]}\n" )       
    return

#--------------------------------------------------------
# WRITE THE PERIODIC STRUCTURE IN AN ASCII FILE WITH THE FOLLOWING SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# 
# v x y  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# 
# u0 x y -> FIRST VECTOR OF PERIODICITY
# v0 x y -> SECOND VECTOR OF PERIODICITY
# 
# p i j k -> PERIODIC VERTEX WITH INTEGER TRIPLET [i,j,k]
#            WHERE i IS THE INDEX OF A UNIT CELL VERTEX
#            AND p = v_i + j * u0 + k * v0
#
# s ks -> STIFFNESS FLOAT VALUE ks
# 
# l i j si -> LENGTH SPRING BETWEEN VERTICES i AND j 
#             WITH STIFFNESS VALUE OF INDEX si
# a i j k si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES i, j AND k,
#               WITH STIFFNESS VALUE OF INDEX si

def writeInIndirectSpringFileFormat( vert, u0, v0, lList, aList, sList, filename: str):

    index, invIndex, nUnitCellVertex, nPeriodicVertex = indexAllVertexInSpringList( lList, aList)

    # for key in index:
    #     print(f"{key}: {index[key]}")
    # for i, triplet in enumerate(invIndex):
    #     print(f"{i}: {triplet}")
    with open( filename, "w" ) as f:
        # WRITE UNIT CELL VERTICES IN THE ORDER GIVEN BY INDEX
        f.write("# UNIT CELL VERTICES COORDINATES\n")
        for viTriplet in invIndex[:nUnitCellVertex]:
            f.write( f"v {vert[viTriplet[0]][0]} {vert[viTriplet[0]][1]}\n")
        # WRITE PERIODICITY VECTORS
        f.write("# PERIODICITY VECTORS\n")
        f.write( f"u0 {u0[0]} {u0[1]}\n")
        f.write( f"v0 {v0[0]} {v0[1]}\n")
        # WRITE PERIODIC VERTICES IN THE ORDER GIVEN BY INDEX
        f.write("# PERIODIC VERTICES USED IN THE SPRINGS\n")
        for viTriplet in invIndex[nUnitCellVertex:]:
            f.write(f"p {viTriplet[0]} {viTriplet[1]} {viTriplet[2]}\n")   
        # WRITE STIFFNESS VALUES
        f.write("# STIFFNESSES\n")
        for s in sList:
            f.write( f"s {s}\n")
        # WRITE LENGTH SPRINGS
        f.write("# LENGTH SPRINGS: INDEX OF FIRST PERIODIC POINT, INDEX OF SECOND PERIODIC POINT, INDEX OF STIFFNESS\n")
        for i,l in enumerate(lList):
            # print(l)
            # l[0]: FIRST PERIODIC POINT, l[1]: SECOND PERIODIC POINT, l[2]: INDEX OF STIFFNESS
            f.write( f"l {index[tuple(l[0])]} {index[tuple(l[1])]} {l[2]}\n" )
         # WRITE ANGULAR SPRINGS
        f.write("# ANGULAR SPRINGS: INDEX OF FIRST, SECOND AND THIRD PERIODIC POINTS, INDEX OF STIFFNESS\n")
        for i, a in enumerate(aList):
            # a[0]: FIRST PERIODIC POINT, al[1,2]: SECOND AND THIRD PERIODIC POINTS, a[3]: INDEX OF STIFFNESS
            f.write( f"a {index[tuple(a[0])]} {index[tuple(a[1])]} {index[tuple(a[2])]} {a[3]}\n" )
    return

def writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, filename: str):
    defaultStiffness = list(np.ones( np.max( [l[2] for l in lList] + [a[3] for a in aList])+1))
    writeInIndirectSpringFileFormat( vert, u0, v0, lList, aList, defaultStiffness, filename)
    return

# GIVEN A PERIODIC NETWORK, DUPLICATE IT IN A 3X3 GRID AND OUTPUT IT IN MECHASPRING FORMAT
#----------------------------------------------------
# GIVEN TWO TRIPLETS OF INTEGERS, RETURN TRUE
# IFF THERE ARE OF THE FORM (a,b,c) (c,b,d)
def isLink( s, t):
    return  (s[1] == t[1]) and (s[2] == t[0])

#----------------------------------------------------
# GIVEN A LIST OF TRIPLET OF INTEGERS (vi,vj,vk), BUILD A NEW LIST
# STARTING WITH THE FIRST INPUT TRIPLET, AND ADDING INPUT TRIPLETS SUCH THAT
# ANY TWO CONSECUTIVE TRIPLETS IN THE NEW LIST ARE OF THE FORM (a,b,c) (c,b,d)
# RETURN THE NEW LIST, AND A BOOLEAN isCylic INDICATING IF ALL INPUT TRIPLETS CAN BE REORDERED
def chainReorder( a):
    if a == []:
        return [], True
    # initialize a list with the first triplet of the input list a
    b=[a[0]]
    # remove the first triplet from a
    a=a[1:]

    nextElementFound = True
    while (a != []) and (nextElementFound):
        nextElementFound = False
        # iterate the remaining triplets e in input list a
        for i, e in enumerate(a):
            # check if last element of b is linked with current element e in a
            if isLink( b[-1], e):
                # if next linked triplet found, add it to b
                b.append(e)
                # and remove it from a
                a = a[:i] + a[i+1:]
                nextElementFound = True
                break
    isCyclic = (a == [])
    c = [e[0] for e in b]
    return c, isCyclic

def writeDuplicateMechaspringMesh( filename, vert, u0, v0, lUnitList, aUnitList):
    # ON LIT LE RESEAU ET ON TRANSLATE LES RESSORTS AU CENTRE
    # ON CREE UNE GRILLE 3X3 DU RESEAU
    width = range(-1,2)
    lList, aList = springInBox( width, lUnitList, aUnitList)
    # ON INITIALISE LES TABLES DE CONVERSION ENTRE TRIPLET ET INDICE ENTIER
    index, invIndex, nUnitCellVertex, nPeriodicVertex = indexAllVertexInSpringList( [], aList)
    # ON CONVERTIT LES TRIPLETS DES SOMMETS DES RESSORTS EN LEUR INDICE ENTIER
    aList = [ (index[tuple(a[0])], index[tuple(a[1])], index[tuple(a[2])]) for a in aList]
    # print(aList)
    # ON ITERE SUR TOUS LES INDICES vi DE SOMMETS
    vertexList = [] 
    for vi in range(nUnitCellVertex+nPeriodicVertex):
        nvi = []
        # ON RECHERCHE TOUS LES RESSORTS DONT LE SOMMET CENTRAL VAUT vi
        for a in aList:
            if (a[1] == vi):
                nvi.append(a)
        # ON REORDONNE LES SOMMETS VOISINS AUTOUR DE vi EN SUIVANT L'ORDRE DES RESSORTS D'ANGLE 
        nvi, isCyclic = chainReorder( nvi)
        vertexList.append(nvi)
    # ON CALCULE LES COORDONNEES DES SOMMETS PERIODIQUES
    points =np.array([ periodicToAbsolute(vTriplet, vert, u0, v0) for vTriplet in invIndex])
    with open( filename, 'w') as f:
        f.write( "{}\n".format(points.shape[0]))
        for vi in range( points.shape[0]):
            f.write( "{} {} {}\n".format(points[vi,0], points[vi][1], 0.))
        for vL in vertexList:
            for vj in vL:
                f.write( "{} ".format(vj+1))
            f.write('\n')
    f.close()
    return

def main():

    #----------------------------------------------
    # WRITE SPG FILE FOR VARIOUS NETWORKS
    # writeInIndirectSpringFileFormatDefaultStiffness( *triangleNetwork(), "triangle.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *squareNetwork(), "square.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *hexagonalNetwork(), "hexagonal.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *irregularNetwork(), "irregular.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *chelouNetwork(), "chelou.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *networkFromPointEdgeAngleFiles(np.array([1.,0.]),np.array([0.,1.])), "pointEdgeAngle.spg")

    # print(vert)

    #---------------------------------
    # READ AN SPG FILE WITH REDUNDANCIES, REMOVE THE REDUNDANCIES AND STORE THE NEW NETWORK
    # vert, u0, v0, lList, aList = networkFromIndirectSpringFile(sys.argv[1])
    # lList = suppressRedundant2Spring( lList)
    # aList = suppressRedundant3Spring( aList)
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, sys.argv[1] + "_unique.spg")
    #---------------------------------

    #---------------------------------
    # CONSTRUCT A NETWORK, PROCESS IT AND SAVE IT
    # vert, u0, v0, lList, aList = hexagonalNetwork()
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "sortie.spg")
    # vert, u0, v0, lList, aList = subdivideNetwork( vert, u0, v0, lList, aList, float(sys.argv[1]))
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "sortie_subdivided.spg")
    #---------------------------------

    #---------------------------------
    # READ A NETWORK IN SPG FORMAT, PROCESS IT AND SAVE IT
    vert, u0, v0, lList, aList, sArray = networkFromIndirectSpringFile(sys.argv[1])
    # # vert, u0,:q v0, lList, aList = subdivideNetwork( vert, u0, v0, lList, aList, float(sys.argv[2]))
    # # lList, aList = modifyStiffnesses( lList, aList)
    vert, u0, v0, lList, aList = subdivideAllEdgeWithGivenStiffness( vert, u0, v0, lList, aList, 0, float(sys.argv[2]))
    writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "sortie_subdivided.spg")
    #---------------------------------   

    # #---------------------------------
    # READ AN OBJ LINE FILE, PROCESS IT AND SAVE IT
    # u0 = np.array([1.,0.])
    # v0 = np.array([0.,1.])
    # # vert, u0, v0, lList, aList = networkFromObjLineFile( u0, v0, "square.obj")
    # vert, u0, v0, lList, aList = networkFromObjLineFile( u0, v0, sys.argv[1])
    # vert, u0, v0, lList, aList = chiralNetworkFromObjLineFile( u0, v0, sys.argv[1])
    # # vert, u0, v0, lList, aList = chiralNetworkFromObjLineFile( u0, v0, "voronoi2DClean.obj")
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "sortie.spg")
    # #---------------------------------

    # exampleByHand()

    # writeDuplicateMechaspringMesh( "toto.txt", *triangleNetwork())

if __name__=="__main__":
    main()
