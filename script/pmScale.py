import periodicMinimalConstruct as pmC
import numpy as np
import sys

if len(sys.argv) != 3:
    print(f"usage: {sys.argv[0]} <spgFilename> <scale>")
    sys.exit(1)

filename = sys.argv[1]
scale = float(sys.argv[2])


# READ A NETWORK IN SPG FORMAT, PROCESS IT AND SAVE IT
vert, u0, v0, lList, aList, sArray = pmC.networkFromIndirectSpringFile( filename)
vert = scale * vert
u0 = scale * u0
v0 = scale * v0

pmC.writeInIndirectSpringFileFormat( vert, u0, v0, lList, aList, sArray, "outputScale.spg")

print("file outputScale.spg has been written")
