import periodicMinimalOptimize as pmO
import numpy as np
import sys
import os

    # oneMesoPR, oneMesoYoung, oneMesoMagnitudeStress = macroStressAndStrainTensor.PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor, stressTensor)


#---------------------------------------------------------------------
# VARYING THETA FROM thetaMin TO thetaMax FOR A FIXED VALUE OF LMBDA
thetaVariation = True
if thetaVariation:
    lmbda = 0.8
    thetaMin = 0.
    thetaMax = np.pi 
    nTraction = 4

    filename = sys.argv[1]
    allTheta = np.linspace( thetaMin, thetaMax, num = nTraction, endpoint = False)

    for ti, theta in enumerate(allTheta):
        print("="*16 + f" lmbda = {lmbda} theta = {theta} " + "="*16)
        pmO.main( filename, lmbda, theta)
        os.system( f"cp myOutput_equilibrium.vtk {filename}_{ti:02d}.vtk")

#-----------------------------------------------------------------------------
# VARYING LMBDA FROM lmbdaMin TO lmbdaMax AND BACK FOR A FIXED VALUE OF theta
lmbdaVariation = False
if lmbdaVariation:
    lmbdaMin = 0.8
    lmbdaMax = 1.2
    theta = 0.
    nTraction = 16

    filename = sys.argv[1]
    allLmbda = np.linspace( lmbdaMin, lmbdaMax, num = nTraction, endpoint = True)

    for li, lmbda in enumerate(allLmbda):
        print("="*16 + f" lmbda = {lmbda} theta = {theta} " + "="*16)
        pmO.main( filename, lmbda, theta)
        os.system( f"cp myOutput_equilibrium.vtk {filename}_{li:02d}.vtk")

#-----------------------------------------------------------------------------
# VARYING LMBDA FROM lmbdaMin TO lmbdaMax AND BACK FOR A FIXED VALUE OF theta
lmbdaAndThetaVariation = False
if lmbdaAndThetaVariation:
    lmbdaMin = 0.8
    lmbdaMax = 1.2
    nLambda = 16
    thetaMin = 0.
    thetaMax = np.pi
    nTheta = 16

    filename = sys.argv[1]
    allLmbda = np.linspace( lmbdaMin, lmbdaMax, num = nLambda, endpoint = True)
    allTheta = np.linspace( thetaMin, thetaMax, num = nTheta, endpoint = False)

    for ti, theta in enumerate(allTheta):
        for li, lmbda in enumerate(allLmbda):
            print("="*16 + f" lmbda = {lmbda} theta = {theta} " + "="*16)
            pmO.main( filename, lmbda, theta)
            os.system( f"cp myOutput_equilibrium.vtk {filename}_lmbda{li:02d}_theta{ti:02d}.vtk")
