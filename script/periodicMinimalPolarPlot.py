import periodicMinimalOptimize as pmO
import macroStressAndStrainTensor
import optimizeStiffnessTensor
import polarPlot
import numpy as np
import matplotlib.pyplot as plt
import sys
import shutil


def polarPlots( filename, lmbda, nTraction):
    thetaMin = 0.
    thetaMax = np.pi 
    # thetaMax = 2 * np.pi 
    # nTraction = 4*3* 8
    # nTraction = 4*3* 2
    # nTraction = 8

    allTheta = np.linspace( thetaMin, thetaMax, num = nTraction, endpoint = False)

    mesoPR=[]
    mesoYoung=[]
    strainTensorVoigt = np.zeros((nTraction, 3))
    stressTensorVoigt = np.zeros((nTraction, 3))

    for ti, theta in enumerate(allTheta):
        print("="*16 + f" lmbda = {lmbda} theta = {theta} " + "="*16)
        # COMPUTE EQUILIBRIUM UNDER UNIAXIAL STRETCHING, AND CORRESPONDING STRAIN AND STRESS TENSORS
        strainTensor, stressTensor = pmO.main( filename, lmbda, theta)
        # CONVERT STRAIN AND STRESS TENSORS TO VOIGT VECTORS
        strainTensorVoigt[ti,:] = optimizeStiffnessTensor.voigtVectorFrom2ndOrderSymmetricMatrix( strainTensor)
        stressTensorVoigt[ti,:] = optimizeStiffnessTensor.voigtVectorFrom2ndOrderSymmetricMatrix( stressTensor)

        oneMesoPR, oneMesoYoung, oneMesoMagnitudeStress = macroStressAndStrainTensor.PRandYoungModulusFromStrainAndStressTensorsUniaxialStretching( strainTensor, stressTensor)
        mesoPR.append( oneMesoPR)
        mesoYoung.append( oneMesoYoung)

        shutil.copyfile( "outputFiles/equilibrium.spg", f"outputFiles/equilibrium_{ti:02d}.spg")
        shutil.copyfile( "outputFiles/equilibrium.vtk", f"outputFiles/equilibrium_{ti:02d}.vtk")

    elasticityTensor = optimizeStiffnessTensor.elasticityTensorFromStrainAndStressTensors( strainTensorVoigt, stressTensorVoigt)

    thetaMacro, macroYoung, macroPR = optimizeStiffnessTensor.youngModulusAndPoissonRatioFromElasticity( elasticityTensor, 100)

    asciiDisplayMacroYoungPR = False
    if asciiDisplayMacroYoungPR:
        print(f"macroPR from strain and stress = {macroPR}")
        print(f"macroYoung from strain and stress = {macroYoung}")

    meanPR = np.average(np.array(mesoPR))
    standardDeviationPR = np.std(np.array(mesoPR))
    meanYoung = np.average(np.array(mesoYoung))
    standardDeviationYoung = np.std(np.array(mesoYoung))   
    # print(f"   mesoPR = {mesoPR}")
    # print(f"mesoYoung = {mesoYoung}")
    print(f"AVERAGE MESO PR = {meanPR}, STANDARD DEVIATION = {standardDeviationPR}")
    print(f"AVERAGE MESO Young = {meanYoung}, STANDARD DEVIATION = {standardDeviationYoung}")

    print(f"elasticity tensor = {elasticityTensor}")

    ax1 = plt.subplot(121, projection='polar')      
    polarPlot.extendedPolarPlot( ax1, 0., 2. * np.pi, macroPR, "")
    polarPlot.extendedPolarPlot( ax1, thetaMin, thetaMax, mesoPR, f"PRmeso={meanPR:5.2f}, std={standardDeviationPR:5.2f}")
    polarPlot.extendedPolarPlot( ax1, thetaMin+np.pi, thetaMax+np.pi, mesoPR, f"PRmeso={meanPR:5.2f}, std={standardDeviationPR:5.2f}")

    ax2 = plt.subplot(122, projection='polar')
    polarPlot.extendedPolarPlot( ax2, 0., 2. * np.pi, macroYoung, "")
    polarPlot.extendedPolarPlot( ax2, thetaMin, thetaMax, mesoYoung, f"YoungMeso={meanYoung:5.2f}, std={standardDeviationYoung:5.2e}")
    polarPlot.extendedPolarPlot( ax2, thetaMin+np.pi, thetaMax+np.pi, mesoYoung, f"YoungMeso={meanYoung:5.2f}, std={standardDeviationYoung:5.2e}")
    plt.suptitle(f"lambda={lmbda}")
    plt.savefig("outputPolarPlot.pdf")
    plt.show()


if __name__=="__main__":
    filename = sys.argv[1]
    lmbda = float(sys.argv[2])
    nTraction = int( sys.argv[3])
    polarPlots( filename, lmbda, nTraction)
