import numpy as np
import periodicMinimalConstruct


#====================================== TRIANGLE
def triangleNetwork():
    # SPRINGS AROUND THE SINGLE "CENTRAL" VERTEX
    l0 = [[0,0,0], [0,1,0]] 
    l1 = [[0,0,0], [0,0,1]]
    l2 = [[0,0,0], [0,-1,1]]
    a0 = [[0,1,0],[0,0,0],[0,0,1]]
    a1 = [[0,0,1],[0,0,0],[0,-1,1]]
    a2 = [[0,-1,1],[0,0,0],[0,-1,0]]
    a3 = [[0,-1,0],[0,0,0],[0,0,-1]]
    a4 = [[0,0,-1],[0,0,0],[0,1,-1]]
    a5 = [[0,1,-1],[0,0,0],[0,1,0]]

    lList = [l0,l1,l2]
    aList = [a0,a1,a2,a3,a4,a5]

    vert = np.array( [
        [ 0., 0.],
    ])
    u0 = np.array([1.,0.])
    v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])
    return vert, u0, v0, lList, aList

def rigidOrthogonalConnection():
    u0 = np.array([1.,0.])*5.
    v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.)])*5.
    v0[1] = v0[1]*0.7
    vert = np.array([ [0.,0.]]) # THE FIRST VERTEX IS NOT USED AS OF TODAY (MAY 30, 2023)

# THREE NEW VERTICES FOR THE CENTRAL RIGID TRIANGLE
    vertNew = np.array( [ [-0.1,-0.1], # index 1
                          [0.1, -0.1], # index 2
                          [ 0., 0.1] # index 3
                          ])
# THREE NEW LENGTH EDGES FOR RIGIDIFYING THE TRIANGLE
    lListCentralTriangle = [
                    [[1,0,0], [2, 0, 0], 1],
                    [[2,0,0], [3, 0, 0], 1],
                    [[3,0,0], [1, 0, 0], 1]
                ]
    vert = np.concatenate( (vert, vertNew))

# SIX NEW CONNECTION POINTS FOR THE SIX RODS SURROUNDING THE CENTER TRIANGLE

    delta = 0.0
    vertNew = np.array( [ [0.6, 0.-0-delta], # index 4
                         [0.3+delta, 0.3], # index 5
                         [-0.3+delta, 0.3], # index 6
                         [-0.6, 0.0+delta], # index 7
                         [-0.3-delta, -0.3], # index 8
                         [0.3-delta, -0.3], # index 9
                         ]) 

    # vertNew = np.array( [ [0.6, 0.0], # index 4
    #                      [0.3, 0.3], # index 5
    #                      [-0.3, 0.3], # index 6
    #                      [-0.6, 0.], # index 7
    #                      [-0.3, -0.3], # index 8
    #                      [0.3, -0.3], # index 9
    #                      ]) 
        
    lListFirstRodToFirstRigid = [
        [[1,0,0], [4,0,0], 1], 
        [[1,0,0], [5,0,0], 1],
        [[1,0,0], [6,0,0], 1],
        [[1,0,0], [7,0,0], 1],
        [[1,0,0], [8,0,0], 1],
        [[1,0,0], [9,0,0], 1]
        ]
    
    lListFirstRodToSecondRigid = [
        [[3,0,0], [4,0,0], 1], 
        [[2,0,0], [5,0,0], 1],
        [[2,0,0], [6,0,0], 1],
        [[3,0,0], [7,0,0], 1],
        [[2,0,0], [8,0,0], 1],
        [[2,0,0], [9,0,0], 1]
        ]
    
    aListSignRigid = [
        [[3,0,0], [4,0,0], [1,0,0], 2], 
        [[1,0,0], [5,0,0], [2,0,0], 2], 
        [[1,0,0], [6,0,0], [2,0,0], 2], 
        [[1,0,0], [7,0,0], [3,0,0], 2], 
        [[2,0,0], [8,0,0], [1,0,0], 2], 
        [[2,0,0], [9,0,0], [1,0,0], 2], 
    ]

    aListTangentRod = [
        [[7,1,0], [4,0,0], [3,0,0], 3], 
        [[8,0,1], [5,0,0], [1,0,0], 3], 
        [[9,-1,1], [6,0,0], [1,0,0], 3], 
        [[4,-1,0], [7,0,0], [1,0,0], 3], 
        [[5,0,-1], [8,0,0], [2,0,0], 3], 
        [[6,1,-1], [9,0,0], [2,0,0], 3], 
    ]

    lListSoft = [
        [[4,0,0], [7,+1,0], 0],
        [[5,0,0], [8,0,+1], 0],
        [[6,0,0], [9,-1,+1], 0]
    ]

    vert = np.concatenate( (vert, vertNew))
    lList = lListCentralTriangle + lListFirstRodToFirstRigid + lListFirstRodToSecondRigid + lListSoft
    aList = aListSignRigid + aListTangentRod

    return vert, u0, v0, lList, aList


def rigidAlmostTangentialConnection():
    cP3 = np.cos(np.pi/3.)
    sP3 = np.sin(np.pi/3.)
    u0 = np.array([2. * cP3, -sP3-0.3])*5.
    v0 = np.array([2. * cP3+0.2, sP3-0.3])*5.
    u0[1] = u0[1] * 0.8
    v0[1] = v0[1] * 0.8
    vert = np.array([ [0.,0.]]) # THE FIRST VERTEX IS NOT USED AS OF TODAY (MAY 30, 2023)

# THREE NEW VERTICES FOR THE CENTRAL RIGID TRIANGLE
    vertNew = np.array( [ [np.cos(-np.pi/6.),np.sin(-np.pi/6.)], # index 1
                          [0., 1.], # index 2
                          [ np.cos(7.*np.pi/6.), np.sin(7.*np.pi/6.)] # index 3
                          ]) * 0.5
# THREE NEW LENGTH EDGES FOR RIGIDIFYING THE TRIANGLE
    lListCentralTriangle = [
                    [[1,0,0], [2, 0, 0], 1],
                    [[2,0,0], [3, 0, 0], 1],
                    [[3,0,0], [1, 0, 0], 1]
                ]
    vert = np.concatenate( (vert, vertNew))

# SIX NEW CONNECTION POINTS FOR THE SIX RODS SURROUNDING THE CENTER TRIANGLE

    vertNew = np.array( [ [1., 0.], # index 4
                         [0.5, sP3], # index 5
                         [-0.5, sP3], # index 6
                         [-1., 0.0], # index 7
                         [-0.5, -sP3], # index 8
                         [0.5, -sP3], # index 9
                         ]) 

    # vertNew = np.array( [ [0.6, 0.0], # index 4
    #                      [0.3, 0.3], # index 5
    #                      [-0.3, 0.3], # index 6
    #                      [-0.6, 0.], # index 7
    #                      [-0.3, -0.3], # index 8
    #                      [0.3, -0.3], # index 9
    #                      ]) 
        
    lListFirstRodToFirstRigid = [
        [[1,0,0], [4,0,0], 1], 
        [[1,0,0], [5,0,0], 1],
        [[2,0,0], [6,0,0], 1],
        [[2,0,0], [7,0,0], 1],
        [[1,0,0], [8,0,0], 1],
        [[1,0,0], [9,0,0], 1]
        ]
    
    lListFirstRodToSecondRigid = [
        [[2,0,0], [4,0,0], 1], 
        [[2,0,0], [5,0,0], 1],
        [[3,0,0], [6,0,0], 1],
        [[3,0,0], [7,0,0], 1],
        [[3,0,0], [8,0,0], 1],
        [[3,0,0], [9,0,0], 1]
        ]
    
    aListSignRigid = [
        [[2,0,0], [4,0,0], [1,0,0], 2], 
        [[2,0,0], [5,0,0], [1,0,0], 2], 
        [[3,0,0], [6,0,0], [2,0,0], 2], 
        [[3,0,0], [7,0,0], [2,0,0], 2], 
        [[1,0,0], [8,0,0], [3,0,0], 2], 
        [[1,0,0], [9,0,0], [3,0,0], 2], 
    ]

    aListTangentRod = [
        [[7,0,1], [4,0,0], [2,0,0], 3], 
        [[8,-1,1], [5,0,0], [2,0,0], 3], 
        [[9,-1,0], [6,0,0], [3,0,0], 3], 
        [[4,0,-1], [7,0,0], [3,0,0], 3], 
        [[5,1,-1], [8,0,0], [1,0,0], 3], 
        [[6,1,0], [9,0,0], [1,0,0], 3], 
    ]

    lListSoft = [
        [[9,0,0], [6,+1,0], 0],
        [[4,0,0], [7,0,1], 0],
        [[5,0,0], [8,-1,1], 0],
        [[6,0,0], [9,-1,0], 0],
        [[7,0,0], [4,0,-1], 0],
        [[8,0,0], [5,1,-1], 0]
    ]

    vert = np.concatenate( (vert, vertNew))
    lList = lListSoft + lListCentralTriangle + lListFirstRodToFirstRigid + lListFirstRodToSecondRigid
    aList = aListSignRigid + aListTangentRod
    # aList = aListTangentRod + []

    return vert, u0, v0, lList, aList


def rigidTangentialConnection():

    cP3 = np.cos(np.pi/3.)
    sP3 = np.sin(np.pi/3.)
    u0 = np.array([1.*5.,0.+2.*sP3])
    v0 = np.array([ [cP3, -sP3], [sP3, cP3]]) @ u0
    vert = np.array([ [0.,0.]]) # THE FIRST VERTEX IS NOT USED AS OF TODAY (MAY 30, 2023)

# THREE NEW VERTICES FOR THE CENTRAL RIGID TRIANGLE
    vertNew = np.array( [ [np.cos(-np.pi/6.),np.sin(-np.pi/6.)], # index 1
                          [0., 1.], # index 2
                          [ np.cos(7.*np.pi/6.), np.sin(7.*np.pi/6.)] # index 3
                          ]) * 0.5
# THREE NEW LENGTH EDGES FOR RIGIDIFYING THE TRIANGLE
    lListCentralTriangle = [
                    [[1,0,0], [2, 0, 0], 3],
                    [[2,0,0], [3, 0, 0], 3],
                    [[3,0,0], [1, 0, 0], 3]
                ]
    vert = np.concatenate( (vert, vertNew))

# SIX NEW CONNECTION POINTS FOR THE SIX RODS SURROUNDING THE CENTER TRIANGLE

    vertNew = np.array( [ [1., 0.], # index 4
                         [0.5, sP3], # index 5
                         [-0.5, sP3], # index 6
                         [-1., 0.0], # index 7
                         [-0.5, -sP3], # index 8
                         [0.5, -sP3], # index 9
                         ]) 

    # vertNew = np.array( [ [0.6, 0.0], # index 4
    #                      [0.3, 0.3], # index 5
    #                      [-0.3, 0.3], # index 6
    #                      [-0.6, 0.], # index 7
    #                      [-0.3, -0.3], # index 8
    #                      [0.3, -0.3], # index 9
    #                      ]) 
        
    lListFirstRodToFirstRigid = [
        [[1,0,0], [4,0,0], 3], 
        [[1,0,0], [5,0,0], 3],
        [[2,0,0], [6,0,0], 3],
        [[2,0,0], [7,0,0], 3],
        [[1,0,0], [8,0,0], 3],
        [[1,0,0], [9,0,0], 3]
        ]
    
    lListFirstRodToSecondRigid = [
        [[2,0,0], [4,0,0], 3], 
        [[2,0,0], [5,0,0], 3],
        [[3,0,0], [6,0,0], 3],
        [[3,0,0], [7,0,0], 3],
        [[3,0,0], [8,0,0], 3],
        [[3,0,0], [9,0,0], 3]
        ]
    
    aListSignRigid = [
        [[2,0,0], [4,0,0], [1,0,0], 4], 
        [[2,0,0], [5,0,0], [1,0,0], 4], 
        [[3,0,0], [6,0,0], [2,0,0], 4], 
        [[3,0,0], [7,0,0], [2,0,0], 4], 
        [[1,0,0], [8,0,0], [3,0,0], 4], 
        [[1,0,0], [9,0,0], [3,0,0], 4], 
    ]

    aListTangentRod = [
        [[7,1,-1], [4,0,0], [2,0,0], 2], 
        [[8,1,0], [5,0,0], [2,0,0], 2], 
        [[9,0,1], [6,0,0], [3,0,0], 2], 
        [[4,-1,1], [7,0,0], [3,0,0], 2], 
        [[5,-1,0], [8,0,0], [1,0,0], 2], 
        [[6,0,-1], [9,0,0], [1,0,0], 2], 
    ]

    lListSoft = [
        [[5,0,0], [8,1,0], 0],
        [[6,0,0], [9,0,1], 0],
        [[7,0,0], [4,-1,1], 0],
        # [[8,0,0], [5,-1,0], 0],
        # [[9,0,0], [6,0,-1], 0],
        # [[4,0,0], [7,1,-1], 0]
    ]

    vert = np.concatenate( (vert, vertNew))
    # lList = lListSoft + lListCentralTriangle
    lList = lListSoft + lListCentralTriangle + lListFirstRodToFirstRigid + lListFirstRodToSecondRigid
    aList = aListSignRigid + aListTangentRod
    # aList = aListTangentRod + []

    return vert, u0, v0, lList, aList
  

if __name__== "__main__":

    # vert, u0, v0, lList, aList = rigidOrthogonalConnection()
    # vert, u0, v0, lList, aList = rigidAlmostTangentialConnection()
    vert, u0, v0, lList, aList = rigidTangentialConnection()
    periodicMinimalConstruct.writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, lList, aList, "sortie.spg")
