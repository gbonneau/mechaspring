import periodicMinimalConstruct3D as pmC
import sys
import numpy as np

def linearStretching( vIn, direction, lmbda):
    projectionAlongDirection = np.dot( vIn, direction) /np.dot( direction, direction) * direction
    orthogonalComponent = vIn - projectionAlongDirection
    vOut = lmbda * projectionAlongDirection + orthogonalComponent
    return vOut

def uniaxialStretchingLinear( vert, u0, v0, w0, lList, aList, direction, lmbda):
    # barycenter = pmc.barycenterSpringList( vert, u0, v0, lList, aList)
    barycenter = pmC.barycenterSpringList( vert, u0, v0, w0, lList, [])
    affineStretchingFunction = lambda x: linearStretching(x - barycenter, direction, lmbda) + barycenter
    vertStretched = np.apply_along_axis( affineStretchingFunction, axis=1, arr=vert)
    u0Stretched = linearStretching(u0, direction, lmbda)
    v0Stretched = linearStretching(v0,  direction, lmbda)
    w0Stretched = linearStretching(w0, direction, lmbda)
    return vertStretched, u0Stretched, v0Stretched, w0Stretched

