function [PR_iso,E_iso,C_iso,delta_iso] = isotropic(CH)      
  %------------------------------------------
    %closest isotropic elasticity tensor [Martinez21]
    c11 = CH(1,1); c12 = CH(1,2); c22 = CH(2,2); c33 = CH(3,3);
    c11_iso = (9*(c11+c22) + 2*c12 + 4*c33)/20;
    c12_iso = (   c11+c22 + 18*c12 - 4*c33)/20;
    C_iso = [c11_iso c12_iso 0;
	   c12_iso c11_iso 0;
	   0 0 (c11_iso-c12_iso)/2];
    % PR and E of isotropic material
    PR_iso = c12_iso / c11_iso;
    E_iso = (c11_iso^2 - c12_iso^2 ) / c11_iso;
    %divergence from isotropy
    delta_iso = norm(CH-C_iso,'fro') / norm(CH,'fro');
end

function CH_becht = computeBechterew(CH_voigt)
  %------------------------------------------
	%compute C in Bechterew form
        CH_becht = CH_voigt; % tenseur symmétrisé
        CH_becht(1,3) = sqrt(2) * CH_voigt(1,3);
        CH_becht(2,3) = sqrt(2) * CH_voigt(2,3);
        CH_becht(3,1) = sqrt(2) * CH_voigt(3,1);
        CH_becht(3,2) = sqrt(2) * CH_voigt(3,2);
        CH_becht(3,3) = 2 * CH_voigt(3,3);
        CH_becht;
end

function S4 = computeCompliance4(S)
  %------------------------------------------
  % output: compliance tensor S4 (4x4)
  %        epsilon = S4*sigma, sigma = [e_11, e_22, sqrt(2)*e_12 sqrt(2)*e_21]^T
  %        S4 = [1111         1122         sqrt(2)*1112 sqrt(2)*1121
  %             2211         2222         sqrt(2)*2212 sqrt(2)*2221
  %             sqrt(2)*1211 sqrt(2)*1222 2*1212       2*1221
  %             sqrt(2)*2111 sqrt(2)*2122 2*2112       2*2121      ]
  %------------------------------------------
  S4 = zeros(2,2,2,2);
  S4(1,1,1,1) = S(1,1);
  S4(1,1,2,2) = S(1,2);
  S4(1,1,1,2) = S(1,3)/sqrt(2);
  S4(1,1,2,1) = S(1,3)/sqrt(2); % ???
  %
  S4(2,2,1,1) = S(2,1);
  S4(2,2,2,2) = S(2,2);
  S4(2,2,1,2) = S(2,3)/sqrt(2);
  S4(2,2,2,1) = S(2,3)/sqrt(2); % ???
  %
  S4(1,2,1,1) = S(3,1)/sqrt(2);
  S4(1,2,2,2) = S(3,2)/sqrt(2);
  S4(1,2,1,2) = S(3,3)/2;
  S4(1,2,2,1) = S(3,3)/2; % ???
  %
  S4(2,1,1,1) = S(3,1)/sqrt(2);
  S4(2,1,2,2) = S(3,2)/sqrt(2);
  S4(2,1,1,2) = S(3,3)/2;
  S4(2,1,2,1) = S(3,3)/2; % ???
  S4;
end

function [PR_dir,E_dir] = computePR_unidirection(C_becht, d)
  %------------------------------------------
  % input: stiffness tensor C (3x3) in Bechterew form
  %        sigma = C*epsilon, epsilon = [e_11, e_22, sqrt(2)e_12]^T
  %	   C_becht = [ 1111         1122         sqrt(2)*1112
  %                    2211         2222         sqrt(2)*2212
  %                    sqrt(2)*1211 sqrt(2)*1222 2*1212      ]
  %         d direction in plane d = [d1 d2]^T
  %         |d| = 1
  % output: PR_dir directional Poisson's Ratio
  %
  % compute S = Cˆ(-1) in 3x3 Bechterew form
  % transform S into 4x4 form
  % compute n orthogonal to d
  % compute directional PR_dir 
  % compute directional Young's modulus E_dir
  %------------------------------------------
  S = inv(C_becht);   % compliance tensor 3x3
  n(1) = -d(2);       % n direction orthogonal to d
  n(2) =  d(1);
S;
  S4 = computeCompliance4(S);
  % PR_dir = (d.dT : S4 : n.nT ) / (d.dT : S4 : d.dT) 
  % E_dir  = 1 / (d.dT : S4 : d.dT)                
  Z = 0; N  = 0;
  for i=1:2
    for j=1:2
      for k=1:2
        for l=1:2
	  Z = Z + (d(i) * d(j) * n(k) * n(l) * S4(i,j,k,l));
	  N = N + (d(i) * d(j) * d(k) * d(l) * S4(i,j,k,l));
        end
      end
    end
  end
  PR_dir = -Z/N;
  E_dir  = 1/N;
  [PR_dir___E_dir___] = [PR_dir, E_dir];
end


function [PR,E,theta] = computePR_multidirection(CH_becht,N)
  %------------------------------------------
  for i=1:N+1
    theta(i,1) = ((i-1) * 2* pi ) / (N-1);
    d(1) = cos(theta(i));
    d(2) = sin(theta(i));
    d = 1 * d/norm(d,2);
    [PR(i,1), E(i,1)] = computePR_unidirection(CH_becht, d);
  end
  PR;
  E;

end
