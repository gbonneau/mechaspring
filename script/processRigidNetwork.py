import sys

# READS A SPRING FILE
# AND TRANSFORM EACH LENGTH SPRING LINE WITH STIFFNESS INDEX 3
# INTO A CONSTRAINT LENGTH SPRING LINE WITH THE SAME STIFFNESS INDEX
# I.E. EACH LINE
#    l  5 1 3
# IS TRANSFORMED INTO LINE
#    lc 5 1 3
if __name__ == "__main__":
    filename = sys.argv[1]

    with open(filename, "r") as f:
        allLines = f.readlines()
        allNewLines = []
        for oneLine in allLines:
            allWords = oneLine.split()
            if (allWords[0] == "l") and (allWords[3] == "3"):
                allWords[0] = "lc"
            else:
                if (allWords[0] == "a") and (allWords[3]=="4"):
                    allWords[0] = "ac"
            allNewLines.append(' '.join(allWords))
        with open("sortie_LongueurFixe.spg", "w") as fout:
            for oneLine in allNewLines:
                fout.write( f"{oneLine}\n")


