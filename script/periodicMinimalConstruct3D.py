#=====================================================================
# IMPLEMENTATION OF A STRUCTURE HANDLING PERIODIC PHYSICAL SYSTEMS
# G.P. BONNEAU, MAY-JUNE 2023, BERLIN
#
# EXTENSION TO 3D
#
# IT IS DEFINED BY
#
# 1) A SET OF N "UNIT CELL" POINTS vert.
#    THESE POINTS MAY BE ARBITRARY AND NEED NOT TO BE INSIDE A PARTICULAR GEOMETRIC DOMAIN
#
# 2) THREE PERIODICITY VECTORS u0 v0 w0
#    THE ONLY CONSTRAINTS IS THAT THESE THREE VECTORS SHOULD SPAN THE SPACE (I.E. BE LINEARLY INDEPENDANT)
#
# 3) THE WHOLE 3D SPACE IS SAMPLED BY PERIODIC POINTS DEFINED BY QUARTET OF INTEGERS [i,j,k,l]
#    WHERE i IS AN INTEGER INDEXING THE UNIT CELL POINTS
#    j, k, l ARE INTEGERS ENCODING THE TRANSLATION ALONG THE PERIODIC VECTORS u0, v0, w0
#    THE GEOMETRIC POSITION OF PERIODIC POINT [i,j,k,l] IS THEREFORE vert[i] + j * u0 + k * v0 + l * w0
#
# 4) A SET OF PRIMITIVE PHYSICAL SYSTEMS,
#    EACH ONE DEPENDEND ON A FINITE NUMBER OF *PERIODIC* (NOT UNIT CELL) POINTS
#    IN THE CURRENT IMPLEMENTATION, LENGTH SPRINGS, NOTED l, DEPENDING ON TWO PERIODIC POINTS,
#    AND ANGULAR SPRINGS, NOTED a, DEPENDING ON THREE PERIODIC POINTS, ARE IMPLEMENTED
#    BUT IT CAN BE EASILY EXTENTED TO ANY NUMBER OF PERIODIC POINTS.
#
#    THEREFORE LENGTH SPRINGS ARE DEFINED BY TWO QUADRUPLET OF INTEGERS [ [i,j,k,l], [m,n,o,p] ]
#    AND ANGULAR SPRINGS ARE DEFINED BY THREE QUADRUPLET OF INTEGERS [ [i,j,k,l], [m,n,o,p], [q,r,s,t] ]
#
#
# 

import numpy as np
from random import randrange
from tqdm import tqdm
import time
import sys

#----------------------------------------------------
# BOUNDING-BOX OF ALL PERIODIC POINTS INVOLVED IN A SET OF SPRINGS
# DIMENSION DEPENDANT
def boundingBox( vert, u0, v0, w0, lList, aList):
    displayLog = False
    allPeriodicPoint = allPeriodicPointInSpringList( lList, aList)
    bbMin = [+sys.float_info.max, +sys.float_info.max, +sys.float_info.max]
    bbMax = [-sys.float_info.max, -sys.float_info.max, -sys.float_info.max]
    xLabel = 0
    yLabel = 1
    zLabel = 2
    for pp in allPeriodicPoint:
        absolutePoint = periodicToAbsolute( pp, vert, u0, v0, w0)
        if displayLog:
            print(absolutePoint)
        for label in [xLabel, yLabel]:
            bbMin[label] = min( bbMin[label], absolutePoint[label])
            bbMax[label] = max( bbMax[label], absolutePoint[label])
    return bbMin, bbMax


# DIMENSION DEPENDANT
def lengthSpringStatistic( vert, u0, v0, w0, lList):
    if lList == []:
        return np.nan, np.nan, np.nan, np.nan
    length = []
    for lSpring in lList:
        p = [ periodicToAbsolute( lSpring[vi], vert, u0, v0, w0) for vi in [0,1]]
        length.append( np.linalg.norm(p[1]-p[0]))
    return np.min(length), np.max(length), np.mean(length), np.std(length)

# DIMENSION DEPENDANT
def angleSpringStatistic( vert, u0, v0, w0, aList):
    if aList == []:
        return np.nan, np.nan, np.nan, np.nan
    angle = []
    for aSpring in aList:
        p = [ periodicToAbsolute( aSpring[vi], vert, u0, v0, w0) for vi in [0,1,2]]
        firstEdge = np.array(p[1] - p[0])
        secondEdge = np.array(p[2] - p[1])
        firstEdgeNorm = np.linalg.norm( firstEdge)
        secondEdgeNorm = np.linalg.norm( secondEdge)
        crossProduct = np.cross( firstEdge, secondEdge)
        normProduct = firstEdgeNorm * secondEdgeNorm
        scalarProduct = np.dot( firstEdge, secondEdge)
        oneAngle = 2. * np.arctan2( crossProduct, normProduct + scalarProduct)
        angle.append(oneAngle)
    return np.min( angle), np.max( angle), np.mean( angle), np.std( angle)

#----------------------------------------------------
# BARYCENTER OF ALL UNIQUE PERIODIC POINTS INVOLVED IN A SET OF SPRINGS
# AKA "BARYCENTER OF THE PHYSICAL SYSTEM"
# DIMENSION DEPENDANT
def barycenterSpringList( vert, u0, v0, w0, lList, aList):
    allPeriodicPoint = allPeriodicPointInSpringList( lList, aList)
    barycenter = np.zeros( vert[0].shape) # VALID IN DIMENSION 2 AND 3 AS WELL
    for pp in allPeriodicPoint:
        absolutePoint = periodicToAbsolute( pp, vert, u0, v0, w0)
        barycenter = barycenter + absolutePoint
    return barycenter / len(allPeriodicPoint)

#----------------------------------------------------
# RETURNS ALL UNIQUE PERIODIC POINTS - INTEGER QUADRUPLETS - INVOLVED IN A LIST OF LENGTH AND ANGULAR SPRINGS
# DIMENSION DEPENDANT
def allPeriodicPointInSpringList( lList, aList):
    pSet = set()
    for l in lList:
        pSet.add((l[0][0],l[0][1],l[0][2],l[0][3]))
        pSet.add((l[1][0],l[1][1],l[1][2],l[1][3]))
    for a in aList:
        pSet.add((a[0][0],a[0][1],a[0][2],a[0][3]))
        pSet.add((a[1][0],a[1][1],a[1][2],a[1][3]))
        pSet.add((a[2][0],a[2][1],a[2][2],a[2][3]))
    return pSet


#----------------------------------------------------
# RETURNS ALL UNIT CELL POINT INDICES INVOLVED IN A LIST OF LENGTH AND ANGULAR SPRINGS
# THIS IS USE ONLY FOR DEBUGGING AT THIS TIME
# DIMENSION AGNOSTIC
def allUnitCellPointInSpringList( lList, aList):
    pSet = set()
    for l in lList:
        pSet.add(l[0][0])
        pSet.add(l[1][0])
    for a in aList:
        pSet.add(a[0][0])
        pSet.add(a[1][0])
        pSet.add(a[2][0])
    return pSet

#----------------------------------------------------
# RETURN TRUE IFF A PERIODIC VERTEX [i,j,k,l] IS A UNIT CELL VERTEX
# DIMENSION DEPENDANT
def isUnitCell( pVert):
    return (pVert[1] == 0) and (pVert[2] == 0) and (pVert[3] == 0)

#----------------------------------------------------
# GIVEN A LIST OF LENGTH AND ANGULAR SPRINGS DEFINED AS TUPLET OF QUADRUPLETS OF INTEGERS
# RETURNS THE DICTIONNARY index AND THE LIST OF QUADRUPLETS invIndex
# SUCH THAT:
# index AND invIndex ENCODES ONLY THE PERIODIC VERTICES APPEARING IN THE LIST OF SPRINGS
#
# index[(i,j,k,l)] IS THE INTEGER INDEXING THE PERIODIC VERTEX (i,j,k,l)
#
#                IF (j=0 AND k=0 AND l=0) THEN index[(i,0,0,l)] < nAbsVertex
#                IF index[(i,j,k,l)] >= nAbsVertex THEN j!=0 OR k!=0 OR l!=0
#
# IF a < nAbsVertex THEN invIndex[a] IS THE QUADRUPLET (i,0,0,0)
#                   WHERE a IS THE NEW INDEX OF UNIT CELL i
#                   I.E. index[(i,0,0,0)] = a
# IF a >= nAbsVertex THEN invIndex[a] IS THE QUADRUPLET (i,j,k,l)
#                    WHERE j!=0 OR k!=0 OR l!= 0 AND a IS THE NEW INDEX OF UNIT CELL i
#                    I.E. index[(i,0,0,0)] = a

# DIMENSION AGNOSTIC
def indexOneUnitCellVertex( oldQuadruplet, newQuadruplet, index, invIndex, nVertex):
    if tuple(oldQuadruplet) not in index:
        index[tuple(oldQuadruplet)] = nVertex
        invIndex.append(tuple(newQuadruplet))
        nVertex = nVertex+1
    return index, invIndex, nVertex

# DIMENSION DEPENDENT
def indexAllVertexInSpringList( lList, aList):
    index = {}
    invIndex = []
    nVertex = 0
    # INDEX ALL UNIT CELL VERTICES
    for s in lList + aList:
        for v in s[:-1]: # ALL BUT LAST INDEX ARE QUADRUPLET OF INTEGERS ENCODING PERIODIC VERTICES
            if isUnitCell( v):
                index, invIndex, nVertex = indexOneUnitCellVertex( v, v, index, invIndex, nVertex)
    # ADD UNIT CELL VERTICES FOR EACH NON UNIT CELL VERTICES WITHOUT CORRESPONDING UNIT CELL VERTICES
    for s in lList + aList:
        for v in s[:-1]:
            if not isUnitCell( v):
                # (v[0],0,0,0) IS A VALID KEY IFF THE UNIT CELL VERTEX APPEARS IN ONE OF THE SPRINGS
                # IF NOT, THEN A NEW UNIT-CELL VERTEX SHOULD BE ADDED
                unitV = (v[0],0,0,0)
                if unitV not in index:
                    # print("CAS TRES SPECIAL: SOMMET NON-UNIT SANS SOMMET UNIT CORRESPONDANT!!!")
                    index, invIndex, nVertex = indexOneUnitCellVertex( unitV, unitV, index, invIndex, nVertex)    
    # INDEX ALL NON UNIT CELL VERTICES
    nAbsVertex = nVertex
    for s in lList + aList:
        for v in s[:-1]:
            if not isUnitCell( v):
                # (v[0],0,0,0) IS A VALID KEY IFF THE UNIT CELL VERTEX APPEARS IN ONE OF THE SPRINGS
                # IF NOT, THEN A NEW UNIT-CELL VERTEX SHOULD BE ADDED
                unitV = (v[0],0,0,0)
                if unitV not in index:
                    print("CA VA PLANTER TOUT DE SUITE!!!")
                    print("CE PROBLEME AURAIT DU ETRE CORRIGE PAR LE PARCOURS PRECEDENT TRAITANT LES SOMMETS NON UNIT SANS SOMMET UNIT ASSOCIES")
                w = (index[unitV], v[1], v[2], v[3])
                index, invIndex, nVertex = indexOneUnitCellVertex( v, w, index, invIndex, nVertex)
    nPerVertex = nVertex - nAbsVertex

    return index, invIndex, nAbsVertex, nPerVertex


#----------------------------------------------------
# 3 FUNCTIONS TO TRANSLATE PERIODIC POINTS, LENGTH SPRINGS, ANGULAR SPRINGS
# DIMENSION DEPENDENT
def translatePeriodicPoint( p, i, j, k):
    return [p[0],p[1]+i,p[2]+j, p[3]+k]

# RETURN LENGTH SPRING
# DIMENSION DEPENDENT
def translate2Spring(l, i, j, k):
    return [translatePeriodicPoint(l[0],i,j,k), translatePeriodicPoint(l[1],i,j,k), l[2]]

# RETURN ANGULAR SPRING
# DIMENSION DEPENDENT
def translate3Spring(a, i, j, k):
    return [translatePeriodicPoint(a[0],i,j,k), translatePeriodicPoint(a[1],i,j,k), translatePeriodicPoint(a[2],i,j,k), a[3]]

#----------------------------------------------------
# RANDOMLY TRANSLATE LENGTH AND ANGULAR SPRINGS
# USEFUL FOR TESTING INVARIANCE OF THE ENERGY FUNCTION AND THE OPTIMIZATION
# RETURN LENGTH SPRING
# DIMENSION DEPENDENT
def randomTranslate2Spring( l, iMax, jMax, kMax):
    return translate2Spring( l, randrange( iMax+1) - iMax//2, randrange( jMax+1) - jMax//2, randrange( kMax+1) - kMax//2)

# RETURN ANGULAR SPRING
# DIMENSION DEPENDENT
def randomTranslate3Spring( a, iMax, jMax, kMax):
    return translate3Spring( a, randrange( iMax+1) - iMax//2, randrange( jMax+1) - jMax//2, randrange( kMax+1) - kMax//2)

#----------------------------------------------------
# RETURNS THE LISTS OF ALL DUPLICATED SPRRINGS IN A BOX OF RANGE widthxwidth
# WIDTH MUST BE A INTERVAL, E.G. width = range(-1,2) or width = range(1)
# RETURN TWO LISTS OF LENGTH SPRING AND ANGULAR SPRING
# DIMENSION AGNOSTIC
def springInBox( width, lUnitList, aUnitList):
    lList=[]
    aList=[]

    for i in width:
        for j in width:
            for k in width:
                lList = lList +[translate2Spring(l,i,j,k) for l in lUnitList] 
                aList = aList +[translate3Spring(a,i,j,k) for a in aUnitList]

    return lList, aList

#----------------------------------------------------
# TRANSLATE THE UNIT CELL VERTICES vert SO THAT THERE NEW BARYCENTER IS [0,0]
# DIMENSION AGNOSTIC
def translateVerticesAroundBarycenter( vert):
    barycenter = np.mean( vert, axis=0)
    return vert - barycenter

#----------------------------------------------------
# RETURNS THE GEOMETRIC POSITION OF A PERIODIC VERTEX v=[i,j,k]
# DIMENSION DEPENDENT
def periodicToAbsolute( v, vert, u0, v0, w0):
    return vert[v[0]] + v[1] * u0 + v[2] * v0 + v[3] * w0

#----------------------------------------------------
# RETURNS TRUE IF POINTS V1 AND V2 ARE EQUAL UP TO PERIODICITY
# DIMENSION AGNOSTIC
def periodicVertexEqual( v1, v2):
    return v1[0] == v2[0]

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRINGS l1 AND l2 ARE PERIODIC DUPLICATES
# DO NOT TAKE INTO ACCOUNT ORIENTATION OF THE EDGE
# DIMENSION AGNOSTIC
def periodic2SpringEqual( l1, l2):
    result = periodic2SpringEqualOriented( l1, l2) or periodic2SpringEqualOriented( reverse2SpringDirection(l1), l2)
    return result

# DIMENSION AGNOSTIC
def reverse2SpringDirection( l):
    return [l[1],l[0],l[2]] # l[2] IS THE STIFFNESS INDEX

# DIMENSION DEPENDENT
def periodic2SpringEqualOriented( l1, l2):
    isEqual =  (l1[0][0] == l2[0][0]) and (l1[1][0] == l2[1][0]) # FIRST AND SECOND UNIT VERTICES SHOULD BE EQUAL
    uTranslate = l2[0][1] - l1[0][1] # Translation of the first vertex from the first spring to the second spring
    vTranslate = l2[0][2] - l1[0][2]
    wTranslate = l2[0][3] - l1[0][3]
    isEqual = isEqual and ((l2[1][1] - l1[1][1]) == uTranslate) # the translation should be the same for the second vertex
    isEqual = isEqual and ((l2[1][2] - l1[1][2]) == vTranslate)
    isEqual = isEqual and ((l2[1][3] - l1[1][3]) == wTranslate)
    return isEqual

#----------------------------------------------------
# RETURNS TRUE IF ANGULAR SPRINGS a1 AND a2 ARE PERIODIC DUPLICATES
# DIMENSION DEPENDENT
def periodic3SpringEqual( a1, a2):
    isEqual =  (a1[0][0] == a2[0][0]) and (a1[1][0] == a2[1][0]) and (a1[2][0] == a2[2][0]) # THE THREE UNIT VERTICES OF a1 SHOULD BE THE SAME AS THOSE OF a2
    uTranslate = a2[0][1] - a1[0][1] # Translation of the first vertex from the first spring to the second spring
    vTranslate = a2[0][2] - a1[0][2]
    wTranslate = a2[0][3] - a1[0][3]
    isEqual = isEqual and ((a2[1][1] - a1[1][1]) == uTranslate) # the translation should be the same for the second vertex
    isEqual = isEqual and ((a2[1][2] - a1[1][2]) == vTranslate)
    isEqual = isEqual and ((a2[1][3] - a1[1][3]) == wTranslate)
    isEqual = isEqual and ((a2[2][1] - a1[2][1]) == uTranslate) # the translation should be the same for the third vertex
    isEqual = isEqual and ((a2[2][2] - a1[2][2]) == vTranslate) 
    isEqual = isEqual and ((a2[2][3] - a1[2][3]) == wTranslate) 
    return isEqual

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRING l DEPENDS ON PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def vertexIn2Spring( v, l):
    return periodicVertexEqual(v, l[0]) or periodicVertexEqual(v,l[1])

#----------------------------------------------------
# RETURNS TRUE IFF ANGULAR SPRING a DEPENDS ON PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def vertexIn3Spring( v, a):
    return periodicVertexEqual(v, a[0]) or periodicVertexEqual(v,a[1]) or periodicVertexEqual(v,a[2])

#----------------------------------------------------
# RETURNS TRUE IFF LENGTH SPRING l IS A PERIODIC DUPLICATE OF ONE OF THE SPRINGS IN TEH LIST lList
# DIMENSION AGNOSTIC
def springIn2SpringList( l, lList):
    for lCompare in lList:
        if (periodic2SpringEqual( l, lCompare)):
            return True
    return False

#----------------------------------------------------
# RETURNS TRUE IFF ANGULAR SPRING a IS A PERIODIC DUPLICATE OF ONE OF THE SPRINGS IN THE LIST aList
# DIMENSION AGNOSTIC
def springIn3SpringList( a, aList):
    for aCompare in aList:
        if (periodic3SpringEqual( a, aCompare)):
            return True
    return False

#----------------------------------------------------
# REMOVE PERIODIC DUPLICATES IN THE LIST OF LENGTH SPRINGS lList
# DIMENSION AGNOSTIC
def suppressRedundant2Spring( lList):
    lListUnique = []
    for l in tqdm(lList):
        if not springIn2SpringList( l, lListUnique):
            lListUnique.append(l)
        # else:
        #     print(f"2Spring {l} is redundant in list")
    return lListUnique

#----------------------------------------------------
# REMOVE PERIODIC DUPLICATES IN THE LIST OF ANGULAR SPRINGS aList
# DIMENSION AGNOSTIC
def suppressRedundant3Spring( aList):
    aListUnique = []
    for a in tqdm(aList):
        if not springIn3SpringList( a, aListUnique):
            aListUnique.append(a)
        # else:
        #     print(f"3Spring {a} is redundant in list")
    return aListUnique

#----------------------------------------------------
# FROM A LIST OF LENGTH SPRINGS lList AND ANGULAR SPRING aList,
# RETURNS THE LIST OF THOSE SPRINGS DEPENDING FROM PERIODIC VERTEX v
# DIMENSION AGNOSTIC
def filterSpringByVertex( v, lList, aList):
    checkValidity = lambda l: vertexIn2Spring( v, l)
    lListContainsVertex = list(filter( checkValidity, lList))
    checkValidity = lambda a: vertexIn3Spring( v, a)
    aListContainsVertex = list(filter( checkValidity, aList))
    return lListContainsVertex, aListContainsVertex

#----------------------------------------------------
# RETURNS THE COORDINATE OF A 3D VECTOR v IN FRAME (u0,v0,w0)
# DIMENSION DEPENDENT
def periodicFrameCoordinate( v, u0, v0, w0):
    return np.linalg.solve( np.array([u0,v0,w0]).transpose(),v)

#----------------------------------------------------
# GIVEN AN ARBITRARY 3D POINT p AND THREE PERIODICY VECTORS u0, v0, w0,
# RETURNS THE UNIQUE POINT q AND THREE INTEGERS j, k, l
# SUCH THAT p = q + j u0 + k v0 + l w0
# AND q = alpha u0 + beta v0 + gamma w0, with 0 <= alpha < 1, 0 <= beta < 1 and 0 <= gamma < 1
# IN OTHER WORDS, q IS THE UNIQUE PERIODIC DUPLICATE OF p WHICH LIES INSIDE THE AFFINE
# RHOMBOID { (0,0) + alpha u0 + beta v0 + gamma w0, 0 <= alpha < 1, 0 <= beta < 1 and 0 <= gamma < 1}
# DIMENSION DEPENDENT
def unitCellPeriodicDuplicate( p, u0, v0, w0):
    intCoord = periodicIntegerCoordinate( p, u0, v0, w0)
    q = p - intCoord[0] * u0 - intCoord[1] * v0 - intCoord[2] * w0
    return q, intCoord[0], intCoord[1], intCoord[2]

#----------------------------------------------------
# CHANGE A UNIT CELL VERTEX BY A PERIODIC DUPLICATE
# CHANGE ALL OCCURENCES OF [ i, r, s, t] IN ANY OF THE SPRINGS BY [ i, r+j, s+k, t+l]
# THEN CHANGE THE UNIT CELL VERTEX vert[i] BY vert[i] - j u0 - k v0 - l w0
# DIMENSION DEPENDENT
def changeUnitCellVertexByPeriodicDuplicate( i, j, k, l, vert, u0, v0, w0, lList, aList):
    lListOut = []
    aListOut = []
    for s in lList + aList: # TRAVERSE ALL SPRINGS
        sOut=[]
        for v in s[:-1]: # TRAVERSE ALL VERTICES IN CURRENT SPRING
            if (v[0] == i):
                sOut.append([ v[0], v[1] + j, v[2] + k, v[3] + l])
            else:
                sOut.append([v[0], v[1], v[2], v[3]])
        if (len(s) == 3): # A LENGTH SPRING HAS 3 ITEMS
            lListOut.append(sOut+[s[-1]])
        else:
            assert(len(s) == 4) # AN ANGULAR SPRING HAS 4 ITEMS
            aListOut.append(sOut+[s[-1]])
    vert[i] = vert[i] - j * u0 - k * v0 - l * w0
    return vert, u0, v0, w0, lListOut, aListOut

#----------------------------------------------------
# DIMENSION DEPENDENT
def centerAllUnitCellVertices( vert, u0, v0, w0, lList, aList):
    for i in range(len(vert)):
        q, j, k, l = unitCellPeriodicDuplicate( vert[i], u0, v0, w0)
        vert, u0, v0, w0, lList, aList = changeUnitCellVertexByPeriodicDuplicate( i, j, k, l, vert, u0, v0, w0, lList, aList)
    return vert, u0, v0, w0, lList, aList

#----------------------------------------------------
# RETURN TRUE IFF TWO POINTS GIVEN BY THEIR 3D CARTESIAN COORDINATES
# ARE PERIODIC DUPLICATES WITH RESPECT TO THE PERIODICITY VECTORS u0, v0, w0
# THIS IS THE ONLY FUNCTION DEPENDING ON A NUMERICAL VALUE EPSILON.
# IN PRACTICE, EPSILON SHOULD BE SMALLER THAN THE SMALLEST DISTANCE
# BETWEEN TWO UNIT CELL VERTICES
#
# THIS FUNCION IS USED TO DETECT PERIODIC POINTS IN A SET OF POINTS
# GIVEN BY THEIR CARTESIAN COORDINATES
# DIMENSION DEPENDENT
def periodicPair( v1, v2, u0, v0, w0):
    EPSILON = 1e-8
    vRel = periodicFrameCoordinate( v2-v1, u0, v0, w0)
    return np.linalg.norm(np.rint( vRel) - vRel) < EPSILON

#----------------------------------------------------
# RETURNS THE NEAREST INTEGER COORDINATES OF  VECTOR v
# DIMENSION DEPENDENT
def periodicIntegerCoordinate( v, u0, v0, w0):
    return np.rint(periodicFrameCoordinate( v, u0, v0, w0)).astype(int)

#----------------------------------------------------
# GIVEN A LIST pList OF POINTS DEFINED BY THEIR 3D CARTESIAN COORDINATES
# AND PERIODICY VECTORS u0, v0, w0,
# RETURNS THE MINIMAL LIST OF UNIT CELL VERTICES unitV AND  LIST OF QUADRUPLETS
# [i,j,k,l] OF PERIODIC VERTICES WHICH CONTAINS pList
#
# IN OTHER WORDS THIS FUNCTION ENCODES REDUNDANT PERIODIC COPIES OF VERTICES
# AS QUADRUPLET OF INTEGERS [i,j,k,l]
# NOTE THAT A PREFERENCE IS GIVEN TO THE FIRST POINTS IN THE LIST
# WHICH MEANS THAT FOR EACH SET OF PERIODIC DUPLICATES, THE CARTESIAN
# COORDINATES OF THE FIRST ENCOUNTERED POINT ARE STORED AS A UNIT CELL POINT
# DIMENSION DEPENDENT
def minimalPeriodicPointSet( pList, u0, v0, w0):
    unitV=[]
    vList=[]
    for p in tqdm(pList):
        isNew = True
        for qi, q in enumerate(unitV):
            isNew = isNew and (not periodicPair( q, p, u0, v0, w0))
            if (not isNew):
                break
        if isNew:
            unitV.append(p)
            vList.append([len(unitV)-1,0,0,0])
        else:
            pPairCoord = periodicIntegerCoordinate( p - q, u0, v0, w0)
            vList.append( [qi, pPairCoord[0], pPairCoord[1], pPairCoord[2]] )

    return np.array(unitV), vList

#----------------------------------------------------
# GIVEN A LIST OF LENGTH AND ANGULAR SPRINGS, RETURNS PERIODIC DUPLICATES OF THESE SPRINGS
# SUCH THAT THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX, I.E. IS ENCODED AS [i, 0, 0], WHERE
# i IS AN INDEX OF A UNIT CELL VERTEX,
# AND THE SECOND - CENTRAL - VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
# DIMENSION DEPENDENT
def translateSpringsInUnitCell( lList, aList):
    # TRANSLATE THE LENGTH SPRINGS SO THEIR FIRST VERTEX IS IN THE UNIT CELL
    lListUnitCell = [ [ [ l[0][0], 0, 0, 0],[ l[1][0], l[1][1] - l[0][1], l[1][2] - l[0][2], l[1][3] - l[0][3]], l[2] ] for l in lList]
    # TRANSLATE THE ANGULAR SPRINGS SO THEIR FIRST VERTEX IS IN THE UNIT CELL
    # aListUnitCell = [ [ [ a[0][0], 0, 0],[ a[1][0], a[1][1] - a[0][1], a[1][2] - a[0][2]],[ a[2][0], a[2][1] - a[0][1], a[2][2] - a[0][2]] ] for a in aList]
    # TRANSLATE THE ANGULAR SPRINGS SO THEIR SECOND, I.E. CENTRAL, VERTEX IS IN THE UNIT CELL
    aListUnitCell = [ [ [ a[0][0], a[0][1] - a[1][1], a[0][2] - a[1][2], a[0][3] - a[1][3]],[ a[1][0], 0, 0, 0],[ a[2][0], a[2][1] - a[1][1], a[2][2] - a[1][2], a[2][3] - a[1][3]], a[3] ] for a in aList]

    return lListUnitCell, aListUnitCell


# DIMENSION DEPENDENT
def changePeriodicityVectors( vert, u0, v0, w0, u1, v1, w1):
    M = np.linalg.inv(np.array([u0,v0, w0]))
    vert = np.dot( vert, M)
    vert = np.dot( vert, np.array([u1, v1, w1]))
    return vert

#----------------------------------------------------
# TRIVIAL EXAMPLES OF USAGE OF minimalPeriodicPointSet
# AND periodicPair
# DIMENSION DEPENDENT
def exampleByHand():

    u0 = np.array([0.9, 0.1, 0.0])
    v0 = np.array([-0.1, 1.05, 0.0])
    w0 = np.array([0.0, 0.0, 1.0])

    a = np.array([-1.2, 2.3, 0.0])

# periodicPair RETURNS TRUE BECAUSE THE CARTESIAN POINTS a AND a+5.*u0-2.*v0+5.*w0 ARE
# PERIODIC DUPLICATES
    print(periodicPair( a, a+5.*u0-2.*v0+5.*w0, u0, v0, w0))

# pList CONTAINS THREE CARTESIAN POINTS, TWO OF WHICH A PERIODIC DUPLICATES
    pList = np.array( [a, a+5.*u0-2.*v0+3.*w0, a-1.2*u0+2.3*v0+1.5*w0])
# minimalPeriodicPointSet RETURNS THE TWO UNIT CELL VERTICES a AND a-1.2*u0+2.3*v0+1.5*w0
# AND THE THREE TRIPLETS OF INTEGER [0, 0, 0], [0, 5, 2], [1, 0, 0]
    print(minimalPeriodicPointSet( pList, u0, v0, w0))

#====================================== SQUARE
# DIMENSION DEPENDENT
def squareNetwork():
    # SPRINGS
    l0 = [[0,0,0,0], [0,1,0,0], 0] # LINEAR SPRING BETWEEN vert[0] in cell[0,0] and vert[0] in cell[1,0]
    l1 = [[0,0,0,0], [0,0,1,0], 0]
    # ANGLES AROUND SINGLE "CENTRAL" VERTEX [0,0,0]
    a0 = [[0,1,0,0],[0,0,0,0],[0,0,1,0], 1]
    a1 = [[0,0,1,0],[0,0,0,0],[0,-1,0,0], 1]
    a2 = [[0,-1,0,0],[0,0,0,0],[0,0,-1,0], 1]
    a3 = [[0,0,-1,0],[0,0,0,0],[0,1,0,0], 1]

    lList = [l0,l1]
    aList = [a0,a1,a2,a3]

    vert = np.array( [
        [ 0., 0., 0.],
    ])
    u0 = np.array([1.,0.,0.])
    v0 = np.array([0.,1.,0.])
    w0 = np.array([0.,0.,1.])
    return vert, u0, v0, w0, lList, aList

#====================================== TRIANGLE
# DIMENSION DEPENDENT
def triangleNetwork():
    # SPRINGS AROUND THE SINGLE "CENTRAL" VERTEX
    l0 = [[0,0,0,0], [0,1,0,0], 0] 
    l1 = [[0,0,0,0], [0,0,1,0], 0]
    l2 = [[0,0,0,0], [0,-1,1,0], 0]
    a0 = [[0,1,0,0],[0,0,0,0],[0,0,1,0], 1]
    a1 = [[0,0,1,0],[0,0,0,0],[0,-1,1,0], 1]
    a2 = [[0,-1,1,0],[0,0,0,0],[0,-1,0,0], 1]
    a3 = [[0,-1,0,0],[0,0,0,0],[0,0,-1,0], 1]
    a4 = [[0,0,-1,0],[0,0,0,0],[0,1,-1,0], 1]
    a5 = [[0,1,-1,0],[0,0,0,0],[0,1,0,0], 1]

    lList = [l0,l1,l2]
    aList = [a0,a1,a2,a3,a4,a5]

    vert = np.array( [
        [ 0., 0., 0.],
    ])
    u0 = np.array([1.,0.,0.])
    v0 = np.array([np.cos(np.pi/3.),np.sin(np.pi/3.),0.])
    w0 = np.array([0.,0.,1.])
    return vert, u0, v0, w0, lList, aList

#====================================== HEXAGON
# DIMENSION DEPENDENT
def hexagonalNetwork():
    # LENGTH SPRINGS AROUND VERTEX [1,0,0]
    l0 = [[0,0,0,0], [1,0,0,0], 0] 
    l1 = [[1,0,0,0], [0,1,0,0], 0]
    l2 = [[1,0,0,0], [0,0,1,0], 0]
    # ANGLES AROUND INTERIOR VERTEX [0,0,0]
    a0 = [[1,0,0,0], [0,0,0,0], [1,0,-1,0], 1]
    a1 = [[1,-1,0,0], [0,0,0,0], [1,0,0,0], 1]
    a2 = [[1,0,-1,0], [0,0,0,0], [1,-1,0,0], 1]
    # ANGLES AROUND INTERIOR VERTEX [1,0,0]
    a3 = [[0,0,0,0], [1,0,0,0], [0,0,1,0], 1]
    a4 = [[0,0,1,0], [1,0,0,0], [0,1,0,0], 1]
    a5 = [[0,1,0,0], [1,0,0,0], [0,0,0,0], 1]

    lList = [l0,l1,l2]
    aList = [a0,a1,a2,a3,a4,a5]

    vert = np.array( [
        [ 0., 0., 0.],
        [ 1., 0., 0.]
    ])

    # vert = np.array( [
    #     [ 0.2, 0., 0.],
    #     [ 0.8, 0., 0.]
    # ])

    # REGULAR HEXAGON
    u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.),0.])
    v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.),0.])
    w0 = np.array([0.,0.,1.])
    # REENTRANT HONEYCOMB ,-1 POISSON RATIO IN X AND Y
    # u0 = np.array([1.+np.cos(np.pi/3.),np.sin(np.pi/3.)])*0.5
    # v0 = np.array([1.+np.cos(np.pi/3.),-np.sin(np.pi/3.)])*0.5


    return vert, u0, v0, w0, lList, aList

#====================================== IRREGULAR
# DIMENSION DEPENDENT
def irregularNetwork():

    l0 = [[0,0,0,0],[1,0,0,0], 0]
    l1 = [[1,0,0,0],[2,0,0,0], 0]
    l2 = [[0,0,0,0],[1,0,-1,0], 0]
    l3 = [[1,0,0,0],[1,1,1,0], 0]
    l4 = [[2,0,0,0],[0,1,1,0], 0]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [0,0,0]
    a0 = [[1,0,0,0],[0,0,0,0],[2,-1,-1,0], 1]
    a1 = [[2,-1,-1,0],[0,0,0,0],[1,0,-1,0], 1]
    a2 = [[1,0,-1,0],[0,0,0,0],[1,0,0,0], 1]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [1,0,0]
    a3 = [[0,0,1,0],[1,0,0,0],[1,-1,-1,0], 1]
    a4 = [[1,-1,-1,0],[1,0,0,0],[0,0,0,0], 1]
    a5 = [[0,0,0,0],[1,0,0,0],[2,0,0,0], 1]
    a6 = [[2,0,0,0],[1,0,0,0],[1,1,1,0], 1]
    a7 = [[1,1,1,0],[1,0,0,0],[0,0,1,0], 1]
    # ANGULAR SPRINGS AROUND CENTRAL VERTEX [2,0,0]
    a8 = [[0,1,1,0],[2,0,0,0],[1,0,0,0], 1]
    a9 = [[1,0,0,0],[2,0,0,0],[0,1,1,0], 1]

    lList = [l0,l1,l2,l3,l4]
    aList = [a0,a1,a2,a3,a4,a5,a6,a7,a8,a9]

    # "REGULAR LOOK"
    # vert = np.array([ [0.5,-0.5],[1.,0.],[1.5,-0.5]])
    # u0 = np.array([ 1., -1.])
    # v0 = np.array([ 1., 1.])

    # "IRREGULAR LOOK"
    vert = np.array([ [0.6,-0.3,0.0],[1.,0.5,0.0],[1.6,-0.2,0.0]])
    u0 = np.array([ 1.3, -1.3,0.0])
    v0 = np.array([ 1.1, 1.4,0.0])
    w0 = np.array([ 0., 0., 1.])
    
    return vert, u0, v0, w0, lList, aList

#--------------------------------------------------------
# GIVEN THE FILES points.txt CONTAINING A LIST OF 3D-COORDINATES OF POINTS
# 2Spring.txt CONTAINING A LIST OF PAIRS OF INTEGERS [i,j] REPRESENTING EDGES
# 3Spring.txt A LIST OF TRIPLET OF INTEGERS [i,j,k] REPRESENTING ANGLES
# AND u0, v0, w0 THREE 3D VECTORS
# RETURNS THE PERIODIC PHYSICAL SYSTEM vert, u0, v0, w0, lList, aList
# WITH *NO* REDUNDANCIES IN THE PERIODIC VERTICES
# AND THE PERIODIC PHYSICAL PRIMITIVES lList, aList
#
# NOTE THAT THERE MAY BE PERIODIC DUPLICATES IN THE FILES 2Spring.txt AND 3Spring.txt
# THESE PERIODIC DUPLICATES ARE DETECTED AND PROPERLY ENCODED IN THE STRUCTURE
# 
 
def networkFromPointEdgeAngleFiles( u0, v0, w0):
    # MULTIPLICATION BY 3 - HACK POUR STEFANIE
    # pList = np.loadtxt("points.txt") * 3.
    pList = np.loadtxt("points.txt")
    # READ LENGTH AND ANGLE SPRINGS WITH ABSOLUTE INTEGER INDICES OF VERTEX
    lListAbsVert =  np.loadtxt("2Spring.txt").astype(int).tolist()
    aListAbsVert =  np.loadtxt("3Spring.txt").astype(int).tolist()
    return networkFromPointEdgeAngle( u0, v0, w0, pList, lListAbsVert, aListAbsVert)
    
def networkFromPointEdgeAngle( u0, v0, w0, pList, lListAbsVert, aListAbsVert):

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    vert, vList = minimalPeriodicPointSet( pList, u0, v0, w0)

    # CONVERT TO SPRING WITH PERIODIC VERTICES
    # STIFFNESS INDEX 0 FOR LENGTH SPRINGS, 1 FOR ANGULAR SPRINGS
    lListRedundant = [ [ vList[ lAbs[0]], vList[ lAbs[1]], 0] for lAbs in lListAbsVert]
    aListRedundant = [ [ vList[ aAbs[0]], vList[ aAbs[1]], vList[ aAbs[2]], 1] for aAbs in aListAbsVert]

    # REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    vert, u0, v0, w0, lList, aList = centerAllUnitCellVertices( vert, u0, v0, w0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    vert = translateVerticesAroundBarycenter( vert)

    return vert, u0, v0, w0, lList, aList

#--------------------------------------------------------
# SUBDIVIDE A NETWORK FOR A GIVEN MAXIMAL targetLength LENGTH OF THE EDGES
# EACH LENGTH SPRING IS *UNIFORMLY* SUBDIVIDED
# SO THAT THE LENGTH OF THE NEW EDGES IS SMALLER THAN targetLength
# ADDITIONNALY EACH ANGULAR SPRING WHICH CONTAINS A SUBDIVIDED EDGE IS ADAPTED TO THE SUBDIVISION


def uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0, w0):
    EPSILON = 1e-10
    lengthOfEdge = np.linalg.norm( p2 - p1)
    # print(f"length of edge = {lengthOfEdge}")
    nSubEdge = np.floor((lengthOfEdge) / (targetLength + EPSILON)).astype(int) + 1
    # nSubEdge = int((lengthOfEdge) / (targetLength + EPSILON)) + 1
    # subLength = lengthOfEdge / nSubEdge
    # print(f"length of subdivided edge = {subLength} < {targetLength} = target length")
    # print(f"number of subdivided edges = {nSubEdge}, total length = {nSubEdge * subLength}={lengthOfEdge}")
    subVector = (p2 - p1) / nSubEdge
    return nSubEdge,  subVector

def subdivideNetwork( vert, u0, v0, w0, lList, aList, targetLength):
    # print(f"subdivide the network for a target maximal length of edges of {targetLength}")
    indexStiffnessNewAngularSpring = 2
    lListOut = []
    aListOut = []
    vertOut = vert.tolist()
    # print(f"number of unit cell before subdivision = {len(vertOut)}")

    for l in lList:
        # print(f"processing length spring {l} with stiffness index {l[2]}")

        # FIRST STEP: SUBDIVIDE THIS LENGTH SPRING

        p1 = periodicToAbsolute( l[0], vert, u0, v0, w0)
        p2 = periodicToAbsolute( l[1], vert, u0, v0, w0)
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0, w0)

        # if (nSubEdge <=1):
            # print("edge is not subdivided")
            # sys.exit(1)
        newVert = p1
        indexPrevious = l[0]
        indexMinus2 = None
        for i in range( nSubEdge-1):

            newVert = newVert + subVector # COORDINATES OF NEW UNIT CELL VERTEX
            indexCurrent = [len(vertOut), 0, 0, 0] # INDEX OF NEW UNIT CELL VERTEX
            vertOut.append( newVert)

            # print(f"adding edge {newEdge}")
            newEdge = [ indexPrevious, indexCurrent, l[2]] # NEW LENGTH EDGE HAS SAME STIFFNESS INDEX AS ORIGINAL EDGE
            lListOut.append( newEdge )

            if (i>=1):
                # print(f"je rajoute deux ressorts d'angle interieur entre les sommets {indexMinus2} {indexPrevious} {indexCurrent}")
                aListOut.append( [indexMinus2, indexPrevious, indexCurrent, indexStiffnessNewAngularSpring])
                aListOut.append( [indexCurrent, indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])

            indexMinus2 = indexPrevious
            indexPrevious = indexCurrent

        newEdge = [ indexPrevious, l[1], l[2]]
        # print(f"adding edge {newEdge}")
        lListOut.append( newEdge )
        if (nSubEdge > 1): # ONLY ADD THE LAST INTERIOR ANGULAR SPRING IF IT EXISTS,
            # E.G. IF THE EDGE IS AT LEAST ONE TIME SUBDIVIDED
            aListOut.append( [indexMinus2, indexPrevious, l[1], indexStiffnessNewAngularSpring])
            aListOut.append( [l[1], indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])
        # else:
        #     print(f"{indexMinus2} {indexPrevious} {l[1]}")

    # SECOND STEP: ADAPT ALL ANGULAR SPRINGS TO THE SUBDIVIDED EDGES

    aListAdapted = []
    for a in aList:
        # print(f"processing angular spring {a}")
        p0 = periodicToAbsolute( a[0], vert, u0, v0, w0)
        p1 = periodicToAbsolute( a[1], vert, u0, v0, w0)
        p2 = periodicToAbsolute( a[2], vert, u0, v0, w0)
        adaptedEdgeNotSubdivided = False
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p0, p1, targetLength, vert, u0, v0, w0)
        if (nSubEdge <=1):
            adaptedEdgeNotSubdivided = True
            # print("adapted edge 1 is not subdivided")
        a0SubdAbs = p1 - subVector
        nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0, w0)
        if (nSubEdge <=1):
            adaptedEdgeNotSubdivided = True
            # print("adapted edge 2 is not subdivided")
        a2SubdAbs = p1 + subVector
        foundA0 = False
        foundA2 = False
        for i, p in enumerate(vertOut):
            if periodicPair( a0SubdAbs, p, u0, v0, w0):
                foundA0 = True
                intCoordFloat = periodicFrameCoordinate( a0SubdAbs - p, u0, v0, w0)
                # print(intCoordFloat)
                intCoord = np.rint(intCoordFloat).astype(int)
                # print(intCoord)
                if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                    sys.exit(1)
                pSubd0 = [ i, intCoord[0], intCoord[1], intCoord[2]]
                checkAbsolute = periodicToAbsolute(pSubd0, vertOut, u0, v0, w0)
                # print(f"found periodic point {pSubd0}={checkAbsolute}={a0SubdAbs} from {p} for anchoring to point {a[0]}")
                if np.linalg.norm( checkAbsolute - a0SubdAbs) > 1e-6:
                    print(f"Fatal error: for periodic point {pSubd0}, {checkAbsolute} != {a0SubdAbs} from {p} for anchoring to point {a[0]}")
                    sys.exit(1)
            if periodicPair( a2SubdAbs, p, u0, v0, w0):
                foundA2 = True
                intCoordFloat = periodicFrameCoordinate( a2SubdAbs - p, u0, v0, w0)
                # print(intCoordFloat)
                intCoord = np.rint(intCoordFloat).astype(int)
                # print(intCoord)
                if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                    sys.exit(1)
                pSubd2 = [ i, intCoord[0], intCoord[1], intCoord[2]]
                checkAbsolute = periodicToAbsolute(pSubd2, vertOut, u0, v0, w0)
                # print(f"found periodic point {pSubd2}={checkAbsolute}={a2SubdAbs} from {p} for anchoring to point {a[2]}")
                if np.linalg.norm( checkAbsolute - a2SubdAbs) > 1e-6:
                    print(f"Fatal error: for periodic point {pSubd2}, {checkAbsolute} != {a2SubdAbs} from {p} for anchoring to point {a[2]}")
                    sys.exit(1)
        if not foundA0:
            print(f"unable to find anchor point for point {a[0]}")
            sys.exit(1)
        if not foundA2:
            print(f"unable to find anchor point for point {a[2]}")
            sys.exit(1)
        # if adaptedEdgeNotSubdivided:
            # print("Adapted edge is not subdivided")
            # sys.exit(1)
        assert(foundA0 and foundA2)
        aListAdapted.append([ pSubd0, a[1], pSubd2, a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
    vertOut = np.array(vertOut)
    aListOut = aListOut + aListAdapted
    return vertOut, u0, v0, w0, lListOut, aListOut

def subdivideAllEdgeWithGivenStiffness( vert, u0, v0, w0, lList, aList, stiffnessIndex, targetLength):
    print(f"Subdividing network with {len(lList)} edges")
    nIter = 0
    allSubdivided = False
    while not allSubdivided: 
        print(nIter)
        nIter = nIter + 1
        allSubdivided = True
        for ei, l in enumerate(lList):
            if l[2] == stiffnessIndex:
                allSubdivided = False
                break
        if not allSubdivided:
            vert, u0, v0, w0, lList, aList = subdivideOneEdge( vert, u0, v0, w0, lList, aList, ei, targetLength)

    for l in lList:
        if l[2] == -1:
            l[2] = stiffnessIndex

    return vert, u0, v0, w0, lList, aList

def subdivideOneEdge( vert, u0, v0, w0, lList, aList, ei, targetLength):
    lSpring = lList[ei]
    # print(f"subdivide the edge {ei}={lSpring} for a target maximal length of edges of {targetLength}")

    indexStiffnessSubdividedEdge = -1
    indexStiffnessNewAngularSpring = 1 # NEW ANGULAR SPRINGS HAVE THIS SPECIFIC STIFFNESS INDEX


    indexStiffnessNewAngularSpring = 1 # NEW ANGULAR SPRINGS HAVE A SPECIFIC STIFFNESS INDEX
    lListOut = []
    aListOut = []
    vertOut = vert.tolist()
    # print(f"number of unit cell before subdivision = {len(vertOut)}")

    # print(f"processing length spring {lSpring} with stiffness index {lSpring[2]}")

    # FIRST STEP: SUBDIVIDE THIS LENGTH SPRING

    p1 = periodicToAbsolute( lSpring[0], vert, u0, v0, w0)
    p2 = periodicToAbsolute( lSpring[1], vert, u0, v0, w0)
    nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, p2, targetLength, vert, u0, v0, w0)

    # if (nSubEdge <=1):
        # print("edge is not subdivided")
        # sys.exit(1)
    newVert = p1
    indexPrevious = lSpring[0]
    indexMinus2 = None
    for i in range( nSubEdge-1):

        newVert = newVert + subVector # COORDINATES OF NEW UNIT CELL VERTEX
        indexCurrent = [len(vertOut), 0, 0, 0] # INDEX OF NEW UNIT CELL VERTEX
        vertOut.append( newVert)

        # print(f"adding edge {newEdge}")
        # newEdge = [ indexPrevious, indexCurrent, lSpring[2]] # NEW LENGTH EDGE HAS SAME STIFFNESS INDEX AS ORIGINAL EDGE
        newEdge = [ indexPrevious, indexCurrent, indexStiffnessSubdividedEdge] # NEW LENGTH EDGE HAS SAME STIFFNESS INDEX AS ORIGINAL EDGE
        lListOut.append( newEdge )

        if (i>=1):
            # print(f"je rajoute deux ressorts d'angle interieur entre les sommets {indexMinus2} {indexPrevious} {indexCurrent}")
            aListOut.append( [indexMinus2, indexPrevious, indexCurrent, indexStiffnessNewAngularSpring])
            aListOut.append( [indexCurrent, indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])

        indexMinus2 = indexPrevious
        indexPrevious = indexCurrent

    # newEdge = [ indexPrevious, lSpring[1], lSpring[2]]
    newEdge = [ indexPrevious, lSpring[1], indexStiffnessSubdividedEdge]
    # print(f"adding edge {newEdge}")
    lListOut.append( newEdge )

    if (nSubEdge > 1): # ONLY ADD THE LAST INTERIOR ANGULAR SPRING IF IT EXISTS,
        # E.G. IF THE EDGE IS AT LEAST ONE TIME SUBDIVIDED
        aListOut.append( [indexMinus2, indexPrevious, lSpring[1], indexStiffnessNewAngularSpring])
        aListOut.append( [lSpring[1], indexPrevious, indexMinus2, indexStiffnessNewAngularSpring])
    # else:
    #     print(f"{indexMinus2} {indexPrevious} {l[1]}")

    # SECOND STEP: ADAPT ANGULAR SPRINGS TO THE SUBDIVIDED EDGE

    aListAdapted = []
    for a in aList:
        # print(f"processing angular spring {a}")
        la1 = [ a[0], a[1], a[3]]
        la2 = [ a[1], a[2], a[3]]
        attached = 0
        if periodic2SpringEqual( la1, lSpring):
            print(f"angle spring {a} is attached to length spring {lSpring}")
            attached = 1
        if periodic2SpringEqual( la2, lSpring):
            print(f"angle spring {a} is attached to length spring {lSpring}")
            attached = 2

        if attached == 0:
            print(f"je recopie l ancien ressort d'angle {a}")
            aListOut.append(a)
        else:
            if attached == 1:
                p1 = periodicToAbsolute( a[1], vert, u0, v0, w0)
                pn1 = periodicToAbsolute( a[0], vert, u0, v0, w0)
            else: 
                p1 = periodicToAbsolute( a[1], vert, u0, v0, w0)
                pn1 = periodicToAbsolute( a[2], vert, u0, v0, w0)
            adaptedEdgeNotSubdivided = False
            nSubEdge, subVector = uniformSubdivisionEdgeFromTargetLength( p1, pn1, targetLength, vert, u0, v0, w0)
            if (nSubEdge <=1):
                adaptedEdgeNotSubdivided = True
                # print("adapted edge 1 is not subdivided")
            a1SubdAbs = p1 + subVector
            foundAnchor = False
            for i, p in enumerate(vertOut):
                if periodicPair( a1SubdAbs, p, u0, v0, w0):
                    foundAnchor = True
                    intCoordFloat = periodicFrameCoordinate( a1SubdAbs - p, u0, v0, w0)
                    # print(intCoordFloat)
                    intCoord = np.rint(intCoordFloat).astype(int)
                    # print(intCoord)
                    if np.linalg.norm( intCoord - intCoordFloat) > 1.e-3:
                        sys.exit(1)
                    pSub1 = [ i, intCoord[0], intCoord[1], intCoord[2]]
                    checkAbsolute = periodicToAbsolute(pSub1, vertOut, u0, v0, w0)
                    # print(f"found periodic point {pSubd0}={checkAbsolute}={a0SubdAbs} from {p} for anchoring to point {a[0]}")
                    if np.linalg.norm( checkAbsolute - a1SubdAbs) > 1e-6:
                        print(f"Fatal error: for periodic point {pSub1}, {checkAbsolute} != {a1SubdAbs} from {p} for anchoring to point {a[1]}")
                        sys.exit(1)
            if not foundAnchor:
                print(f"unable to find anchor point for point {a[0]}")
                sys.exit(1)
            # if adaptedEdgeNotSubdivided:
                # print("Adapted edge is not subdivided")
                # sys.exit(1)
            assert(foundAnchor)
            if (attached == 1):
                aListAdapted.append([ pSub1, a[1], a[2], a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
            else:
                aListAdapted.append([ a[0], a[1], pSub1, a[3]] ) # NEW ANGULAR SPRING HAS SAME STIFFNESS INDEX a[3] AS ORIGNAL ONE
    vertOut = np.array(vertOut)
    lListOut = lList[:ei] + lListOut + lList[ei+1:]
    aListOut = aListOut + aListAdapted
    return vertOut, u0, v0, w0, lListOut, aListOut

def modifyStiffnesses( lList, aList):
    for l in lList:
        match l[2]:
            case 0:
                l[2] = 0
            case 1:
                l[2] = 3
            case 2:
                l[2] = 1
            case 3:
                l[2] = 2
            case 4:
                l[2] = 4
    for a in aList:
        match a[3]:
            case 0:
                a[3] = 0
            case 1:
                a[3] = 3
            case 2:
                a[3] = 1
            case 3:
                a[3] = 2
            case 4:
                a[3] = 4  
    return lList, aList


#--------------------------------------------------------
# GIVEN AN OBJ FILE filename WITH ONLY EDGES (KEYCHAR "l")
# AND 3 PERIODICITIY VECTORS
# RETURNS A PERIODIC NETWORK WITH LENGTH SPRINGS AT EACH EDGE
# AND ANGLE SPRINGS BETWEEN EACH PAIR OF EDGES CONNECTING AT ONE VERTEX
def networkFromObjLineFile( u0, v0, w0, filename):

    softSpringLengthStiffnessIndex = 0
    softSpringAngularStiffnessIndex = 1
    tangentAngularStiffnessIndex = 2
    rigidSpringLengthStiffnessIndex = 3
    rigidSpringAngularStiffnessIndex = 4
    vertDuplicate = []
    lListAbsVert = []
    with open( filename, "r") as f:
        allLines = f.readlines()
        for oneLine in allLines:
            word = oneLine.split()
            if (len(word)>0):
                if (word[0] == "v"):
                    vertDuplicate.append([float(word[1]), float(word[2]), float(word[3])])
                elif (word[0] == "l"):
                    lListAbsVert.append([int(word[1])-1, int(word[2])-1])

    nNode = len(vertDuplicate)
    nEdge = len(lListAbsVert)

    print(f"nNode = {nNode} nEdge = {nEdge}")

    # ADD TWO VERTICES PER NODE FOR ENCODING RIGID CONNECTIONS
    vertDuplicate = np.array(vertDuplicate)
    vU0 = vertDuplicate + (u0+v0+w0) / 10.
    vV0 = vertDuplicate + (u0-v0+w0) / 10.
    vertDuplicate = np.concatenate( (vertDuplicate, vU0, vV0))

    # RECOVER UNIT VERTICES AND INTEGER PERIODIC COORDINATES
    vert, vList = minimalPeriodicPointSet( vertDuplicate, u0, v0, w0)
    print(f"nNode + 2 * nNode = {len(vertDuplicate)}")
    print(f"nNode after removing duplicated periodic nodes = {len(vert)}")
    assert(len(vList) == vertDuplicate.shape[0])

    # ADD TWO EDGES FOR ENCODING RIGID CONNECTIONS
    for i in range(nNode):
        lListAbsVert.append([i,i+nNode])
        lListAbsVert.append([i,i+2*nNode])

# SOFT LENGTH SPRINGS FOR RODS: INDEX EDGE < nEdge
    lListRedundant = [ [ vList[ lAbs[0]], vList[ lAbs[1]], softSpringLengthStiffnessIndex] for lAbs in lListAbsVert[:nEdge]]
# RIGID LENGTH SPRINGS FOR RIDIG CONNECTIONS: INDEX EDGE >= nEdge
    lListRedundant = lListRedundant + [ [ vList[ lAbs[0]], vList[ lAbs[1]], rigidSpringLengthStiffnessIndex] for lAbs in lListAbsVert[nEdge:]]
# RIGID ANGULAR SPRINGS, INDEX NODE < nNode
    aListRedundant = [ [ vList[i+nNode], vList[i] , vList[i+2*nNode], rigidSpringAngularStiffnessIndex]  for i in range( nNode)]
# TANGENT ANGULAR SPRINGS, INDEX < nEdge
    aListRedundant = aListRedundant + [ [ vList[lAbs[1]], vList[lAbs[0]], vList[lAbs[0] + nNode], tangentAngularStiffnessIndex]  for lAbs in lListAbsVert[:nEdge]]
    aListRedundant = aListRedundant + [ [ vList[lAbs[1]], vList[lAbs[0]], vList[lAbs[0] + 2 * nNode], tangentAngularStiffnessIndex]  for lAbs in lListAbsVert[:nEdge]]
    aListRedundant = aListRedundant + [ [ vList[lAbs[0]], vList[lAbs[1]], vList[lAbs[1] + nNode], tangentAngularStiffnessIndex]  for lAbs in lListAbsVert[:nEdge]]
    aListRedundant = aListRedundant + [ [ vList[lAbs[0]], vList[lAbs[1]], vList[lAbs[1] + 2 * nNode], tangentAngularStiffnessIndex]  for lAbs in lListAbsVert[:nEdge]]

    print( f"number of length springs, including duplicates = {len(lListRedundant)}")
    print( f"number of angular springs, including duplicates = {len(aListRedundant)}")

# REMOVE REDUNDANT SPRINGS
    lList = suppressRedundant2Spring( lListRedundant)
    aList = suppressRedundant3Spring( aListRedundant)

    print(f"number of unique length springs  = {len(lList)}")
    print(f"number of unique angular springs  = {len(aList)}")

    # TRANSLATE UNIT CELL VERTICES SO THAT THEY LIE IN THE UNIT PARALELLOGRAM
    vert, u0, v0, w0, lList, aList = centerAllUnitCellVertices( vert, u0, v0, w0, lList, aList)

    # TRANSLATE THE SPRINGS SO THAT
    #     THE FIRST VERTEX OF LENGTH SPRINGS IS A UNIT CELL VERTEX
    #     THE CENTRAL VERTEX OF ANGULAR SPRINGS IS A UNIT CELL VERTEX
    lList, aList = translateSpringsInUnitCell( lList, aList)

    # TRANSLATE THE UNIT CELL VERTICES SO THAT THEIR BARYCENTER IS THE AFFINE ORIGIN
    vert = translateVerticesAroundBarycenter( vert)

    return vert, u0, v0, w0, lList, aList

#--------------------------------------------------------
# READ FROM AN ASCII FILE WITH THE SPRING FILE FORMAT SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# v x y z  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# u0 x y z -> FIRST VECTOR OF PERIODICITY
# v0 x y z -> SECOND VECTOR OF PERIODICITY
# w0 x y z -> THIRD VECTOR OF PERIODICITY
# s ks -> STIFFNESS FLOAT VALUE ks
# l i j k l p q r s si -> LENGTH SPRING BETWEEN PERIODIC VERTICES [i,j,k,l] AND [p,q,r,s] 
#                     WITH STIFFNESS VALUE OF INDEX si
# a i j k l m n o p q r s t si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES [i,j,k,l]
#                           [m,n,o,p] AND [q,r,s,t] WITH STIFFNESS VALUE OF INDEX si
def networkFromSpringFile( filename: str):
    vert = np.zeros((0,3))
    u0 = np.zeros(3)
    v0 = np.zeros(3)
    w0 = np.zeros(3)
    lList = []
    aList = []  
    with open( filename, "r" ) as f:
        allLines = f.readlines()
        for aLine in allLines:
            allWords = aLine.split()
            match allWords[0]:
                case "v":
                    # ARRAY OF SHAPE (1,3)
                    onePoint = np.array([[float(allWords[1]), float(allWords[2]), float(allWords[3])]])
                    vert = np.concatenate(  (vert, onePoint))
                case "u0":
                    # ARRAY OF SHAPE (3,)
                    u0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "v0":
                    # ARRAY OF SHAPE (3,)
                    v0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "w0":
                    # ARRAY OF SHAPE (3,)
                    w0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "s":
                    print("stiffness value ignored")
                case "l":
                    index = [int(aWord) for aWord in allWords[1:]]
                    # [0,1,2,3]: FIRST PERIODIC VERTEX OF LENGTH SPRING, [4,5,6,7]: SECOND VERTEX, [8]: STIFFNESS INDEX
                    lList.append( [ [index[0], index[1], index[2], index[3]], [index[4], index[5], index[6], index[7]], index[8]])
                case "a":
                    index = [int(aWord) for aWord in allWords[1:]]
                    # [0,1,2,3]: FIRST PERIODIC VERTEX OF ANGULAR SPRING, [4,5,6,7],[8,9,10,11]: SECOND AND THIRD VERTEX, [9]: STIFFNESS INDEX
                    aList.append( [ [index[0], index[1], index[2], index[3]], [index[4], index[5], index[6], index[7]], [index[8], index[9], index[10], index[11]], index[12]])
    # lList, aList = translateSpringsInUnitCell( lList, aList)
    
    return vert, u0, v0, w0, lList, aList

#--------------------------------------------------------
# READ FROM AN ASCII FILE WITH THE INDIRECT SPRING FILE FORMAT SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# 
# v x y z  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y, z
# 
# u0 x y z -> FIRST VECTOR OF PERIODICITY
# v0 x y z -> SECOND VECTOR OF PERIODICITY
# w0 x y z -> THIRD VECTOR OF PERIODICITY
# 
# p i j k l -> PERIODIC VERTEX WITH INTEGER TRIPLET [i,j,k,l]
#            WHERE i IS THE INDEX OF A UNIT CELL VERTEX
#            AND p = v_i + j * u0 + k * v0 + l * w0

# s ks -> STIFFNESS FLOAT VALUE ks
# 
# l i j si -> LENGTH SPRING BETWEEN VERTICES i AND j 
#             WITH STIFFNESS VALUE OF INDEX si
# a i j k si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES i, j AND k,
#               WITH STIFFNESS VALUE OF INDEX si

def networkFromIndirectSpringFile( filename: str):
    vert = np.zeros((0,3))
    u0 = np.zeros(3)
    v0 = np.zeros(3)
    w0 = np.zeros(3)
    lList = []
    aList = []
    sList = []
    allQuadruplet = []
    with open( filename, "r" ) as f:
        allLines = f.readlines()
        for aLine in allLines:
            allWords = aLine.split()
            match allWords[0]:
                case "v":
                    # ARRAY OF SHAPE (1,2)
                    onePoint = np.array([[float(allWords[1]), float(allWords[2]), float(allWords[3])]])
                    allQuadruplet.append([ vert.shape[0], 0, 0, 0])
                    vert = np.concatenate(  (vert, onePoint))
                case "u0":
                    # ARRAY OF SHAPE (2)
                    u0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "v0":
                    # ARRAY OF SHAPE (2)
                    v0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "w0":
                    # ARRAY OF SHAPE (2)
                    w0 = np.array([float(allWords[1]), float(allWords[2]), float(allWords[3])])
                case "p":
                    index = [int(aWord) for aWord in allWords[1:]]
                    allQuadruplet.append([ index[0], index[1], index[2], index[3]])
                case "s":
                    print(f"stiffness value {allWords[1]} ignored")
                    sList.append(float(allWords[1]))
                case "l": # READING A LENGTH SPRING [3,1,5]. 3,1: (INDIRECT) INDICES OF THE TWO VERTICES, 5: INDEX OF STIFFNESS
                    index = [int(aWord) for aWord in allWords[1:]]
                    # allTriplet[index[0]] IS THE INTEGER TRIPLET ENCODING THE FIRST PERIODIC VERTEX OF THE LENGTH SPRING,
                    # allTriplet[index[1]] IS THE SECOND VERTEX, index[2] IS THE STIFFNESS INDEX
                    lList.append( [allQuadruplet[index[0]], allQuadruplet[ index[1]], index[2]])
                case "a": # READING AN ANGULAR SPRING [3,1,2,5]. 3,1,2: (INDIRECT) INDICES OF THE THREE VERTICES, 5: INDEX OF STIFFNESS
                    index = [int(aWord) for aWord in allWords[1:]]
                    # allTriplet[index[0]] IS THE INTEGER TRIPLET ENCODING THE FIRST PERIODIC VERTEX OF THE ANGULAR SPRING,
                    # allTriplet[index[1,2]] ENCODE THE SECOND AND THIRD VERTICES, index[3] IS THE STIFFNESS INDEX
                    aList.append( [allQuadruplet[index[0]], allQuadruplet[ index[1]], allQuadruplet[ index[2]], index[3]])
    # lList, aList = translateSpringsInUnitCell( lList, aList)
    
    return vert, u0, v0, w0, lList, aList, sList

#--------------------------------------------------------
# WRITE THE PERIODIC STRUCTURE IN AN ASCII FILE WITH THE FOLLOWING SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# v x y z -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y, z
# u0 x y z -> FIRST VECTOR OF PERIODICITY
# v0 x y z -> SECOND VECTOR OF PERIODICITY
# w0 x y z -> THIRD VECTOR OF PERIODICITY
# s ks -> STIFFNESS FLOAT VALUE ks
# l i j k l p q r s si -> LENGTH SPRING BETWEEN PERIODIC VERTICES [i,j,k,l] AND [p,q,r,s] 
#                     WITH STIFFNESS VALUE OF INDEX si
# a i j k l m n o p q r s t si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES [i,j,k,l]
#                           [m,n,o,p] AND [q,r,s,t] WITH STIFFNESS VALUE OF INDEX si

def writeInSpringFileFormat( vert, u0, v0, w0, lList, aList, filename: str):
    with open( filename, "w" ) as f:
        # WRITE UNIT CELL VERTICES
        f.write("# UNIT CELL VERTICES COORDINATES\n")
        for p in vert[:,]:
            f.write( f"v {p[0]} {p[1]} {p[2]}\n")
        # WRITE PERIODICITY VECTORS
        f.write("# PERIODICITY VECTORS\n")
        f.write( f"u0 {u0[0]} {u0[1]} {u0[2]}\n")
        f.write( f"v0 {v0[0]} {v0[1]} {v0[2]}\n")
        f.write( f"w0 {w0[0]} {w0[1]} {w0[2]}\n")
        # WRITE FICTIVE STIFFNESS VALUES FOR LENGTH AND ANGULAR SPRINGS
        # THIS IS FOR COMPATIBLITY WITH MECHASPRING
        # COUNT THE NUMBER OF UNIQUE INDICES OF STIFFNESS. l[2] IS THE INDEX OF STIFFNESS FOR LENGTH SPRINGS, a[3] FOR ANGULAR SPRINGS
        defaultStiffnesses = np.ones( np.max( [l[2] for l in lList] + [a[3] for a in aList])+1)
        f.write("# DEFAULT STIFFNESSES\n")
        for s in defaultStiffnesses:
            f.write( f"s {s}\n")
        # WRITE LENGTH SPRINGS
        f.write("# LENGTH SPRINGS: 4 INDICES OF FIRST PERIODIC POINT, 4 INDICES OF SECOND PERIODIC POINT, INDEX OF STIFFNESS\n")
        for l in lList: # l[0][0,1,2,3]: FIRST PERIODIC POINT, l[1][0,1,2,3]: SECOND PERIODIC POINT, l[2]: INDEX OF STIFFNESS
            f.write( f"l {l[0][0]} {l[0][1]} {l[0][2]} {l[0][3]} {l[1][0]} {l[1][1]} {l[1][2]} {l[1][3]} {l[2]}\n" )
         # WRITE ANGULAR SPRINGS
        f.write("# ANGULAR SPRINGS: 4 INDICES OF FIRST, SECOND AND THIRD PERIODIC POINTS, INDEX OF STIFFNESS\n")
        for a in aList: # a[0][0,1,2]: FIRST PERIODIC POINT, a[1,2][0,1,2]: SECOND AND THIRD PERIODIC POINTS, a[3]: INDEX OF STIFFNESS
            f.write( f"a {a[0][0]} {a[0][1]} {a[0][2]} {a[0][3]} {a[1][0]} {a[1][1]} {a[1][2]} {a[1][3]} {a[2][0]} {a[2][1]} {a[2][2]} {a[2][3]} {a[3]}\n" )       
    return

#--------------------------------------------------------
# WRITE THE PERIODIC STRUCTURE IN AN ASCII FILE WITH THE FOLLOWING SPECIFICATION
# LINES BEGINNING WITH "#" ARE IGNORED
# PORTION OF LINES AFTER "#" ARE IGNORED
# 
# v x y z  -> UNIT CELL VERTEX WITH FLOAT COORDINATES x, y
# 
# u0 x y z -> FIRST VECTOR OF PERIODICITY
# v0 x y z -> SECOND VECTOR OF PERIODICITY
# w0 x y z -> THIRD VECTOR OF PERIODICITY
# 
# p i j k l -> PERIODIC VERTEX WITH INTEGER TRIPLET [i,j,k,l]
#             WHERE i IS THE INDEX OF A UNIT CELL VERTEX
#             AND p = v_i + j * u0 + k * v0 + l * w0

# s ks -> STIFFNESS FLOAT VALUE ks
# 
# l i j si -> LENGTH SPRING BETWEEN VERTICES i AND j 
#             WITH STIFFNESS VALUE OF INDEX si
# a i j k si -> ANGULAR SPRING BETWEEN PERIODIC VERTICES i, j AND k,
#               WITH STIFFNESS VALUE OF INDEX si

def writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, filename: str):

    index, invIndex, nUnitCellVertex, nPeriodicVertex = indexAllVertexInSpringList( lList, aList)

    # for key in index:
    #     print(f"{key}: {index[key]}")
    # for i, triplet in enumerate(invIndex):
    #     print(f"{i}: {triplet}")
    with open( filename, "w" ) as f:
        # WRITE UNIT CELL VERTICES IN THE ORDER GIVEN BY INDEX
        f.write("# UNIT CELL VERTICES COORDINATES\n")
        for viQuadruplet in invIndex[:nUnitCellVertex]:
            f.write( f"v {vert[viQuadruplet[0]][0]} {vert[viQuadruplet[0]][1]} {vert[viQuadruplet[0]][2]}\n")
        # WRITE PERIODICITY VECTORS
        f.write("# PERIODICITY VECTORS\n")
        f.write( f"u0 {u0[0]} {u0[1]} {u0[2]} \n")
        f.write( f"v0 {v0[0]} {v0[1]} {v0[2]}\n")
        f.write( f"w0 {w0[0]} {w0[1]} {w0[2]}\n")
        # WRITE PERIODIC VERTICES IN THE ORDER GIVEN BY INDEX
        f.write("# PERIODIC VERTICES USED IN THE SPRINGS\n")
        for viQuadruplet in invIndex[nUnitCellVertex:]:
            f.write(f"p {viQuadruplet[0]} {viQuadruplet[1]} {viQuadruplet[2]} {viQuadruplet[3]}\n")   
        # WRITE FICTIVE STIFFNESS VALUES FOR LENGTH AND ANGULAR SPRINGS
        # THIS IS FOR COMPATIBLITY WITH MECHASPRING
        # COUNT THE NUMBER OF UNIQUE INDICES OF STIFFNESS. l[2] IS THE INDEX OF STIFFNESS FOR LENGTH SPRINGS, a[3] FOR ANGULAR SPRINGS
        defaultStiffnesses = np.ones( np.max( [l[2] for l in lList] + [a[3] for a in aList])+1)
        f.write("# DEFAULT STIFFNESSES\n")
        for s in defaultStiffnesses:
            f.write( f"s {s}\n")
        # WRITE LENGTH SPRINGS
        f.write("# LENGTH SPRINGS: INDEX OF FIRST PERIODIC POINT, INDEX OF SECOND PERIODIC POINT, INDEX OF STIFFNESS\n")
        for i,l in enumerate(lList):
            # print(l)
            # l[0]: FIRST PERIODIC POINT, l[1]: SECOND PERIODIC POINT, l[2]: INDEX OF STIFFNESS
            f.write( f"l {index[tuple(l[0])]} {index[tuple(l[1])]} {l[2]}\n" )
         # WRITE ANGULAR SPRINGS
        f.write("# ANGULAR SPRINGS: INDEX OF FIRST, SECOND AND THIRD PERIODIC POINTS, INDEX OF STIFFNESS\n")
        for i, a in enumerate(aList):
            # a[0]: FIRST PERIODIC POINT, al[1,2]: SECOND AND THIRD PERIODIC POINTS, a[3]: INDEX OF STIFFNESS
            f.write( f"a {index[tuple(a[0])]} {index[tuple(a[1])]} {index[tuple(a[2])]} {a[3]}\n" )
    return

if __name__=="__main__":

    #----------------------------------------------
    # WRITE SPG FILE FOR VARIOUS NETWORKS
    # writeInIndirectSpringFileFormatDefaultStiffness( *triangleNetwork(), "triangle.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *squareNetwork(), "square.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *hexagonalNetwork(), "hexagonal.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *irregularNetwork(), "irregular.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *chelouNetwork(), "chelou.spg")
    # writeInIndirectSpringFileFormatDefaultStiffness( *networkFromPointEdgeAngleFiles(np.array([1.,0.]),np.array([0.,1.])), "pointEdgeAngle.spg")

    # print(vert)

    #---------------------------------
    # READ AN SPG FILE WITH REDUNDANCIES, REMOVE THE REDUNDANCIES AND STORE THE NEW NETWORK
    # vert, u0, v0, lList, aList = networkFromIndirectSpringFile(sys.argv[1])
    # lList = suppressRedundant2Spring( lList)
    # aList = suppressRedundant3Spring( aList)
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, sys.argv[1] + "_unique.spg")
    #---------------------------------

    #---------------------------------
    # CONSTRUCT A NETWORK, PROCESS IT AND SAVE IT
    # vert, u0, v0, w0, lList, aList = hexagonalNetwork()
    # vert, u0, v0, w0, lList, aList = triangleNetwork()
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, "sortie.spg")
    # vert, u0, v0, w0, lList, aList = subdivideNetwork( vert, u0, v0, w0, lList, aList, float(sys.argv[1]))
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, "sortie_subdivided.spg")
    #---------------------------------

    #---------------------------------
    # READ A NETWORK IN SPG FORMAT, PROCESS IT AND SAVE IT
    # vert, u0, v0, w0, lList, aList = networkFromIndirectSpringFile(sys.argv[1])
    # vert, u0, v0, w0, lList, aList = subdivideAllEdgeWithGivenStiffness( vert, u0, v0, w0, lList, aList, 0, float(sys.argv[2]))
    # writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, "sortie_subdivided.spg")
    #---------------------------------   

    # READ A NETWORK IN OBJ FORMAT, PROCESS IT AND SAVE IT
    u0 = np.array([1.,0.,0.])
    v0 = np.array([0.,1.,0.])
    w0 = np.array([0.,0.,1.])
    vert, u0, v0, w0, lList, aList = networkFromObjLineFile( u0, v0, w0, sys.argv[1])
    writeInIndirectSpringFileFormatDefaultStiffness( vert, u0, v0, w0, lList, aList, "sortie.spg")


    # exampleByHand()

    # writeDuplicateMechaspringMesh( "toto.txt", *triangleNetwork())
