import numpy as np
import matplotlib.pyplot as plt
import sys

def Rot( angle):
    return np.array([ [np.cos(angle), -np.sin(angle)],
                        [np.sin(angle), np.cos(angle)]])

def poissonRatioFromFile( perReferenceFileName, perFinalFileName, angle=0.):
    perReference = np.transpose(np.loadtxt(perReferenceFileName)[:,0:2]) # READ ONLY TWO FIRST COORDINATES 
    perFinal = np.transpose(np.loadtxt(perFinalFileName)[:,0:2])
    poissonRatio( perReference, perFinal, angle)

def poissonRatio( perReference, perFinal, angle=0.):
    print( f"-- Compute from poissonRatioFromPeriodicityVectors and angle of stretching ...")
    deformationGradient = np.dot( perFinal, np.linalg.inv( perReference)) # compute the deformation gradient, according to formula (12) in Schumacher's Appendix
    gradientDisplacement = deformationGradient - np.identity(2)
    infinitesimalStrainTensor = 0.5 * (np.transpose( gradientDisplacement) + gradientDisplacement) # infinitesimal strain tensor - linearization valid for small extension
    finiteStrainTensor = infinitesimalStrainTensor + 0.5 * np.dot(np.transpose( gradientDisplacement), gradientDisplacement) # finite strain tensor, non linear formula valid also for large extension
    rotMat = Rot( angle)
    longVec = rotMat[:,0] # longitudinal direction: direction of stretch
    transVec = rotMat[:,1] # transverse direction: orthogonal to stretch
    longInfScalar = np.dot( np.dot(np.transpose( longVec), infinitesimalStrainTensor), longVec) # extension in the longitudinal direction, computed from the infinitesimal strain tensor
    transInfScalar = np.dot( np.dot(np.transpose( transVec), infinitesimalStrainTensor), transVec) # extension (or compression) orthogonal to the longitudinal direction
    longFinScalar = np.dot( np.dot(np.transpose( longVec), finiteStrainTensor), longVec) # same as longInfScalar, computed with the *finite* strain tensor
    transFinScalar = np.dot( np.dot(np.transpose( transVec), finiteStrainTensor), transVec) # sane as transInfScalar, computed from the finite strain tensor

    displayLog = True
    if displayLog:
        print( f"deformationGradient = {deformationGradient}")
        print( f"gradientDisplacement = {gradientDisplacement}")
        print( f"infinitesimalStrainTensor = {infinitesimalStrainTensor}")
        print( f"finiteStrainTensor = {finiteStrainTensor}")
        print( f"longitudinal direction = {longVec}")
        print( f"angle of stretching = {angle}")
        print( f"longInfScalar = {longInfScalar}")
        print( f"transInfScalar = {transInfScalar}")  
        print( f"longFinScalar = {longFinScalar}")
        print( f"transFinScalar = {transFinScalar}")  
        print( f"poissonInfinitesimal = {-transInfScalar/longInfScalar}") # Poisson ratio with the infinitesimal strain tensor
        print( f"poissonFinite = {-transFinScalar/longFinScalar}") # Poisson ratio with the finite strain tensor

    print( f"... done computing Poisson Ratio")
    return -transFinScalar/longFinScalar

if __name__=="__main__":

    if (len(sys.argv) != 4):
        print(f"usage: {sys.argv[0]} periodicityReference.txt periodicityFinal.txt angle")
        sys.exit(1)
        
    angle = float( sys.argv[3])
    poissonRatioFromFile( sys.argv[1], sys.argv[2], angle)


