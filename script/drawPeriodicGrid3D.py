import meshio
import numpy as np
import sys
import argparse

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="repeat multiple copies of a periodic network given in vtk format")
    parser.add_argument( 'mesh', type=str, help="input mesh in vtk format. The output has the same name with \"_repeat\" at the end of it")
    parser.add_argument( 'periodicity', type=str, help="an ascii file containing the two periodicity vectors encoded as 3 floats each (6 floats total)")
    parser.add_argument('repeat_u',type=int, help="number of copies in the periodic u direction")
    parser.add_argument('repeat_v',type=int, help="number of copies in the periodic v direction")
    parser.add_argument('repeat_w',type=int, help="number of copies in the periodic w direction")
    args = parser.parse_args()
    filename = args.mesh
    perFilename = args.periodicity
    if not filename.endswith(".vtk"):
        print("input mesh must be in vtk format")
    if not filename.endswith(".vtk"):
        print("input mesh must be in vtk format")
    outputFilename = filename.removesuffix(".vtk")+"_repeat.vtk"

    myMesh = meshio.read( filename)
    myMesh.write( outputFilename)

    perVectors = np.loadtxt( perFilename)
    assert(perVectors.shape==(3,3))

    print(perVectors[0,:])
    print(perVectors[1,:])
    print(perVectors[2,:])
    currentEdges = myMesh.cells_dict['line']

    # startingPointU = myMesh.points
    startingPointU = myMesh.points - perVectors[0,:] * (args.repeat_u+0)  - perVectors[1,:] * (args.repeat_v+0) - perVectors[2,:] * (args.repeat_w+0)
    allPoints=np.zeros((0,3))
    allEdges=np.zeros((0,2), dtype=int)
    for ui in range(-1,2*args.repeat_u):
        startingPointV = startingPointU
        for vj in range(-1,2*args.repeat_v):
            currentPoints = startingPointV
            for wj in range(-1,2*args.repeat_w):
                allPoints = np.concatenate( (allPoints, currentPoints))
                allEdges = np.concatenate( (allEdges, currentEdges))
                currentPoints = currentPoints + perVectors[2,:]
                currentEdges = currentEdges + currentPoints.shape[0]
            startingPointV = startingPointV + perVectors[1,:]
        startingPointU = startingPointU + perVectors[0,:]

    outputCells = [("line", allEdges)]
    meshio.write_points_cells( outputFilename, allPoints, outputCells)


