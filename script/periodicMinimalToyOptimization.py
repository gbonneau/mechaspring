import numpy as np
import torch
from torchmin import minimize
from torchmin import minimize_constr
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from torchvision import io

import time
import sys

import periodicMinimalConstruct
import periodicMinimalVisualization



#----------------------------------------------------
# PART I
# SCALAR FUNCTIONS MEASURING DISTANCE TO CONSTRAINTS
#----------------------------------------------------

# DISTANCE OF NETWORK BARYCENTRE TO A GIVEN POINT
def squareDistanceOfBarycentreToPoint( doF):
    vert = doF.reshape(-1,2)
    barycenter = torch.mean( vert, axis=0)
    # print(vert)
    # print(barycenter)
    return (barycenter - torch.tensor([0.,0.])).square().sum()

#-------------------------------------------------------------
# PART II
# VECTOR FUNCTIONS MEASURING GEOMETRIC INTRINSINCS OF NETWORK
#-------------------------------------------------------------

# VECTOR OF ALL LENGTHS OF LENGTH SPRINGS
def lengthOfLengthSpring( vert, u0, v0, pIndex, uTrans, vTrans):
    # TWO NEXT LINES VERY IMPORTANT: SPARSE ENCODING OF ALL VERTICES INVOLVED IN THE LENGTH SPRINGS
    # ALLOWED THANKS TO THE THREE CONSTANT ARRAYS pIndex, uTrans AND vTrans, COMPUTED IN A PREPROCESS STEP
    beginPoint = vert[pIndex[0]] + uTrans[0][:,np.newaxis] * u0 + vTrans[0][:,np.newaxis] * v0
    endPoint = vert[pIndex[1]] + uTrans[1][:,np.newaxis] * u0 + vTrans[1][:,np.newaxis] * v0

    length = torch.linalg.vector_norm( endPoint - beginPoint, axis=1)
    return length

# HELP FUNCTION FOR COMPUTING CROSS-PRODUCTS OF MULTIPLE 2D VECTORS
def computeMultiple2dCrossProduct( a, b):
    ap = F.pad( a, (0,1)) # ADD 0 TO TRANSFORM THE 2D VECTORS a INTO 3D VECTORS ap
    bp = F.pad( b, (0,1))
    vectorCross = torch.cross( ap, bp, dim=1) # NOW THAT WE HAVE 3D VECTORS, COMPUTE THE CROSS PRODUCT
    return vectorCross[...,2] # ONLY RETURNS THE LAST COORDINATE, THE FIRST TWO ARE ZERO

# VECTOR OF ALL ANGLES OF ANGLES SPRINGS
def angleOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans):

    # THREE NEXT LINES VERY IMPORTANT: SPARSE ENCODING OF ALL VERTICES INVOLVED IN THE ANGLE SPRINGS
    # ALLOWED THANKS TO THE THREE CONSTANT ARRAYS pIndex, uTrans AND vTrans, COMPUTED IN A PREPROCESS STEP
    beginPoint = vert[pIndex[0]] + uTrans[0][:,np.newaxis] * u0 + vTrans[0][:,np.newaxis] * v0
    centerPoint = vert[pIndex[1]] + uTrans[1][:,np.newaxis] * u0 + vTrans[1][:,np.newaxis] * v0
    endPoint = vert[pIndex[2]] + uTrans[2][:,np.newaxis] * u0 + vTrans[2][:,np.newaxis] * v0

    firstEdge = centerPoint - beginPoint
    secondEdge = endPoint - centerPoint

    firstEdgeNorm = torch.linalg.vector_norm( firstEdge, axis=1)
    secondEdgeNorm = torch.linalg.vector_norm( secondEdge, axis=1)

    crossProduct = computeMultiple2dCrossProduct( firstEdge, secondEdge)
    normProduct = firstEdgeNorm * secondEdgeNorm
    scalarProduct = torch.sum( firstEdge * secondEdge, dim=-1) # SCALAR PRODUCTS OF ROWS

    # curvatureVector = 2. * crossProduct / (normProduct + scalarProduct)

    angle = 2. * torch.atan2( crossProduct, normProduct + scalarProduct)

    displayLog = False
    if displayLog:
        print(firstEdge)
        print(secondEdge)
        print(crossProduct)
        print(normProduct)
        print(scalarProduct)
        # print(curvatureVector)
        print(f"(norm + scalar, cross)= ({normProduct + scalarProduct}, {crossProduct})")
        # print(f"curvature vector = {curvatureVector}")
        print(f"angle = {angle}")

    return angle

#-------------------------------------------------------------
# PART III
# PREPROCESS FOR EFFICIENT SPARSE ENCODING OF ALL VERTICES IN SPRINGS
#-------------------------------------------------------------

def constantArrayForSystemOfVertex( springList, dim):
    pIndex = [ [] for i in range(dim)]  # INITIALIZE LIST OF EMPTY LISTS
    uTrans = [ [] for i in range(dim)] 
    vTrans = [ [] for i in range(dim)] 
    sIndex = []
    for s in springList:
        # SPRING s = [ [i,j,k], [l,m,n], ... ] i,l: INDEX OF UNIT CELL VERTICES, j,k,m,n: INDEX OF PERIODIC CELL
        for p in range(dim): # TWO POINTS IN A LENGTH SPRING
            pIndex[p].append( s[p][0]) # INDEX OF UNIT CELL VERTEX
            uTrans[p].append( s[p][1]) # NUMBER OF UNITS TO TRANSLATE IN THE U PERIODIC DIRECTION
            vTrans[p].append( s[p][2]) # SAME AS PREVIOUS FOR THE V PERIODIC DIRECTION
        sIndex.append( s[dim]) # INDEX OF SPRING STIFFNESS 
    return pIndex, np.array(uTrans), np.array(vTrans), sIndex

#-------------------------------------------------------------
# PART IV
# OBJECTIVE FUNCTIONS
#-------------------------------------------------------------

def sumOfSquaredDeltaLength( vert, u0, v0, pIndex, uTrans, vTrans, lengthRest):
    length = lengthOfLengthSpring( vert, u0, v0, pIndex, uTrans, vTrans)
    deltaLength = (length - lengthRest).square().sum()
    return deltaLength

def sumOfSquaredDeltaAngle( vert, u0, v0, pIndex, uTrans, vTrans, angleAtRest):
    angle = angleOfAngleSpring( vert, u0, v0, pIndex, uTrans, vTrans)
    deltaAngle = (angle - angleAtRest).square().sum()
    return deltaAngle

def objectiveFunctionDeltaLengthAngleAndConstraint( doF):
    global u0Torch, v0Torch, pIndexLength, uTransLengthTorch, vTransLengthTorch, lengthRest, \
                                pIndexAngle, uTransAngleTorch, vTransAngleTorch, angleAtRest
    global monitorLength, monitorAngle, monitorConstraint, monitorObjectiveFunction

    lengthTerm = sumOfSquaredDeltaLength( doF.reshape(-1,2), u0Torch, v0Torch, pIndexLength, uTransLengthTorch, vTransLengthTorch, lengthRest)
    angleTerm = sumOfSquaredDeltaAngle( doF.reshape(-1,2), u0Torch, v0Torch, pIndexAngle, uTransAngleTorch, vTransAngleTorch, angleAtRest)
    constraintTerm = squareDistanceOfBarycentreToPoint( doF)
    # print(f"   norm length term = {torch.linalg.vector_norm(lengthTerm)}")
    # print(f"norm curvature term = {torch.linalg.vector_norm(curvatureVectorTerm)}")

    weightLength = 1.
    weightAngle = 0.0001
    weightConstraint = 10.

    objectiveFunction = weightLength * lengthTerm + weightAngle * angleTerm + weightConstraint * constraintTerm

    monitorLength = weightLength * lengthTerm
    monitorAngle = weightAngle * angleTerm
    monitorConstraint = weightConstraint * constraintTerm
    monitorObjectiveFunction = objectiveFunction

    return objectiveFunction

def displayProgress( currentDoF):
    global nIter
    global monitorLength, monitorAngle, monitorConstraint, monitorObjectiveFunction
    global lListUnitDisplay, aListUnitDisplay

    tensorBoardScalarLog = True
    if tensorBoardScalarLog:
        logToTensorBoard.add_scalars(
            "myData", {
                "length": monitorLength.item(),
                "angle": monitorAngle.item(),
                "constraint": monitorConstraint.item(),
                "objective wo constraint": (monitorLength + monitorAngle).item(),
                "objective": monitorObjectiveFunction.item()
            },
            global_step=nIter,walltime=True)
        
    tensorBoardImageLog = True
    stepIterSaveImage = 10
    if tensorBoardImageLog and nIter % stepIterSaveImage == 0:
        showGraphics( currentDoF.reshape(-1,2).detach().numpy(), u0Torch.detach().numpy(), v0Torch.detach().numpy(), "currentNetwork")

    periodicMinimalConstruct.writeInIndirectSpringFileFormatDefaultStiffness( currentDoF.reshape(-1,2).detach().numpy(), u0Torch.detach().numpy(), v0Torch.detach().numpy(), lUnitListDisplay, aUnitListDisplay, f"network_{nIter}.spg")
    
    nIter = nIter+1
    return None

def showGraphics( vert, u0, v0, filename):
    global lListDisplay, aListDisplay, nIter

    periodicMinimalVisualization.initPeriodicVisualization( vert, u0, v0)
    periodicMinimalVisualization.displayPeriodicGrid( widthDisplay)
    periodicMinimalVisualization.displaySpringList( lListDisplay, aListDisplay)
    periodicMinimalVisualization.showFigure( filename, display=True, save=True)
    imageAsTensor = io.read_image(filename+".png")
    print(f"je sauve l'image {filename} a l'iteration {nIter}")
    logToTensorBoard.add_image( "networkImage", imageAsTensor, nIter)

#---------------------------------------------------------------
# MAIN ROUTINE: OPTIMIZATION
#---------------------------------------------------------------
def testOptimization( vert, u0, v0, lUnitList, aUnitList):
    global u0Torch, v0Torch, pIndexLength, uTransLengthTorch, vTransLengthTorch, lengthRest, \
                                pIndexAngle, uTransAngleTorch, vTransAngleTorch, angleAtRest
    global nIter

# FIRST STEP: PREPROCESSING FOR EFFICIENT SPARSE ENCODING
    # ENCODING LENGTHS
    pIndexLength, uTransLength, vTransLength, sIndexLength = constantArrayForSystemOfVertex( lUnitList, dim=2)
    uTransLengthTorch = torch.Tensor(uTransLength)
    vTransLengthTorch = torch.Tensor(vTransLength)

    # ENCODING ANGLES
    pIndexAngle, uTransAngle, vTransAngle, sIndexAngle = constantArrayForSystemOfVertex( aUnitList, dim=3)
    uTransAngleTorch = torch.Tensor(uTransAngle)
    vTransAngleTorch = torch.Tensor(vTransAngle)

# SECOND STEP: COMPUTING GEOMETRIC INTRINSINCS AT REST
    vertRestTorch = torch.tensor( vert, requires_grad=False)
    u0RestTorch = torch.tensor( u0, requires_grad=False)
    v0RestTorch = torch.tensor( v0, requires_grad=False)

    lengthRest = lengthOfLengthSpring( vertRestTorch, u0RestTorch, v0RestTorch, pIndexLength, uTransLengthTorch, vTransLengthTorch)
    angleAtRest = angleOfAngleSpring( vertRestTorch, u0RestTorch, v0RestTorch, pIndexAngle, uTransAngleTorch, vTransAngleTorch) # CAREFUL: ANGLE AND NOT LENGTH HERE!!!

# THIRD STEP: LOADING
    bbMin, bbMax = periodicMinimalConstruct.boundingBox( vert, u0, v0, lUnitList, aUnitList)
    lengthDiagonalBB = np.linalg.norm(np.array(bbMax) - np.array(bbMin))
    perturbationMagnitude = 0.01
    vertTorch = torch.tensor( vert + np.random.randn(*vert.shape)*perturbationMagnitude * lengthDiagonalBB, requires_grad=True)
    u0Torch = torch.tensor( u0, requires_grad=True)
    # u0Torch = torch.tensor( u0 - np.array([0.5,0.]), requires_grad=True)
    v0Torch = torch.tensor( v0, requires_grad=True)

    showGraphics( vert, u0, v0, "restNetwork")
    nIter = nIter+1
    showGraphics( vertTorch.detach().numpy(), u0Torch.detach().numpy(), v0Torch.detach().numpy(), "initNetwork")
    nIter = nIter+1

    print("="*16+" STARTING OPTIMIZATION "+"="*16)
    start = time.time()
    # CONSTRAINT MINIMIZATION
    # res = minimize_constr( objectiveFunctionLengthAndCurvatureVector, vertTorch, \
    #                           constr=dict( \
    #     fun=lambda x: constraintFunction(x), \
    #     lb=0., ub=0.), disp=3)

    # UNCONSTRAINED MINIMIZATION
    # 'newton-exact','newton-cg','cg','bfgs','l-bfgs'
    # newton-exact very slow
    # newton-cg fast and accurate
    # cg fast but innacurate
    # bfgs slow and innacurate
    # l-bfgs fast but innacurate
    # BEST IS NEWTON-CG
    res = minimize( objectiveFunctionDeltaLengthAngleAndConstraint, vertTorch.reshape(-1), method='newton-cg', disp=True, callback=displayProgress)
    
    stop = time.time()
    print("="*16+f" OPTIMIZATION ACHIEVED IN {round(stop-start,2)}s "+"="*16)
    print(f"objective function at optimum = {res.fun.item()}")
    print(f"norm of gradient at optimum = {torch.linalg.vector_norm(res.grad)}")

    showGraphics( res.x.reshape(-1,2).detach().numpy(), u0Torch.detach().numpy(), v0Torch.detach().numpy(), "optimNetwork")

def testComputeAngle( vert, u0, v0, aUnitList):
    pIndex, uTrans, vTrans = constantArrayForSystemOfVertex( aUnitList, dim=3)
    uTransTorch = torch.Tensor(uTrans)
    vTransTorch = torch.Tensor(vTrans)
    vertTorch = torch.tensor( vert, requires_grad=True)
    u0Torch = torch.tensor( u0, requires_grad=True)
    v0Torch = torch.tensor( v0, requires_grad=True)
    angle = angleOfAngleSpring( vertTorch, u0Torch, v0Torch, pIndex, uTransTorch, vTransTorch)
    print(angle)

def main():
    global widthDisplay, lListDisplay, aListDisplay, lUnitListDisplay, aUnitListDisplay
    global nIter, logToTensorBoard

    nIter=0 # FOR MONITORING CONVERGENCE
    logToTensorBoard = SummaryWriter("mesLogs") # FOR LOGGING IN TENSORBOARD
    filename = sys.argv[1]
    vert, u0, v0, lUnitList, aUnitList, sList = periodicMinimalConstruct.networkFromIndirectSpringFile( filename)

    # COMPUTE PERIODICALLY EXTENDED LIST OF SPRINGS, FOR DISPLAY ONLY
    # widthDisplay = range(-1,2)
    widthDisplay = range(1)
    lListDisplay, aListDisplay = periodicMinimalConstruct.springInBox( widthDisplay, lUnitList, aUnitList)
    lUnitListDisplay = lUnitList # COPY IN GLOBAL VARIABLES FOR LATER ACCESS IN WRITING FUNCTIONS
    aUnitListDisplay = aUnitList

    testOptimization( vert, u0, v0, lUnitList, aUnitList)
    # testComputeCurvatureVector( vert, u0, v0, aUnitList)
    
if __name__=="__main__":


    main()

