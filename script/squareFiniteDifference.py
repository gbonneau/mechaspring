import sys
import numpy as np
from matplotlib import pyplot as plt

# PROGRAMME ILLUSTRANT LE CALCUL PAR DIFFERENCE FINIE DELA DERIVEE DE (x-x0)^2 EN x0
# LA FONCTION EST CALCULEE DE DEUX MANIERES: (x-x0)*(x-x0) OU x*x - 2.*x0*x + x0*x0
# AVEC LA PREMIERE MANIERE, LA DIFFERENCE FINIE A 10-8 POUR x0 VARIANT DE 1000 A 2000
# EST CONSTANTE ET VAUT  9.999788493105083e-09
# AVEC LA DEUXIEME METHODE, EXACTEMENT LA MEME DIFFERENCE FINIE POUR LES MEMES VALEURS DE x0
# PRENDS DES VALEURS ENTRE -0.1862645149230957 ET 0.1862645149230957

def mySquareFunctionMethod1( x, x0):
    return (x-x0)*(x-x0)

def mySquareFunctionMethod2( x, x0):
    return x*x - 2.*x0*x + x0*x0

def finiteDifference( f, x, x0):
    finiteDiff = (f(x+h,x0)-f(x,x0))
    finiteDiff /= h
    return finiteDiff

if __name__ == '__main__':
    h = 1.e-8
    xTest = np.linspace( 2000.,3000.,num=1001)
    fdMethod1 = []
    fdMethod2 = []
    for x0 in xTest:
        fdMethod1.append( finiteDifference( mySquareFunctionMethod1, x0,x0))
        fdMethod2.append( finiteDifference( mySquareFunctionMethod2, x0,x0))
    plt.figure()
    plt.plot( xTest, fdMethod1, label="method correct")
    plt.plot( xTest, fdMethod2, label="wrong method")
    plt.legend()
    plt.show()