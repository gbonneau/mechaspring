import sys
import numpy as np

def printPoint( x, y):
	print( x, y, 0.)

def printEdge( vi, vj, offset):
	print( vi+offset, vj+offset)

def printIntraConnectivity( offset):
	L = [0, 4, 1, 5, 2, 6, 3, 7, 0]
	[printEdge(L[i], L[i+1], offset) for i in range(len(L)-1)]

def fourStarNetwork( nHorizontal = 5, nVertical = 4, width = 10., height = 10., delta = 0.2, gamma = 0.5):

	nVertex = 8 * nHorizontal * nVertical

	# ------------------ NUMBER OF VERTICES ----------------------

	print(nVertex)

	# ----------------------- GEOMETRY ------------------------
	if (nHorizontal > 1):
		dX = width / (nHorizontal-1)
	else:
		dX = 0.
	if (nVertical > 1):
		dY = height / (nVertical-1)
	else:
		dY = 0.

	xC = 0.
	yC = 0.
	for j in range(nVertical):
		xC = 0.
		for i in range(nHorizontal):
			printPoint( xC, yC - delta)
			printPoint( xC + delta, yC)
			printPoint( xC, yC + delta)
			printPoint( xC - delta, yC)
			printPoint( xC + gamma, yC - gamma)
			printPoint( xC + gamma, yC + gamma)
			printPoint( xC - gamma, yC + gamma)
			printPoint( xC - gamma, yC - gamma)
			xC = xC + dX
		yC = yC + dY

	# ----------------------- CONNECTIVITY ------------------------

	# CONNECTIVITY INSIDE STARS

	offset = 0
	for j in range(nVertical):
		for i in range(nHorizontal):
			printIntraConnectivity( offset)
			offset = offset + 8


	# CONNECTIVITY BETWEEN STARS
	offset = 0
	for j in range(nVertical):
		for i in range(nHorizontal):
			if (i != nHorizontal-1):
				print( 1 + offset, 3 + offset + 8)
			if (j != nVertical-1):
				print( 2 + offset, 0 + offset + nHorizontal * 8)
			offset = offset + 8

	# --------------------- BOUNDARY CONDITIONS ----------------------

	print("Boundary conditions")
	# ----- LEFT BOUNDARY -----
	print(nVertical)
	offset = 0
	for i in range(nVertical):
		print( 7 + offset, end=" ")
		print( 6 + offset, end=" ")
		offset = offset + 8 * nHorizontal
	print("")
	# ----- RIGHT BOUNDARY -----
	print(nVertical)
	offset = 8 * (nHorizontal-1)
	for i in range(nVertical):
		print( 4 + offset, end=" ")
		print( 5 + offset, end=" ")
		offset = offset + 8 * nHorizontal
	print("")

if __name__ == "__main__":
	if (len(sys.argv) != 3):
		print("Usage: " + sys.argv[0] + "<nHorizontal> <nVertical>")
		sys.exit(1)

	nHorizontal = int(sys.argv[1])
	nVertical = int(sys.argv[2])

	fourStarNetwork( nHorizontal = nHorizontal, nVertical = nVertical, width = 6., height = 6., delta = 1.5 / max(nHorizontal, nVertical), gamma = 2.5 / max(nHorizontal, nVertical))



