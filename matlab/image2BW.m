function BW = image2BW(I)
%==================================
% transforms PNG image (nxmx3) or (nxmx1)
% into indicator (material) matrix
%
% Input images  where
%	WHITE defines VOID material and
%	BLACK defines SOLID material
%	----------------------
% 	RBG = 0 0 0  black
% 	RGB = 255 255 255 white
%	----------------------
%
% Output: matrial matrix BW with
%
%==================================
% 	==> value 1 = solid material  (black pixels R,G,B < 200)
% 	==> value 0 = void material   (white pixels R,G,B > 200)
%==================================

% Remark:  si on imprime la material matrix BW as an ppm image, 
%	   it inverese black/white colors of the input image
%==================================

  plot_true = 0;

    if plot_true
      figure; imshow(I); title('image2BW: input image');
    end

    [N1,N2,N3] = size(I);

	%==================================
	% RBG = 0 0 0  black      ===> 1 (solid)
	% RGB = 255 255 255 white ===> 0 (void)
	%==================================

%%  GRAY-SCALE IMAGE  :   I is a two-dimensional (M-by-N) array
    if N3 == 1 
      display( 'Gray-scale Image  (NxMx1)');
      % Indicatior Matrix BW: void=0,  solid=1
      BW = zeros(N1,N2);  %init to void phase (=0) all pixels correspond to white input pixels
      for i=1:N1
        for j=1:N2
          if I(i,j,1) < 200   % not white 
    	     % solid phase
             BW(i,j) = 1;     % solid in material matrix
          end
        end
      end

%%  RGB-COLOR IMAGE   : I is a three-dimensional (M-by-N-by-3) array.
    elseif N3 == 3
      display( 'Color Image  (NxMx3)');
      % Indicator Matrix BW: void=0,  solid=1
      BW = zeros(N1,N2);  %init to void phase (=0) all pixels correspond to white input pixels
      for i=1:N1
        for j=1:N2
          % if I(i,j,1) < 210 && I(i,j,2) < 210 && I(i,j,3) < 210  % not white    % moins de solid
            if I(i,j,1) < 200 && I(i,j,2) < 200 && I(i,j,3) < 200  % not white     % plus de solid
          % if I(i,j,1) < 100 && I(i,j,2) < 100 && I(i,j,3) < 100  % not white    % moins de solid
    	 %void phase
             BW(i,j) = 1;        % solid in material matrix
          end
        end
    end
% test periodicity top/bottom
%BW(1,1:30)
%BW(end,1:30)

    if plot_true
      figure; imshow(BW);
      %write BW as ppm file
      imwrite(BW,'OUTPUT/test_out.ppm','ppm');
    end
end
