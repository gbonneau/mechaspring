clear all; close all;

%====================================================
%====================================================
function [CH] = numerical_homogenization(v,E,BW,ignore_void)
  %---------------------------------------------------
  % Input: BM indicator matrix, specifies whether a finite element contains
  %	    material 1 (value = 1) or material 2 (xe = 2).
  %
  % Lame coefficients: lambda,mu (mu shear modulus - module de cisaillement)
  %---------------------------------------------------
  %%    %==== SOLID PHASE ====  material 1
             %Caoutchouk E=0.01, v=0.5
      lambda1 = E * v / ((1.0 + v) * (1.0-2.0 * v)); %first lamé parameter
      mu1 = E/(2*(1+v));               %shear modulus - second lamé parameter
      %to get plane stress properties in 2D => modify lambda1
      lambda1 = 2*mu1*lambda1 / (lambda1+2*mu1); % modified first lamé parameter
      %E = (4*mu1*(lambda1+mu1))/ (lambda1+2*mu1)  % elastic modulus
      %v = lambda1/(lambda1+2*mu1)
  %%    %==== VOID PHASE ====  material 2
      if ignore_void
    % ignorer void : set void=0 for faster solution  >>>> activate lines 59-62 in homogenize.m
    % WARNING:  requires connected solid material)
        lambda2 =0;
        mu2 = 0;
      else
    % void material
        lambda2 = 1e-10*lambda1;
        mu2 = 1e-10*mu1;
      end
      lame = [lambda2,lambda1;mu2,mu1]
  %%    %======= Call homogenization =========	
      lambda  = [lambda2, lambda1]; %first lame parameter for material 1 and material 2
      mu      = [mu2, mu1]; % shear modulus - module de cisaillement for material 1 and material 2
      [ny,nx] = size(BW);  % Unit cell length in x,y-directions. <<<<<<<<  !!!!!!!!!! >>>>>>>>> ny = size in x >>>>>>>>>>>
      lx      = nx;        % Size of finite element: dx=lx/nx, dy=ly/ny 
      ly      = ny;	 % used to compute basis material properties
                           % STEF: il semble que lx=ly=1 ou =nx ou 1000*nx ne change rien au resultat.
      display('   computing homogenized constitutive matrix ...');
          % code from Andreassen, E., & Andreasen, C. S. (2014).
          % How to determine composite material properties using numerical homogenization.
          % Computational Materials Science, 83, 488-495.
    % Test: x = randi([1 2],200); C = homogenize(1,1,[.01 2],[0.02 4],90,BW);
      % CH = homogenize_2d_JONAS(1,1,lambda, mu,90,BW,ignore_void);
      %C = homogenize_2d_JONAS(1000*lx,1000*ly,lambda, mu,90,BW,ignore_void);
      
      CH = homogenize_2d_JONAS(lx,ly,lambda, mu,90,BW,ignore_void); %%material 1(x==1 solid), material2(x==2 void)
      % CH = homogenize_2d_JONAS(lx,ly*sqrt(3.)/2.,lambda, mu,60,BW,ignore_void); %material 1(x==0 void), material2(x==1 solid)
      % CH = homogenize_2d_JONAS(lx,ly,lambda, mu,60,BW,ignore_void); %material 1(x==0 void), material2(x==1 solid)
  
      PR = CH(1,2)/CH(1,1);
      E  = (CH(1,1)^2 - CH(1,2)^2) / CH(1,1);
  
  end
  
  function [PR_iso,E_iso,C_iso,delta_iso] = isotropic(CH)      
    %------------------------------------------
    %closest isotropic elasticity tensor [Martinez21]
    c11 = CH(1,1); c12 = CH(1,2); c22 = CH(2,2); c33 = CH(3,3);
    c11_iso = (9*(c11+c22) + 2*c12 + 4*c33)/20;
    c12_iso = (   c11+c22 + 18*c12 - 4*c33)/20;
    C_iso = [c11_iso c12_iso 0;
      c12_iso c11_iso 0;
      0 0 (c11_iso-c12_iso)/2];
    % PR and E of isotropic material
    PR_iso = c12_iso / c11_iso;
    E_iso = (c11_iso^2 - c12_iso^2 ) / c11_iso;
    %divergence from isotropy
    delta_iso = norm(CH-C_iso,'fro') / norm(CH,'fro');
  end
  
  function CH_becht = computeBechterew(CH_voigt)
    %------------------------------------------
    %compute C in Bechterew form
    CH_becht = CH_voigt; % tenseur symmétrisé
    CH_becht(1,3) = sqrt(2) * CH_voigt(1,3);
    CH_becht(2,3) = sqrt(2) * CH_voigt(2,3);
    CH_becht(3,1) = sqrt(2) * CH_voigt(3,1);
    CH_becht(3,2) = sqrt(2) * CH_voigt(3,2);
    CH_becht(3,3) = 2 * CH_voigt(3,3);
    CH_becht;
  end
  
  function S4 = computeCompliance4(S)
    %------------------------------------------
    % output: compliance tensor S4 (4x4)
    %        epsilon = S4*sigma, sigma = [e_11, e_22, sqrt(2)*e_12 sqrt(2)*e_21]^T
    %        S4 = [1111         1122         sqrt(2)*1112 sqrt(2)*1121
    %             2211         2222         sqrt(2)*2212 sqrt(2)*2221
    %             sqrt(2)*1211 sqrt(2)*1222 2*1212       2*1221
    %             sqrt(2)*2111 sqrt(2)*2122 2*2112       2*2121      ]
    %------------------------------------------
    S4 = zeros(2,2,2,2);
    S4(1,1,1,1) = S(1,1);
    S4(1,1,2,2) = S(1,2);
    S4(1,1,1,2) = S(1,3)/sqrt(2);
    S4(1,1,2,1) = S(1,3)/sqrt(2); % ???
    %
    S4(2,2,1,1) = S(2,1);
    S4(2,2,2,2) = S(2,2);
    S4(2,2,1,2) = S(2,3)/sqrt(2);
    S4(2,2,2,1) = S(2,3)/sqrt(2); % ???
    %
    S4(1,2,1,1) = S(3,1)/sqrt(2);
    S4(1,2,2,2) = S(3,2)/sqrt(2);
    S4(1,2,1,2) = S(3,3)/2;
    S4(1,2,2,1) = S(3,3)/2; % ???
    %
    S4(2,1,1,1) = S(3,1)/sqrt(2);
    S4(2,1,2,2) = S(3,2)/sqrt(2);
    S4(2,1,1,2) = S(3,3)/2;
    S4(2,1,2,1) = S(3,3)/2; % ???
    S4;
  end
  
  function [PR_dir,E_dir] = computePR_unidirection(C_becht, d)
    %------------------------------------------
    % input: stiffness tensor C (3x3) in Bechterew form
    %        sigma = C*epsilon, epsilon = [e_11, e_22, sqrt(2)e_12]^T
    %	   C_becht = [ 1111         1122         sqrt(2)*1112
    %                    2211         2222         sqrt(2)*2212
    %                    sqrt(2)*1211 sqrt(2)*1222 2*1212      ]
    %         d direction in plane d = [d1 d2]^T
    %         |d| = 1
    % output: PR_dir directional Poisson's Ratio
    %
    % compute S = Cˆ(-1) in 3x3 Bechterew form
    % transform S into 4x4 form
    % compute n orthogonal to d
    % compute directional PR_dir 
    % compute directional Young's modulus E_dir
    %------------------------------------------
    S = inv(C_becht);   % compliance tensor 3x3
    %%% Utiliser C_becht ici, pour que S est bien C^-1 (see cours_Francois_tenseurs.pdf)
    % Tenseurs en Mécanique X3PM040 Anisotropie et composites M2 Mécanique et Fiabilité des Structures M.L.M. François
    n(1) = -d(2);       % n direction orthogonal to d
    n(2) =  d(1);
    S;
    S4 = computeCompliance4(S);
    % PR_dir = (d.dT : S4 : n.nT ) / (d.dT : S4 : d.dT) 
    % E_dir  = 1 / (d.dT : S4 : d.dT)                
    Z = 0; N  = 0;
    for i=1:2
      for j=1:2
        for k=1:2
          for l=1:2
      Z = Z + (d(i) * d(j) * n(k) * n(l) * S4(i,j,k,l));
      N = N + (d(i) * d(j) * d(k) * d(l) * S4(i,j,k,l));
          end
        end
      end
    end
    PR_dir = -Z/N;
    E_dir  = 1/N;
    [PR_dir___E_dir___] = [PR_dir, E_dir];
  end
  
  
  function [PR,E,theta] = computePR_multidirection(CH_becht,N)
    %------------------------------------------
    for i=1:N
      theta(i,1) = ((i-1) * 2* pi ) / (N-1);
      d(1) = cos(theta(i));
      d(2) = sin(theta(i));
      d = 1 * d/norm(d,2);
      [PR(i,1), E(i,1)] = computePR_unidirection(CH_becht, d);
    end
    PR;
    E;
  end


%==================================
% mainHomogenization.m
%==================================
% INPUT: ntw.txt
%	 crop_size
%	 material properties: E (module de Young), v (Poisson's ratio)
%
% 1. display ntw in figure('visible', 'off',...) pour eviter ouverture de 150 figures)
% 2. figure is written in RGB-pixel matrix I
% 3. I is transformed in Black/White matrix BW (1=black,2=white)
%
% 	  BW is the indicator material matrix 1=material_1, 2=material_2
%	  here: material_1 (ntw)
%	        material_2 (void)
% 4. compute Lamé parameters for both materials from E and v
% 5. call numerical homogenization homogenize.m [Author: Andreassen, Computational Material Science 83, 2014]
%
% OUTPUT: C	homogenized elasticity tensor (3,3)-matrix
%	  PR	Poisson's ratio, column vector (1:ntw)
%	  E	Young's modulus, column vector (1:ntw)
%
% 	  saved in txt-file optional : PR.txt C.txt
%==================================

  %dossier = '../DATA/SP431_Z3.6';
  %dossier    = 'PERIODIC_NTW/N07_431_Z3.6_T3';
  dossier    = 'OUTPUT_N40_3.4/ImPNG_1_400_2pt';
  dossier    = 'OUTPUT';
  dossier    = 'PERIODIC_NTW/IMG_HOMOG/N07_431_Z3.6_T3/ImPNG_1_200_4pt';
  dossier    = 'HOMOGENIZATION';
  dossier    = 'PERIODIC_NTW/IMG_HOMOG/N10_431_Z3.8_T3/ImgPNG2';
  dossier    = 'PERIODIC_NTW/IMG_HOMOG/N20_431_Z3.6_T3/ImPNG_1_200_1pt';
  dossier    = 'INPUT_HOMOG/Jonas';
  dossier    = 'INPUT_HOMOG';
  % dossier    = 'INPUT_HOMOG/sortieTrans';
  % dossier    = '../DATA/PERIODIC_NTW/IMG_HOMOG/N07_431_Z3.6_T3/ImPNG_1_200';
  % dossier    = '../DATA/PERIODIC_NTW/IMG_HOMOG/N10_431_Z3.8_T3/ImgPNG2';
  
  %dossierOut = 'OUTPUT';
  dossierOut = 'OUTPUT_N40_3.4/ImPNG_1_400_2pt';
  dossierOut = 'PERIODIC_NTW/N07_431_Z3.8_T3/ImPNG_1_200_4pt';
  dossierOut = 'OUTPUT';

  %PARAMETERS
  plot_true  = 0;
  saving     = 1;
  ignore_void = 1;
  PR_solid    = 0.45;
  E_solid     = 1.0;  % +++> ne change pas le PR en sortie, uniquement E;
  n_dir       = 100; % number of directions for radial plots

  % - singleImage - periodic NTW (p,s,u)
  fileName  = 'test';
  fileName  = 'white';
  fileName  = 'black';
  fileName  = 'Schumacher/Sch_isotropic'; %honeycomb
  fileName  = 'Schumacher/Sch_orthotropic';
  fileName  = 'Schumacher/Sch_tetragonal';
  fileName  = 'Jonas/optimized_triangular';
  fileName  = 'Jonas/optimized_hexagonal';
  fileName  = 'Jonas/optimized_delaunay';
  fileName  = 'Jonas/optimized';
  fileName  = 'Schumacher/Sch_Fig11_6';
  fileName  = 'Schumacher/Sch_Fig11_1';
  fileName  = 'Schumacher/Sch_Fig11_3';
  fileName  = 'Schumacher/Sch_Fig11_5';
  fileName  = 'Schumacher/Sch_Fig11_7';
  fileName  = 'Schumacher/Sch_Fig11_8';
  fileName  = 'chiral_kreuz';
  fileName  = 'hexagonLight';
  fileName  = 'hexagonBolduni';
  fileName  = 'hexagonExtraBolduni';
  fileName  = 'networkMechaspring';
  

  crop_size = 0;


  input = 'ntwImages';
  input = 'singleImage';

  PRp= []; PRs= []; PRu= [];
  Ep = []; Es = []; Eu = [];
  Cp = []; Cs = []; Cu = [];
    
  switch input
%% NTW   H O M O G E N I Z A T I O N %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'singleImage'
      display( 'Single Image Input');
      %read Image
      %imread returns the image data in the array A. If the file contains a grayscale image, 
      %A is a two-dimensional (M-by-N) array. If the file contains a color image, 
      %A is a three-dimensional (M-by-N-by-3) array. 
      %-----------------
      %fn = strcat(dossier,'/',fileName,'.ppm')
      %I = imread(fn, 'ppm');
      fn = strcat(dossier,'/',fileName,'.png')

      fn = argv(){1} % READ IMAGE NAME IN ARGUMENTS

      I = imread(fn, 'png');
      [N,M,~]=size(I); imageSize = [N,M]
      % imshow(I); title('input image');
      %crop image
      if N-2*crop_size > 50 && M-2*crop_size>50
        if crop_size>0
            I(1:crop_size,:,:) = []; %TOP
            I(end-crop_size+1:end,:,:) = []; %BOTTOM
            I(:,1:crop_size,:) = []; %LEFT
            I(:,end-crop_size+1:end,:) = []; %RIGHT
            [N,M,~]=size(I); croped_imageSize = [N,M]
            figure; imshow(I);
            title('croped image');
        end
	%% conversion image => BW material matrix
        BWp = image2BW(I);        % input image where BLACK is SOLID material
        %BWp = image2BWinverse(I); % input image where WHITE is SOLID material (Jonas' image for example)

%BWp(1,  1:70)
%BWp(end,1:70)
        % compute homogenized stiffness tensor (3x3) in Voigt form
        [CH_voigt] = numerical_homogenization(PR_solid,E_solid,BWp,ignore_void);        
        [PR_iso,E_iso,C_iso,delta_iso] = isotropic(CH_voigt); % C_iso closest isotropic tensor

        PR_iso_____E_iso_____delta_iso = [PR_iso,E_iso,delta_iso]

	%%% DIRECTIONAL PR, E %%%%%
        CH_becht = computeBechterew(CH_voigt);
        % chose direction (normalized) 
        d = [-0.5,0.5]; % direction en dl
        d = [0.5,0.5]; % direction en dr
        d = [0.0,1.0]; % direction en y
        d = [1.0,0.0]; % direction en x
        d = 1* d/norm(d,2);
      	direction = d;
	% compute directional PR and E
        [PR_dir, E_dir] = computePR_unidirection(CH_becht, d);
        % [PR_dir___E_dir___] = [PR_dir, E_dir]
        PR_dir_ = PR_dir
        E_dir_ = E_dir
	% compute RADIAL PLOTS
      	[PR_plot,E_plot,theta] = computePR_multidirection(CH_becht,n_dir);
        % min(PR_plot)
        % max(PR_plot)
	% save Polar Plots
        % pp2 = figure; polarplot(theta,E_plot,'Linewidth', 2);        rlim([0 1.1*max(E_plot)]);
        showPlot = false
        if showPlot
          pp2 = figure; polar(theta,E_plot);   
          % hold on; polar(theta,zeros(n_dir,1));hold off;
          title('\fontsize{16} Young''s modulus');
          % exportgraphics(pp2,strcat(dossierOut,'/',fileName, '_Eplot.png'));
          % print(strcat(dossierOut,'/',fileName, '_Eplot.png'))
          pp1 = figure; polar(theta,PR_plot); 
          % hold on; polar(theta,zeros(n_dir,1));hold off; pause
          title('\fontsize{16} Poisson''s ratio'); pause
          % exportgraphics(pp1,strcat(dossierOut,'/',fileName, '_PRplot.png')); %exportgraphics(pp1,'ff.png');
          % print(strcat(dossierOut,'/',fileName, '_PRplot.png'))
          % polarplot(theta,ones(n_dir,1));
        end
      else
        display('>>>>>>>>>>> ERROR & STOP : crop_size est trop grand');
      end
      
%% IMAGE     H O M O G E N I Z A T I O N %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'ntwImages'
      for ntw =1:30
      %% read image
        fn = strcat(dossier,'/N',num2str(ntw),'p.png')
        Ip = imread(fn);
        % fn = strcat(dossier,'/N',num2str(ntw),'s.png')
        % Is = imread(fn);
        % fn = strcat(dossier,'/N',num2str(ntw),'u.png')
        % Iu = imread(fn);
      %% conversion image => BW material matrix
        BWp = image2BW(Ip);
        % BWs = image2BW(Is);
        % BWu = image2BW(Iu);
      %% Numerical Homogenization           
        [CHp_voigt] = numerical_homogenization(PR_solid,E_solid,BWp,ignore_void);         
        % [CHs_voigt] = numerical_homogenization(PR_solid,E_solid,BWs,ignore_void);         
        % [CHu_voigt] = numerical_homogenization(PR_solid,E_solid,BWu,ignore_void);         
        [PRp_iso(ntw,1),Ep_iso(ntw,1),Cp_iso,delta_isop(ntw,1)] = isotropic(CHp_voigt);         
        % [PRs_iso(ntw,1),Es_iso(ntw,1),Cs_iso,delta_isos(ntw,1)] = isotropic(CHs_voigt);         
        % [PRu_iso(ntw,1),Eu_iso(ntw,1),Cu_iso,delta_isou(ntw,1)] = isotropic(CHu_voigt);         
        p___PR_iso_____E_iso_____delta_iso = [PRp_iso(ntw,1),Ep_iso(ntw,1),delta_isop(ntw,1)]
        % s___PR_iso_____E_iso_____delta_iso = [PRs_iso(ntw,1),Es_iso(ntw,1),delta_isos(ntw,1)]
        % u___PR_iso_____E_iso_____delta_iso = [PRu_iso(ntw,1),Eu_iso(ntw,1),delta_isou(ntw,1)]
        CHp_becht = computeBechterew(CHp_voigt);
	      [PR_plot,E_plot,theta] = computePR_multidirection(CHp_becht,n_dir);
	      ei = zeros(n_dir+1,1);
        % pp1 = figure; polarplot(theta,PR_plot, 'r', 'Linewidth', 2); rlim([-1.5 1.5]); 
	      pp1 = figure; polar(theta,PR_plot); 
        pause
	      % hold on; polarplot(theta,zeros(n_dir+1,1),'--k');
	      hold on; polar(theta,zeros(n_dir+1,1));
	      % polarplot(theta,0.34*ones(n_dir+1,1),'b'); hold off;
	      polar(theta,0.34*ones(n_dir+1,1)); hold off;
	      title('\fontsize{16} Poisson''s ratio');
        print(strcat(dossierOut,'/N',num2str(ntw),'_PRplot.png'))
	      % exportgraphics(pp1,strcat(dossierOut,'/N',num2str(ntw),'_PRplot.png')); %exportgraphics(pp1,'ff.png');
        CHp(ntw,:)  = reshape(CHp_voigt,[1,9]); %from (3,3)matrix to (1,9)line-vector
        % CHs(ntw,:)  = reshape(CHs_voigt,[1,9]); %from (3,3)matrix to (1,9)line-vector
        % CHu(ntw,:)  = reshape(CHu_voigt,[1,9]); %from (3,3)matrix to (1,9)line-vector
      end %ntw
  end %switch

%% SAVING in PNG files  
  if saving
    if strcmp(input,'ntwImages')
      fid = fopen(strcat(dossierOut,'/PRp'),'w'); fprintf(fid,'%8.4f\n',PRp_iso); fclose(fid);
      % fid = fopen(strcat(dossierOut,'/PRs'),'w'); fprintf(fid,'%8.4f\n',PRs_iso); fclose(fid);
      % fid = fopen(strcat(dossierOut,'/PRu'),'w'); fprintf(fid,'%8.4f\n',PRu_iso); fclose(fid);
      fid = fopen(strcat(dossierOut,'/Ep'),'w');  fprintf(fid,'%8.4f\n',Ep_iso); fclose(fid);
      % fid = fopen(strcat(dossierOut,'/Es'),'w');  fprintf(fid,'%8.4f\n',Es_iso); fclose(fid);
      % fid = fopen(strcat(dossierOut,'/Eu'),'w');  fprintf(fid,'%8.4f\n',Eu_iso); fclose(fid);
      % %CH = (9,ntw)-matrix
        fid = fopen(strcat(dossierOut,'/CHp'),'w');  
        for i=1:ntw
          fprintf(fid,'%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n',CHp(i,:));
        end;
        fclose(fid);
      %fid = fopen(strcat(dossierOut,'/CHs'),'w');  
      %for i=1:ntw fprintf(fid,'%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n',CHs(i,:));
      %end; fclose(fid);
      %fid = fopen(strcat(dossierOut,'/CHu'),'w');  
      %for i=1:ntw fprintf(fid,'%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n',CHu(i,:));
      %end; fclose(fid);
      fid =fopen(strcat(dossierOut,'/DIsop'),'w');  fprintf(fid,'%8.4f\n',delta_isop); fclose(fid);
      %fid = fopen(strcat(dossierOut,'/DIsos'),'w');  fprintf(fid,'%8.4f\n',delta_isos); fclose(fid);
      %fid = fopen(strcat(dossierOut,'/DIsou'),'w');  fprintf(fid,'%8.4f\n',delta_isou); fclose(fid);
    else % if not ntwImages
      fid = fopen(strcat(dossierOut,'/PR_plot'),'w'); fprintf(fid,'%20.10f\n',PR_plot); fclose(fid);
      fid = fopen(strcat(dossierOut,'/E_plot'),'w');  fprintf(fid,'%20.10f\n',E_plot); fclose(fid);
      fid = fopen(strcat(dossierOut,'/PR'),'w'); fprintf(fid,'%20.10f\n',PR_iso); fclose(fid);
      fid = fopen(strcat(dossierOut,'/E'),'w');  fprintf(fid,'%20.10f\n',E_iso); fclose(fid);
      fid = fopen(strcat(dossierOut,'/CH_voigt'),'w');
            CH  = reshape(CH_voigt,[1,9]); %from (3,3)matrix to (1,9)line-vector
      fprintf(fid,'%8.8f %8.8f %8.8f %8.8f %8.8f %8.8f %8.8f %8.8f %8.8f\n',CH);
      fclose(fid);
      fid = fopen(strcat(dossierOut,'/DIso'),'w');  fprintf(fid,'%8.4f\n',delta_iso); fclose(fid);
    end % if not ntwImages
  end % if saving


%====================================================
