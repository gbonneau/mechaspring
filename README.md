![](gallery/honeyComb10x5_Blender.png)
<!-- ![](gallery/honeyCombCylinder10x5_Blender.png) -->
# mechaspring

##### INSTALLATION

- ``conda create --name "springTorch"``
- ``conda activate springTorch``
- ``conda install pytorch::pytorch torchvision torchaudio -c pytorch``
- ``conda install tqdm matplotlib meshio tensorboard --channel conda-forge``
- ``pip install pytorch-minimize``

```mechaspring``` is a library for modeling and simulating a periodic network of length and angular springs.


